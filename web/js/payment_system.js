var PaymentSystem = {
    cloneField: function (button) {
        var url = button.attr('data-url');
        var payment_system_id = button.attr('data-system-id');
        var field = $('.cloneField-' + payment_system_id).val();

        if (field) {
            button.attr('disabled', true);

            $.ajax({
                type: 'post',
                dataType: 'html',
                url: url,
                data: {
                    'title': field,
                    'payment_system_id': payment_system_id
                },
                success: function (data) {
                    $('.paymentSystemFieldsBlock-' + payment_system_id).append(data);
                    button.attr('disabled', false);
                },
                error: function () {
                    button.attr('disabled', false);
                }
            });
        }
    },
    removeField: function (button) {
        var url = button.attr('data-url');
        var id = button.attr('data-id');

        if (id) {
            button.attr('disabled', true);

            $.ajax({
                type: 'post',
                dataType: 'html',
                url: url,
                data: {
                    'id': id
                },
                success: function (data) {
                    $('#field-' + id).remove();
                    button.attr('disabled', false);
                },
                error: function () {
                    button.attr('disabled', false);
                }
            });
        }
    }
};

$(document)
        .on('click', '.clonePaymentFieldSubmit', function () {
            PaymentSystem.cloneField($(this));
        })
        .on('click', '.removePaymentField', function () {
            PaymentSystem.removeField($(this));
        });