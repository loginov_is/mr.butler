yii.app.forms = (function ($) {
    var pub = {
        isActive: true,

        init: function() {
            //console.log($('[data-ajax-option="1"]'));
            $('[data-ajax-option="1"]').on('change', function () {
                var $form = $(this).closest('form'),
                    $target = $('#' + $(this).attr('data-target')),
                    url = $target.attr('data-url'),
                    postData = $form.serialize();

                $.post( url, postData, function( data ) {
                    $target.html( data );
                });
            });
            $('[data="ajax-option"]').trigger('change');
        }
    };

    return pub;
})(jQuery);
