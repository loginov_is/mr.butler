$("#sortable").sortable();
$("#sortable").disableSelection();
$(".photo-item-gallery").colorbox({rel:'gal'});

//Сохранение объекта недвижимости
$(".realEstateSubmitButton").click(function () {
    var data = new Object();
    var photos = $('.photo-item');
    console.log(photos);

    for (var i = 0; i < photos.length; i++)
    {
        console.log("Photo id: " + $(photos[i]).attr('id') + " index is: " + i);
        data['photo[' + i + ']'] = new Object();
        data['photo[' + i + '][id]'] = $(photos[i]).attr('id');
        data['photo[' + i + '][index]'] = i;
    }
    console.log(data);
    $.ajax({
        url: "/real-estate/photo-sort-save",
        type: "POST",
        async: false,
        data: data,
        success: function (html) {

        }
    });
});

//Удаление фотографии
$(".photo-delete").click(function() {
    $.ajax({
        url: $(this).attr("href"),
        type: "POST",
        async: false
    });
    $(this).closest(".photo-item").remove();
    return false;
});

//Выбор главной фотографии
$(".photo-setmain").click(function() {
    $.ajax({
        url: $(this).attr("href"),
        type: "POST",
        async: false
    });
    $(this).closest("ul").find(".photo-item-main a.photo-setmain").show();
    $(this).closest("ul").find(".photo-item-main").removeClass("photo-item-main");
    $(this).closest("li").addClass("photo-item-main");
    $(this).hide();
    return false;
});
