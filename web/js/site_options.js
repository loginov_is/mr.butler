var SiteOptions = {
    cloneFeedbackEmail: function (button) {
        var url = button.attr('data-url');
        var type = button.attr('data-type');
        var feedbackEmail = $('.cloneEmailField-' + type).val();
        
        if (feedbackEmail) {
            button.attr('disabled', true);

            $.ajax({
                type: 'post',
                dataType: 'html',
                url: url,
                data: {
                    'feedbackEmail': feedbackEmail,
                    'type': type
                },
                success: function (data) {
                    $('.feedbackEmailsBlock-' + type).append(data);
                    button.attr('disabled', false);
                },
                error: function () {
                    button.attr('disabled', false);
                }
            });
        }
    },
    removeFeedbackEmail: function (button) {
        var url = button.attr('data-url');
        var id = button.attr('data-id');
        var type = button.attr('data-type');

        if (id) {
            button.attr('disabled', true);

            $.ajax({
                type: 'post',
                dataType: 'html',
                url: url,
                data: {
                    'id': id
                },
                success: function (data) {
                    $('#feedbackEmail-' + type + '-' + id).remove();
                    button.attr('disabled', false);
                },
                error: function () {
                    button.attr('disabled', false);
                }
            });
        }
    }
};

$(document)
        .on('click', '.cloneEmailSubmit', function () {
            SiteOptions.cloneFeedbackEmail($(this));
        })
        .on('click', '.cloneEmailRemove', function () {
            SiteOptions.removeFeedbackEmail($(this));
        });