var Main = {
    mapInit: function (coords, title) {
        var coordsArr;
        if (!!coords) {
            coordsArr = coords.split(',');
            var centerLatLng = new google.maps.LatLng(parseFloat(coordsArr[0]), parseFloat(coordsArr[1]));
        } else {
            var centerLatLng = new google.maps.LatLng(12.951029, 100.980835);
        }

        var mapOptions = {
            zoom: 11,
            center: centerLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            navigationControlOptions: {
                style: google.maps.NavigationControlStyle.ZOOM_PAN
            }
        };
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
        if (!!coords) {
            var marker = new google.maps.Marker({
                position: centerLatLng,
                map: map,
                title: title
            });
        } else {
            var marker = new google.maps.Marker({
                map: map,
                title: title,
            });
        }

        var panorama;
        panorama = new google.maps.StreetViewPanorama(
                document.getElementById('map2'), {
            position: {lat: parseFloat(coordsArr[0]), lng: parseFloat(coordsArr[1])},
            pov: {heading: 165, pitch: 0},
            zoom: 1
        }
        );

        var isChangePositionMarker = false;
        var markerNewPosition = '';
        function placeMarker(location) {
            marker.setPosition(location);
            window.isChangePositionMarker = true;
            var posLat = marker.getPosition().lat();
            var posLng = marker.getPosition().lng();
            window.markerNewPosition = posLat + ', ' + posLng;
            $('.hiddenCoords').attr('value', window.markerNewPosition);
        }

        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng);
        });
    },
    showHiddenItems: function (link) {
        //$('#' + link.attr('data-shown')).removeClass('hidden');
        //$('#' + link.attr('data-removed')).remove();
    },
    reviewsSameHeiht: function () {
        var mostHeight = 0;
        $.each($('.feedbackItem'), function () {
            var itemHeight = $(this).height();
            if (itemHeight > mostHeight) {
                mostHeight = itemHeight;
            }
        });

        $.each($('.feedbackItem'), function () {
            $(this).css("height", function (index) {
                return mostHeight;
            });
            $(this).find('.feedbackItemPhoto img').css('max-height', mostHeight-20);
        });

        var halfMostHeight = parseInt(Math.round(mostHeight / 2)) + 30;
        $('.reviewLeftArrow').attr('style', 'position: absolute; margin-left: -57px; margin-top: -' + halfMostHeight + 'px; cursor:pointer;');
        $('.reviewRightArrow').attr('style', 'position: absolute; margin-left: 21px; margin-top: -' + halfMostHeight + 'px; cursor:pointer;');
    }
};
//FAQ.
//SHOW ALL HIDDEN BLOCKS
$(document)
        .on('click', '.showMore', function () {
            var hiddenBlock = $('.hiddenMore-' + $(this).attr('data-id'));
            $(this).remove();
            hiddenBlock.removeClass('hidden');
        })
//SEARCH EXAMPLES
        .on('click', '.searchExample', function () {
            var content = $(this).html();
            $('.faqSearchField').val($('.faqSearchField').val() + ' ' + content);
        })
//END FAQ.
//REGISTRATION MODAL
        .on('click', '.showRegistrModal', function () {
            $('.registrationModal').modal('show');
            return false;
        })
//END REGISTRATION MODAL
//AUTH MODAL
        .on('click', '.showLoginModal', function () {
            $('.authModal').modal('show');
            return false;
        })
//AUTH REGISTRATION MODAL
        .on('change', '.currencyDropDown', function () {
            $('#currencyForm').submit();
        })
        .on('click', '.viewMoreLink', function () {
            //Main.showHiddenItems($(this));
            $('.realEstateHidden').show();
            $('.viewAllItems').hide();
            return false;
        })
        .on('click', '.openAdvanceOptions', function () {
            if ($('.advanceOptions').hasClass('hidden')) {
                $('.advanceOptions').removeClass('hidden');
            } else {
                $('.advanceOptions').addClass('hidden');
            }
        })
        .on('click', '.bootbox', function () {
            var message = $(this).attr('bootbox-message');
            var url = $(this).attr('bootbox-url');
            bootbox.confirm(message, function (result) {
                if (result == true) {
                    window.location.href = url;

                }
            });
            return false;
        })
        //REAL ESTATE VIEW PAGE - SHOW FULL DESCRIPTION
        .on('click', '.getFullInfo', function () {
            if ($('.fullInfo').hasClass('hidden')) {
                $('.fullInfo').removeClass('hidden');
            } else {
                $('.fullInfo').addClass('hidden')
            }

            return false;
        })
        .on('click', '.leftMenuButton', function () {
            var button = $(this);

            if (!button.hasClass('inProgress')) {
                button.addClass('inProgress');
                if (button.hasClass('menuHidden')) {
                    $('.leftMenu').animate({
                        left: "+=290"
                    }, 1000, function () {
                        button.removeClass('menuHidden');
                        button.removeClass('inProgress');
                    });
                } else {
                    $('.leftMenu').animate({
                        left: "-=290"
                    }, 1000, function () {
                        button.addClass('menuHidden');
                        button.removeClass('inProgress');
                    });
                }
            }

        })
        .on('click', '.confirmPayment', function () {
            var errors = 0;

            $.each($('.payment_field_required'), function () {
                $(this).removeClass('field-error');
                if (!$(this).val()) {
                    $(this).addClass('field-error');
                    errors++;
                }
            });

            if (errors) {
                return false;
            }
        })
//OBJECT VIEW - BOOKING AVAILABLE CHECK
        .on('click', '.bookingButton', function () {
            if ($('#bookingform-startdate').val().length == 0 || $('#bookingform-enddate').val().length == 0) {
                bootbox.alert('Вы должны выбрать даты.');
                return false;
            }
            $.ajax({
                url: '/booking/ajax-check-available',
                type: 'GET',
                dataType: 'html',
                //async: false,
                data: {
                    realEstateId: $('#bookingform-realestateid').val(),
                    startDate: $('#bookingform-startdate').val(),
                    endDate: $('#bookingform-enddate').val()
                },
                success: function (data) {
                    if (data == 0) {
                        bootbox.alert('Объект недоступен на выбранные даты.');
                    }
                    else {
                        $('#bookingFormModal').modal('show');
                    }
                }
            });
            return false;
        })
        .on('click', '#bookingFormModal .btn', function () {
            $('#bookingform-phone').val($('#bookingFormModal input[name="phone"]').val());
            $('#bookingform-comment').val($('#bookingFormModal textarea[name="comment"]').val());
            $('.bookingButton').closest('form').submit();
        })
//OBJECT BOOKING - SERVICE PRICE AJAX LOADING
        .on('change', '.bookingPage .serviceList', function () {
            var serviceList = $(this);
            var peopleCount = serviceList.find('option:selected').val();
            var serviceId = serviceList.attr('data-service-id');
            $.get('/booking/ajax-get-service-price', {
                serviceId: serviceId,
                peopleCount: peopleCount
            }).done(function (data) {
                serviceList.closest("tr").find(".servicePrice").html(data);
                //Calculate services total price
                calculateServicesPrice();
            });
        })
        .on('change', '.bookingPage .table input[type="checkbox"]', function () {
            if ($(this).prop('checked')) {
                calculateServicesPrice();
            }
        })
//ADMIN - BOOKING SERVICES - SERVICE PRICE UPDATE/CREATE
        .on('click', '.servicePriceUpdateButton', function () {
            var id = $(this).closest('tr').attr('data-key');
            $.ajax({
                'url': '/service/ajax-get-service-price',
                'method': 'GET',
                'data': {id: id},
                'dataType': 'json',
                'success': function (servicePrice) {
                    var modalWindow = $('#servicePriceUpdateModal');
                    modalWindow.find('input[name="ServicePrice[people_min]"]').val(servicePrice.people_min);
                    modalWindow.find('input[name="ServicePrice[people_max]"]').val(servicePrice.people_max);
                    modalWindow.find('input[name="ServicePrice[price]"]').val(servicePrice.price);
                    modalWindow.find('input[name="id"]').val(servicePrice.id);
                    modalWindow.modal('show');
                }
            });
            return false;
        })
        .on('click', '.servicePriceCreateButton', function () {
            var modalWindow = $('#servicePriceCreateModal');
            modalWindow.modal('show');
            return false;
        })
        .on('change', '#service-price_type_id', function () {
            handleServicePriceType();
        })
        .on('ready', $(this), function () {
            handleServicePriceType();
        })
//PROFILE SHOW REAL ESTATE SERVICES
        .on('click', '.toggleRealEstateServices', function () {
            $(this).closest('.panel-body').find('.realEstateServices').toggle();
            return false;
        })
        .on('click', '.serviceOrderDelete', function () {
            var requestUrl = $(this).attr('href');
            var serviceRow = $(this).closest('tr');
            bootbox.confirm("Отменить данную услугу?", function (result) {
                $.ajax({
                    'url': requestUrl,
                    'method': 'GET',
                    'async': false,
                    'success': function () {
                        serviceRow.remove();
                    }
                });
            });
            return false;
        })
//REAL ESTATE VIEW - DATES CALENDAR TOGGLE
        .on('click', '.showEstateCalendarButton', function() {
            $('.datesCalendar').toggle();
            return false;
        })
//REAL ESTATE VIEW - PROMOTE POPUP
    .on('ready', $(this), function() {
        if($('.promoteBlock').length > 0) {
            $('.promoteBlock').stick_in_parent({parent: '.container', offset_top: 20});
            $('.promoteBlock').delay(4000).fadeIn(1500);
        }
    });

//REAL ESTATE VIEW - MOBILE VERSION THUMBS HIDE
$( document).on('ready', function() {
    handleGalleryThumbs();
    handleProfileObjectCalendar();
});

$( window ).on('resize', function() {
    handleGalleryThumbs();
});

$( '.profileCalendarSelector' ).on('change', function() {
    handleProfileObjectCalendar();
});

function handleProfileObjectCalendar() {
    var key = $('.profileCalendarSelector option:selected').val();
    $('.row.show').addClass('hidden');
    $('.row.show').removeClass('show');
    $('.row[data-key="' + key + '"]').removeClass('hidden');
    $('.row[data-key="' + key + '"]').addClass('show');
}

function handleGalleryThumbs() {
    if($( window ).width() < 995) {
        $('.projectPhotoSlider .fotorama__nav-wrap').hide();
        $('.real-estate-gallery .fotorama__nav-wrap').hide();
    }
    if($( window ).width() >= 995) {
        $('.projectPhotoSlider .fotorama__nav-wrap').show();
        $('.real-estate-gallery .fotorama__nav-wrap').show();
    }
}

function handleServicePriceType() {
    if ($('#service-price_type_id').find('option:selected').val() == 0) {
        $('.serviceGroupPrices').show();
        $('.field-service-priceperman').hide();
    }
    if ($('#service-price_type_id').find('option:selected').val() == 1) {
        $('.serviceGroupPrices').hide();
        $('.field-service-priceperman').show();
    }
}

//REAL ESTATE SEARCH - REGIONS AJAX LOAD
function reloadRegions() {
    var cityId = $('select#searchform-cityid option:selected').attr('value');
    if (cityId) {
        $.get(
                '/site/ajax-get-regions',
                {'cityId': cityId}
        )
                .done(function (data) {
                    var regions = $.parseJSON(data);
                    console.log(regions);
                    if (regions) {
                        $('.topSearchRegionsRow').empty();
                        for (var i = 0; i < regions.length; i++) {
                            var element = '';
                            element += '<div class="col-md-4 topSearchRegionsItem">';
                            element += '<input type="checkbox" value="1" name="SearchForm[regions][' + regions[i].id + ']"> ' + regions[i].name;
                            element += '</div>';
                            $('.topSearchRegionsRow').append(element);
                        }
                    }
                });
    }
}

$('select#searchform-cityid').change(function () {
    reloadRegions();
});

//REAL ESTATE SEARCH - SORT BY
$('.siteSearchSortDropDown').change(function () {
    $('#searchform-sortby').val($(this).find('option:selected').val());
    $('.siteSearchForm form').submit();
});

$(document).ready(function () {
    var sortBy = $('#searchform-sortby').val();
    if (sortBy) {
        $('.siteSearchSortDropDown option[value=' + sortBy + ']').prop('selected', true);
    }

    Main.reviewsSameHeiht();
});

//ABOUT PAGE - PARTNER SLIDER
$('.partners .pageAboutSlider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    prevArrow: '.partners .leftArrow',
    nextArrow: '.partners .rightArrow'
});

$('.team .pageAboutSlider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    prevArrow: '.team .leftArrow',
    nextArrow: '.team .rightArrow'
});

//PROFILE PAGE - OBJECTS SLIDERS
$('.viewedObjectsCarousel').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 4,
    prevArrow: '.viewedObjectsLeft',
    nextArrow: '.viewedObjectsRight'
});

$('.userObjectsCarousel').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 4,
    prevArrow: '.userObjectsLeft',
    nextArrow: '.userObjectsRight'
});

$('.reviewsSlider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '.reviewLeftArrow',
    nextArrow: '.reviewRightArrow'
});

function calculateServicesPrice() {
    var services = $('.bookingPage .table tbody tr');
    var totalPrice = 0;
    for (var i = 0; i < services.length; i++) {
        var servicePrice = parseInt($(services[i]).find('.servicePrice').text());
        if ($(services[i]).find('input[type="checkbox"]').prop('checked')) {
            totalPrice += servicePrice;
        }
    }
    $('.totalServicesPrice').text(totalPrice);
}