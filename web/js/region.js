//VIDEO TO REGION
$(document).on('click', '.addVideo', function () {
    var button = $(this);
    var videoId = $('.selectVideo').val();
    var url = button.attr('url');
    var regionId = button.attr('regionId');

    if (videoId) {
        button.attr('disabled', true);
        $.ajax({
            type: 'post',
            dataType: 'html',
            url: url,
            data: {
                'videoId': videoId,
                'regionId': regionId
            },
            success: function (data) {
                $('.videoBlock').append(data);
                button.attr('disabled', false);
            }
        });
    }

    return false;
});

$(document).on('click', '.removeVideo', function () {
    var button = $(this);
    var regionId = button.attr('regionId');
    var videoId = button.attr('videoId');
    var blockToRemove = $('#video-' + videoId);
    var url = button.attr('url');

    button.attr('disabled', true);
    $.ajax({
        type: 'post',
        dataType: 'html',
        url: url,
        data: {
            'videoId': videoId,
            'regionId': regionId
        },
        success: function (data) {
            blockToRemove.remove();
            button.attr('disabled', false);
        }
    });
});
//END VIDEO TO REGION
//
//ALBUM TO REGION
$(document).on('click', '.addAlbum', function () {
    var button = $(this);
    var typeId = button.attr('typeId');
    var albumId = $('.selectAlbum-' + typeId).val();
    var url = button.attr('url');
    var regionId = button.attr('regionId');

    if (albumId) {
        button.attr('disabled', true);
        $.ajax({
            type: 'post',
            dataType: 'html',
            url: url,
            data: {
                'albumId': albumId,
                'regionId': regionId,
                'typeId': typeId
            },
            success: function (data) {
                $('.albumBlock-' + typeId).append(data);
                button.attr('disabled', false);
            }
        });
    }

    return false;
});

$(document).on('click', '.removeAlbum', function () {
    var button = $(this);
    var typeId = button.attr('typeId');
    var regionId = button.attr('regionId');
    var albumId = button.attr('albumId');
    var blockToRemove = $('#album-' + typeId + '-' + albumId);
    var url = button.attr('url');

    button.attr('disabled', true);
    $.ajax({
        type: 'post',
        dataType: 'html',
        url: url,
        data: {
            'albumId': albumId,
            'regionId': regionId,
            'typeId': typeId
        },
        success: function (data) {
            blockToRemove.remove();
            button.attr('disabled', false);
        }
    });
});
//END ALBUM TO REGION

//REGION TO REGION

$(document).on('click', '.addChildRegion', function () {
    var button = $(this);
    var childId = $('.selectChildRegion').val();
    var url = button.attr('url');
    var regionId = button.attr('regionId');

    if (childId) {
        button.attr('disabled', true);
        $.ajax({
            type: 'post',
            dataType: 'html',
            url: url,
            data: {
                'childId': childId,
                'regionId': regionId
            },
            success: function (data) {
                $('.childRegionBlock').append(data);
                button.attr('disabled', false);
            }
        });
    }

    return false;
});


$(document).on('click', '.removeRegionChild', function () {
    var button = $(this);
    var regionId = button.attr('regionId');
    var childId = button.attr('childId');
    var blockToRemove = $('#region-' + regionId + '-child-' + childId);
    var url = button.attr('url');

    button.attr('disabled', true);
    $.ajax({
        type: 'post',
        dataType: 'html',
        url: url,
        data: {
            'regionId': regionId,
            'childId': childId
        },
        success: function (data) {
            blockToRemove.remove();
            button.attr('disabled', false);
        }
    });
});
//END REGION TO REGION