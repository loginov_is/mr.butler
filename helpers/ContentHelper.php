<?php

namespace app\helpers;

use app\models\User;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

class ContentHelper {

    const ITEM_STATUS_NOT_ACTIVE = 0;
    const ITEM_STATUS_ACTIVE = 1;
    const MAP_COORDS_DEFAULT = '12.91020598061841, 100.8816146850586';

    public static function getActiveListData($id = null) {
        $data = [
            ContentHelper::ITEM_STATUS_NOT_ACTIVE => Yii::t('app', 'Не активна'),
            ContentHelper::ITEM_STATUS_ACTIVE => Yii::t('app', 'Активна'),
        ];

        if (isset($id)) {
            if (isset($data[$id])) {
                return $data[$id];
            } else {
                return $id;
            }
        } else {
            return $data;
        }
    }

    public static function replaceImages($model, $contentColumn = 'content', $postName = 'images') {
        $code = Inflector::camel2id(StringHelper::basename(get_class($model)));
        $key = join('-', $model->getPrimaryKey(true));
        $path = Yii::getAlias("@webroot/upload/{$code}");
        $baseUrl = Yii::getAlias("@web/upload/{$code}/{$key}");

        $images = Yii::$app->request->post($postName, []);

        $newPath = "{$path}/{$key}";
        if (!FileHelper::createDirectory($newPath)) {
            Yii::error("Can't create path: {$newPath}");
        }

        foreach ($images as $image) {
            $oldName = "{$path}/tmp/" . basename($image);
            $newName = "{$newPath}/" . basename($image);

            if (rename($oldName, $newName)) {
                $model->$contentColumn = str_replace($image, $baseUrl . '/' . basename($image), $model->$contentColumn);
            }
        }

        if (!empty($images)) {
            $model->save(false, [$contentColumn]);
        }
    }

    public static function generateKeywords($content, $wordCount = 8, $stopWords = []) {
        $text = preg_replace('/\'\w*\b/', ' ', strip_tags($content));
        preg_match_all("/\p{L}[\p{L}\p{Mn}\p{Pd}'\x{2019}]*/u", $text, $matches);
        $words = $matches[0];

        array_walk($words, function (&$val) {
            $val = mb_strtolower($val, Yii::$app->charset);
        });

        $words = array_diff($words, $stopWords);
        $words = array_count_values($words);

        arsort($words);
        $words = array_slice($words, 0, $wordCount);

        return array_keys($words);
    }

    public static function userLinkById($id) {
        $user = User::findOne(['id' => $id]);

        if ($user === null) {
            return $id;
        }

        return Html::a($user->email, '#', ['title' => $user->email]);
    }

    public static function getHumanAmount($amount, $currency) {
        $returnAmount = $amount;

        switch (strlen($amount)) {
            case 1:
            case 2:
            case 3:
                $returnAmount = $amount;
                break;
            case 4:
                $returnAmount = substr($amount, 0, 1) . ' ' . substr($amount, 1, strlen($amount) - 1);
                break;
            case 5:
                $returnAmount = substr($amount, 0, 2) . ' ' . substr($amount, 2, strlen($amount) - 2);
                break;
            case 6:
                $returnAmount = substr($amount, 0, 3) . ' ' . substr($amount, 3, strlen($amount) - 3);
                break;
            case 7:
                $returnAmount = substr($amount, 0, 1) . ' ' . substr($amount, 1, 3) . ' ' . substr($amount, 4, strlen($amount) - 4);
                break;
            case 8:
                $returnAmount = substr($amount, 0, 2) . ' ' . substr($amount, 2, 3) . ' ' . substr($amount, 5, strlen($amount) - 5);
                break;
            case 9:
                $returnAmount = substr($amount, 0, 3) . ' ' . substr($amount, 3, 3) . ' ' . substr($amount, 6, strlen($amount) - 6);
                break;
            default :
                $returnAmount = $amount;
                break;
        }

        return $currency . ' ' . $returnAmount . ',00';
    }

}
