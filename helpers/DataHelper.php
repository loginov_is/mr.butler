<?php

namespace app\helpers;

class DataHelper {

    public static function getYoutubeId($url) {
        preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $matches);

        if ($matches) {
            return $matches[0];
        }

        return null;
    }

}
