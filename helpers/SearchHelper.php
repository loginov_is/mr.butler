<?php

namespace app\helpers;

use app\models\RealEstate;
use app\models\SearchForm;
use app\helpers\ContentHelper;

class SearchHelper {

    public static function getSearchData($params = null) {
        $whereCondition = [];
        $whereParams = [];

        $whereCondition[] = "re.status_id = :status_id";
        $whereParams[":status_id"] = ContentHelper::ITEM_STATUS_ACTIVE;

        if ($params->regions) {
            $regionsArray = [];

            foreach ($params->regions as $key => $index) {
                $regionsArray[] = $key;
            }

            $whereCondition[] = "p.region_id IN (:region_id)";
            $whereParams[":region_id"] = implode(',', $regionsArray);
        } else if ($params->cityId) {
            $whereCondition[] = "p.city_id = :city_id";
            $whereParams[":city_id"] = $params->cityId;
        }

        if ($params->guestsCount) {
            $whereCondition[] = "re.residents >= :residents";
            $whereParams[":residents"] = $params->typeId;
        }

        if ($params->typeId) {
            $whereCondition[] = "re.housing_type_id = :housing_type_id";
            $whereParams[":housing_type_id"] = $params->typeId;
        }

        $priceCondition = "re.daily_price";
        if ($params->price) {
            $priceExplode = explode(',', $params->price);
            $priceCondition = ($params->priceType) ? "re.monthly_price" : "re.daily_price";
            if (isset($priceExplode[0])) {
                $whereCondition[] = "$priceCondition >= :min_price";
                $whereParams[":min_price"] = $priceExplode[0];
            }

            if (isset($priceExplode[1])) {
                $whereCondition[] = "$priceCondition <= :max_price";
                $whereParams[":max_price"] = $priceExplode[1];
            }
        }

        $sortBy = '';
        switch ($params->sortBy) {
            case SearchForm::SORT_BY_RATING:
                $sortBy = "ORDER BY re.rating DESC";
                break;
            case SearchForm::SORT_BY_PRICE:
                $sortBy = "ORDER BY $priceCondition ASC";
                break;
            case SearchForm::SORT_BY_DISTANCE_TO_SEA:
                $sortBy = "ORDER BY re.to_the_sea ASC";
                break;
            default :
                $sortBy = "ORDER BY re.rating DESC";
                break;
        }

        if ($params->bedrooms) {
            $whereCondition[] = "re.bedrooms = :bedrooms";
            $whereParams[":bedrooms"] = $params->bedrooms;
        }

        if ($params->bathrooms) {
            $whereCondition[] = "re.bathrooms = :bathrooms";
            $whereParams[":bathrooms"] = $params->bathrooms;
        }

        if ($params->appartamentsType) {
            $whereCondition[] = "re.type_id = :type_id";
            $whereParams[":type_id"] = $params->appartamentsType;
        }

        $where = '';
        if ($whereCondition) {
            $where = "WHERE ";
            $where .= implode(" AND ", $whereCondition);
        }

        $facilitiesCondition = "";
        $havingCondition = "";
        if ($params->facilities) {
            $facilitiesCount = count($params->facilities);
            $facilitiesArray = [];

            foreach ($params->facilities as $key => $index) {
                $facilitiesArray[] = $key;
            }

            $facilitiesImplode = implode(',', $facilitiesArray);
            $facilitiesCondition = ", (select count(distinct(lookup_id)) from (
                                        select retl.lookup_id as lookup_id, retl.real_estate_id as object_id from real_estate_to_lookup as retl where retl.lookup_id IN(:object_facilities)
                                        union
                                        select ptl.lookup_id as lookup_id, ptl.project_id as object_id from project_to_lookup as ptl where ptl.lookup_id IN(:project_facilities)) as lookups
                                        where object_id in (object_id, project_id)) as lookups_count";
            $whereParams[":object_facilities"] = $facilitiesImplode;
            $whereParams[":project_facilities"] = $facilitiesImplode;
//            $havingCondition = "HAVING lookups_count >= $facilitiesCount";
        }


        $sql = "SELECT *, re.id FROM {{%real_estate}} as re JOIN {{%project}} as p ON p.id = re.project_id $where $havingCondition $sortBy";
//        \yii\helpers\VarDumper::dump(array($sql, $whereParams), 10, true);
//        die;
//        \yii\helpers\VarDumper::dump(RealEstate::findBySql($sql, $whereParams)->all(), 10, true);
//        die;
        $model = RealEstate::findBySql($sql, $whereParams)->all();
//        \yii\helpers\VarDumper::dump($model,10,true);die;
        return $model;
    }

}
