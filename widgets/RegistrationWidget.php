<?php

namespace app\widgets;

use app\models\SignupForm;
use yii\base\Widget;

class RegistrationWidget extends Widget {

    public function run() {
        $model = new SignupForm(); 
        return $this->render('registration_modal', array(
                    'model' => $model
        ));
    }

}
