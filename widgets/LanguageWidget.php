<?php

namespace app\widgets;

use app\components\LanguageManager;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class LanguageWidget extends Widget
{
    public $options = [];

    public function run()
    {
        /** @var LanguageManager $manager */
        $manager = Yii::$app->get('languageManager');

        $items = [];
        $languages = $manager->languages;

        foreach ($languages as $code => $language) {
            $items[] = Html::a('', Url::current([$manager->requestParam => $code]), [
                'hreflang' => $code,
                'rel' => 'alternate',
                'title' => $language,
                'class' => (Yii::$app->language === $code ? 'active' : false) . " lang-{$code}"
            ]);
        }

        return Html::ul($items, [
            'encode' => false,
            'class' => 'language-drop-down list-inline',
        ]);
    }
}
