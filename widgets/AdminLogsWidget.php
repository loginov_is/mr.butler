<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\UserLog;
use yii\data\ActiveDataProvider;

class AdminLogsWidget extends Widget {

    public $className;
    public $modelId;

    public function run() {
        $dataProvider = new ActiveDataProvider([
            'query' => UserLog::find()->where([
                'class_name' => $this->className,
                'model_id' => $this->modelId
            ])->orderBy('created_at desc'),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('admin_log', [
                    'dataProvider' => $dataProvider
        ]);
    }

}
