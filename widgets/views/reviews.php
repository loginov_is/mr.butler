<?php

use yii\helpers\Html;

?>

<div class="reviewsBlock" id="reviews">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php $cssClass = (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'about') ? "aboutReviewTitle" : "pageTitle"; ?>
                <h1 class="<?= $cssClass ?> text-center"><?php echo \Yii::t('app', 'Отзывы'); ?></h1>
                <div class="pageTitleHr"></div>          
            </div>
        </div>
        <?php if ($model) { ?>
            <div class="row reviewsSlider">
                <?php foreach ($model as $review) { ?>
                    <div class="col-md-12 feedbackItem reviewSlide">
                        <div class="row">
                            <div class="col-md-3 feedbackItemPhoto">
                                <?php $userAvatar = $review->createdBy->getAvatarUrl(); ?>
                                <?= Html::img($userAvatar, ['alt' => 'user picture', 'style' => 'max-width: 100%;']) ?>
                            </div>
                            <div class="col-md-9 feedbackItemContent">
                                <p class="date"><?= date('d.m.Y', $review->created_at) ?></p>
                                <span class="content">
                                    <?= nl2br($review->review) ?>
                                </span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="row hidden-xs">
                <div class="col-md-6 col-xs-6 text-left">
                    <a class="reviewLeftArrow"><img src="<?= Yii::getAlias('@web/images/about_left_arrow.png') ?>"></a>
                </div>
                <div class="col-md-6 col-xs-6 text-right">
                    <a class="reviewRightArrow"><img src="<?= Yii::getAlias('@web/images/about_right_arrow.png') ?>"></a>
                </div>
            </div>
        <?php } else { ?>
            <div class="row aboutReviewsLinkBlock">
                <div class="col-md-4 col-md-offset-4 col-xs-12">
                    <?php echo Html::a(\Yii::t('app', 'Оставить отзыв'), ['/profile/reviews'], ['class' => 'realEstateSubmitButton']); ?>
                </div>
            </div>
        <?php } ?>
        <?php if (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'about') { ?>
            <div class="row aboutReviewsLinkBlock">
                <div class="col-md-4 col-md-offset-4 col-xs-12">
                    <?php echo Html::a(\Yii::t('app', 'Показать все отзывы'), ['/site/reviews'], ['class' => 'realEstateSubmitButton']); ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>