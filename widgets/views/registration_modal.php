<?php

use app\models\SignupForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;
?>

<div class="modal fade registrationModal registrationBlock">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close closeAdvance" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center"><?= Yii::t('app', 'Зарегистрироваться с помощью:') ?></h4>
                <div class="text-center">
                    <?=
                    AuthChoice::widget([
                        'baseAuthUrl' => ['site/auth'],
                        'popupMode' => true,
                    ])
                    ?>
                </div>
                <div class="clearfix clear"></div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="modal-title text-center"><?= Yii::t('app', 'или через электронную почту') ?></p>
                        <?php $form = ActiveForm::begin([
                            'id' => 'form-signup',
                            'action' => ['/site/signup'],
                        ]); ?>
                        <?= $form->field($model, 'email')->input('text', ['class' => 'form-control fieldAdvance', 'placeholder' => \Yii::t('app', 'Email')]) ?>
                        <?= $form->field($model, 'first_name')->input('text', ['class' => 'form-control fieldAdvance', 'placeholder' => \Yii::t('app', 'Имя')]) ?>
                        <?= $form->field($model, 'last_name')->input('text', ['class' => 'form-control fieldAdvance', 'placeholder' => \Yii::t('app', 'Фамилия')]) ?>
                        <?= $form->field($model, 'patronymic')->input('text', ['class' => 'form-control fieldAdvance', 'placeholder' => \Yii::t('app', 'Отчество')]) ?>
                        <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control fieldAdvance', 'placeholder' => \Yii::t('app', 'Пароль')]) ?>
                        <?= $form->field($model, 'password_repeat')->passwordInput(['class' => 'form-control fieldAdvance', 'placeholder' => \Yii::t('app', 'Повтор пароля')]) ?>
                        <?=
                        $form->field($model, 'account_type')->radioList([
                            SignupForm::ACCOUNT_TYPE_RENTER => Yii::t('app', 'Снять жилье'),
                            SignupForm::ACCOUNT_TYPE_LANDLORD => Yii::t('app', 'Сдать недвижимость в доверительное управление'),
                        ])->label(false)
                        ?>
                        <?= $form->field($model, 'accept_rules')->checkbox() ?>
                        <div class="form-group text-center">
                            <?= Html::submitButton(Yii::t('app', 'Зарегистрироваться'), ['class' => 'btn btn-primary submitAdvance', 'name' => 'signup-button']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>