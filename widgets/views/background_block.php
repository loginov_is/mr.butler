<?php

use yii\helpers\Html;
use app\models\Page;
use app\models\SiteImage;
?>

<?php if (!is_null($sliderType)) { ?>
    <?php if ($sliderType == Page::SLIDER_TYPE_DEFAULT_IMAGE) { ?>
        <div class="<?= ($class) ? $class . ' backgroundBlock' : 'backgroundBlock' ?>" style="background-image: url(<?= Yii::getAlias('@web/upload/' . SiteImage::findOne(['type' => 'background_block_default'])->src) ?>);">
            <?php if ($content) echo $content; ?>
        </div>
    <?php } else if ($sliderType == Page::SLIDER_TYPE_BACKGROUND_IMAGE) { ?>
        <div style="<?= $style ?>" class="<?= $class ?>">
            <?php if ($content) echo $content; ?>
        </div>
    <?php } else if ($sliderType == Page::SLIDER_TYPE_SLIDER) { ?>
        <div class="backgroundBlock <?= ($class) ? $class : '' ?>">
            <div class="backgroundBlockContent">
                <?php if ($content) echo $content; ?>
            </div>
            <div class="backgroundBlockGallery">
                <div id="gal" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php for ($i = 0; $i < count($images); $i++) { ?>
                            <div class="item<?php if ($i == 0) echo ' active'; ?>" style="background-size: cover; background-image: url(<?= Yii::getAlias('@web/upload/' . $images[$i]->image) ?>);"></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>
