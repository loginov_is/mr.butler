<?php

use yii\helpers\Html;

echo Html::beginForm('', 'post', ['id' => 'currencyForm']);

echo Html::dropDownList('currency', $currentCurrency, $items, [
    'class' => 'form-control currencyDropDown',
    'format' => 'html',
    'encode' => false,
]);

echo Html::endForm();