<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use app\models\BookingForm;
?>

<div class="bookingForm">
    <?php $form = ActiveForm::begin([
        'action' => ['profile/book', 'scenario' => BookingForm::SCENARIO_STEP_2]
    ]); ?>
    <div class="row">
        <div class="col-md-12">
            <h4><?= Yii::t('app', 'Дата заезда') ?></h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?=
            $form->field($model, 'startDate')->widget(
                DatePicker::className(), [
                    'type' => DatePicker::TYPE_INPUT,
                    'language' => Yii::$app->language,
                    'options' => [
                        'placeholder' => 'mm/dd/yyyy',
                        'class' => 'form-control',
                    ],
                    'pluginOptions' => [
                        'format' => 'mm/dd/yyyy',
                        'todayHighlight' => true,
                    ]
                ]
            )->label(false)
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h4><?= Yii::t('app', 'Дата отъезда') ?></h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?=
            $form->field($model, 'endDate')->widget(
                DatePicker::className(), [
                    'type' => DatePicker::TYPE_INPUT,
                    'language' => Yii::$app->language,
                    'options' => [
                        'placeholder' => 'mm/dd/yyyy',
                        'class' => 'form-control',
                    ],
                    'pluginOptions' => [
                        'format' => 'mm/dd/yyyy',
                        'todayHighlight' => true,
                    ]
                ]
            )->label(false)
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= Html::submitButton(Yii::t('app', 'Забронировать'), ['class' => 'bookingButton']) ?>
        </div>
    </div>
    <?= $form->field($model, 'realEstateId')->hiddenInput(['value' => $realEstateId])->label(false) ?>
    <?= $form->field($model, 'phone')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'comment')->hiddenInput()->label(false) ?>
    <?php ActiveForm::end(); ?>
</div>