<?php

use yii\helpers\Html;
?>

<?php
$a = 0;
foreach ($items as $key => $item) {

    if ($a >= 5) {
        break;
    }

    echo $this->render('_promotions_item', [
        'item' => $item
    ]);
    unset($items[$key]);
    $a++;
}
?>
<?php if ($items) { ?>
    <div class="row" id="viewMoreLinkBlock">
        <div class="col-md-12 text-center viewAllItems">
            <div>
                <?= Html::img('@web/images/view_more_plus.png', ['alt' => 'view more']); ?>
            </div>
            <div>
                <a href="" data-shown="promotionsHidden" data-removed="viewMoreLinkBlock" class="viewMoreLink"><?= \Yii::t('app', 'Показать все акции и скидки'); ?></a>
            </div>
            <div>
                <span class="viewMoreSummary"><?= \Yii::t('app', 'Показано {x} из {y}', ['x' => 5, 'y' => $itemsCount]) ?></span>
            </div>
        </div>
    </div>
<div class="hidden" id="promotionsHidden">
    <?php
    foreach ($items as $key => $item) {
        echo $this->render('_promotions_item', [
            'item' => $item
        ]);
    }
    ?>
</div>
<?php } ?>