<?php

use yii\helpers\Html;
?>

<div class="panel panel-default panel-white">
    <div class="panel-body">
        <div class="row promotionChild">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8 col-xs-12">
                        <h3 class="text-uppercase"><?= $item->title ?></h3>
                    </div>
                    <div class="col-md-4 col-xs-12 text-right text-muted">
                        <div class="date"><?= date('d.m.Y', $item->created_at) ?></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= Html::a($item->title, ['page/view', 'id' => $item->id]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="content">
                            <?= $item->preview ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
