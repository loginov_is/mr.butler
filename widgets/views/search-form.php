<?php

use yii\helpers\Html;
use kartik\widgets\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\City;
use app\models\Lookup;
use kartik\slider\Slider;
use app\models\GuidebookForm;
?>
<div class="container searchPage">
    <?php $cssClass = 'siteSearchTopSearchBlock'; ?>
    <?php if (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') { ?>
        <?php $cssClass = ''; ?>
        <div class="row">
            <div class="col-md-12 text-right">
                <?= Html::a(\Yii::t('app', 'Популярные объявления'), ['/site/search'], ['class' => 'searchFormLink']); ?>
                <?= Html::a(\Yii::t('app', 'Гид по районам'), ['/guidebook/index'], ['class' => 'searchFormLink']); ?>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <div class="topSearchBlock <?= $cssClass ?>">
                <?php $form = ActiveForm::begin(['action' => ['/site/search'], 'method' => 'get']) ?>
                <div class="row">
                    <div class="col-md-2 col-xs-12">
                        <?=
                        $form->field($model, 'cityId')->dropDownList(ArrayHelper::map(City::find()->all(), 'id', 'name'), [
                            'prompt' => '- ' . $model->getAttributeLabel('cityId') . ' -',
                        ])
                        ?>
                    </div>
                    <div class="col-md-2 col-xs-12">
                        <?=
                        $form->field($model, 'dateFrom')->widget(
                                DatePicker::className(), [
                            'type' => DatePicker::TYPE_INPUT,
                            'language' => Yii::$app->language,
                            'options' => [
                                'placeholder' => 'mm/dd/yyyy',
                                'class' => 'form-control fieldAdvance',
                            ],
                            'pluginOptions' => [
                                'format' => 'mm/dd/yyyy',
                                'todayHighlight' => true,
                            ]
                                ]
                        )
                        ?>
                        <p class="help-block">
                            <?= Html::error($model, 'dateFrom') ?>
                        </p>
                    </div>
                    <div class="col-md-2 col-xs-12">
                        <div class="form-group field-searchform-dateTo required">
                            <?=
                            $form->field($model, 'dateTo')->widget(
                                    DatePicker::className(), [
                                'type' => DatePicker::TYPE_INPUT,
                                'language' => Yii::$app->language,
                                'options' => [
                                    'placeholder' => 'mm/dd/yyyy',
                                    'class' => 'form-control fieldAdvance',
                                ],
                                'pluginOptions' => [
                                    'format' => 'mm/dd/yyyy',
                                    'todayHighlight' => true,
                                ]
                                    ]
                            )
                            ?>
                            <p class="help-block">
                                <?= Html::error($model, 'dateTo') ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12">
                        <?=
                        $form->field($model, 'guestsCount')->dropDownList([
                            1 => \Yii::t('app', '1 гость'),
                            2 => \Yii::t('app', '2 гостя'),
                            3 => \Yii::t('app', '3 гостя'),
                            4 => \Yii::t('app', '4 гостя'),
                            5 => \Yii::t('app', '5 гостей'),
                            6 => \Yii::t('app', '6 гостей'),
                            7 => \Yii::t('app', '7 гостей'),
                            8 => \Yii::t('app', '8 гостей'),
                            9 => \Yii::t('app', '9 гостей'),
                            10 => \Yii::t('app', '10+ гостей'),
                                ], [
                            'prompt' => '- ' . $model->getAttributeLabel('guestsCount') . ' -',
                        ])
                        ?>
                    </div>
                    <div class="col-md-3 col-xs-12">
                        <?= $form->field($model, 'typeId')->dropDownList(Lookup::items(Lookup::TYPE_HOUSING_TYPE)) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-xs-12">
                        <?= $form->field($model, 'priceType')->dropDownList(Lookup::items(Lookup::TYPE_REAL_ESTATE_PRICE_RAGE)) ?>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <?php
                        $minPrice = GuidebookForm::MIN_FORM_PRICE;
                        $maxPrice = GuidebookForm::MAX_FORM_PRICE;
                        $priceStep = GuidebookForm::FORM_PRICE_STEP;
                        echo $form->field($model, 'price')->widget(Slider::classname(), [
                            'value' => "$minPrice,$maxPrice",
                            'sliderColor' => Slider::TYPE_WARNING,
                            'pluginOptions' => [
                                'min' => $minPrice,
                                'max' => $maxPrice,
                                'step' => $priceStep,
                                'range' => true,
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-md-2 col-xs-12">
                        <?=
                        Html::submitButton('Ok', [
                            'class' => 'btn btn-default btnAdnvace pull-right'
                        ])
                        ?>
                    </div>
                </div>
                <div class="row openAdvanceOptionsBlock">
                    <div class="col-md-3 col-xs-12">
                        <?= $form->field($model, 'showOptions')->hiddenInput() ?>
                        <?= Html::button(\Yii::t('app', 'Дополнительные фильтры'), ['class' => ' btn realEstateSubmitButton openAdvanceOptions']) ?>
                    </div>
                </div>
                <?php $cssClass = ($model->showOptions) ? "" : "hidden"; ?>
                <div class="row advanceOptions <?= $cssClass ?>">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="bold"><?= \Yii::t('app', 'Районы') ?></p>
                                <?php $regionData = $model->getRegionCheckboxData($model->regions); ?>
                                <div class="row topSearchRegionsRow">
                                    <?php foreach ($regionData as $key => $item) { ?>
                                        <div class="col-md-4 topSearchRegionsItem">
                                            <?= Html::checkbox('SearchForm[regions][' . $key . ']', $item['checked']) ?> <?= $item['name'] ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="row topSearchBlockRegions hidden">
                            <div class="col-md-12">
                                <p class="bold"><?= \Yii::t('app', 'Районы') ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row" style="margin-top: 20px;">
                                    <div class="col-md-4">
                                        <?=
                                        $form->field($model, 'bedrooms')->dropDownList([
                                            1 => \Yii::t('app', '1'),
                                            2 => \Yii::t('app', '2'),
                                            3 => \Yii::t('app', '3'),
                                            4 => \Yii::t('app', '4'),
                                            5 => \Yii::t('app', '5'),
                                            6 => \Yii::t('app', '6'),
                                            7 => \Yii::t('app', '7'),
                                            8 => \Yii::t('app', '8'),
                                            9 => \Yii::t('app', '9'),
                                            10 => \Yii::t('app', '10+'),
                                                ], [
                                            'prompt' => '- ' . $model->getAttributeLabel('bedrooms') . ' -',
                                        ])
                                        ?>
                                    </div>
                                    <div class="col-md-4">
                                        <?=
                                        $form->field($model, 'bathrooms')->dropDownList([
                                            1 => \Yii::t('app', '1'),
                                            2 => \Yii::t('app', '2'),
                                            3 => \Yii::t('app', '3'),
                                            4 => \Yii::t('app', '4'),
                                            5 => \Yii::t('app', '5'),
                                            6 => \Yii::t('app', '6'),
                                            7 => \Yii::t('app', '7'),
                                            8 => \Yii::t('app', '8'),
                                            9 => \Yii::t('app', '9'),
                                            10 => \Yii::t('app', '10+'),
                                                ], [
                                            'prompt' => '- ' . $model->getAttributeLabel('bathrooms') . ' -',
                                        ])
                                        ?>
                                    </div>
                                    <div class="col-md-4">
                                        <?= $form->field($model, 'appartamentsType')->dropDownList(Lookup::items(Lookup::TYPE_REAL_ESTATE_TYPES)) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="bold"><?= \Yii::t('app', 'Удобства') ?></p>
                                <?php $comfortData = $model->getComfortCheckboxData($model->facilities); ?>
                                <div class="row">
                                    <?php foreach ($comfortData as $key => $item) { ?>
                                        <div class="col-md-4 topSearchFacilitiesItem">
                                            <?= Html::checkbox('SearchForm[facilities][' . $key . ']', $item['checked']) ?> <?= $item['title'] ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?= $form->field($model, 'sortBy')->hiddenInput()->label(false) ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>