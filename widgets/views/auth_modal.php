<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;
?>

<div class="modal fade authModal registrationBlock">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close closeAdvance" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center"><?= Yii::t('app', 'Войти с помощью:') ?></h4>
                <div class="text-center">
                    <?=
                    AuthChoice::widget([
                        'baseAuthUrl' => ['site/auth'],
                        'popupMode' => true,
                    ])
                    ?>
                </div>
                <div class="clearfix clear"></div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="modal-title text-center"><?= Yii::t('app', 'или через электронную почту') ?></p>
                        <?php $form = ActiveForm::begin([
                            'id' => 'form-login',
                            'action' => ['/site/login'],
                        ]); ?>
                        <?= $form->field($model, 'email')->input('text', ['class' => 'form-control fieldAdvance', 'placeholder' => \Yii::t('app', 'Email')]) ?>
                        <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control fieldAdvance', 'placeholder' => \Yii::t('app', 'Пароль')]) ?>
                        <?= $form->field($model, 'rememberMe')->checkbox() ?>
                        <div class="form-group text-center">
                            <?= Html::submitButton(Yii::t('app', 'Войти'), ['class' => 'btn btn-primary submitAdvance', 'name' => 'signup-button']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>