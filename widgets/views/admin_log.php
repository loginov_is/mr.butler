<?php

use yii\grid\GridView;
use app\models\UserLog;
?>
<h3><?= \Yii::t('app', 'История изменений') ?></h3>
<?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'created_at:datetime',
        [
            'attribute' => 'created_by',
            'value' => function ($row) {
                if (isset($row->createdBy->email)) {
                    return $row->createdBy->email;
                } else {
                    return $row->created_by;
                }
            }
        ],
        [
            'attribute' => 'action_id',
            'value' => function ($row) {
                return UserLog::getActionList($row->action_id);
            }
        ],
    ],
]);
?>