<?php

use yii\helpers\Html;
?>
<div class="row">
    <div class="col-md-12">
        <h1 class="pageTitle text-center"><?php echo \Yii::t('app', 'Как это работает?'); ?></h1>
        <div class="pageTitleHr"></div>
    </div>
</div>
<div class="row howItWork">
    <?php foreach ($model as $item) { ?>
        <div class="col-md-2 text-center">
            <?= Html::img('@web/upload/motivation_pictures/' . $item->image, ['alt' => $item->title]); ?>
            <p><?= \app\models\Translation::t($item, 'title') ?></p>
        </div>
    <?php } ?>
</div>