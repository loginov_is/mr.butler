<?php

use yii\helpers\Html;
?>

<div class="container">
    <div class="row bottomMotivation">
        <?php foreach ($model as $item) { ?>
            <div class="col-md-3 text-center">
                <?= Html::img('@web/upload/motivation_pictures/' . $item->image, ['alt' => $item->title]); ?>
                <p><?= \app\models\Translation::t($item, 'title') ?></p>
            </div>
        <?php } ?>
    </div>
</div>