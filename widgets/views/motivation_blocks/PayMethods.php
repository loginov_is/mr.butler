<?php

use yii\helpers\Html;
?>

<div class="row PayMethodsItems">
    <?php foreach ($model as $item) { ?>
        <div class="col-md-4 text-center PayMethods">
            <div class="block">
                <?= Html::img('@web/upload/motivation_pictures/' . $item->image, ['alt' => $item->title]); ?>
                <p><?= \app\models\Translation::t($item, 'title') ?></p>
            </div>
        </div>
    <?php } ?>
</div>