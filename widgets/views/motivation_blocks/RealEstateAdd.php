<?php

use yii\helpers\Html;
?>

<div class="container">
    <div class="row realEstateAddMotivation">
        <?php foreach ($model as $item) { ?>
            <div class="col-md-4 text-center">
                <?= Html::img('@web/upload/motivation_pictures/' . $item->image, ['alt' => $item->title]); ?>
                <p class="title"><?= \app\models\Translation::t($item, 'title') ?></p>
            </div>
        <?php } ?>
        <p class="motivationDescr text-center">
            <?= \Yii::t('app', 'Предложите нам свою недвижимость в доверительное управление и занимайтесь своими делами. Мы позаботимся о ваших проблемах') ?>
        </p>
    </div>
</div>