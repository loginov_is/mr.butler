<div class="topSocial">
    <div class="hidden-xs"><?= \Yii::t('app', 'Наше сообщество') ?>:</div>
    <?php if ($model->vkontakte) { ?>
        <div><a href="<?= $model->vkontakte ?>" target="_blank"><i class="fa fa-vk"></i></a></div>
    <?php } ?>
    <?php if ($model->facebook) { ?>
        <div><a href="<?= $model->facebook ?>" target="_blank"><i class="fa fa-facebook"></i></a></div>
    <?php } ?>
    <?php if ($model->youtube) { ?>
        <div><a href="<?= $model->youtube ?>" target="_blank"><i class="fa fa-youtube"></i></a></div>
            <?php } ?>
</div>