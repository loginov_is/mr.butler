<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin(); ?>
<div class="footerFeedback">
    <div class="container">
        <div class="row">
            <div class="col-md-1 col-md-offset-1 imgBlock col-xs-3">
                <?php echo Html::img('@web/images/feedback.png', ['alt' => 'feedback']); ?>
            </div>
            <div class="col-md-1 col-xs-9">
                <span class="title"><?php echo \Yii::t('app', 'Свяжитесь с нами'); ?></span>
            </div>
            <div class="col-md-2">
                <?=
                $form->field($model, 'email')->textInput([
                    'maxlength' => true,
                    'class' => 'form-control fieldAdvance',
                    'placeholder' => \Yii::t('app', 'E-mail')
                ])
                ?>
            </div>
            <div class="col-md-2">
                <?=
                $form->field($model, 'phone')->textInput([
                    'maxlength' => true,
                    'class' => 'form-control fieldAdvance',
                    'placeholder' => \Yii::t('app', '+7 (000) 000-00-00')
                ])
                ?>
            </div>
            <div class="col-md-2">
                <?=
                $form->field($model, 'name')->textInput([
                    'maxlength' => true,
                    'class' => 'form-control fieldAdvance',
                    'placeholder' => \Yii::t('app', 'Ваше имя')
                ])
                ?>
            </div>
            <div class="col-md-2">
                <?=
                Html::submitButton(\Yii::t('app', 'Отправить'), [
                    'class' => 'btn btn-default btnAdnvace'
                ]);
                ?>
            </div>
            <div class="col-md-1">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-10 col-md-offset-1">
                <?= $this->render('//default/flash') ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>