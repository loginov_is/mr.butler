<?php

use yii\helpers\Html;
?>
<?php if (!Yii::$app->user->isGuest) { ?>
    <div class="userTopNavigation">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-12 text-center">
                    <?= Html::a(\Yii::t('app', 'Профиль'), ['profile/index']) ?>
                </div>
                <div class="col-md-3 col-xs-12 text-center">
                    <?php if (Yii::$app->user->can('landlord')) { ?>
                        <?= Html::a(\Yii::t('app', 'Мой баланс'), ['profile/my-balance']) ?>
                    <?php } else { ?>
                        <?= Html::a(\Yii::t('app', 'Просмотренные объекты недвижимости'), ['profile/viewed-objects']) ?>
                    <?php } ?>

                </div>
                <div class="col-md-3 col-xs-12 text-center">
                    <?php if (Yii::$app->user->can('landlord')) { ?>
                        <?= Html::a(\Yii::t('app', 'Мои объекты'), ['profile/my-objects']) ?>
                    <?php } else { ?>
                        <?= Html::a(\Yii::t('app', 'Арендованные объекты недвижимости'), ['profile/selected-objects']) ?>
                    <?php } ?>
                </div>
                <div class="col-md-3 col-xs-12 text-center">
                    <?= Html::a(\Yii::t('app', 'Акции и скидки'), ['profile/promotions']) ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>