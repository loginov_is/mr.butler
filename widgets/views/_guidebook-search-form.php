<?php
/* @var $this yii\web\View */
/* @var $model \app\models\GuidebookForm */

use app\models\City;
use app\models\Lookup;
use app\models\UserLine;
use app\models\Region;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\slider\Slider;
use app\models\Translation;
use app\models\GuidebookForm;
?>

<div class="container guidBookSearch">
    <?php $titleValue = ($region) ? Translation::t($region, 'name') : \Yii::t('app', 'Гид по районам'); ?>
    <h1 class="text-center"><?= $titleValue ?></h1>
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-xs-12">
            <ul class="options">
                <?php if ($comfort) { ?>
                    <?php foreach ($comfort as $item) { ?>
                        <li><?= Translation::t($item, 'name') ?></li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="userActions row">
        <div class="col-md-4 col-xs-7">
            <?php if ($model->regionId) { ?>
                <?php if (!Yii::$app->user->isGuest && !$userLine) { ?>
                    <a href="<?= Url::to(['user-line/create', 'modelId' => $model->regionId, 'typeId' => UserLine::TYPE_ID_REGION_ADDED_TO_USERLINE]) ?>" class="saveRegion"><i class="fa fa-plus"></i> <span><?= Yii::t('app', 'сохранить этот район в строку пользователя') ?></span></a>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="col-md-2 col-md-offset-6 col-xs-12">
            <?php if ($weatherTemperature) { ?>
                <div class="row weatherBlock">
                    <div class="col-md-4 col-xs-4 text-center">
                        <i class="wi wi-forecast-io-<?= $weatherIcon ?>"></i>
                    </div>
                    <div class="col-md-8 col-xs-8 text-center">
                        <p class="weather"><?= $weatherTemperature ?></p>
                        <p class="weatherOptions"><?= $weatherDescription ?></p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="row topSearchBlock">
        <?php
        $form = ActiveForm::begin([
                    'action' => ['/guidebook/search'],
                    'method' => 'get',
        ]);
        ?>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <?=
                    $form->field($model, 'cityId')->dropDownList(ArrayHelper::map(City::find()->all(), 'id', 'name'), [
                        'prompt' => '- ' . $model->getAttributeLabel('cityId') . ' -',
                        'data' => [
                            'ajax-option' => true,
                            'target' => Html::getInputId($model, 'regionId'),
                        ],
                    ])
                    ?>
                </div>
                <div class="col-md-3 col-xs-12">
                    <?=
                    $form->field($model, 'regionId')->dropDownList(ArrayHelper::map(Region::find()->where(['city_id' => $model->cityId])->all(), 'id', 'name'), [
                        'prompt' => '- ' . $model->getAttributeLabel('regionId') . ' -',
                        'data' => [
                            'url' => Url::to(['/guidebook/ajax-region']),
                        ],
                    ])
                    ?>
                </div>
                <div class="col-md-3 col-xs-12">
                    <?= $form->field($model, 'housingType')->dropDownList(Lookup::items(Lookup::TYPE_HOUSING_TYPE)) ?>
                </div>
                <div class="col-md-3 col-xs-12">
                    <?= $form->field($model, 'residents')->dropDownList(array_combine(range(1, 10), range(1, 10))) ?>
                </div>
            </div>
            <div class="row guidebookSearchParameters guidebookSearchExtraParameters">
                <div class="col-md-7 col-xs-12">
                    <?php
                    $minPrice = GuidebookForm::MIN_FORM_PRICE;
                    $maxPrice = GuidebookForm::MAX_FORM_PRICE;
                    $priceStep = GuidebookForm::FORM_PRICE_STEP; 
                    echo $form->field($model, 'price')->widget(Slider::classname(), [
                        'value' => "$minPrice,$maxPrice",
                        'sliderColor' => Slider::TYPE_WARNING,
                        'pluginOptions' => [
                            'min' => $minPrice,
                            'max' => $maxPrice,
                            'step' => $priceStep,
                            'range' => true,
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-md-3 col-xs-12">
                    <?= $form->field($model, 'sights')->checkbox(); ?>
                    <?= $form->field($model, 'sameRegions')->checkbox(); ?>
                </div>
                <div class="col-md-2 col-xs-12 text-center" style="padding-top: 13px;">
                    <?=
                    Html::submitButton(Yii::t('app', 'Поиск'), [
                        'class' => 'btn btn-default realEstateSubmitButton'
                    ])
                    ?>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <?= $form->field($model, 'priceType')->dropDownList(['daily_price' => 'Стоимость за сутки', 'monthly_price' => 'Стоимость за месяц']) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
