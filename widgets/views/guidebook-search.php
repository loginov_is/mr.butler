<?php
/* @var $this yii\web\View */
/* @var $model \app\models\GuidebookForm */

use app\models\Region;
use app\widgets\BackgroundWidget;
use app\models\Page;
?>



<?php
    echo BackgroundWidget::widget([
        'content' => $this->render('_guidebook-search-form', [
            'model' => $model,
            'weatherTemperature' => $weatherTemperature,
            'weatherDescription' => $weatherDescription,
            'weatherIcon' => $weatherIcon,
            'comfort' => $comfort,
            'region' => $region,
            'userLine' => $userLine,
        ]),
//    'htmlClass' => 'site-dynamic-page',
    'page' => Page::getByAction()
    ]);
?>

<?php
$this->render('_guidebook-search-form', [
    'model' => $model,
    'weatherTemperature' => $weatherTemperature,
    'weatherDescription' => $weatherDescription,
    'weatherIcon' => $weatherIcon,
    'comfort' => $comfort,
    'region' => $region,
    'userLine' => $userLine,
])
?>
