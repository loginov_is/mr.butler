<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Reviews;

class ReviewsWidget extends Widget {

    public function run() {
        $model = Reviews::find()->where(['status_id' => Reviews::STATUS_ACTIVE])->orderBy('created_at desc')->all();
        return $this->render('reviews', [
            'model' => $model
        ]);
    }

}
