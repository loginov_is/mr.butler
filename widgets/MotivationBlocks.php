<?php

namespace app\widgets;

use yii\base\Widget;
use app\models\MotivationPicture;

class MotivationBlocks extends Widget {

    public $type;

    public function init() {
        $model = MotivationPicture::find()->where('type = :type', array(':type' => $this->type))->orderBy('order')->all();
        echo $this->render('motivation_blocks/' . $this->type, [
            'model' => $model
        ]);
    }

}
