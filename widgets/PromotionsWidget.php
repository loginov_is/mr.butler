<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Page;

class PromotionsWidget extends Widget {

    public function run() {
        $items = Page::find()->where('type_id = :type_id AND language_id = :language_id', [
                    'type_id' => Page::TYPE_PROMOTIONS,
                    'language_id' => Yii::$app->languageInfo->id
                ])->orderBy('id desc')->all();

        $itemsCount = count($items);

        return $this->render('promotions', [
                    'items' => $items,
                    'itemsCount' => $itemsCount
        ]);
    }

}
