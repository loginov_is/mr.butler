<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\SearchForm;

class TopSearchForm extends Widget {

    public function run() {
        $model = new SearchForm();
        $model->load(Yii::$app->request->get());

        return $this->render('search-form', [
                    'model' => $model
        ]);
    }

}
