<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Contact;

class ContactsWidget extends Widget {

    public function run() {
        $model = Contact::find()->joinWith('language', true, 'RIGHT JOIN')->where(['{{%language}}.code' => Yii::$app->language])->one();
        $mapCoords = ($model->map) ? $model->map : 0;
        $this->view->registerJs('Main.mapInit("' . $mapCoords . '", "Title")');
        return $this->render('contacts', [
            'model' => $model
        ]);
    }

}
