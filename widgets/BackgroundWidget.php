<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Page;
use app\models\ImageAlbum;

class BackgroundWidget extends Widget {

    public $content;
    public $htmlClass;
    public $page;

    public function run() {
        $style = '';
        $images = [];
        $sliderType = null;

        if ($this->page) {
            $backgroundImage = $this->page->background_image;
            $sliderType = $this->page->slider_type;
            $style = 'style="background-image: url(' . Yii::getAlias('@web/upload/' . $backgroundImage) . ');"';

            if ($this->page->slider_type == Page::SLIDER_TYPE_SLIDER) {
                $images = ImageAlbum::findOne(['id' => $this->page->album_id])->getAlbumImages()->all();
            }
        }

        return $this->render('background_block', [
                    'style' => $style,
                    'content' => $this->content,
                    'class' => $this->htmlClass,
                    'images' => $images,
                    'sliderType' => $sliderType,
        ]);
    }

}
