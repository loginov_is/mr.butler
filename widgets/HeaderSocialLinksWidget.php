<?php

namespace app\widgets;

use yii\base\Widget;
use app\models\SiteOption;

class HeaderSocialLinksWidget extends Widget {

    public function run() {
        $model = SiteOption::find()->one();
        return $this->render('header_social_links', array(
                    'model' => $model
        ));
    }

}
