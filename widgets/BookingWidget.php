<?php

namespace app\widgets;


use Yii;
use yii\base\Widget;
use app\models\Booking;
use app\models\BookingForm;
use app\models\RealEstate;

class BookingWidget extends Widget
{
    public $realEstateId;

    public function run()
    {
        $model = new BookingForm(['scenario' => BookingForm::SCENARIO_STEP_1]);

        if($model->load(Yii::$app->request->post())) {
            d($model->startDateTimestamp);
        }

        return $this->render('booking', [
            'model' => $model,
            'realEstateId' => $this->realEstateId,
        ]);
    }
}
