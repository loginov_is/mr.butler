<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;

class UserTopNavigationWidget extends Widget {

    public function run() {
        return $this->render('user_top_navigation', [
        ]);
    }

}
