<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Currency;

class CurrencyWidget extends Widget {

    public function run() {
        $model = Currency::find()->all();
        $items = [];
        $currentCurrency = Currency::DEFAULT_CURRENCY;
        $postedCurrency = Yii::$app->request->post('currency');

        if ($postedCurrency) {
            $currentCurrency = $postedCurrency;
        } else if (Yii::$app->session->get('currency') !== null) {
            $currentCurrency = Yii::$app->session->get('currency');
        }

        Yii::$app->session->set('currency', $currentCurrency);

        foreach ($model as $currency) {
            $items[$currency->code] = $currency->code . ' <span class="currencyDescription">' . $currency->description . '</span>';
        }

        return $this->render('currency', array(
                    'items' => $items,
                    'currentCurrency' => $currentCurrency
        ));
    }

}
