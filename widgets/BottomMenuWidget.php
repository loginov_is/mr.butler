<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Menu;
use app\helpers\ContentHelper;

class BottomMenuWidget extends Widget {

    public function run() {
        $items = [
            [
                'label' => Yii::t('app', 'Главная'),
                'url' => ['site/index'],
            ],
            [
                'label' => Yii::t('app', 'Передать недвижимость в управление'),
                'url' => ['real-estate/add'],
            ],
            [
                'label' => Yii::t('app', 'Помощь'),
                'url' => ['faq/index'],
            ],
            [
                'label' => Yii::t('app', 'О нас'),
                'url' => ['site/about'],
            ],
            [
                'label' => Yii::t('app', 'Правила'),
                'url' => ['/regulation/index'],
            ],
            [
                'label' => Yii::t('app', 'Контакты'),
                'url' => ['contact/view'],
            ],
            [
                'label' => Yii::t('app', 'Условия и конфеденциальность'),
                'url' => ['site/terms'],
            ],
            [
                'label' => Yii::t('app', 'Акции и скидки'),
                'url' => ['site/promotions'],
            ],
            [
                'label' => Yii::t('app', 'Способы оплаты'),
                'url' => ['site/payments'],
            ],
        ];

        $menuItems = Menu::find()->where('active = :active AND type_id = :type_id AND language_id = :language_id', [
                    'active' => ContentHelper::ITEM_STATUS_ACTIVE,
                    'type_id' => Menu::TYPE_BOTTOM_MENU,
                    'language_id' => Yii::$app->languageInfo->id
                ])->orderBy('order')->all();

        foreach ($menuItems as $menuItem) {
            $items[] = [
                'label' => $menuItem->page->title,
                'url' => ['page/view', 'id' => $menuItem->page->id]
            ];
        }

        return $this->render('bottom_menu', [
                    'items' => $items
        ]);
    }

}
