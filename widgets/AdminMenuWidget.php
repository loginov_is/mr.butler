<?php

namespace app\widgets;

use Yii;
use app\models\Menu;
use app\models\Language;
use app\models\Feedback;
use app\models\Payment;
use app\models\Ticket;
use app\models\Reviews;

class AdminMenuWidget extends \yii\widgets\Menu {

    /**
     * @inheritdoc
     */
    public $options = ['class' => 'sidebar-menu'];

    /**
     * @inheritdoc
     */
    public $submenuTemplate = "\n<ul class='treeview-menu'>\n{items}\n</ul>\n";

    /**
     * @inheritdoc
     */
    public $encodeLabels = false;

    /**
     * @inheritdoc
     */
    public $activateParents = true;

    /**
     * @inheritdoc
     */
    public $linkTemplate = '<a href="{url}">{label}</a>';

    public function init() {
        $items = [];

//        $langItems = [];
//        foreach (Language::find()->all() as $language) {
//            $langItems[] = [
//                'label' => Yii::t('app', 'Просмотр (' . $language->code . ')'),
//                'url' => ['/menu/admin', 'type' => Menu::TYPE_TOP_MENU, 'language_id' => $language->id]
//            ];
//        }
//        $subItems = [];
//        $subItems[] = [
//            'label' => Yii::t('app', 'Верхнее меню') . '<i class="fa fa-angle-left pull-right"></i>',
//            'url' => '#',
//            'options' => ['class' => 'treeview'],
//            'items' => $langItems,
//        ];

        $langItems = [];
        foreach (Language::find()->all() as $language) {
            $langItems[] = [
                'label' => Yii::t('app', 'Просмотр (' . $language->code . ')'),
                'url' => ['/menu/admin', 'type' => Menu::TYPE_BOTTOM_MENU, 'language_id' => $language->id]
            ];
        }

        $subItems[] = [
            'label' => Yii::t('app', 'Нижнее меню') . '<i class="fa fa-angle-left pull-right"></i>',
            'url' => '#',
            'options' => ['class' => 'treeview'],
            'items' => $langItems,
        ];

        $subItems[] = [
            'label' => '<i class="fa fa-plus"></i> ' . Yii::t('app', 'Добавить'),
            'url' => ['/menu/create'],
        ];

        $items[] = [
            'label' => '<i class="fa fa-database"></i> <span>' . Yii::t('app', 'Меню') . '</span><i class="fa fa-angle-left pull-right"></i>',
            'url' => '#',
            'options' => ['class' => 'treeview'],
            'items' => $subItems,
        ];

        $items[] = [
            'label' => '<i class="fa fa-globe"></i> <span>' . Yii::t('app', 'Справочники') . '</span><i class="fa fa-angle-left pull-right"></i>',
            'url' => '#',
            'options' => ['class' => 'treeview'],
            'items' => [
                [
                    'label' => Yii::t('app', 'Страны'),
                    'url' => ['/country/admin']
                ],
                [
                    'label' => Yii::t('app', 'Города'),
                    'url' => ['/city/admin']
                ],
                [
                    'label' => Yii::t('app', 'Районы'),
                    'url' => ['/region/admin']
                ],
                [
                    'label' => Yii::t('app', 'Языки'),
                    'url' => ['/language/admin']
                ],
                [
                    'label' => Yii::t('app', 'Списки'),
                    'url' => ['/lookup/admin']
                ],
                [
                    'label' => Yii::t('app', 'Валюты'),
                    'url' => ['/currency/admin']
                ],
                [
                    'label' => Yii::t('app', 'Мотивирующие блоки'),
                    'url' => ['/motivation-pictures/admin']
                ],
                [
                    'label' => Yii::t('app', 'Пресса'),
                    'url' => ['/press/admin']
                ],
                [
                    'label' => Yii::t('app', 'Пресса о нас'),
                    'url' => ['/press-article/admin']
                ],
                [
                    'label' => Yii::t('app', 'Команда'),
                    'url' => ['/team/admin']
                ],
                [
                    'label' => Yii::t('app', 'Партнеры'),
                    'url' => ['/partner/admin']
                ],
                [
                    'label' => Yii::t('app', 'Платежные системы'),
                    'url' => ['/payment-system/admin']
                ],
                [
                    'label' => Yii::t('app', 'Контакты'),
                    'url' => ['/contact/admin']
                ],
                [
                    'label' => Yii::t('app', 'Дополнительные услуги'),
                    'url' => ['/service/admin']
                ],
                [
                    'label' => Yii::t('app', 'Вопрос/Ответ') . '<i class="fa fa-angle-left pull-right"></i>',
                    'url' => '#',
                    'options' => ['class' => 'treeview'],
                    'items' => [
                        [
                            'label' => Yii::t('app', 'Группы'),
                            'url' => ['/faq-group/admin'],
                        ],
                        [
                            'label' => Yii::t('app', 'Вопросы'),
                            'url' => ['/faq/admin']
                        ],
                    ],
                ],
                [
                    'label' => Yii::t('app', 'Правила') . '<i class="fa fa-angle-left pull-right"></i>',
                    'url' => '#',
                    'options' => ['class' => 'treeview'],
                    'items' => [
                        [
                            'label' => Yii::t('app', 'Группы'),
                            'url' => ['/regulation-group/admin'],
                        ],
                        [
                            'label' => Yii::t('app', 'Правила'),
                            'url' => ['/regulation/admin']
                        ],
                    ],
                ],
                [
                    'label' => Yii::t('app', 'Медиа') . '<i class="fa fa-angle-left pull-right"></i>',
                    'url' => '#',
                    'options' => ['class' => 'treeview'],
                    'items' => [
                        [
                            'label' => Yii::t('app', 'Альбомы') . '<i class="fa fa-angle-left pull-right"></i>',
                            'url' => '#',
                            'items' => [
                                [
                                    'label' => '<i class="fa fa-cog"></i> ' . Yii::t('app', 'Управление'),
                                    'url' => ['/album/admin']
                                ],
                                [
                                    'label' => '<i class="fa fa-plus"></i> ' . Yii::t('app', 'Добавить'),
                                    'url' => ['/album/create']
                                ],
                            ],
                        ],
                        [
                            'label' => Yii::t('app', 'Слайды') . '<i class="fa fa-angle-left pull-right"></i>',
                            'url' => '#',
                            'items' => [
                                [
                                    'label' => '<i class="fa fa-cog"></i> ' . Yii::t('app', 'Управление'),
                                    'url' => ['/image/admin']
                                ],
                                [
                                    'label' => '<i class="fa fa-plus"></i> ' . Yii::t('app', 'Добавить'),
                                    'url' => ['/image/create']
                                ],
                            ],
                        ],
                        [
                            'label' => Yii::t('app', 'Видео') . '<i class="fa fa-angle-left pull-right"></i>',
                            'url' => '#',
                            'items' => [
                                [
                                    'label' => '<i class="fa fa-cog"></i> ' . Yii::t('app', 'Управление'),
                                    'url' => ['/video/admin']
                                ],
                                [
                                    'label' => '<i class="fa fa-plus"></i> ' . Yii::t('app', 'Добавить'),
                                    'url' => ['/video/create']
                                ],
                            ],
                        ],
                    ],
                ]
            ],
        ];

        $items[] = [
            'label' => '<i class="fa fa-file"></i> <span>' . Yii::t('app', 'Страницы') . '</span><i class="fa fa-angle-left pull-right"></i>',
            'url' => '#',
            'options' => ['class' => 'treeview'],
            'items' => [
                [
                    'label' => Yii::t('app', 'Просмотр'),
                    'url' => ['/page/admin']
                ],
                [
                    'label' => Yii::t('app', 'Создать'),
                    'url' => ['/page/create']
                ],
                [
                    'label' => Yii::t('app', 'О нас'),
                    'url' => ['/page-about/update']
                ],
            ],
        ];

        $items[] = [
            'label' => '<i class="fa fa-file"></i> <span>' . Yii::t('app', 'Недвижимость') . '</span><i class="fa fa-angle-left pull-right"></i>',
            'url' => '#',
            'options' => ['class' => 'treeview'],
            'items' => [
                [
                    'label' => Yii::t('app', 'Объекты'),
                    'url' => ['/real-estate/admin']
                ],
                [
                    'label' => Yii::t('app', 'Проекты'),
                    'url' => ['/project/admin']
                ],
                [
                    'label' => Yii::t('app', 'Бронирование'),
                    'url' => ['/booking/admin']
                ],
            ],
        ];

        $items[] = [
            'label' => '<i class="fa fa-user"></i> <span>' . Yii::t('app', 'Пользователи') . '</span><i class="fa fa-angle-left pull-right"></i>',
            'url' => '#',
            'options' => ['class' => 'treeview'],
            'items' => [
                [
                    'label' => '<i class="fa fa-cog"></i> ' . Yii::t('app', 'Управление'),
                    'url' => ['/user/admin']
                ],
                [
                    'label' => Yii::t('app', 'Права') . '<i class="fa fa-angle-left pull-right"></i>',
                    'url' => '#',
                    'options' => ['class' => 'treeview'],
                    'items' => [
                        [
                            'label' => '<i class="fa fa-cog"></i> ' . Yii::t('app', 'Назначение'),
                            'url' => ['/rbac/assignment/index'],
                        ],
                        [
                            'label' => '<i class="fa fa-cog"></i> ' . Yii::t('app', 'Роли'),
                            'url' => ['/rbac/role/index'],
                        ],
                        [
                            'label' => '<i class="fa fa-cog"></i> ' . Yii::t('app', 'Права'),
                            'url' => ['/rbac/permission/admin'],
                        ],
                    ],
                ]
            ],
        ];

        $items[] = [
            'label' => '<i class="fa fa-cogs"></i> ' . Yii::t('app', 'Настройки сайта'),
            'url' => ['/site-options/update']
        ];

        $items[] = [
            'label' => '<i class="fa fa-envelope"></i> ' . Yii::t('app', 'Обратная связь') . ' ' . '<span class="badge">' . $this->getNewFeedbackCount() . '</span>',
            'url' => ['/feedback/admin']
        ];

        $items[] = [
            'label' => '<i class="fa fa-dollar"></i> ' . Yii::t('app', 'Запросы на выплату') . ' ' . '<span class="badge">' . $this->getNewPaymentCount() . '</span>',
            'url' => ['/payment/admin']
        ];

        $items[] = [
            'label' => '<i class="fa fa-ticket"></i> ' . Yii::t('app', 'Тикеты (тех. поддержка)') . ' ' . '<span class="badge">' . $this->getNewTicketCount() . '</span>',
            'url' => ['/ticket/admin']
        ];

        $items[] = [
            'label' => '<i class="fa fa-comments"></i> ' . Yii::t('app', 'Отзывы') . ' ' . '<span class="badge">' . $this->getNewReviews() . '</span>',
            'url' => ['/reviews/admin']
        ];

        $items[] = [
            'label' => '<i class="fa fa-file-o"></i> ' . Yii::t('app', 'Бухгалтерия'),
            'url' => ['/accounting/admin']
        ];

        $this->items = $items;
    }

    public function getNewFeedbackCount() {
        $model = Feedback::find()->where('is_viewed = :not_viewed', [':not_viewed' => Feedback::IS_VIEWED_FALSE])->all();
        return count($model);
    }

    public function getNewPaymentCount() {
        $model = Payment::find()->where('status_id = :status', [':status' => Payment::STATUS_PENDING])->all();
        return count($model);
    }

    public function getNewTicketCount() {
        $model = Ticket::find()->where('viewed_admin = :status', [':status' => Ticket::VIEWED_ADMIN_FALSE])->all();
        return count($model);
    }

    public function getNewReviews() {
        $model = Reviews::find()->where('status_id = :status', [':status' => Reviews::STATUS_PENDING])->all();
        return count($model);
    }

}
