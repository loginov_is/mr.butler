<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Feedback;

class FooterFeedbackWidget extends Widget {

    public function run() {
        $model = new Feedback();
        $model->created_at = time();
        $model->user_id = (Yii::$app->user->isGuest) ? 0 : Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Сообещение успешно отправлено, мы свяжемся с Вами в ближайшее время.'));
        }

        return $this->render('footer_feedback', array(
                    'model' => $model
        ));
    }

}
