<?php

namespace app\widgets;

use app\models\GuidebookForm;
use app\models\Region;
use Yii;
use yii\base\Widget;
use Forecast\Forecast;
use app\models\UserLine;

class GuidebookSearch extends Widget {

    public function run() {
        /** @var GuidebookForm $model */
        $userLine = null;
        $region = null;
        $model = new GuidebookForm();
        $weatherTemperature = '';
        $weatherDescription = '';
        $weatherIcon = '';
        $comfort = [];
        if ($model->load(Yii::$app->request->get())) {
            if ($region = Region::findOne($model->regionId)) {
                $map = explode(',', $region->map);
                $forecast = new Forecast('b199b0cf1f70788efb04ff355c7a79da');
                $weather = $forecast->get(trim($map[0]), trim($map[1]), null, ['units' => 'si' , 'lang' => Yii::$app->language]);
                $comfort = $region->getComfort()->all();
                $weatherTemperature = sprintf("%+d", round($weather->currently->temperature));
                $weatherDescription = $weather->currently->summary;
                $weatherIcon = $weather->currently->icon;
                $userLine = UserLine::findOne([
                            'created_by' => Yii::$app->user->id,
                            'type_id' => UserLine::TYPE_ID_REGION_ADDED_TO_USERLINE,
                            'model_id' => $model->regionId
                ]);
            }
        }

        return $this->render('guidebook-search', [
                    'model' => $model,
                    'weatherTemperature' => $weatherTemperature,
                    'weatherDescription' => $weatherDescription,
                    'weatherIcon' => $weatherIcon,
                    'comfort' => $comfort,
                    'region' => $region,
                    'userLine' => $userLine
        ]);
    }

}
