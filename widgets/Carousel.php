<?php

namespace app\widgets;

use app\models\Page;
use yii\helpers\Html;

class Carousel extends \yii\bootstrap\Carousel
{
    public function init()
    {
        parent::init();
        $page = Page::getByAction();

        $images = ($page !== null && $page->album) ? $page->album->albumImages : [];

        Html::addCssClass($this->options, 'slide');

        if (count($images) > 1) {
            $this->controls = [
                '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><span class="sr-only">Previous</span>',
                '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span><span class="sr-only">Next</span>',
            ];
        } else {
            $this->controls = false;
        }

        if (empty($images)) {
            Html::addCssClass($this->options, 'empty');
        }

        foreach ($images as $image) {
            $this->items[] = [
                'content' => Html::img($image->imageUrl),
                //'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
                //'options' => [...],
            ];
        }

    }
}
