<?php

namespace app\widgets;

use app\models\LoginForm;
use yii\base\Widget;

class AuthWidget extends Widget {

    public function run() {
        $model = new LoginForm();
        return $this->render('auth_modal', [
                    'model' => $model
        ]);
    }

}
