<?php

namespace app\widgets;


use app\models\Language;
use Yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\bootstrap\Tabs;
use yii\widgets\ActiveField;

class Translation extends Widget
{
    /**
     * @var ActiveField
     */
    public $field;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->field === null || !$this->field instanceof ActiveField) {
            throw new InvalidConfigException();
        }
    }

    public function run()
    {
        $field = $this->field;

        if (!$field->model || ! $field->attribute) {
            throw new Exception('Not implemented!');
        }

        if ($field->model->isNewRecord) {
            return $field;
        }

        //var_dump($this->field);exit;
        /** @var Language $languages */
        $languages = Language::find()->all();
        $items = [];

        foreach ($languages as $language) {
            if ($language->code == Yii::$app->language) {
                $content = $field;
            } else {
                $t = \app\models\Translation::getModel($field->model, $field->attribute, $language->code);
                $inputOptions = $field->inputOptions;
                $inputOptions['value'] = $t->value;
                $content = $field->form->field($t, "value[$language->code][$field->attribute]")->textInput($inputOptions);
            }
            $items[] = [
                'label' => $language->name,
                'content' => $content,
                'active' => $language->code == Yii::$app->language,
            ];
        }
        
        return Tabs::widget([
            'items' => $items,
        ]);
    }
}
