<?php
namespace app\commands;

use app\models\User;
use app\rbac\RentOwnerRule;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $createRent = $auth->createPermission('createRent');
        $createRent->description = 'Сдать в аренду';
        $auth->add($createRent);

        $updateRent = $auth->createPermission('updateRent');
        $updateRent->description = 'Изменить аренду';
        $auth->add($updateRent);

        $landlord = $auth->createRole('landlord');
        $landlord->description = 'Арендодатель';

        $auth->add($landlord);
        $auth->addChild($landlord, $createRent);

        $renter = $auth->createRole('renter');
        $renter->description = 'Арендатор';
        $auth->add($renter);

        // add the rule
        $rule = new RentOwnerRule();
        $auth->add($rule);

        $updateOwnRent = $auth->createPermission('updateOwnRent');
        $updateOwnRent->description = 'Изменить свою аренду';
        $updateOwnRent->ruleName = $rule->name;
        $auth->add($updateOwnRent);

        $auth->addChild($updateOwnRent, $updateRent);
        $auth->addChild($landlord, $updateOwnRent);

        $tech = $auth->createRole('tech');
        $tech->description = 'Тех. поддержка';
        $auth->add($tech);

        $updateFAQ = $auth->createPermission('updateFAQ');
        $updateFAQ->description = 'Ответы на вопросы пользователей';
        $auth->add($updateFAQ);

        $auth->addChild($tech, $landlord);
        $auth->addChild($tech, $renter);
        $auth->addChild($tech, $updateRent);
        $auth->addChild($tech, $updateFAQ);

        $bookkeeper = $auth->createRole('bookkeeper');
        $bookkeeper->description = 'Бухгалтер';
        $auth->add($bookkeeper);

        $seo = $auth->createRole('seo');
        $seo->description = 'Сео';
        $auth->add($seo);

        $updatePage = $auth->createPermission('updatePage');
        $updatePage->description = 'Изменение страниц';
        $auth->add($updatePage);

        $editor = $auth->createRole('editor');
        $editor->description = 'Редактор';
        $auth->add($editor);

        $manager = $auth->createRole('manager');
        $manager->description = 'Менеджер';
        $auth->add($manager);

        $managingDirector = $auth->createRole('managingDirector');
        $managingDirector->description = 'Управляющий';
        $auth->add($managingDirector);

        // Admin
        $admin = $auth->createRole('admin');
        $admin->description = 'Администратор';
        $auth->add($admin);
    }


    public function actionRemoveAll()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
    }

    public function actionList($q)
    {
        $users = User::find()->where(['like', 'email', $q])->asArray()->all();

        foreach ($users as $user) {
            echo "ID[{$user['id']}]: {$user['email']} ({$user['email']})\n";
        }

    }

    public function actionAssign($id, $role = 'admin')
    {
        $auth = Yii::$app->authManager;

        $auth->assign($auth->getRole($role), $id);
    }
}
