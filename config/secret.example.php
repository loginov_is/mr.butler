<?php

return [
    'cookieValidationKey' => '',
    'db' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=localhost;dbname=DATABASE',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',

        //cache
        'enableSchemaCache' => true,
        'schemaCacheDuration' => 3600,
        'schemaCache' => 'cache',
        'enableQueryCache' => true,
        'queryCacheDuration' => 3600,
        'queryCache' => 'cache',
    ],
    'authClientCollection' => [
        'google' => [
            'clientId' => 'google_client_id',
            'clientSecret' => 'google_client_secret',
        ],
        'facebook' => [
            'clientId' => 'facebook_client_id',
            'clientSecret' => 'facebook_client_secret',
        ],
        'linkedin' => [
            'clientId' => 'linkedin_client_id',
            'clientSecret' => 'linkedin_client_secret',
        ],
        'live' => [
            'clientId' => 'live_client_id',
            'clientSecret' => 'live_client_secret',
        ],
        'twitter' => [
            'consumerKey' => 'twitter_consumer_key',
            'consumerSecret' => 'twitter_consumer_secret',
        ],
        'vkontakte' => [
            'clientId' => 'vkontakte_client_id',
            'clientSecret' => 'vkontakte_client_secret',
        ],
        'yandex' => [
            'clientId' => 'yandex_client_id',
            'clientSecret' => 'yandex_client_secret',
        ],
    ],
];
