<?php

$secret = require(__DIR__ . '/secret.php');
$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'mr.butler',
    'name' => 'mr. Butler',
    'language' => 'ru',
    'sourceLanguage' => 'ru',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'languageManager'],
    'components' => [
        'urlManager' => [
            'class' => 'app\components\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => true,
            'rules' => require(__DIR__ . '/url-rules.php'),
        ],
        'languageManager' => [
            'class' => 'app\components\LanguageManager',
        ],
        'i18n' => [
            'translations' => [
                'yii' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en',
                    'basePath' => '@yii/messages'
                ],
                'app*' => [
                    'class' => 'yii\i18n\GettextMessageSource',
                    //'forceTranslation' => true,
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'ru',
                    'useMoFile' => false,
                    'catalog' => 'messages',
                ],
                '*' => [
                    'class' => 'yii\i18n\GettextMessageSource',
                    //'forceTranslation' => true,
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'ru',
                    'useMoFile' => false,
                    'catalog' => 'messages',
                ],
            ],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                    'class' => 'yii\authclient\clients\GoogleOAuth',
                    'clientId' => $secret['authClientCollection']['google']['clientId'],
                    'clientSecret' => $secret['authClientCollection']['google']['clientSecret'],
                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => $secret['authClientCollection']['facebook']['clientId'],
                    'clientSecret' => $secret['authClientCollection']['facebook']['clientSecret'],
                ],
//                'linkedin' => [
//                    'class' => 'yii\authclient\clients\LinkedIn',
//                    'clientId' => $secret['authClientCollection']['linkedin']['clientId'],
//                    'clientSecret' => $secret['authClientCollection']['linkedin']['clientSecret'],
//                ],
//                'live' => [
//                    'class' => 'yii\authclient\clients\Live',
//                    'clientId' => $secret['authClientCollection']['live']['clientId'],
//                    'clientSecret' => $secret['authClientCollection']['live']['clientSecret'],
//                ],
//                'twitter' => [
//                    'class' => 'yii\authclient\clients\Twitter',
//                    'consumerKey' => $secret['authClientCollection']['twitter']['clientId'],
//                    'consumerSecret' => $secret['authClientCollection']['twitter']['clientSecret'],
//                ],
                'vkontakte' => [
                    'class' => 'yii\authclient\clients\VKontakte',
                    'clientId' => $secret['authClientCollection']['vkontakte']['clientId'],
                    'clientSecret' => $secret['authClientCollection']['vkontakte']['clientSecret'],
                    'scope' => 'email',
                ],
//                'yandex' => [
//                    'class' => 'yii\authclient\clients\YandexOAuth',
//                    'clientId' => $secret['authClientCollection']['vkontakte']['clientId'],
//                    'clientSecret' => $secret['authClientCollection']['vkontakte']['clientSecret'],
//                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'request' => [
            'cookieValidationKey' => $secret['cookieValidationKey'],
        ],
        'cache' => [
            //'class' => 'yii\caching\DummyCache',
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            //'useFileTransport' => true,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'vano2054',
                'password' => 'qwerty66',
                'port' => '465',
                'encryption' => 'SSL',
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'categories' => ['translation'],
                    'logFile' => '@runtime/logs/translation.log',
                    'logVars' => [],
                ],
            ],
        ],
        'db' => $secret['db'],
    ],
    'modules' => [
        'rbac' => [
            'class' => 'app\modules\rbac\Module',
            'layout' => '/admin',
            'userModel' => 'app\models\User',
            'filterModel' => 'app\models\UserSearch',
            'userNameColumn' => 'email',
            'allowedRoles' => ['admin'],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*']
    ];
}

return $config;
