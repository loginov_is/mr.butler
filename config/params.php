<?php

return [
    'adminEmail' => 'admin@example.com',
    'systemEmail' => 'system@example.com',
    'supportEmail' => 'support@example.com',
    'feedbackEmail' => 'feedback@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
