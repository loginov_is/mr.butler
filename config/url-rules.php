<?php

return [
    // language/route/to
    '<l:(\w{2}|\w{2}-\w{2})>' => 'site/index',
    '<l:(\w{2}|\w{2}-\w{2})>/<controller:(contact)>s' => '<controller>/view',
    '<l:(\w{2}|\w{2}-\w{2})>/<controller>/<action>' => '<controller>/<action>',

    // route/to
    ''=>'site/index',
    '<controller:(contact)>s' => '<controller>/view',
    '<controller>/<action>' => '<controller>/<action>',
];
