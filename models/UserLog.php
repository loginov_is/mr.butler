<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%user_log}}".
 *
 * @property integer $id
 * @property integer $created_by
 * @property integer $created_at
 * @property string $class_name
 * @property integer $model_id
 * @property integer $is_admin
 * @property integer $action_id
 */
class UserLog extends \yii\db\ActiveRecord {

    const ACTION_CREATE = 1;
    const ACTION_UPDATE = 2;
    const ACTION_DELETE = 3;
    const ACTION_VIEW = 4;
    const IS_ADMIN_FALSE = 2;
    const IS_ADMIN_TRUE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['created_by', 'created_at', 'class_name', 'model_id', 'action_id'], 'required'],
            [['created_by', 'created_at', 'model_id', 'is_admin', 'action_id'], 'integer'],
            [['class_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_by' => Yii::t('app', 'Пользователь'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'class_name' => Yii::t('app', 'Название модели'),
            'model_id' => Yii::t('app', 'ID модели'),
            'is_admin' => Yii::t('app', 'Админ'),
            'action_id' => Yii::t('app', 'Действие'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public static function setLog($className, $modelId, $actionId, $isAdmin) {
        $model = new UserLog();
        $model->created_by = Yii::$app->user->id;
        $model->created_at = time();
        $model->class_name = $className;
        $model->model_id = $modelId;
        $model->is_admin = $isAdmin;
        $model->action_id = $actionId;
        $model->save();
    }
    
    public static function getActionList($id = null) {
        $array = [
            self::ACTION_CREATE => \Yii::t('app', 'Создание записи'),
            self::ACTION_UPDATE => \Yii::t('app', 'Редактирование записи'),
            self::ACTION_DELETE => \Yii::t('app', 'Удаление записи'),
            self::ACTION_VIEW => \Yii::t('app', 'Просмотр записи')
        ];

        if (isset($id)) {
            if (isset($array[$id])) {
                return $array[$id];
            } else {
                return $id;
            }
        } else {
            return $array;
        }
    }
}
