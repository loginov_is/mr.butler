<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Accounting;

/**
 * BookingSearch represents the model behind the search form about `app\models\Booking`.
 */
class AccountingSearch extends Accounting {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['created_by', 'created_at', 'user_id', 'lookup_id', 'operation'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Accounting::find()->orderBy('created_at desc');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'user_id' => $this->user_id,
            'lookup_id' => $this->lookup_id,
            'operation' => $this->operation
        ]);

        $query->andFilterWhere(['like', 'amount', $this->amount])
                ->andFilterWhere(['like', 'created_at', $this->created_at]);

        return $dataProvider;
    }

}
