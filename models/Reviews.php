<?php

namespace app\models;

use Yii;
use app\helpers\DataHelper;
use app\models\RealEstate;

/**
 * This is the model class for table "{{%reviews}}".
 *
 * @property integer $id
 * @property integer $real_estate_id
 * @property integer $status_id
 * @property string $youtube_id
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $review
 * @property string $video_review
 * @property string $minuses
 * @property string $pluses
 * @property double $rating
 *
 * @property User $createdBy
 * @property RealEstate $realEstate
 */
class Reviews extends \yii\db\ActiveRecord {

    const RATING_DEFAULT = 5;
    const RATING_STEP = 1;
    const STATUS_PENDING = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DENIED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%reviews}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['real_estate_id', 'status_id', 'created_by', 'created_at', 'updated_at', 'review', 'rating'], 'required'],
            [['real_estate_id', 'status_id', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['review', 'minuses', 'pluses'], 'string'],
            [['rating'], 'number'],
            [['youtube_id', 'video_review'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['real_estate_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstate::className(), 'targetAttribute' => ['real_estate_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'real_estate_id' => Yii::t('app', 'Объект'),
            'status_id' => Yii::t('app', 'Статус'),
            'youtube_id' => Yii::t('app', 'Youtube ID'),
            'created_by' => Yii::t('app', 'Создатель'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата редактирования'),
            'review' => Yii::t('app', 'Отзыв'),
            'video_review' => Yii::t('app', 'Видеоотзыв'),
            'minuses' => Yii::t('app', 'Недостатки'),
            'pluses' => Yii::t('app', 'Преимущества'),
            'rating' => Yii::t('app', 'Оцените арендованную недвижимость'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstate() {
        return $this->hasOne(RealEstate::className(), ['id' => 'real_estate_id']);
    }

    public static function getUserReviews($userId, $asCount = false) {
        $model = self::find()->where(['created_by' => $userId])->orderBy('created_at desc')->all();
        return ($asCount) ? count($model) : $model;
    }

    public function beforeValidate() {
        if ($this->isNewRecord) {
            $this->created_at = $this->updated_at = time();
        } else {
            $this->updated_at = time();
        }

        if (!$this->status_id) {
            $this->status_id = self::STATUS_PENDING;
        }

        if (!$this->created_by) {
            $this->created_by = Yii::$app->user->id;
        }

        $this->review = htmlspecialchars($this->review);
        $this->pluses = htmlspecialchars($this->pluses);
        $this->minuses = htmlspecialchars($this->minuses);
        $this->youtube_id = DataHelper::getYoutubeId($this->video_review);
        return parent::beforeValidate();
    }

    public function afterSave($insert, $changedAttributes) {
        RealEstate::findOne($this->real_estate_id)->ratingRecalculate();
        return parent::afterSave($insert, $changedAttributes);
    }

    public static function getList($id = null) {
        $array = [
            self::STATUS_PENDING => \Yii::t('app', 'Запрос не подтвержден'),
            self::STATUS_ACTIVE => \Yii::t('app', 'Опубликован'),
            self::STATUS_DENIED => \Yii::t('app', 'Отказано'),
        ];

        if (isset($id)) {
            if (isset($array[$id])) {
                return $array[$id];
            } else {
                return \Yii::t('app', 'Неопределено');
            }
        } else {
            return $array;
        }
    }

}
