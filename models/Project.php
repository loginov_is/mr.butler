<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "{{%project}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $map
 * @property string $description
 * @property integer $city_id
 * @property integer $region_id
 *
 * @property RealEstate[] $realEstates
 */
class Project extends ActiveRecord {

    public $images;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%project}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['comfort_ids'], 'each', 'rule' => ['integer']],
            [['name'], 'unique'],
            [['name', 'created_at', 'updated_at', 'created_by', 'updated_by', 'map', 'city_id', 'region_id'], 'required'],
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'city_id', 'region_id'], 'integer'],
            [['name', 'map'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['images'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 4, 'minSize' => 10240, 'maxSize' => 10485760],
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'comfort_ids' => 'comfort',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название проекта'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'map' => Yii::t('app', 'Карта'),
            'description' => Yii::t('app', 'Описание'),
            'city_id' => Yii::t('app', 'Город'),
            'region_id' => Yii::t('app', 'Регион'),
            'comfort_ids'=> Yii::t('app', 'Инфраструктура'),
            'images' => Yii::t('app', 'Изображения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstates() {
        return $this->hasMany(RealEstate::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity() {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion() {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComfort() {
        return $this->hasMany(Lookup::className(), ['id' => 'lookup_id'])
                        ->viaTable('{{%project_to_lookup}}', ['project_id' => 'id']);
    }

    public function beforeValidate() {
        $this->created_at = $this->updated_at = time();
        $this->updated_by = Yii::$app->user->id;

        if ($this->isNewRecord) {
            $this->created_by = Yii::$app->user->id;
        }

        return parent::beforeValidate();
    }

    public static function listAll($keyField = 'id', $valueField = 'name', $asArray = true) {
        $query = static::find();
        if ($asArray) {
            $query->select([$keyField, $valueField])->asArray();
        }

        return ArrayHelper::map($query->all(), $keyField, $valueField);
    }

    public function getProjectPhotos() {
        return $this->hasMany(ProjectPhoto::className(), ['project_id' => 'id'])->orderBy("index");
    }

    public function getProjectSliderPhotos() {
        return $this->hasMany(ProjectPhoto::className(), ['project_id' => 'id'])->where(['head_slider' => 1])->orderBy("index");
    }

    public function upload() {
        if ($this->isNewRecord) {
            return false;
        }
        $baseDir = Yii::getAlias('@webroot/upload/project/' . $this->id);
        FileHelper::createDirectory($baseDir);
        $counter = 0;
        if (!$this->isNewRecord) {
            $counter = ProjectPhoto::find()->orderBy('index DESC')->one()->index + 1;
        }
        foreach ($this->images as $file) {
            $baseName = md5(time() . $file->baseName) . '.' . $file->extension;
            if (!$file->saveAs($baseDir . '/' . $baseName)) {
                return false;
            }
            $photo = new ProjectPhoto();
            $photo->src = Yii::getAlias('/upload/project/' . $this->id) . '/' . $baseName;
            $photo->project_id = $this->id;
            $photo->index = $counter;
            $photo->save();

            $counter++;
        }
        return true;
    }

}
