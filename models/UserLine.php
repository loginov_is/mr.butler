<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%user_line}}".
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $model_id
 * @property string $created_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $createdBy
 */
class UserLine extends \yii\db\ActiveRecord {

    const TYPE_ID_OBJECT_VIEWED = 1;
    const TYPE_ID_OBJECT_ADDED_TO_USERLINE = 2;
    const TYPE_ID_REGION_ADDED_TO_USERLINE = 3;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user_line}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['type_id', 'model_id', 'created_by', 'created_at', 'updated_at'], 'required'],
            [['type_id', 'model_id', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'model_id' => 'Model ID',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public static function saveViewedObject($id) {
        //Добавляем запись в строку пользователя о просмотренном объекте
        if (!Yii::$app->user->isGuest) {
            $userId = Yii::$app->user->id;
            $viewRecord = UserLine::findOne(['created_by' => $userId, 'type_id' => UserLine::TYPE_ID_OBJECT_VIEWED, 'model_id' => $id]);

            if (!$viewRecord) {
                $viewRecord = new UserLine();
                $viewRecord->type_id = UserLine::TYPE_ID_OBJECT_VIEWED;
                $viewRecord->model_id = $id;
                $viewRecord->created_by = $userId;
                $viewRecord->created_at = time();
            }

            $viewRecord->updated_at = time();
            $viewRecord->save();
        }
    }

}
