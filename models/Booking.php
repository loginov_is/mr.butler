<?php

namespace app\models;

use Yii;
use app\models\ServiceOrder;

/**
 * This is the model class for table "{{%booking}}".
 *
 * @property string $id
 * @property string $status
 * @property string $real_estate_id
 * @property string $source_type
 * @property integer $user_phone
 * @property integer $start_date
 * @property integer $end_date
 * @property string $rent_price
 * @property string $comment
 * @property integer $created_at
 * @property string $created_by
 *
 * @property User $createdBy
 */
class Booking extends \yii\db\ActiveRecord
{
    const STATUS_NOT_PAYED = 'Не оплачено';
    const STATUS_PREPAYED = 'Внесена предоплата';
    const STATUS_PAYED = 'Оплачено';
    const STATUS_FINISHED = 'Выполнено';
    const STATUS_CANCELED = 'Отмена бронирования';
    const STATUS_BOOKED = 'Забронировано';

    const SOURCE_TYPE_SITE = 'Сайт';
    const SOURCE_TYPE_EXTERNAL_BOOKING = 'Внешние системы бронирования';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%booking}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'real_estate_id', 'source_type', 'user_phone', 'start_date', 'end_date', 'rent_price', 'comment', 'created_at', 'created_by'], 'safe'],
            [['id', 'real_estate_id', 'rent_price', 'created_by'], 'integer'],
            [['comment'], 'string'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер бронирования'),
            'status' => Yii::t('app', 'Статус бронирования'),
            'real_estate_id' => Yii::t('app', 'Объект'),
            'source_type' => Yii::t('app', 'Источник бронирования'),
            'user_phone' => Yii::t('app', 'Телефон клиента'),
            'start_date' => Yii::t('app', 'Дата заезда'),
            'end_date' => Yii::t('app', 'Дата отъезда'),
            'rent_price' => Yii::t('app', 'Стоимость аренды'),
            'comment' => Yii::t('app', 'Комментарий'),
            'created_at' => Yii::t('app', 'Дата бронирования'),
            'created_by' => Yii::t('app', 'Клиент'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getRealEstate() {
        return $this->hasOne(RealEstate::className(), ['id' => 'real_estate_id']);
    }

    public function getServiceOrders() {
        return $this->hasMany(ServiceOrder::className(), ['booking_id' => 'id']);
    }

    public function getTotalPrice() {
        $totalPrice = $this->rent_price;
        $serviceOrders = $this->getServiceOrders()->all();

        foreach($serviceOrders as $serviceOrder) {
            $totalPrice += $serviceOrder->price;
        }

        return $totalPrice;
    }
}
