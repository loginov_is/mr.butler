<?php

namespace app\models;

use Yii;

class Country extends \yii\db\ActiveRecord {

    public static function tableName() {
        return '{{%country}}';
    }

    public function rules() {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => \Yii::t('app', 'Название')
        ];
    }

    public function getCities() {
        return $this->hasMany(City::className(), ['country_id' => 'id']);
    }

}
