<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Feedback;

class FeedbackSearch extends Feedback {

    public function rules() {
        return [
            [['created_at', 'user_id', 'is_viewed'], 'integer'],
            [['email', 'name', 'phone'], 'safe']
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = Feedback::find()->orderBy('is_viewed');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'user_id', $this->user_id])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['like', 'phone', $this->phone])
                ->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'is_viewed', $this->is_viewed]);

        return $dataProvider;
    }

}
