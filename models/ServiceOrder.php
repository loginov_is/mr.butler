<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%service_order}}".
 *
 * @property integer $id
 * @property string $booking_id
 * @property integer $service_id
 * @property integer $people_count
 * @property integer $price
 *
 * @property Booking $booking
 * @property Service $service
 */
class ServiceOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%service_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['booking_id', 'service_id', 'people_count', 'price'], 'required'],
            [['booking_id', 'service_id', 'people_count', 'price'], 'integer'],
            [['booking_id'], 'exist', 'skipOnError' => true, 'targetClass' => Booking::className(), 'targetAttribute' => ['booking_id' => 'id']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'booking_id' => 'Booking ID',
            'service_id' => 'Service ID',
            'people_count' => 'People Count',
            'price' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooking()
    {
        return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    public function getServiceName() {
        return $this->getService()->one()->name;
    }
}
