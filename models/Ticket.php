<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%ticket}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $message
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $viewed_admin
 *
 * @property User $createdBy
 * @property TicketComment[] $ticketComments
 */
class Ticket extends \yii\db\ActiveRecord {

    const VIEWED_ADMIN_FALSE = 0;
    const VIEWED_ADMIN_TRUE = 1;
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%ticket}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'message', 'created_by'], 'required'],
            [['message'], 'string'],
            [['created_by', 'created_at', 'viewed_admin'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Тема сообщения'),
            'message' => Yii::t('app', 'Сообщение'),
            'created_by' => Yii::t('app', 'Создатель'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'viewed_admin' => Yii::t('app', 'Просмотрено администратором'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketComments() {
        return $this->hasMany(TicketComment::className(), ['ticket_id' => 'id']);
    }

    public function beforeValidate() {
        if ($this->isNewRecord) {
            $this->created_at = time();
            $this->created_by = Yii::$app->user->id;
        }
        return parent::beforeValidate();
    }
    
    public static function getAdminViewedList($id = null) {
        $array = [
            self::VIEWED_ADMIN_FALSE => \Yii::t('app', 'Не просмотрено'),
            self::VIEWED_ADMIN_TRUE => \Yii::t('app', 'Просмотрено'),
        ];

        if (isset($id)) {
            if (isset($array[$id])) {
                return $array[$id];
            } else {
                return \Yii::t('app', 'Неопределено');
            }
        } else {
            return $array;
        }
    }

}
