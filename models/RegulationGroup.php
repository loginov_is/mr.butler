<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%regulation_group}}".
 *
 * @property integer $id
 * @property integer $language_id
 * @property string $title
 * @property integer $sort_order
 *
 * @property Regulation[] $regulations
 * @property Language $language
 */
class RegulationGroup extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%regulation_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id', 'title'], 'required'],
            [['language_id', 'sort_order'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'language_id' => Yii::t('app', 'Язык'),
            'title' => Yii::t('app', 'Заголовок'),
            'sort_order' => Yii::t('app', 'Сортировка'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegulations()
    {
        return $this->hasMany(Regulation::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    public static function getItems($active_id = false)
    {
        /** @var RegulationGroup[] $models */
        $models = static::find()->where(['language_id' => Yii::$app->getLanguageInfo()->id])->all();
        $items = [];

        foreach ($models as $model) {
            $regulations = $model->regulations;
            $class = in_array($active_id, ArrayHelper::map($regulations, 'id', 'id')) ? 'in' : '';
            $items[] = [
                'label' => $model->title,
                'content' => ArrayHelper::map($regulations, 'id', function ($regulation) {
                    return Html::a($regulation->title, ['regulation/view', 'id' => $regulation->id]);
                }),
                'contentOptions' => ['class' => $class],
            ];
        }

        return $items;
    }
}
