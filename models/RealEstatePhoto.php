<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "real_estate_photo".
 *
 * @property integer $id
 * @property integer $real_estate_id
 * @property string $src
 * @property integer $index
 * @property integer $main
 *
 * @property RealEstate $realEstate
 */
class RealEstatePhoto extends ActiveRecord {

    public static function tableName() {
        return '{{%real_estate_photo}}';
    }

    public function rules() {
        return [
            [['real_estate_id', 'src'], 'required'],
            [['real_estate_id'], 'integer'],
            [['src'], 'string', 'max' => 255],
            [['real_estate_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstate::className(), 'targetAttribute' => ['real_estate_id' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'real_estate_id' => Yii::t('app', 'ID недвижимости'),
            'src' => Yii::t('app', 'Путь до изображения'),
        ];
    }

    public function getUrl() {
        return Yii::getAlias("@web/{$this->src}");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstate() {
        return $this->hasOne(RealEstate::className(), ['id' => 'real_estate_id']);
    }

    public function unsetImage($path) {
        if (file_exists($path)) {
            unlink($path);
        }
    }

}
