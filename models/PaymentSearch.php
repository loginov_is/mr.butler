<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Payment;

class PaymentSearch extends Payment {

    public function rules() {
        return [
            [['created_by', 'created_at', 'updated_at', 'status_id', 'payment_system_id'], 'integer'],
            [['amount'], 'number'],
            [['requisites'], 'string'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = Payment::find()->orderBy('created_at desc');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
//
//        if (!$this->validate()) {
//            return $dataProvider;
//        }
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at
        ]);

        $query->andFilterWhere(['like', 'amount', $this->amount]);
        $query->andFilterWhere(['like', 'created_by', $this->created_by]);
        $query->andFilterWhere(['like', 'status_id', $this->status_id]);
        $query->andFilterWhere(['like', 'payment_system_id', $this->payment_system_id]);
        return $dataProvider;
    }

}
