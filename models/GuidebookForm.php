<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * GuidebookForm.
 */
class GuidebookForm extends Model
{
    public $cityId;
    public $regionId;
    public $housingType;
    public $quota;
    public $price;
    public $priceMin;
    public $priceMax;
    public $priceType;
    public $sights;
    public $sameRegions;
    public $residents;
    
    const MIN_FORM_PRICE = 50;
    const MAX_FORM_PRICE = 100000;
    const FORM_PRICE_STEP = 50;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['cityId', 'regionId', 'housingType', 'count', 'price', 'priceMin', 'priceMax', 'quota', 'priceType', 'sights', 'sameRegions', 'residents'], 'safe'],
            [['cityId', 'regionId', 'housingType', 'count', 'priceMin', 'priceMax'], 'required'],
            [['cityId', 'regionId', 'housingType', 'count', 'priceMin', 'priceMax'], 'integer'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'cityId' => Yii::t('app', 'Город'),
            'regionId' => Yii::t('app', 'Район'),
            'housingType' => Yii::t('app', 'Тип жилья'),
            'quota' => Yii::t('app', 'Квота'),
            'residents' => Yii::t('app', 'Количество жильцов'),
            'priceMin' => Yii::t('app', 'Минимальная цена'),
            'priceMax' => Yii::t('app', 'Максимальная цена'),
            'priceType' => Yii::t('app', 'Тип оплаты'),
            'sights' => Yii::t('app', 'Достопримечательности'),
            'sameRegions' => Yii::t('app', 'Похожие регионы'),
            'price' => Yii::t('app', 'Цена'),
        ];
    }
}
