<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Reviews;

class ReviewsSearch extends Reviews {

    public function rules() {
        return [
            [['real_estate_id', 'status_id', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['review', 'minuses', 'pluses'], 'string'],
            [['rating'], 'number'],
            [['youtube_id', 'video_review'], 'string', 'max' => 255],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = Reviews::find()->orderBy('status_id, created_at desc');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
//
//        if (!$this->validate()) {
//            return $dataProvider;
//        }
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'real_estate_id' => $this->real_estate_id,
            'rating' => $this->rating,
            'status_id' => $this->status_id
        ]);

        return $dataProvider;
    }

}
