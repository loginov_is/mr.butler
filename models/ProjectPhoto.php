<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%project_photo}}".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $src
 * @property integer $index
 * @property integer $main
 * @property integer $head_slider
 */
class ProjectPhoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_photo}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'src'], 'required'],
            [['project_id', 'index', 'main', 'head_slider'], 'integer'],
            [['src'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'src' => 'Src',
            'index' => 'Index',
            'main' => 'Main',
            'head_slider' => 'Head Slider',
        ];
    }

    public function unsetImage($path) {
        if (file_exists($path)) {
            unlink($path);
        }
    }
}
