<?php

namespace app\models;

use Yii;

class FeedbackEmail extends \yii\db\ActiveRecord {
    
    const TYPE_FEEDBACK = 1;
    const TYPE_TICKET = 2;

    public static function tableName() {
        return '{{%feedback_email}}';
    }

    public function rules() {
        return [
            [['email', 'type'], 'required'],
            [['type'], 'integer'],
            [['email', 'type'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'type' => Yii::t('app', 'Тип'),
        ];
    }

}
