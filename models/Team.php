<?php

namespace app\models;

use yii\helpers\FileHelper;
use Yii;

/**
 * This is the model class for table "{{%team}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $image
 * @property string $phone
 * @property string $email
 * @property string $position
 */
class Team extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $uploadedFile;

    public static function tableName() {
        return '{{%team}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'image', 'phone', 'email', 'position'], 'required'],
            [['name', 'phone', 'email', 'position'], 'string', 'max' => 50],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Имя'),
            'image' => Yii::t('app', 'Изображение'),
            'phone' => Yii::t('app', 'Телефон'),
            'email' => Yii::t('app', 'Email'),
            'position' => Yii::t('app', 'Должность'),
        ];
    }

    public function beforeSave($insert) {
        $baseDir = Yii::getAlias('@webroot/upload/team');
        FileHelper::createDirectory($baseDir);
        return parent::beforeSave($insert);
    }

}
