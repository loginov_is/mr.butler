<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class CitySearch extends City {

    public function rules() {
        return [
            [['country_id'], 'integer'],
            [['name', 'country_id'], 'safe']
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = City::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'country_id', $this->country_id]);

        return $dataProvider;
    }

}
