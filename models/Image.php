<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class Image
 *
 * @property string $imageUrl
 */
class Image extends ActiveRecord
{

    public $uploadedFile;
    public $albums;

    public static function tableName()
    {
        return '{{%image}}';
    }

    public function rules()
    {
        return [
            [['image'], 'required'],
            [['image'], 'string', 'max' => 255],
            [['uploadedFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => Yii::t('app', 'Изображение'),
            'order' => 'Order',
            'albums' => Yii::t('app', 'Альбомы'),
        ];
    }

    public function getImageToAlbums()
    {
        return $this->hasMany(ImageToAlbum::className(), ['image_id' => 'id']);
    }

    public function getImageUrl()
    {
        return Yii::getAlias('@web') . '/upload/' . $this->image;
    }

    public function getImageAlbums()
    {
        $albumsArray = [];
        $imageRelations = $this->getImageToAlbums()->asArray()->all();

        if ($imageRelations) {
            foreach ($imageRelations as $imageRelation) {
                $albumsArray[] = ImageAlbum::findOne($imageRelation['album_id']);
            }
        }

        return $albumsArray;
    }
}
