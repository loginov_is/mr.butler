<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%regulation}}".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $title
 * @property string $content
 * @property integer $sort_order
 *
 * @property RegulationGroup $group
 */
class Regulation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%regulation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'title', 'content'], 'required'],
            [['group_id', 'sort_order'], 'integer'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => RegulationGroup::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'group_id' => Yii::t('app', 'Группа'),
            'title' => Yii::t('app', 'Заголовок'),
            'content' => Yii::t('app', 'Контент'),
            'sort_order' => Yii::t('app', 'Сортировка'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(RegulationGroup::className(), ['id' => 'group_id']);
    }
}
