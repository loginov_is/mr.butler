<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "{{%press_article}}".
 *
 * @property integer $id
 * @property integer $press_id
 * @property string $title
 * @property string $url
 * @property integer publish_date
 * @property integer created_at
 * @property integer updated_at
 *
 * @property Press $press
 */
class PressArticle extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $dateField;

    public static function tableName() {
        return '{{%press_article}}';
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['press_id', 'title', 'url', 'dateField'], 'required'],
            [['press_id', 'publish_date', 'created_at', 'updated_at'], 'integer'],
            ['dateField', 'date', 'format' => 'php:d/m/Y', 'timestampAttribute' => 'publish_date'],
            [['title', 'url'], 'string', 'max' => 255],
            [['press_id'], 'exist', 'skipOnError' => true, 'targetClass' => Press::className(), 'targetAttribute' => ['press_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'press_id' => Yii::t('app', 'Пресса'),
            'title' => Yii::t('app', 'Заголовок'),
            'url' => Yii::t('app', 'Ссылка'),
            'publish_date' => Yii::t('app', 'Дата публикации'),
            'dateField' => Yii::t('app', 'Дата публикации'),
        ];
    }

    public function afterFind() {
        $this->dateField = date('d/m/Y', $this->publish_date);
        parent::afterFind();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPress() {
        return $this->hasOne(Press::className(), ['id' => 'press_id']);
    }

}
