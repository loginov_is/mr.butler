<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class ImageAlbum
 *
 * @property Image[] $albumImages
 */
class ImageAlbum extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%image_album}}';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Название альбома'),
            'imagesCount' => Yii::t('app', 'Кол-во изображений в альбоме'),
        ];
    }

    public function getImageToAlbums()
    {
        return $this->hasMany(ImageToAlbum::className(), ['album_id' => 'id']);
    }

    public function getAlbumImages()
    {
        return $this->hasMany(Image::className(), ['id' => 'image_id'])->via('imageToAlbums');
    }
}
