<?php

namespace app\models;

class City extends \yii\db\ActiveRecord {

    public static function tableName() {
        return '{{%city}}';
    }

    public function rules() {
        return [
            [['country_id', 'name'], 'required'],
            [['country_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'country_id' => \Yii::t('app', 'Страна'),
            'name' => \Yii::t('app', 'Название')
        ];
    }

    public function getCountry() {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    public function getRegions() {
        return $this->hasMany(Region::className(), ['city_id' => 'id']);
    }

}
