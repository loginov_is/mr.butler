<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "real_estate_to_lookup".
 *
 * @property integer $real_estate_id
 * @property integer $lookup_id
 *
 * @property Lookup $lookup
 * @property RealEstate $realEstate
 */
class RealEstateToLookup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'real_estate_to_lookup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['real_estate_id', 'lookup_id'], 'required'],
            [['real_estate_id', 'lookup_id'], 'integer'],
            [['lookup_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lookup::className(), 'targetAttribute' => ['lookup_id' => 'id']],
            [['real_estate_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstate::className(), 'targetAttribute' => ['real_estate_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'real_estate_id' => 'Real Estate ID',
            'lookup_id' => 'Lookup ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLookup()
    {
        return $this->hasOne(Lookup::className(), ['id' => 'lookup_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstate()
    {
        return $this->hasOne(RealEstate::className(), ['id' => 'real_estate_id']);
    }
}
