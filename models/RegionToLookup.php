<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%region_to_lookup}}".
 *
 * @property integer $region_id
 * @property integer $lookup_id
 */
class RegionToLookup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%region_to_lookup}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region_id', 'lookup_id'], 'required'],
            [['region_id', 'lookup_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'region_id' => 'Region ID',
            'lookup_id' => 'Lookup ID',
        ];
    }
}
