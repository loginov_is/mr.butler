<?php

namespace app\models;

use Yii;

class FaqGroup extends \yii\db\ActiveRecord {

    public static function tableName() {
        return '{{%faq_group}}';
    }

    public function rules() {
        return [
            [['name', 'language_id'], 'required'],
            [['language_id', 'active'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'language_id' => Yii::t('app', 'Язык'),
            'active' => Yii::t('app', 'Активность'),
        ];
    }

    public function getFaqs() {
        return $this->hasMany(Faq::className(), ['faq_group_id' => 'id']);
    }

    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

}
