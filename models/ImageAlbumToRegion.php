<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class ImageAlbumToRegion extends ActiveRecord
{
    const TYPE_ID_PHOTOALBUMS = 1;
    const TYPE_ID_SIGHT = 2;

    public static function tableName()
    {
        return '{{%image_album_to_region}}';
    }

    public function rules()
    {
        return [
            [['region_id', 'album_id'], 'required'],
            [['region_id', 'album_id', 'type_id'], 'integer'],
            [
                ['album_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ImageAlbum::className(),
                'targetAttribute' => ['album_id' => 'id']
            ],
            [
                ['region_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Region::className(),
                'targetAttribute' => ['region_id' => 'id']
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'region_id' => Yii::t('app', 'Region ID'),
            'album_id' => Yii::t('app', 'Album ID'),
            'type_id' => Yii::t('app', 'Type ID'),
        ];
    }

    public function getAlbum()
    {
        return $this->hasOne(ImageAlbum::className(), ['id' => 'album_id']);
    }

    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    public function beforeSave($insert)
    {
        $isset = self::findOne([
            'region_id' => $this->region_id,
            'album_id' => $this->album_id,
            'type_id' => $this->type_id
        ]);

        if ($isset) {
            return false;
        }

        return parent::beforeSave($insert);
    }
}
