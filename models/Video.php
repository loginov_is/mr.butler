<?php

namespace app\models;

use Yii;
use app\helpers\DataHelper;

class Video extends \yii\db\ActiveRecord {

    public static function tableName() {
        return '{{%video}}';
    }

    public function rules() {
        return [
            [['url', 'name', 'youtube_id'], 'required'],
            [['url', 'name', 'youtube_id'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'Ссылка на видео (youtube)'),
            'name' => Yii::t('app', 'Название видео (введите понятное название видео, чтобы его потом легко было найти в списке)'),
            'youtube_id' => Yii::t('app', 'ID ссылки на youtube)')
        ];
    }

    public function getVideoToRegions() {
        return $this->hasMany(VideoToRegion::className(), ['video_id' => 'id']);
    }

    public function beforeValidate() {
        $this->youtube_id = DataHelper::getYoutubeId($this->url);
        return parent::beforeValidate();
    }

    public function getYoutubeFullUrl($youtubeId) {
        return "https://www.youtube.com/embed/$youtubeId";
    }

}
