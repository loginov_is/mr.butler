<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%translation}}".
 *
 * @property integer $id
 * @property integer $language_id
 * @property string $model
 * @property integer $model_id
 * @property string $column
 * @property string $value
 * @property integer $need_translate
 *
 * @property Language $language
 */
class Translation extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['need_translate'], 'default', 'value' => true],
            [['language_id', 'model', 'column'], 'required'],
            [['language_id', 'model_id', 'need_translate'], 'integer'],
            [['model'], 'string', 'max' => 255],
            [['column'], 'string', 'max' => 100],
            [['value'], 'string'],
            [['language_id', 'model', 'model_id', 'column'], 'unique', 'targetAttribute' => ['language_id', 'model', 'model_id', 'column'], 'message' => 'The combination of Language ID, Model, Model ID and Column has already been taken.'],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'language_id' => Yii::t('app', 'Язык'),
            'model' => Yii::t('app', 'Модель'),
            'model_id' => Yii::t('app', 'Запись'),
            'column' => Yii::t('app', 'Колонка'),
            'value' => Yii::t('app', 'Перевод'),
            'need_translate' => Yii::t('app', 'Требуется перевод'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @param ActiveRecord $model
     * @param string $column Column name
     * @param null|string $language Language code
     * @return null|static
     */
    public static function getModel($model, $column, $language) {
        /** @var static $t */
        $t = static::find()
                ->joinWith('language', true, 'RIGHT JOIN')
                ->where(['model' => $model::className(), 'model_id' => $model->primaryKey, 'column' => $column, '{{%language}}.code' => $language])
                ->one();

        //@todo Оставить && !empty($model->$column) ???
        if ($t === null) {
            $columnName = $model->getAttributeLabel($column);
            Yii::warning("Перевод не найден [{$language}] '{$model::className()}::{$column}' ($columnName)", 'translation');

            try {
                $lang = Language::find()->where(['code' => $language])->one();
                $m = (new static([
                    'language_id' => $lang->primaryKey,
                    'model' => $model::className(),
                    'model_id' => $model->primaryKey,
                    'column' => $column,
                    'value' => $model->$column,
                ]));
                if (!$m->save()) {
                    return null;
                }
                $t = $m;
            } catch (\Exception $e) {
                Yii::error($e->getMessage(), 'translation');
                return null;
            }
        }

        return $t;
    }

    /**
     * @param ActiveRecord $model
     * @param string $column Column name
     * @param null|string $language Language code
     * @return mixed
     */
    public static function t($model, $column, $language = null) {
        if ($language === null) {
            $language = Yii::$app->language;
        }

        $result = static::getDb()->cache(function ($db) use ($model, $column, $language) {
            $t = self::getModel($model, $column, $language);

            if ($t === null || $t->need_translate) {
                return $model->$column;
            }

            return $t->value;
        });

        return $result;
    }

    public static function loadTranslations($model) {
        $translations = ArrayHelper::getValue(Yii::$app->request->post(), 'Translation.value');
        //var_dump($translations);exit;
        if ($translations === null) {
            return;
        }

        foreach ($translations as $language => $attributes) {
            foreach ($attributes as $attribute => $value) {
                $t = static::getModel($model, $attribute, $language);
                $t->setAttributes([
                    'need_translate' => (int) empty($value),
                    'value' => $value,
                ]);
                if (!$t->save()) {
                    var_dump($t->errors);
                    exit;
                }
            }
        }
    }

}
