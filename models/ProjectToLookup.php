<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%project_to_lookup}}".
 *
 * @property integer $project_id
 * @property integer $lookup_id
 *
 * @property Lookup $lookup
 * @property Project $project
 */
class ProjectToLookup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_to_lookup}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'lookup_id'], 'required'],
            [['project_id', 'lookup_id'], 'integer'],
            [['lookup_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lookup::className(), 'targetAttribute' => ['lookup_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'project_id' => 'Project ID',
            'lookup_id' => 'Lookup ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLookup()
    {
        return $this->hasOne(Lookup::className(), ['id' => 'lookup_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
}
