<?php

namespace app\models;

use Yii;

class Feedback extends \yii\db\ActiveRecord {

    const IS_VIEWED_FALSE = 0;
    const IS_VIEWED_TRUE = 1;

    public static function tableName() {
        return '{{%feedback}}';
    }

    public function rules() {
        return [
            [['created_at', 'user_id', 'is_viewed'], 'integer'],
            [['email', 'phone', 'name'], 'required'],
            [['email', 'phone', 'name'], 'string', 'max' => 50],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Телефон'),
            'name' => Yii::t('app', 'Имя'),
            'is_viewed' => Yii::t('app', 'Просмотрено'),
        ];
    }
    
    public static function getViewedStatuses() {
                return [
            self::IS_VIEWED_FALSE => Yii::t('app', 'Не просмотрено'),
            self::IS_VIEWED_TRUE => Yii::t('app', 'Просмотрено'),
        ];
    }

}
