<?php

namespace app\models;

use Yii;

class VideoToRealEstate extends \yii\db\ActiveRecord {

    public static function tableName() {
        return '{{%video_to_real_estate}}';
    }

    public function rules() {
        return [
            [['real_estate_id', 'video_id'], 'required'],
            [['real_estate_id', 'video_id'], 'integer'],
            [['video_id'], 'exist', 'skipOnError' => true, 'targetClass' => Video::className(), 'targetAttribute' => ['video_id' => 'id']],
            [['real_estate_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealEstate::className(), 'targetAttribute' => ['real_estate_id' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'real_estate_id' => Yii::t('app', 'Real estate ID'),
            'video_id' => Yii::t('app', 'Video ID'),
        ];
    }

    public function getVideo() {
        return $this->hasOne(Video::className(), ['id' => 'video_id']);
    }

    public function getRealEstate() {
        return $this->hasOne(RealEstate::className(), ['id' => 'real_estate_id']);
    }

    public function beforeSave($insert) {
        $isset = self::findOne(['real_estate_id' => $this->real_estate_id, 'video_id' => $this->video_id]);

        if ($isset) {
            return false;
        }

        return parent::beforeSave($insert);
    }

}
