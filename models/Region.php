<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class Region
 * @package app\models
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $name
 * @property string $description
 * @property string $map
 * @property string $avatar
 */
class Region extends ActiveRecord {

    public static function tableName() {
        return '{{%region}}';
    }

    public function rules() {
        return [
            [['comfort_ids'], 'each', 'rule' => ['integer']],
            [['id', 'city_id', 'name', 'description', 'map', 'avatar'], 'safe'],
            [['city_id', 'name', 'map'], 'required'],
            [['city_id'], 'integer'],
            [['name', 'map'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['avatar'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif'],
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'comfort_ids' => 'comfort',
                ],
            ],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'city_id' => \Yii::t('app', 'Город'),
            'name' => \Yii::t('app', 'Название'),
            'description' => \Yii::t('app', 'Описание района'),
            'map' => \Yii::t('app', 'Карта'),
            'avatar' => \Yii::t('app', 'Аватар'),
            'comfort_ids'=> Yii::t('app', 'Удобства'),
        ];
    }

    public function upload() {
        if ($this->validate()) {
            if($this->avatar) {
                $this->avatar->saveAs(Yii::getAlias('@webroot/upload/' . $this->avatar->baseName . '.' . $this->avatar->extension));
            }
            return true;
        } else {
            return false;
        }
    }

    public function getImageAlbumToRegions() {
        return $this->hasMany(ImageAlbumToRegion::className(), ['region_id' => 'id']);
    }

    public function getCity() {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function getVideoToRegions() {
        return $this->hasMany(VideoToRegion::className(), ['region_id' => 'id']);
    }

    public function getVideos() {
        return $this->hasMany(Video::className(), ['id' => 'video_id'])->via('videoToRegions');
    }

    public function getRegionToRegion() {
        return $this->hasMany(RegionToRegion::className(), ['parent_id' => 'id']);
    }

    public function getChildren() {
        return $this->hasMany(Region::className(), ['id' => 'child_id'])->via('regionToRegion');
    }

    public function getImageAlbums($typeId) {
        $data = [];
        $albums = $this->getImageAlbumToRegions()->asArray()->all();

        if ($albums) {
            foreach ($albums as $album) {
                if ($album['type_id'] == $typeId) {
                    $data[] = ImageAlbum::findOne($album['album_id']);
                }
            }
        }

        return $data;
    }

    public function getUrl() {
        return ['/guidebook/region', 'id' => $this->id];
    }

    public function getComfort() {
        return $this->hasMany(Lookup::className(), ['id' => 'lookup_id'])
                        ->viaTable('{{%region_to_lookup}}', ['region_id' => 'id']);
    }

    public function getImages($albumTypeId) {
        $images = [];
        $albums = $this->getImageAlbums($albumTypeId);
        foreach ($albums as $album) {
            $albumImages = $album->getAlbumImages()->all();
            foreach ($albumImages as $image) {
                $images[] = $image;
            }
        }
        return $images;
    }

    public function getRealEstates() {
        return $this->hasMany(RealEstate::className(), ['project_id' => 'id'])->viaTable(Project::tableName(), ['region_id' => 'id']);
    }

}
