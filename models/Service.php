<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%service}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ServicePrice[] $servicePrices
 */
class Service extends \yii\db\ActiveRecord
{
    public $pricePerMan;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'price_type_id', 'pricePerMan'], 'safe'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер услуги'),
            'name' => Yii::t('app', 'Название услуги'),
            'price_type_id' => Yii::t('app', 'Расчет стоимости'),
            'pricePerMan' => Yii::t('app', 'Стоимость'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicePrices()
    {
        return $this->hasMany(ServicePrice::className(), ['service_id' => 'id']);
    }

    /**
     * Возвращает стоимость услуги в пересчет на количество человек
     *
     */
    public function getServicePrice($peopleCount = 1) {
        if($this->price_type_id == 0) {
            $price = $this->hasOne(ServicePrice::className(), ['service_id' => 'id'])
                ->andWhere(['<=', 'people_min', $peopleCount])
                ->andWhere(['>=', 'people_max', $peopleCount])
                ->one()
                ->price;
            return $price;
        }
        if($this->price_type_id == 1) {
            $price = $this->hasOne(ServicePrice::className(), ['service_id' => 'id'])
                ->andWhere(['price_type_id' => 1])
                ->one()
                ->price;
            return $price * $peopleCount;
        }
    }
}
