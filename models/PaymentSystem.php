<?php

namespace app\models;

use yii\helpers\FileHelper;
use Yii;

/**
 * This is the model class for table "{{%payment_system}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * $property integer $language_id
 *
 * @property PaymentSystemField[] $paymentSystemFields
 */
class PaymentSystem extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $uploadedFile;
    public $fields;
    
    public static function tableName() {
        return '{{%payment_system}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'image', 'language_id'], 'required'],
            [['language_id'], 'integer'],
            [['title'], 'string', 'max' => 50],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Название'),
            'image' => Yii::t('app', 'Изображение'),
            'language_id' => Yii::t('app', 'Язык'),
            'fields' => Yii::t('app', 'Поля'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentSystemFields() {
        return $this->hasMany(PaymentSystemField::className(), ['payment_system_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    public function beforeSave($insert) {
        $baseDir = Yii::getAlias('@webroot/upload/payment_system');
        FileHelper::createDirectory($baseDir);
        return parent::beforeSave($insert);
    }

}
