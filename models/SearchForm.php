<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Lookup;
use app\models\Region;

/**
 * SearchForm is the model behind the contact form.
 */
class SearchForm extends Model {

    public $cityId;
    public $dateFrom;
    public $dateTo;
    public $guestsCount;
    public $typeId;
    public $priceType;
    public $priceFrom;
    public $priceTo;
    public $bedrooms;
    public $bathrooms;
    public $bedroomItems;
    public $appartamentsType;
    public $facilities;
    public $regions;
    public $price;
    public $showOptions;
    public $sortBy;

    const SORT_BY_RATING = 1;
    const SORT_BY_PRICE = 2;
    const SORT_BY_DISTANCE_TO_SEA = 3;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            [['facilities', 'appartamentsType', 'typeId', 'dateFrom', 'dateTo', 'regions', 'bedrooms', 'bathrooms', 'bedroomItems', 'price', 'sortBy'], 'safe'],
            [['cityId', 'dateFrom', 'dateTo', 'guestsCount', 'typeId', 'priceType', 'priceFrom', 'priceTo'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        return [
            'cityId' => Yii::t('app', 'Город'),
            'dateFrom' => Yii::t('app', 'Заезд'),
            'dateTo' => Yii::t('app', 'Отъезд'),
            'typeId' => Yii::t('app', 'Тип размещения'),
            'guestsCount' => Yii::t('app', 'Число гостей'),
            'priceType' => Yii::t('app', 'Диапазон цен'),
            'priceFrom' => Yii::t('app', 'Цена от'),
            'priceTo' => Yii::t('app', 'Цена до'),
            'bedrooms' => Yii::t('app', 'Кол-во спален'),
            'bathrooms' => Yii::t('app', 'Кол-во ванных комнат'),
            'bedroomItems' => Yii::t('app', 'Кол-во спальных мест'),
            'appartamentsType' => Yii::t('app', 'Тип жилья'),
            'facilities' => Yii::t('app', 'Удобства'),
            'regions' => Yii::t('app', 'Районы'),
            'price' => Yii::t('app', 'Цена')
        ];
    }

    /**
     * @return array active comfort acheckboxes
     */
    public function getComfortCheckboxData($checked = []) {
        $items = Lookup::items(Lookup::TYPE_HOUSING_COMFORT, true);
        $checkboxes = [];

        foreach ($items as $key => $item) {
            $checkboxes[$key] = [
                'title' => $item,
                'checked' => false
            ];

            if($checked) {
                foreach ($checked as $key => $check) {
                    if ($check) {
                        $checkboxes[$key]['checked'] = true;
                    }
                }
            }
        }
        return $checkboxes;
    }

    public function getRegionCheckboxData($checked = []) {
        $items = Region::find()->all();
        $checkboxes = [];

        foreach ($items as $item) {
            $checkboxes[$item->id] = [
                'name' => $item->name,
                'checked' => false
            ];

            if($checked) {
                foreach ($checked as $key => $check) {
                    if ($check) {
                        $checkboxes[$key]['checked'] = true;
                    }
                }
            }
        }
        return $checkboxes;
    }
}
