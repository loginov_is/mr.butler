<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%page_about}}".
 *
 * @property integer $id
 * @property string $who_is_mr_butler_title
 * @property string $who_is_mr_butler_promotions
 * @property string $wanna_get_to_rent_title
 * @property string $wanna_get_to_rent_promotions
 * @property string $wanna_rent_title
 * @property string $wanna_rent_promotions
 * @property string $wanna_rent_video
 * @property string $partners_title
 * @property string $partners_promotions
 * @property string $team_title
 * @property string $team_promotions
 * @property string $press_title
 * @property string $press_promotions
 */
class PageAbout extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_about}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['who_is_mr_butler_title', 'wanna_get_to_rent_title', 'wanna_rent_title', 'wanna_rent_video', 'partners_title', 'team_title', 'press_title'], 'required'],
            [['who_is_mr_butler_promotions', 'wanna_get_to_rent_promotions', 'wanna_rent_promotions', 'partners_promotions', 'team_promotions', 'press_promotions'], 'string'],
            [['who_is_mr_butler_title', 'wanna_get_to_rent_title', 'wanna_rent_title', 'wanna_rent_video', 'partners_title', 'team_title', 'press_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'who_is_mr_butler_title' => Yii::t('app', 'Заголовок'),
            'who_is_mr_butler_promotions' => Yii::t('app', 'Описание'),
            'wanna_get_to_rent_title' => Yii::t('app', 'Заголовок'),
            'wanna_get_to_rent_promotions' => Yii::t('app', 'Описание'),
            'wanna_rent_title' => Yii::t('app', 'Заголовок'),
            'wanna_rent_promotions' => Yii::t('app', 'Описание'),
            'wanna_rent_video' => Yii::t('app', 'Ссылка на youtube видео'),
            'partners_title' => Yii::t('app', 'Заголовок'),
            'partners_promotions' => Yii::t('app', 'Описание'),
            'team_title' => Yii::t('app', 'Заголовок'),
            'team_promotions' => Yii::t('app', 'Описание'),
            'press_title' => Yii::t('app', 'Заголовок'),
            'press_promotions' => Yii::t('app', 'Описание'),
        ];
    }
}
