<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ticket;

class TicketSearch extends Ticket {

    public function rules() {
        return [
            [['message'], 'string'],
            [['created_by', 'created_at', 'viewed_admin'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = Ticket::find()->orderBy('viewed_admin ,created_at desc');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
//
//        if (!$this->validate()) {
//            return $dataProvider;
//        }
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'created_by', $this->created_by]);
        $query->andFilterWhere(['like', 'viewed_admin', $this->viewed_admin]);
        $query->andFilterWhere(['like', 'message', $this->message]);
        return $dataProvider;
    }

}
