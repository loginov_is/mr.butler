<?php

namespace app\models;

use app\helpers\ContentHelper;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%page}}".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $language_id
 * @property string $slug
 * @property integer $system
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $title
 * @property string $content
 * @property integer $type_id
 * @property string $preview
 *
 * @property Menu[] $menus
 * @property ImageAlbum|null $album
 * @property Language $language
 */
class Page extends ActiveRecord
{
    
    const TYPE_PAGE = 1;
    const TYPE_PROMOTIONS = 2;

    const SLIDER_TYPE_SLIDER = 0;
    const SLIDER_TYPE_DEFAULT_IMAGE = 1;
    const SLIDER_TYPE_BACKGROUND_IMAGE =2;

    public $uploadedFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['background_image', 'slider_type', 'uploadedFile'], 'safe'],
            [['language_id', 'slug', 'title', 'type_id'], 'required'],
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'language_id', 'system', 'album_id', 'type_id'], 'integer'],
            [['content', 'preview'], 'string'],
            [['slug', 'title'], 'string', 'max' => 255],
            [['meta_keywords', 'meta_description'], 'string', 'max' => 160],
            [['language_id', 'slug'], 'unique', 'targetAttribute' => ['language_id', 'slug'], 'message' => 'The combination of Language ID and Slug has already been taken.'],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
            [['background_image'], 'string', 'max' => 255],
            ['slider_type', function ($attribute, $params) {
                if($this->slider_type == Page::SLIDER_TYPE_BACKGROUND_IMAGE && is_null(UploadedFile::getInstance($this, 'uploadedFile')) && empty($this->background_image)) {
                    $this->addError($attribute, Yii::t('app', 'Вы должны загрузить фоновое изображение.'));
                }
                if($this->slider_type == Page::SLIDER_TYPE_SLIDER && empty($this->album_id)) {
                    $this->addError($attribute, Yii::t('app', 'Вы должны выбрать альбом для слайдера.'));
                }
            }],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Создано'),
            'updated_at' => Yii::t('app', 'Изменено'),
            'created_by' => Yii::t('app', 'Создал'),
            'updated_by' => Yii::t('app', 'Изменил'),
            'language_id' => Yii::t('app', 'Язык'),
            'slug' => Yii::t('app', 'Служебное имя'),
            'system' => Yii::t('app', 'Системная'),
            'meta_keywords' => Yii::t('app', 'Meta ключевые слова'),
            'meta_description' => Yii::t('app', 'Meta описание'),
            'title' => Yii::t('app', 'Заголовок'),
            'content' => Yii::t('app', 'Контент'),
            'album_id' => Yii::t('app', 'Слайдер'),
            'type_id'=> Yii::t('app', 'Тип'),
            'slider_type'=> Yii::t('app', 'Тип фонового изображения'),
            'background_image'=> Yii::t('app', 'Фоновое изображение'),
            'uploadedFile'=> Yii::t('app', 'Фоновое изображение'),
            'preview' => Yii::t('app', 'Превью (только для Акций и скидок)'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbum()
    {
        return $this->hasOne(ImageAlbum::className(), ['id' => 'album_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return null|self
     */
    public static function getByAction()
    {
        $route = Yii::$app->controller->getRoute();
        $params = Yii::$app->controller->actionParams;

        /** @todo find by slug from action params */
        if ($route === 'page/view' && !empty($params['id'])) {
            return self::findOne(['id' => $params['id']]);
        } else {
            $slug = str_replace('/', '_', $route);
            return self::findOne(['slug' => $slug, 'language_id' => Yii::$app->languageInfo->id]);
        }
    }

    public function beforeSave($insert)
    {
        // Always generate keywords
        //if (!$this->meta_keywords) {
            $this->meta_keywords = implode(', ', ContentHelper::generateKeywords(
                $this->content,
                16,
                file(Yii::getAlias('@app/data/stopwords.txt'), FILE_IGNORE_NEW_LINES)
            ));
        //}

        return parent::beforeSave($insert);
    }
}
