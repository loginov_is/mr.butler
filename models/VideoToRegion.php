<?php

namespace app\models;

use Yii;

class VideoToRegion extends \yii\db\ActiveRecord {

    public static function tableName() {
        return '{{%video_to_region}}';
    }

    public function rules() {
        return [
            [['region_id', 'video_id'], 'required'],
            [['region_id', 'video_id'], 'integer'],
            [['video_id'], 'exist', 'skipOnError' => true, 'targetClass' => Video::className(), 'targetAttribute' => ['video_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'region_id' => Yii::t('app', 'Region ID'),
            'video_id' => Yii::t('app', 'Video ID'),
        ];
    }

    public function getVideo() {
        return $this->hasOne(Video::className(), ['id' => 'video_id']);
    }

    public function getRegion() {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    public function beforeSave($insert) {
        $isset = self::findOne(['region_id' => $this->region_id, 'video_id' => $this->video_id]);

        if ($isset) {
            return false;
        }

        return parent::beforeSave($insert);
    }

}
