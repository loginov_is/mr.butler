<?php

namespace app\models;

use Yii;

class Currency extends \yii\db\ActiveRecord {

    const DEFAULT_CURRENCY = 'RUB';

    public static function tableName() {
        return '{{%currency}}';
    }

    public function rules() {
        return [
            [['code'], 'unique'],
            [['code', 'description'], 'required'],
            [['code'], 'string', 'max' => 3],
            [['description'], 'string', 'max' => 50],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Код'),
            'description' => Yii::t('app', 'Описание'),
        ];
    }

}
