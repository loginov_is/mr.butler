<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use app\models\Translation;
use app\models\Booking;
use app\models\Reviews;

/**
 * This is the model class for table "{{%real_estate}}".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $status_id
 * @property integer $type_id
 * @property string $titlehou
 * @property integer $flat_number
 * @property double $square
 * @property integer $floor
 * @property integer $bedrooms
 * @property integer $bathrooms
 * @property integer $furnishings
 * @property integer $pool
 * @property string $description
 * @property string $information
 * @property string $owner_name
 * @property string $owner_email
 * @property string $owner_phone
 * @property string $owner_info
 * @property string $contacts_name
 * @property string $contacts_email
 * @property string $contacts_phone
 * @property string $contacts_info
 * @property integer $quota
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $user_id
 * @property integer $no_photo
 * @property integer $to_the_sea
 * @property double $client_price
 * @property double $daily_price
 * @property double $monthly_price
 * @property integer $residents
 * @property integer $rating
 * @property integer $cancel_booking_type;
 *
 * @property Project $project
 * @property User $user
 * @property RealEstatePhoto[] $realEstatePhotos
 */
class RealEstate extends ActiveRecord {

    const CANCEL_BOOKING_NOT_HARD = 0;
    const CANCEL_BOOKING_HARD = 1;

    const SCENARIO_USER_CREATE = 0;
    const SCENARIO_ADMIN_CREATE = 1;

    /**
     * @var UploadedFile[]
     */
    public $images;
    public $project_autocomplete;
    public $lookups_count;

    public static function tableName() {
        return '{{%real_estate}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'comfort_ids' => 'comfort',
                ],
            ],
        ];
    }

    public function rules() {
        return [
            [['comfort_ids'], 'each', 'rule' => ['integer']],
            [['status_id'], 'default', 'value' => 1],
            [['project_id', 'status_id', 'type_id', 'title', 'flat_number', 'square', 'floor', 'bedrooms', 'bathrooms', 'furnishings', 'pool', 'owner_name', 'owner_email', 'owner_phone', 'contacts_name', 'contacts_email', 'contacts_phone', 'quota', 'user_id', 'no_photo', 'to_the_sea', 'client_price', 'project_autocomplete', 'daily_price', 'monthly_price', 'residents', 'housing_type_id', 'cancel_booking_type'], 'safe'],
            [['project_id', 'status_id', 'type_id', 'title', 'flat_number', 'square', 'floor', 'bedrooms', 'bathrooms', 'furnishings', 'pool', 'owner_name', 'owner_email', 'owner_phone', 'contacts_name', 'contacts_email', 'contacts_phone', 'quota', 'user_id', 'no_photo', 'to_the_sea', 'client_price', 'project_autocomplete', 'daily_price', 'monthly_price', 'residents', 'housing_type_id', 'cancel_booking_type'], 'required'],
            [['project_id'], 'required'],
            [['project_id', 'rating', 'status_id', 'type_id', 'flat_number', 'floor', 'bedrooms', 'bathrooms', 'furnishings', 'pool', 'quota', 'created_at', 'updated_at', 'user_id', 'no_photo', 'to_the_sea', 'residents', 'housing_type_id', 'cancel_booking_type'], 'integer'],
            [['square', 'client_price', 'daily_price', 'monthly_price'], 'number'],
            [['description', 'information'], 'string'],
            [['owner_name', 'owner_email', 'owner_phone', 'contacts_name', 'contacts_email', 'contacts_phone'], 'string', 'max' => 50],
            [['owner_email', 'contacts_email'], 'email'],
            [['owner_info', 'contacts_info', 'title'], 'string', 'max' => 255],
            [['images'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 4, 'minSize' => 10240, 'maxSize' => 10485760],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_USER_CREATE] = ['project_id', 'status_id', 'type_id', 'title', 'flat_number', 'square', 'floor', 'bedrooms', 'bathrooms', 'furnishings', 'pool', 'owner_name', 'owner_email', 'owner_phone', 'contacts_name', 'contacts_email', 'contacts_phone', 'quota', 'user_id', 'no_photo', 'to_the_sea', 'client_price', 'project_autocomplete', 'daily_price', 'monthly_price', 'residents', 'housing_type_id'];
        $scenarios[self::SCENARIO_ADMIN_CREATE] = ['project_id', 'status_id', 'type_id', 'title', 'flat_number', 'square', 'floor', 'bedrooms', 'bathrooms', 'furnishings', 'pool', 'owner_name', 'owner_email', 'owner_phone', 'contacts_name', 'contacts_email', 'contacts_phone', 'quota', 'user_id', 'no_photo', 'to_the_sea', 'client_price', 'daily_price', 'monthly_price', 'residents', 'housing_type_id', 'cancel_booking_type'];
        return $scenarios;
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'project_id' => Yii::t('app', 'Название проекта'),
            'project_autocomplete' => Yii::t('app', 'Название проекта'),
            'status_id' => Yii::t('app', 'Статус'),
            'type_id' => Yii::t('app', 'Тип недвижимости'),
            'housing_type_id' => Yii::t('app', 'Тип размещения'),
            'title' => Yii::t('app', 'Заголовок объекта'),
            'flat_number' => Yii::t('app', '№ квартиры'),
            'square' => Yii::t('app', 'Площадь'),
            'floor' => Yii::t('app', 'Этаж'),
            'bedrooms' => Yii::t('app', 'Кол-во спален'),
            'bathrooms' => Yii::t('app', 'Кол-во ванн'),
            'furnishings' => Yii::t('app', 'Меблировка'),
            'pool' => Yii::t('app', 'Бассейн'),
            'description' => Yii::t('app', 'Описание'),
            'information' => Yii::t('app', 'Дополнительная информация'),
            'owner_name' => Yii::t('app', 'Имя владельца'),
            'owner_email' => Yii::t('app', 'Email владельца'),
            'owner_phone' => Yii::t('app', 'Телефон владельца'),
            'owner_info' => Yii::t('app', 'Доп. информация о владельце'),
            'contacts_name' => Yii::t('app', 'Имя контакт. лица'),
            'contacts_email' => Yii::t('app', 'Email контакт. лица'),
            'contacts_phone' => Yii::t('app', 'Телефон контакт. лица'),
            'contacts_info' => Yii::t('app', 'Доп. информация o контакт. лице'),
            'quota' => Yii::t('app', 'Квота'),
            'created_at' => Yii::t('app', 'Создано'),
            'updated_at' => Yii::t('app', 'Обновлено'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'no_photo' => Yii::t('app', 'Нет фото'),
            'images' => Yii::t('app', 'Изображения'),
            'to_the_sea' => Yii::t('app', 'Расстояние до моря (метры)'),
            'client_price' => Yii::t('app', 'Желаемая стоимость аренды'),
            'comfort_ids' => Yii::t('app', 'Инфраструктура'),
            'daily_price' => Yii::t('app', 'Цена за день'),
            'monthly_price' => Yii::t('app', 'Цена за месяц'),
            'residents' => Yii::t('app', 'Макс. кол-во гостей'),
            'rating' => Yii::t('app', 'Рейтинг'),
            'cancel_booking_type' => Yii::t('app', 'Отмена бронирования'),
        ];
    }

    public function upload() {
        if ($this->isNewRecord) {
            return false;
        }
        $baseDir = Yii::getAlias('@webroot/upload/real-estate/' . $this->id);
        FileHelper::createDirectory($baseDir);
        $counter = 0;
        if (!$this->isNewRecord) {
            $counter = RealEstatePhoto::find()->orderBy('index DESC')->one();

            if (!$counter) {
                $counter = 1;
            } else {
                $counter = $counter->index + 1;
            }
        }
        foreach ($this->images as $file) {
            $baseName = md5(time() . $file->baseName) . '.' . $file->extension;
            if (!$file->saveAs($baseDir . '/' . $baseName)) {
                return false;
            }
            $photo = new RealEstatePhoto();
            $photo->src = Yii::getAlias('/upload/real-estate/' . $this->id) . '/' . $baseName;
            $photo->link('realEstate', $this);
            $photo->index = $counter;

            $counter++;
        }
        return true;
    }

    public function getAllPhotosAsArray() {
        $photos = [];
        $objectPhotos = $this->getRealEstatePhotos()->all();
        $projectPhotos = $this->project->getProjectPhotos()->all();

        foreach ($objectPhotos as $photo) {
            $photos[] = $photo->src;
        }

        foreach ($projectPhotos as $photo) {
            $photos[] = $photo->src;
        }

        return $photos;
    }

    public function getAllComfortAsArray() {
        $comfort = [];
        $objectComfort = $this->getComfort()->all();
        $projectComfort = $this->project->getComfort()->all();

        foreach ($objectComfort as $item) {
            $comfort[$item->id] = Translation::t($item, 'name');
        }

        foreach ($projectComfort as $item) {
            $comfort[$item->id] = Translation::t($item, 'name');
        }

        return $comfort;
    }

    public function constructTitle() {
        if ($this->isNewRecord && !$this->title) {
            $this->title = ($this->bedrooms) ? \Yii::t('ap', '{x} комнатная квартира', ['x' => $this->bedrooms]) : \Yii::t('app', 'Студия');

            if (isset($this->project->name)) {
                $this->title .= '. ' . $this->project->name;
            }
        }
    }

    public function getComfort() {
        return $this->hasMany(Lookup::className(), ['id' => 'lookup_id'])
                        ->viaTable('{{%real_estate_to_lookup}}', ['real_estate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject() {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstatePhotos() {
        return $this->hasMany(RealEstatePhoto::className(), ['real_estate_id' => 'id'])->orderBy("index");
    }

    public function getVideoToRealEstate() {
        return $this->hasMany(VideoToRealEstate::className(), ['real_estate_id' => 'id']);
    }

    public function getVideos() {
        return $this->hasMany(Video::className(), ['id' => 'video_id'])->via('videoToRealEstate');
    }

    public function getReviews() {
        return $this->hasMany(Reviews::className(), ['real_estate_id' => 'id']);
    }

    public function getUrl() {
        return ['/site/object', 'id' => $this->id];
    }

    public function getBooking() {
        return $this->hasMany(Booking::className(), ['real_estate_id' => 'id']);
    }

    public function beforeValidate() {
        $this->constructTitle();
        $this->daily_price = (!$this->daily_price) ? 0 : $this->daily_price;
        $this->monthly_price = (!$this->monthly_price) ? 0 : $this->monthly_price;

        if (!$this->isNewRecord) {
            $this->project_autocomplete = $this->project_id;
        }
        return parent::beforeValidate();
    }

    public function beforeSave($insert) {
        //@todo разобраться почему в $images есть пустой элемент с нулевым индексом
        if (empty($this->images) || empty($this->images[0]))
            $this->no_photo = 1;
        $this->no_photo = 1;

        return parent::beforeSave($insert);
    }

    public function getRatingTitle() {
        switch ($this->rating) {
            case 0:
                return \Yii::t('app', 'неопределено');
            case 1:
            case 2:
                return \Yii::t('app', 'хорошо');
            case 3:
                return \Yii::t('app', 'очень хорошо');
            case 4:
                return \Yii::t('app', 'отлично');
            case 5:
                return \Yii::t('app', 'великолепно');
            default :
                return \Yii::t('app', 'неопределено');
        }
    }

    public function isAvailableForBooking($startDate, $endDate) {
        $booking = Booking::find()
                ->andWhere(['real_estate_id' => $this->id])
                ->andWhere(['<>', 'status', Booking::STATUS_NOT_PAYED])
                ->andWhere(['<>', 'status', Booking::STATUS_CANCELED])
                ->andWhere(
                    [
                        'or',
                        ['and', 'start_date <= :startDate', 'end_date >= :startDate'],
                        ['and', 'start_date <= :endDate', 'end_date >= :endDate'],
                    ],
                    [
                        'startDate' => $startDate,
                        'endDate' => $endDate,
                    ]
                )
                ->all();

        return $booking ? false : true;
    }

    public function isAvailableForBookingDay($date) {
        $booking = Booking::find()
            ->andWhere(['real_estate_id' => $this->id])
            ->andWhere(['and', 'start_date <= :date', 'end_date >= :date'], ['date' => $date])
            ->all();

        return $booking ? false : true;
    }

    public function ratingRecalculate() {
        $avgRating = Reviews::find()
                ->select('AVG(rating)')
                ->where(['real_estate_id' => $this->id, 'status_id' => Reviews::STATUS_ACTIVE])
                ->scalar();
        $rating = ($avgRating) ? round($avgRating) : 0;
        $this->rating = $rating;
        return $this->save();
    }

    public function getReviewsCount() {
        return Reviews::find()
                        ->select('count(id)')
                        ->where(['real_estate_id' => $this->id, 'status_id' => Reviews::STATUS_ACTIVE])
                        ->scalar();
    }

    public static function getCancelBookingList($id = null) {
        $array = [
            self::CANCEL_BOOKING_NOT_HARD => \Yii::t('app', 'Не строгая'),
            self::CANCEL_BOOKING_HARD => \Yii::t('app', 'Строгая'),
        ];

        if (isset($id)) {
            if (isset($array[$id])) {
                return $array[$id];
            } else {
                return \Yii::t('app', 'Неопределено');
            }
        } else {
            return $array;
        }
    }

}
