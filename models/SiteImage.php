<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%site_image}}".
 *
 * @property integer $id
 * @property string $type
 * @property string $src
 */
class SiteImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_image}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type', 'src'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'src' => 'Src',
        ];
    }
}
