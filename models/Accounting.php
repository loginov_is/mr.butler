<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%accounting}}".
 *
 * @property integer $id
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $user_id
 * @property integer $lookup_id
 * @property double $amount
 * @property integer $operation
 */
class Accounting extends \yii\db\ActiveRecord {

    const OPERATION_MINUS_BALANCE = 0;
    const OPERATION_PLUS_BALANCE = 1;
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%accounting}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['created_by', 'created_at', 'user_id', 'lookup_id', 'amount', 'operation'], 'required'],
            [['created_by', 'created_at', 'user_id', 'lookup_id', 'operation'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_by' => Yii::t('app', 'Создатель'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'lookup_id' => Yii::t('app', 'Шаблон'),
            'amount' => Yii::t('app', 'Сумма'),
            'operation' => Yii::t('app', 'Операция'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLookup() {
        return $this->hasOne(Lookup::className(), ['id' => 'lookup_id']);
    }

    public static function getOperationList($id = null) {
        $array = [
            self::OPERATION_MINUS_BALANCE => \Yii::t('app', 'Списание с баланса пользователя'),
            self::OPERATION_PLUS_BALANCE => \Yii::t('app', 'Пополнение баланса пользователя')
        ];

        if (isset($id)) {
            if (isset($array[$id])) {
                return $array[$id];
            } else {
                return $id;
            }
        } else {
            return $array;
        }
    }
    
    public function setUserBalanceAction() {
        
    }

    public function beforeValidate() {
        if ($this->isNewRecord) {
            $this->created_at = time();
            $this->created_by = Yii::$app->user->id;
        }

        return parent::beforeValidate();
    }

}
