<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%service_price}}".
 *
 * @property integer $id
 * @property integer $service_id
 * @property integer $people_min
 * @property integer $people_max
 * @property string $price
 *
 * @property Service $service
 */
class ServicePrice extends \yii\db\ActiveRecord
{
    const TYPE_PER_GROUP = 0;
    const TYPE_PER_MAN = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%service_price}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price'], 'required'],
            [['service_id', 'people_min', 'people_max', 'price'], 'integer'],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'service_id' => Yii::t('app', 'Услуга'),
            'people_min' => Yii::t('app', 'Минимальное количество человек'),
            'people_max' => Yii::t('app', 'Максимальное количество человек'),
            'price' => Yii::t('app', 'Стоимость'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }
}
