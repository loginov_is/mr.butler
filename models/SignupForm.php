<?php
namespace app\models;

use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $first_name;
    public $last_name;
    public $patronymic;
    public $password;
    public $password_repeat;

    public $account_type;
    public $accept_rules;

    const ACCOUNT_TYPE_NONE = 0;
    const ACCOUNT_TYPE_LANDLORD = 1;
    const ACCOUNT_TYPE_RENTER = 2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'first_name', 'last_name', 'patronymic'], 'trim'],

            [['email', 'first_name', 'last_name', 'password', 'password_repeat'], 'required'],
            [['account_type'], 'required', 'message' => Yii::t('app', 'Необходимо выбрать тип аккаунта.')],

            ['account_type', 'default', 'value' => self::ACCOUNT_TYPE_NONE],
            ['account_type', 'integer'],
            ['account_type', 'in', 'range' => [self::ACCOUNT_TYPE_LANDLORD, self::ACCOUNT_TYPE_RENTER]],

            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => Yii::t('app', 'Этот адрес электронной почты уже занят.')],

            ['password', 'string', 'min' => 6],
            ['password', 'compare'],
            ['accept_rules', 'compare', 'compareValue' => true, 'operator' => '==', 'message' => Yii::t('app', 'Необходимо принять правила.')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Электронная почта'),
            'first_name' => Yii::t('app', 'Имя'),
            'last_name' => Yii::t('app', 'Фамилия'),
            'patronymic' => Yii::t('app', 'Отчество'),
            'password' => Yii::t('app', 'Пароль'),
            'password_repeat' => Yii::t('app', 'Повтор пароля'),
            'accept_rules' => Yii::t('app', 'Регистрируясь, вы соглашаетесь с правилами, Политикой конфиденциальности '),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User([
                'email' => $this->email,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'patronymic' => $this->patronymic,
            ]);
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generatePasswordResetToken();
            if ($user->save()) {
                $auth = Yii::$app->authManager;

                // Set role
                switch ($this->account_type) {
                    case self::ACCOUNT_TYPE_LANDLORD:
                        $auth->assign($auth->getRole('landlord'), $user->id);
                        break;
                    case self::ACCOUNT_TYPE_RENTER:
                        $auth->assign($auth->getRole('renter'), $user->id);
                        break;
                }

                return $user;
            }
        }

        return null;
    }
}
