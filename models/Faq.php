<?php

namespace app\models;
use app\helpers\ContentHelper;

use Yii;
use yii\db\ActiveQuery;

class Faq extends \yii\db\ActiveRecord {

    public static function tableName() {
        return '{{%faq}}';
    }

    public function rules() {
        return [
            [['faq_group_id', 'active'], 'integer'],
            [['question', 'answer', 'faq_group_id'], 'required'],
            [['question', 'answer'], 'string'],
            [['faq_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => FaqGroup::className(), 'targetAttribute' => ['faq_group_id' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'faq_group_id' => Yii::t('app', 'Группа FAQ'),
            'question' => Yii::t('app', 'Вопрос'),
            'answer' => Yii::t('app', 'Ответ'),
            'active' => Yii::t('app', 'Активность'),
        ];
    }

    public function getFaqGroup() {
        return $this->hasOne(FaqGroup::className(), ['id' => 'faq_group_id']);
    }

    public static function getData($words = null) {
        $data = [];

        $faqGroups = FaqGroup::findAll(['active' => ContentHelper::ITEM_STATUS_ACTIVE]);

        foreach ($faqGroups as $k => $group) {
            $data[$group->id]['groupName'] = $group->name;
            /** @var ActiveQuery $query */
            $query = $group->getFaqs();
            $query->where(['=', 'active', ContentHelper::ITEM_STATUS_ACTIVE]);

            if (!empty($words)) {
                $query->andWhere(['or like', 'question', $words]);
                $query->orWhere(['or like', 'answer', $words]);
                //var_dump($query->createCommand()->getRawSql());exit;
            }

            $data[$group->id]['items'] = $query->all();
            if (empty($data[$group->id]['items'])) {
                unset($data[$group->id]);
            }
        }


        return $data;
    }

}
