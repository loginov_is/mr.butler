<?php
namespace app\models;

use yii\base\Model;
use Yii;

/**
 * Booking form
 */
class BookingForm extends Model
{
    const SCENARIO_STEP_1 = 'step_1';
    const SCENARIO_STEP_2 = 'step_2';
    const SCENARIO_STEP_3 = 'step_3';

    public $startDate;
    public $endDate;
    public $realEstateId;
    public $phone;
    public $comment;
    public $services;
    public $submitPay;
    public $submitSave;
    public $submitBook;
    public $price;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['startDate', 'endDate', 'realEstateId', 'submitSave', 'submitPay', 'submitBook', 'phone', 'comment', 'price'], 'safe'],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_STEP_1] = ['startDate', 'endDate', 'realEstateId', 'services', 'submitSave', 'submitPay', 'submitBook', 'phone', 'comment', 'price'];
        $scenarios[self::SCENARIO_STEP_2] = ['startDate', 'endDate', 'realEstateId', 'services', 'submitSave', 'submitPay', 'submitBook', 'phone', 'comment', 'price'];
        $scenarios[self::SCENARIO_STEP_3] = ['startDate', 'endDate', 'realEstateId', 'services', 'submitSave', 'submitPay', 'submitBook', 'phone', 'comment', 'price'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
//            'email' => Yii::t('app', 'Электронная почта'),
        ];
    }

    public function getStartDateTimestamp() {
        date_default_timezone_set('UTC');
        return strtotime($this->startDate);
    }

    public function getEndDateTimestamp() {
        date_default_timezone_set('UTC');
        return strtotime($this->endDate);
    }

}
