<?php

namespace app\models;

use yii\helpers\FileHelper;
use Yii;

/**
 * This is the model class for table "partner".
 *
 * @property integer $id
 * @property string $name
 * @property string $image
 * @property string $description
 */
class Partner extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $uploadedFile;

    public static function tableName() {
        return 'partner';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'image'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 50],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'image' => Yii::t('app', 'Изображение'),
            'description' => Yii::t('app', 'Описание'),
        ];
    }

    public function beforeSave($insert) {
        $baseDir = Yii::getAlias('@webroot/upload/partner');
        FileHelper::createDirectory($baseDir);
        return parent::beforeSave($insert);
    }

}
