<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%lookup}}".
 *
 * @property integer $id
 * @property string $type
 * @property integer $code
 * @property string $name
 * @property integer $sort_order
 */
class Lookup extends ActiveRecord {

    const TYPE_POST_STATUS = 'PostStatus';
    const TYPE_HOUSING_TYPE = 'HousingType';
    const TYPE_HOUSING_COMFORT = 'HousingComfort';
    const TYPE_REAL_ESTATE_TYPES = 'RealEstateTypes';
    const TYPE_REAL_ESTATE_QOUTA = 'RealEstateQuota';
    const TYPE_REAL_ESTATE_FURNISHINGS = 'RealEstateFurnishings';
    const TYPE_REAL_ESTATE_POOL = 'RealEstatePool';
    const TYPE_USER_PAYMENT_METHOD = 'UserPaymentMethod';
    const TYPE_PAGE_TYPE = 'PageType';
    const TYPE_REAL_ESTATE_PRICE_RAGE = 'RealEstatePriceRage';
    const TYPE_REGION_CONFORM = 'RegionComfort';
    const TYPE_ACCOUNTING_TEMPLATE = 'AccountingTemplate';

    public $max_order;
    public $max_code;
    private static $_items = [];

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%lookup}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['type', 'code', 'name'], 'required'],
            [['code', 'sort_order'], 'integer'],
            [['type', 'name'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Тип'),
            'code' => Yii::t('app', 'Код'),
            'name' => Yii::t('app', 'Наименование'),
            'sort_order' => Yii::t('app', 'Сортировка'),
        ];
    }

    /**
     * Returns the items for the specified type.
     * @param string $type item type (e.g. 'PostStatus').
     * @return array item names indexed by item code. The items are order by their position values.
     * An empty array is returned if the item type does not exist.
     */
    public static function items($type, $asId = false) {
        if (!isset(self::$_items[$type])) {
            self::loadItems($type, $asId);
        }

        return self::$_items[$type];
    }

    public static function types() {
        return [
            Lookup::TYPE_POST_STATUS => Yii::t('app', 'Статус записи'),
            Lookup::TYPE_HOUSING_TYPE => Yii::t('app', 'Тип жилья'),
            Lookup::TYPE_HOUSING_COMFORT => Yii::t('app', 'Удобства'),
            Lookup::TYPE_REAL_ESTATE_TYPES => Yii::t('app', 'Типы недвижимости'),
            Lookup::TYPE_REAL_ESTATE_QOUTA => Yii::t('app', 'Квота'),
            Lookup::TYPE_REAL_ESTATE_FURNISHINGS => Yii::t('app', 'Меблировка'),
            Lookup::TYPE_REAL_ESTATE_POOL => Yii::t('app', 'Бассейн'),
            Lookup::TYPE_USER_PAYMENT_METHOD => Yii::t('app', 'Способ оплаты'),
            Lookup::TYPE_PAGE_TYPE => Yii::t('app', 'Тип страницы'),
            Lookup::TYPE_REAL_ESTATE_PRICE_RAGE => Yii::t('app', 'Диапазон цен'),
            Lookup::TYPE_REGION_CONFORM => Yii::t('app', 'Удобства районов'),
            Lookup::TYPE_ACCOUNTING_TEMPLATE => Yii::t('app', 'Шаблон бухгалтера'),
        ];
    }

    public static function type($type) {
        $types = self::types();

        return isset($types[$type]) ? $types[$type] : Yii::t('app', 'Неопределено');
    }

    /**
     * Returns the item name for the specified type and code.
     * @param string $type the item type (e.g. 'PostStatus').
     * @param integer $code the item code (corresponding to the 'code' column value)
     * @return string the item name for the specified the code. False is returned if the item type or code does not exist.
     */
    public static function item($type, $code) {
        if (!isset(self::$_items[$type])) {
            self::loadItems($type);
        }

        return isset(self::$_items[$type][$code]) ? self::$_items[$type][$code] : false;
    }

    /**
     * Loads the lookup items for the specified type from the database.
     * @param string $type the item type
     */
    private static function loadItems($type, $asId = false) {
        self::$_items[$type] = [];

        $models = static::find()
                ->where(['type' => $type])
                ->orderBy(['sort_order' => SORT_ASC, 'name' => SORT_ASC])
                ->all();

        self::$_items[$type] = ArrayHelper::map($models, ($asId) ? 'id' : 'code', function ($model) {
                    return Translation::t($model, 'name');
                });
    }

    public function beforeValidate() {
        if ($this->isNewRecord) {
            $params = Lookup::find()->select('max(code) as max_code, max(sort_order) as max_order')->where(['type' => $this->type])->one();
            $this->sort_order = ($params->max_order) ? $params->max_order + 1 : 1;
            $this->code = ($params->max_code) ? $params->max_code + 1 : 1;
        }

        return parent::beforeValidate();
    }

}
