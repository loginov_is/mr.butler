<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%payment_system_field}}".
 *
 * @property integer $id
 * @property integer $payment_system_id
 * @property string $title
 *
 * @property PaymentSystem $paymentSystem
 */
class PaymentSystemField extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_system_field}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_system_id', 'title'], 'required'],
            [['payment_system_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['payment_system_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentSystem::className(), 'targetAttribute' => ['payment_system_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'payment_system_id' => Yii::t('app', 'Платежная система'),
            'title' => Yii::t('app', 'Поле'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentSystem()
    {
        return $this->hasOne(PaymentSystem::className(), ['id' => 'payment_system_id']);
    }
    
    public static function getFieldTitle($id) {
        $model = self::findOne($id);
        return ($model)? $model->title: $id; 
    }
}
