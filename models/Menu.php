<?php

namespace app\models;

use Yii;

class Menu extends \yii\db\ActiveRecord {

    const TYPE_TOP_MENU = 1;
    const TYPE_BOTTOM_MENU = 2;

    public static function tableName() {
        return '{{%menu}}';
    }

    public function rules() {
        return [
            [['page_id', 'type_id', 'language_id'], 'required'],
            [['page_id', 'type_id', 'active', 'order', 'language_id'], 'integer'],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => \Yii::t('app', 'ID'),
            'page_id' => \Yii::t('app', 'Страница'),
            'type_id' => \Yii::t('app', 'Тип'),
            'active' => \Yii::t('app', 'Активность'),
            'order' => \Yii::t('app', 'Порядок отображения'),
            'language_id' => \Yii::t('app', 'Язык'),
        ];
    }

    public function getPage() {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    public function getTypeListData($id = null) {
        $data = [
            Menu::TYPE_TOP_MENU => Yii::t('app', 'Верхнее меню'),
            Menu::TYPE_BOTTOM_MENU => Yii::t('app', 'Нижнее меню'),
        ];

        if (isset($id)) {
            if (isset($data[$id])) {
                return $data[$id];
            } else {
                return $id;
            }
        } else {
            return $data;
        }
    }

    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $maxOrder = $this->getMenuMaxOrder($this->type_id, $this->language_id);
            $this->order = $maxOrder + 1;
        }

        return parent::beforeSave($insert);
    }

    public function getMenuMaxOrder($typeId, $languageId) {
        $model = Menu::findBySql("SELECT MAX(m.order) FROM {{%menu}} m WHERE m.type_id = :type_id AND m.language_id = :language_id", [
                    'type_id' => $typeId,
                    'language_id' => $languageId
                ])->scalar();

        return $model;
    }

}
