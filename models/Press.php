<?php

namespace app\models;
use yii\helpers\FileHelper;

use Yii;

/**
 * This is the model class for table "{{%press}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 *
 * @property PressArticle[] $pressArticles
 */
class Press extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $uploadedFile;
    
    public static function tableName() {
        return '{{%press}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'image'], 'required'],
            [['title'], 'string', 'max' => 50],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Заголовок'),
            'image' => Yii::t('app', 'Изображение'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPressArticles() {
        return $this->hasMany(PressArticle::className(), ['press_id' => 'id']);
    }

    public function beforeSave($insert) {
        $baseDir = Yii::getAlias('@webroot/upload/press');
        FileHelper::createDirectory($baseDir);
        return parent::beforeSave($insert);
    }

}
