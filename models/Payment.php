<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\models\User;

/**
 * This is the model class for table "{{%payment}}".
 *
 * @property integer $id
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status_id
 * @property integer $payment_system_id
 * @property double $amount
 * @property string $requisites
 */
class Payment extends \yii\db\ActiveRecord {

    const STATUS_NO_REQUISITES = 0;
    const STATUS_PENDING = 1;
    const STATUS_COMPLETE = 2;
    const STATUS_DENIED = 3;

    public $fields;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%payment}}';
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['created_by', 'created_at', 'updated_at', 'status_id', 'payment_system_id', 'amount'], 'required'],
            [['created_by', 'created_at', 'updated_at', 'status_id', 'payment_system_id'], 'integer'],
            [['amount'], 'number'],
            [['requisites'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_by' => Yii::t('app', 'Создатель'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата обновления'),
            'status_id' => Yii::t('app', 'Статус'),
            'payment_system_id' => Yii::t('app', 'Платежная система'),
            'amount' => Yii::t('app', 'Сумма вывода'),
            'requisites' => Yii::t('app', 'Реквизиты'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentSystem() {
        return $this->hasOne(PaymentSystem::className(), ['id' => 'payment_system_id']);
    }

    public function beforeValidate() {
        if ($this->isNewRecord) {
            $this->status_id = self::STATUS_NO_REQUISITES;
            $this->created_at = $this->updated_at = time();
            $this->created_by = Yii::$app->user->id;
            $userBalance = User::findOne($this->created_by)->getBalance();

            if ($this->amount > $userBalance) {
                $this->addError($this->amount, \Yii::t('core', 'Недостаточно средств на балансе для операции. Ваш баланс: THB {balance}', [
                            'balance' => $userBalance
                ]));
            }
        }

        return parent::beforeValidate();
    }

    public static function getList($id = null) {
        $array = [
            self::STATUS_NO_REQUISITES => \Yii::t('app', 'Запрос не подтвержден'),
            self::STATUS_PENDING => \Yii::t('app', 'Запрос обрабатывается'),
            self::STATUS_COMPLETE => \Yii::t('app', 'Выполнено'),
            self::STATUS_DENIED => \Yii::t('app', 'Отказано')
        ];

        if (isset($id)) {
            if (isset($array[$id])) {
                return $array[$id];
            } else {
                return \Yii::t('app', 'Неопределено');
            }
        } else {
            return $array;
        }
    }

}
