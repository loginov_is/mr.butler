<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RealEstate;

/**
 * RealEstateSearch represents the model behind the search form about `app\models\RealEstate`.
 */
class RealEstateSearch extends RealEstate {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'project_id', 'status_id', 'type_id', 'flat_number', 'floor', 'bedrooms', 'bathrooms', 'furnishings', 'pool', 'quota', 'created_at', 'updated_at', 'user_id', 'no_photo', 'to_the_sea'], 'integer'],
            [['title', 'description', 'information', 'owner_name', 'owner_email', 'owner_phone', 'owner_info', 'contacts_name', 'contacts_email', 'contacts_phone', 'contacts_info'], 'safe'],
            [['square', 'client_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = RealEstate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

//        @todo Разобраться с функцией $this->constructTitle
//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'project_id' => $this->project_id,
            'status_id' => $this->status_id,
            'type_id' => $this->type_id,
            'flat_number' => $this->flat_number,
            'square' => $this->square,
            'floor' => $this->floor,
            'bedrooms' => $this->bedrooms,
            'bathrooms' => $this->bathrooms,
            'furnishings' => $this->furnishings,
            'pool' => $this->pool,
            'quota' => $this->quota,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
            'no_photo' => $this->no_photo,
            'to_the_sea' => $this->to_the_sea,
            'rating' => $this->rating
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'information', $this->information])
                ->andFilterWhere(['like', 'owner_name', $this->owner_name])
                ->andFilterWhere(['like', 'owner_email', $this->owner_email])
                ->andFilterWhere(['like', 'owner_phone', $this->owner_phone])
                ->andFilterWhere(['like', 'owner_info', $this->owner_info])
                ->andFilterWhere(['like', 'contacts_name', $this->contacts_name])
                ->andFilterWhere(['like', 'contacts_email', $this->contacts_email])
                ->andFilterWhere(['like', 'contacts_phone', $this->contacts_phone])
                ->andFilterWhere(['like', 'contacts_info', $this->contacts_info])
                ->andFilterWhere(['like', 'client_price', $this->client_price])
                ->andFilterWhere(['like', 'daily_price', $this->daily_price])
                ->andFilterWhere(['like', 'monthly_price', $this->monthly_price]);

        return $dataProvider;
    }

}
