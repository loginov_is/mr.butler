<?php

namespace app\models;

use Yii;

class SiteOption extends \yii\db\ActiveRecord {

    public static function tableName() {
        return '{{%site_option}}';
    }

    public function rules() {
        return [
            [['facebook', 'vkontakte', 'youtube'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'facebook' => Yii::t('app', 'Facebook'),
            'vkontakte' => Yii::t('app', 'Vkontakte'),
            'youtube' => Yii::t('app', 'Youtube'),
        ];
    }

}
