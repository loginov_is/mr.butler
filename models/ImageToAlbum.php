<?php

namespace app\models;

use yii\db\ActiveRecord;

class ImageToAlbum extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%image_to_album}}';
    }

    public function rules()
    {
        return [
            [['image_id', 'album_id'], 'required'],
            [['image_id', 'album_id', 'order'], 'integer'],
            [
                ['album_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ImageAlbum::className(),
                'targetAttribute' => ['album_id' => 'id']
            ],
            [
                ['image_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Image::className(),
                'targetAttribute' => ['image_id' => 'id']
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'image_id' => 'Image ID',
            'album_id' => 'Album ID',
            'order' => 'Order'
        ];
    }

    public function getAlbum()
    {
        return $this->hasOne(ImageAlbum::className(), ['id' => 'album_id']);
    }

    public function getImage()
    {
        return $this->hasOne(Image::className(), ['id' => 'image_id']);
    }

    public function beforeSave($insert)
    {
        $isset = self::findOne(['image_id' => $this->image_id, 'album_id' => $this->album_id]);

        if ($isset) {
            return false;
        }

        return parent::beforeSave($insert);
    }

    public static function resetAndGetMaxOrder($albumId)
    {
        $album = ImageAlbum::findOne($albumId);
        $images = $album->albumImages;
        $a = 1;

        foreach ($images as $image) {
            self::updateAll(["order" => $a], "album_id = $albumId AND image_id = $image->id");
            $a++;
        }

        return $a;
    }
}
