<?php
namespace app\models;

use yii\base\Model;
use Yii;

/**
 * Site images form
 */
class SiteImageForm extends Model
{
    public $images;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['images'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxFiles' => 2],
        ];
    }
}
