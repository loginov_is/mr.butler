<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%ticket_comment}}".
 *
 * @property integer $id
 * @property integer $ticket_id
 * @property string $message
 * @property integer $created_at
 * @property integer $viewed_user
 * @property integer $created_by
 *
 * @property Ticket $ticket
 */
class TicketComment extends \yii\db\ActiveRecord {

    const VIEWED_TYPE_VIEWED = 1;
    const VIEWED_TYPE_NOT_VIEWED = 0;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%ticket_comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['ticket_id', 'message', 'created_by'], 'required'],
            [['ticket_id', 'created_at', 'viewed_user', 'created_by'], 'integer'],
            [['message'], 'string'],
            [['ticket_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ticket::className(), 'targetAttribute' => ['ticket_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'ticket_id' => Yii::t('app', 'Тикет'),
            'message' => Yii::t('app', 'Комментарий'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'viewed_user' => Yii::t('app', 'Просмотрено пользователем'),
            'created_by' => Yii::t('app', 'Создатель')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicket() {
        return $this->hasOne(Ticket::className(), ['id' => 'ticket_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function beforeValidate() {
        if ($this->isNewRecord) {
            $this->created_at = time();
            $this->created_by = Yii::$app->user->id;
        }

        if (Yii::$app->user->id == $this->ticket->created_by) {
            $this->viewed_user = 1;
        }

        $this->message = htmlspecialchars($this->message);
        return parent::beforeValidate();
    }

}
