<?php

namespace app\models;

use Yii;

class RegionToRegion extends \yii\db\ActiveRecord {

    public static function tableName() {
        return '{{%region_to_region}}';
    }

    public function rules() {
        return [
            [['parent_id', 'child_id'], 'required'],
            [['parent_id', 'child_id'], 'integer'],
            [['child_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['child_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'parent_id' => Yii::t('app', 'Parent ID'),
            'child_id' => Yii::t('app', 'Child ID'),
        ];
    }

    public function getChild() {
        return $this->hasOne(Region::className(), ['id' => 'child_id']);
    }

    public function getParent() {
        return $this->hasOne(Region::className(), ['id' => 'parent_id']);
    }

    public function beforeSave($insert) {
        $isset = self::findOne(['parent_id' => $this->parent_id, 'child_id' => $this->child_id]);

        if ($isset) {
            return false;
        }

        return parent::beforeSave($insert);
    }

}
