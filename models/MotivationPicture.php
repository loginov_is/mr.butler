<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%motivation_picture}}".
 *
 * @property integer $id
 * @property string $type
 * @property string $image
 * @property string $title
 * @property integer $order
 */
class MotivationPicture extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const TYPE_HOW_IT_WORK = 'HowItWork';
    const TYPE_BOTTOM_LAYOUT_MOTIVATION = 'BottomLayoutMotivation';
    const TYPE_WHO_IS_MR_BUTLER = 'WhoIsMrButler';
    const TYPE_PAY_METHODS = 'PayMethods';
    const TYPE_REAL_ESTATE_ADD = 'RealEstateAdd';

    public $uploadedFile;
    public $maxOrder;

    public static function tableName() {
        return '{{%motivation_picture}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['type', 'image', 'title', 'order'], 'required'],
            [['title'], 'string'],
            [['order'], 'integer'],
            [['type'], 'string', 'max' => 50],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Тип'),
            'image' => Yii::t('app', 'Изображение'),
            'title' => Yii::t('app', 'Заголовок'),
            'order' => Yii::t('app', 'Сортировка'),
        ];
    }

    public static function types() {
        return [
            self::TYPE_HOW_IT_WORK => Yii::t('app', 'Как это работает?'),
            self::TYPE_BOTTOM_LAYOUT_MOTIVATION => Yii::t('app', 'Мотивация внизу шаблона'),
            self::TYPE_WHO_IS_MR_BUTLER => Yii::t('app', 'Кто такой Mr Butler'),
            self::TYPE_PAY_METHODS => \Yii::t('app', 'Варианты оплаты'),
            self::TYPE_REAL_ESTATE_ADD => \Yii::t('app', 'Передать недвижимость в управление')
        ];
    }

    public static function type($type) {
        $types = self::types();

        return isset($types[$type]) ? $types[$type] : Yii::t('app', 'Неопределено');
    }

    public function beforeValidate() {
        if ($this->isNewRecord) {
            $result = self::find()->select('max(`order`) as maxOrder')->where('type = :type', [':type' => $this->type])->one();

            if ($result) {
                $this->order = $result['maxOrder'] + 1;
            } else {
                $this->order = 1;
            }
        }

        return parent::beforeValidate();
    }

}
