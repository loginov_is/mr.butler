<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\FormatConverter;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;
use app\models\RealEstate;
use app\models\Booking;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $first_name
 * @property string $last_name
 * @property string $patronymic
 * @property integer $phone
 * @property integer $birthday
 * @property double $balance
 * @property double $frozen_balance
 * @property string|UploadedFile $avatar
 * @property integer $payment_method
 * @property integer $news_subscription
 *
 * @property string $fullName read-only
 * @property Auth[] $auths
 * @property array $statuses
 * @property string $avatarUrl
 */
class User extends ActiveRecord implements IdentityInterface {

    const STATUS_BANNED = 0;
    const STATUS_ACTIVE = 10;

    private $_assignedRoles;
    public $birthdayField;

    public static function getStatuses() {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Активен'),
            self::STATUS_BANNED => Yii::t('app', 'Забанен'),
        ];
    }

    public static function getStatusText($status) {
        $statuses = self::getStatuses();
        return isset($statuses[$status]) ? $statuses[$status] : Yii::t('app', 'Не определен');
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_BANNED]],
            [['email', 'first_name', 'last_name', 'patronymic'], 'trim'],
            [['auth_key', 'password_hash', 'password_reset_token', 'email', 'first_name', 'last_name'], 'required'],
            ['birthdayField', 'date', 'format' => 'php:d/m/Y', 'timestampAttribute' => 'birthday'],
            [['status', 'created_at', 'updated_at', 'phone', 'payment_method', 'birthday', 'news_subscription'], 'integer'],
            [['auth_key'], 'string', 'max' => 32],
            [['balance', 'frozen_balance'], 'number'],
            [['password_hash', 'password_reset_token', 'email', 'first_name', 'last_name', 'patronymic', 'avatar'], 'string', 'max' => 255],
            ['assignedRoles', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            //'auth_key' => Yii::t('app', 'Auth Key'),
            //'password_hash' => Yii::t('app', 'Password Hash'),
            //'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Электронная почта'),
            'status' => Yii::t('app', 'Статус'),
            'created_at' => Yii::t('app', 'Создан'),
            'updated_at' => Yii::t('app', 'Изменен'),
            'first_name' => Yii::t('app', 'Имя'),
            'last_name' => Yii::t('app', 'Фамилия'),
            'patronymic' => Yii::t('app', 'Отчество'),
            'phone' => Yii::t('app', 'Телефон'),
            'avatar' => Yii::t('app', 'Изображение'),
            'birthdayField' => Yii::t('app', 'Дата рождения'),
            'birthday' => Yii::t('app', 'Дата рождения'),
            'assignedRoles' => Yii::t('app', 'Роли'),
            'payment_method' => Yii::t('app', 'Способ оплаты'),
            'news_subscription' => Yii::t('app', 'Подписка на новости'),
            'balance' => Yii::t('app', 'Баланс'),
            'frozen_balance' => Yii::t('app', 'Замороженный баланс'),
        ];
    }

    private function formatTimestamp($timestamp, $format) {
        if (strncmp($format, 'php:', 4) === 0) {
            $format = substr($format, 4);
        } else {
            $format = FormatConverter::convertDateIcuToPhp($format, 'date');
        }

        $date = new \DateTime();
        $date->setTimestamp($timestamp);
        $date->setTimezone(new \DateTimeZone('UTC'));
        return $date->format($format);
    }

    public function afterFind() {
        $this->birthdayField = $this->formatTimestamp($this->birthday, 'php:d/m/Y');
        parent::afterFind();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuths() {
        return $this->hasMany(Auth::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealEstates() {
        return $this->hasMany(RealEstate::className(), ['user_id' => 'id']);
    }

    public function getFullName() {
        return implode(' ', [$this->last_name, $this->first_name, $this->patronymic]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        //return static::findOne(['auth_key' => $token]);
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email) {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token,
                    'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    public function getRoles() {
        $auth = Yii::$app->authManager;
        return $auth->getRolesByUser($this->id);
    }

    public function getAssignedRoles() {
        if ($this->_assignedRoles === null) {
            $this->_assignedRoles = ArrayHelper::map($this->getRoles(), 'name', 'name');
        }
        return $this->_assignedRoles;
    }

    public function setAssignedRoles($value) {
        $this->_assignedRoles = $value;
    }

    public function upload() {
        if ($this->isNewRecord || !$this->avatar instanceof UploadedFile) {
            return false;
        }

        $baseDir = Yii::getAlias('@webroot/upload/user/' . $this->id);
        FileHelper::createDirectory($baseDir);

        $baseName = md5(time() . $this->avatar->baseName) . '.' . $this->avatar->extension;
        if (!$this->avatar->saveAs($baseDir . '/' . $baseName)) {
            return false;
        }
        $this->avatar = $baseName;

        return $this->save();
    }

    public function getAvatarUrl() {
        return $this->avatar ? "@web/upload/user/{$this->id}/{$this->avatar}" : '@web/images/no_avatar.png';
    }

    public function getViewedObjects() {
        return $this->hasMany(RealEstate::className(), ['id' => 'model_id'])
                        ->viaTable(UserLine::tableName(), ['created_by' => 'id'], function($query) {
                            $query->andWhere(['type_id' => UserLine::TYPE_ID_OBJECT_VIEWED]);
                        }
        );
    }

    public function getUserLineObjects() {
        return $this->hasMany(RealEstate::className(), ['id' => 'model_id'])
                        ->viaTable(UserLine::tableName(), ['created_by' => 'id'], function($query) {
                            $query->andWhere(['type_id' => UserLine::TYPE_ID_OBJECT_ADDED_TO_USERLINE]);
                        }
        );
    }

    public function getUserLineRegions() {
        return $this->hasMany(Region::className(), ['id' => 'model_id'])
                        ->viaTable(UserLine::tableName(), ['created_by' => 'id'], function($query) {
                            $query->andWhere(['type_id' => UserLine::TYPE_ID_REGION_ADDED_TO_USERLINE]);
                        }
        );
    }

    public function getBookedRealEstates() {
        return $this->hasMany(RealEstate::className(), ['id' => 'real_estate_id'])->viaTable(Booking::tableName(), ['created_by' => 'id']);
    }

    public function getBookedRealEstatesAsList() {
        $items = $this->getBookedRealEstates()->select(['id', 'title'])->asArray()->all();
        $array = [];
        
        foreach ($items as $item) {
            $array[$item['id']] = $item['title'];
        }
        
        return $array;
    }

    public function getBalance() {
        return $this->balance;
    }

    public function getFrozenBalance() {
        return $this->frozen_balance;
    }

    public function minusBalance($amount) {
        self::updateAll(['balance' => ($this->balance - $amount)], ['id' => $this->id]);
    }

    public function plusBalance($amount) {
        self::updateAll(['balance' => ($this->balance + $amount)], ['id' => $this->id]);
    }

    public function minusFrozenBalance($amount) {
        self::updateAll(['frozen_balance' => ($this->frozen_balance - $amount)], ['id' => $this->id]);
    }

    public function plusFozenBalance($amount) {
        self::updateAll(['frozen_balance' => ($this->frozen_balance + $amount)], ['id' => $this->id]);
    }

}
