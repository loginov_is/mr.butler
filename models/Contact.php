<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%contact}}".
 *
 * @property integer $id
 * @property integer $language_id
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $skype
 * @property string $map
 *
 * @property Language $language
 */
class Contact extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contact}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id'], 'unique'],
            [['language_id', 'address', 'phone', 'email', 'skype', 'map'], 'required'],
            [['language_id'], 'integer'],
            [['address', 'phone', 'map'], 'string', 'max' => 255],
            [['email', 'skype'], 'string', 'max' => 100],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'language_id' => Yii::t('app', 'Язык'),
            'address' => Yii::t('app', 'Адрес'),
            'phone' => Yii::t('app', 'Телефон'),
            'email' => Yii::t('app', 'Электронная почта'),
            'skype' => Yii::t('app', 'Skype'),
            'map' => Yii::t('app', 'Карта'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }
}
