<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'lib/colorbox/colorbox.css',
        'css/admin.css',
        'css/site.css'
    ];
    public $js = [
        'js/region.js',
        'js/main.js',
        'js/site_options.js',
        'js/app.js',
        'js/app.forms.js',
        'lib/colorbox/jquery.colorbox-min.js',
        'js/real_estate.js',
        'js/bootbox.js',
        'js/admin.js',
        'js/payment_system.js'
    ];
    public $depends = [
    ];

}
