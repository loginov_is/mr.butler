<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/carousel.css',
        'css/navbar.css',
        'css/search.css',
        'lib/weather_icons/css/weather-icons.min.css',
        'lib/slick/slick.css',
    ];
    public $js = [
        'js/app.js',
        'js/app.forms.js',
        'js/bootbox.js',
        'js/jquery.sticky-kit.min.js',
        'lib/slick/slick.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
