<?php

namespace app\assets;

use yii\web\AssetBundle;

class GalleryAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'lib/fotorama/fotorama.css',
    ];
    public $js = [
        'lib/fotorama/fotorama.js',
    ];
    public $depends = [
    ];

}
