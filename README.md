mr.butler
=========

REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

```php
'request' => [
    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
    'cookieValidationKey' => '<secret random string goes here>',
],
```

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.0.3"
~~~

http://localhost/mr.butler/web/

```sh
cp config/secret.example.php config/secret.php
```

* Run migrations:

```sh
./yii migrate
./yii migrate --migrationPath=@yii/rbac/migrations
```

* Initialize RBAC data:

```sh
./yii rbac/init
```

# Assign role to user:

* search user

```sh
./yii rbac/list <email> 
```

* assign role
    
```sh
./yii rbac/list <id> [role (default = `admin`)] 
```

INTERNATIONALIZATION
--------------------

* Generate messages:

```sh
./yii message config/i18n.php 
```