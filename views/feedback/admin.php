<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Feedback;

$this->title = Yii::t('app', 'Обратная связь');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'email',
            'name',
            'phone',
            [
                'attribute' => 'created_at',
                'value' => function ($row) {
                    return date('d/m/Y H:i', $row->created_at);
                }
            ],
            [
                'attribute' => 'is_viewed',
                 'filter' => Feedback::getViewedStatuses(),
                'format' => 'html',
                'value' => function ($row) {
                    if ($row->is_viewed == Feedback::IS_VIEWED_TRUE) {
                        return Html::a(Html::tag('span', \Yii::t('app', 'Просмотрено'), ['class' => 'text-muted']), ['feedback/viewed', 'id' => $row->id, 'status' => Feedback::IS_VIEWED_FALSE]);
                    } else {
                        return Html::a(Html::tag('span', \Yii::t('app', 'Не просмотрено'), ['class' => 'text-danger']), ['feedback/viewed', 'id' => $row->id, 'status' => Feedback::IS_VIEWED_TRUE]);
                    }
                }
                    ],
                    ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {delete}', 'options' => ['style' => 'width:60px']],
                ],
            ]);
            ?>

</div>
