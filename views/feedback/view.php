<?php

use yii\helpers\Html;
use app\models\Feedback;

$this->title = Yii::t('app', 'Просмотр обратной связи от') . ' ' . date('d/m/Y H:i', $model->created_at);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Обратная связь'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-md-12">
            <?= \Yii::t('app', 'Имя') ?>: <?= $model->name ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= \Yii::t('app', 'Телефон') ?>: <?= $model->phone ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= \Yii::t('app', 'Email') ?>: <?= Html::mailto($model->email) ?>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-12">
            <?= Html::a(\Yii::t('app', 'Просмотрено'), ['feedback/viewed', 'id' => $model->id, 'status' => Feedback::IS_VIEWED_TRUE], ['class' => 'btn btn-success']) ?>
            <?= Html::a(\Yii::t('app', 'Не просмотрено'), ['feedback/viewed', 'id' => $model->id, 'status' => Feedback::IS_VIEWED_FALSE], ['class' => 'btn btn-danger']) ?>
        </div>
    </div>
</div>
