<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Regulation */

$this->title = Yii::t('app', 'Добавить правило');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Управление правилами'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="regulation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
