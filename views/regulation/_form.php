<?php

use app\models\RegulationGroup;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Regulation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="regulation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'group_id')->dropDownList(ArrayHelper::map(RegulationGroup::find()->all(), 'id', 'title'), [
        'prompt' => '- ' . $model->getAttributeLabel('group_id') . ' -'
    ]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(yii\imperavi\Widget::className(), [
        'options' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'imageUpload' => Url::to([
                'image/upload',
                'type' => Inflector::camel2id(StringHelper::basename(get_class($model))),
                'id' => $model->isNewRecord ? 'tmp' : $model->id
            ]),
            /*'fileUpload' => Url::to([
                'file/upload',
                'type' => Inflector::camel2id(StringHelper::basename(get_class($model))),
                'id' => $model->isNewRecord ? 'new' : $model->id
            ]),*/
            'uploadImageFields' => [
                Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
            ],
            'convertImageLinks' => true,
            'convertVideoLinks' => true,
            //'imageGetJson' => Url::to(['/admin/gallery/json-list']),
            //'imageManagerJson' => Url::to(['/admin/gallery/json-list']),
            //'fileManagerJson' => Url::to(['/admin/gallery/json-list']),
            'imageUploadCallback' => $model->isNewRecord ? new \yii\web\JsExpression(<<<JS
                function(image, json) {
                    $(".regulation-form")
                        .find('form')
                        .append($('<input type="hidden" name="images[]" value="" />').val($(image).attr("src")));
                }
JS
            ) : false,
            //'definedLinks' => '/defined-links.json', // @todo Action to site pages
        ],
        'plugins' => [
            'fullscreen',
            'table',
            'video',
            'imagemanager',
            'filemanager',
            'definedlinks',
            'fontsize',
            'fontfamily',
            'fontcolor',

            //'clips',
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
