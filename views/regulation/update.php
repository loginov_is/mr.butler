<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Regulation */

$this->title = Yii::t('app', 'Изменить правило: ') . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Управление правилами'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Изменить');
?>
<div class="regulation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
