<?php

use app\models\RegulationGroup;
use yii\bootstrap\Collapse;
use yii\helpers\Html;
use app\widgets\BackgroundWidget;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Правила'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=
BackgroundWidget::widget([
    'content' => '<h1 class="text-center">' . $this->title . '</h1>',
    'htmlClass' => 'site-dynamic-page',
    'page' => $page
])
?>
<div class="regulation regulation-view container">
    <div class="row">
        <div class="col-md-4">
            <?=
            Collapse::widget([
                'items' => RegulationGroup::getItems($model->id),
                'encodeLabels' => false,
            ])
            ?>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="regulationTitle"><?= $model->title ?></h3>
                    <div class="regulationContent">
                        <?= $model->content ?>
                    </div>
                </div>
            </div>
            <div class="row regulationLinks">
                <div class="col-md-3 col-xs-5">
                    <p><?= \Yii::t('app', 'Поделиться') ?>:</p>
                </div>
                <div class="col-md-9 col-xs-7">

                </div>
            </div>
        </div>
    </div>
</div>
