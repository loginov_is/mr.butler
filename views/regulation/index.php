<?php

use app\models\RegulationGroup;
use yii\bootstrap\Collapse;
use yii\helpers\Html;
use app\widgets\BackgroundWidget;

$this->params['breadcrumbs'][] = $this->title;
?>
<?=
BackgroundWidget::widget([
    'content' => '<h1 class="text-center">' . $this->title . '</h1>',
    'htmlClass' => 'site-dynamic-page',
    'page' => $page
])
?>
<div class="regulation regulation-index container">
    <div class="row">
        <div class="col-md-4">
            <?=
            Collapse::widget([
                'items' => RegulationGroup::getItems(),
                'encodeLabels' => false,
            ])
            ?>
        </div>
        <div class="col-md-6">

        </div>
    </div>
</div>
