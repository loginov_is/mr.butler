<?php

use app\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Пользователи');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'email:email',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    /** @var User $model */
                    return $model::getStatusText($model->status);
                },
                'filter' => User::getStatuses(),
                'format' => 'raw',
            ],
            //'created_at:datetime',
            //'updated_at:datetime',
            'first_name',
            'last_name',
            'patronymic',
            [
                'attribute' => 'roles',
                'label' => 'Роли',
                'value' => function ($model) {
                    /** @var User $model */
                    return implode(', ', ArrayHelper::map($model->roles, 'name', 'description')) ?: null;
                },
                'format' => 'raw',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ],
        ],
    ]); ?>

</div>
