<?php

use yii\helpers\Html;

echo Html::a('<span class="glyphicon glyphicon-upload"></span>', ['/motivation-pictures/set-item-order-up', 'id' => $id, 'type' => $type], ['style' => 'margin-right:10px;']);
echo Html::a('<span class="glyphicon glyphicon-download"></span>', ['/motivation-pictures/set-item-order-down', 'id' => $id, 'type' => $type]);
