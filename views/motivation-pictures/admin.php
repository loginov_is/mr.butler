<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\MotivationPicture;
$this->title = Yii::t('app', 'Мотивирующие блоки');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить блок'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<div class="row">
    <div class="col-md-12">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                [
                    'attribute' => 'type',
                    'value' => function ($row) {
                        return MotivationPicture::type($row->type);
                    }
                ],
                [
                    'attribute' => 'image',
                    'format' => 'html',
                    'value' => function ($row) {
                        return Html::img('@web/upload/motivation_pictures/' . $row->image, ['style' => 'height: 100px;']);
                    }
                        ],
                        'title',
                        [
                            'attribute' => 'order',
                            'format' => 'html',
                            'value' => function ($row) {
                                return $this->render('_orderButtons', [
                                            'id' => $row->id,
                                            'type' => $row->type
                                ]);
                            }
                                ],
                                [
                                    'class' => \yii\grid\ActionColumn::className(),
                                    'template' => '{update} {delete}',
                                ],
                            ],
                        ])
                        ?>
    </div>
</div>