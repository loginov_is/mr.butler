<?php

use app\models\Faq;
?>
<div class="row faqItems">
    <div class="col-md-12 col-xs-12">
        <h3><?= $dataItem['groupName']; ?></h3>
        <?php $a = 0; ?>
        <?php foreach ($dataItem['items'] as $key => $item) { ?>
            <?php
            if ($a == 5) {
                break;
            }
            ?>
            <div class="row faqItem">
                <div class="col-md-12 col-xs-12">
                    <a class="collapsed question" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?= $item->id; ?>" aria-expanded="false" aria-controls="collapse-<?= $item->id; ?>">
                        <?= $item->question; ?>
                    </a>
                    <div id="collapse-<?= $item->id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapse-<?= $item->id; ?>" >
                        <div class="panel-body answer">
                            <?= nl2br($item->answer); ?> 
                        </div>
                    </div>
                </div>
            </div>
            <?php $a++; ?>
            <?php unset($dataItem['items'][$key]); ?>
        <?php } ?>
        <?php if ($dataItem['items']) { ?>
            <a class="showMore" data-id="<?= $groupId; ?>"><?= \Yii::t('app', 'Еще вопросы ({x})', ['x' => count($dataItem['items'])]); ?></a>
            <div class="hiddenMore-<?= $groupId; ?> hidden">
                <?php foreach ($dataItem['items'] as $key => $item) { ?>
                    <div class="row faqItem">
                        <div class="col-md-12 col-xs-12">
                            <a class="collapsed question" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?= $item->id; ?>" aria-expanded="false" aria-controls="collapse-<?= $item->id; ?>">
                                <?= $item->question; ?>
                            </a>
                            <div id="collapse-<?= $item->id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapse-<?= $item->id; ?>" >
                                <div class="panel-body answer">
                                    <?= nl2br($item->answer); ?> 
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>
<hr />