<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\FaqGroup;
?>
<?php $form = ActiveForm::begin() ?>
<?= $form->errorSummary([$model]) ?>
<div class="row">
    <div class="col-md-12">
        <?php echo $form->field($model, 'question')->textInput(); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php echo $form->field($model, 'answer')->textarea(['rows' => 10]); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php
        echo $form->field($model, 'faq_group_id')->dropDownList(
                FaqGroup::find()->select(['name', 'id'])->indexBy('id')->column(), [
            'prompt' => \Yii::t('app', 'Выберите группу FAQ'),
            'class' => 'form-control'
                ]
        );
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php echo $form->field($model, 'active')->checkbox(); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=
        Html::submitButton(($model->getIsNewRecord()) ? \Yii::t('app', 'Создать') : \Yii::t('app', 'Сохранить'), [
            'class' => 'btn btn-success'
        ]);
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>