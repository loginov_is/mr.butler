<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helpers\ContentHelper;

$this->title = Yii::t('app', 'FAQ');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<p>
    <?= Html::a(Yii::t('app', 'Добавить FAQ'), ['create'], ['class' => 'btn btn-success']) ?>
</p>
<?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'question',
        [
            'attribute' => 'faq_group_id',
            'value' => function ($data) {
                return $data->faqGroup->name;
            }
        ],
        [
            'attribute' => 'active',
            'value' => function ($data) {
                return ContentHelper::getActiveListData($data->active);
            }
        ],
        [
            'class' => \yii\grid\ActionColumn::className(),
            'template' => '{update} {delete}',
        ],
    ],
])
?>