<?php

use yii\helpers\Html;
use app\widgets\BackgroundWidget;

$this->params['breadcrumbs'][] = $this->title;
?>
<?=
BackgroundWidget::widget([
    'content' => '<h1 class="text-center">' . $this->title . '</h1>',
    'htmlClass' => 'site-dynamic-page',
    'page' => $page
])
?>
<div class="container faqContainer">
    <?= Html::beginForm(['/faq/index'], 'get'); ?>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?=
            Html::textInput('search', $search, [
                'class' => 'form-control faqSearchField',
                'placeholder' => \Yii::t('app', 'Поиск по словам')
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-xs-10">
        </div>
        <div class="col-md-2 col-xs-2 text-right faqSearchSubmitBlock">
            <?=
            Html::submitButton('<i class="fa fa-search"></i>', [
                'class' => 'btn btn-default faqSearchSubmit'
            ]);
            ?>
        </div>
    </div>
    <div class="row annotation">
        <div class="col-md-12 col-xs-12">
            <?= \Yii::t('app', 'Например'); ?>: 
            <span class="searchExample"><?= \Yii::t('app', 'обмен'); ?></span>, 
            <span class="searchExample"><?= \Yii::t('app', 'возврат'); ?></span>, 
            <span class="searchExample"><?= \Yii::t('app', 'скидки'); ?></span>
        </div>
    </div>
    <?= Html::endForm(); ?>
    <?php
    foreach ($data as $key => $item) {
        echo $this->render('_view_faq_group', [
            'dataItem' => $item,
            'groupId' => $key
        ]);
    }
    ?>
</div>