<?php

use yii\helpers\Html;
use app\models\Translation;
?>
<div class="col-md-4">
    <div class="reviewItem">
        <?php if (isset($item->realEstate)) { ?>
            <div class="reviewImage">
                <?php $photos = $item->realEstate->getRealEstatePhotos()->orderBy('main, index')->all(); ?>
                <?php if ($photos) { ?>
                    <div class="appartamentPhoto real-estate-photos">
                        <?php foreach ($photos as $photo) { ?>
                            <div class="real-estate-photo">
                                <?= Html::img('@web' . $photo->src) ?>
                            </div>
                        <?php } ?>
                    </div>
                    <?php $this->registerJs("$('.real-estate-photos').fotorama({nav: false, fit: 'cover', width: '100%', height: 283});"); ?>
                <?php } else { ?>
                    <?= Html::img('@web/images/no-photo.jpg', ['class' => 'noPhoto']) ?>
                <?php } ?>
            </div>
        <?php } ?>
        <div style="clear: both"></div>
        <div class="row description">
            <div class="col-md-12">
                <?php if (isset($item->realEstate)) { ?>
                    <div class="row">
                        <div class="col-md-10">
                            <p>
                                <?= Html::a(Translation::t($item->realEstate, 'title'), $item->realEstate->getUrl(), ['class' => 'projectName']) ?>
                            </p>
                            <?php if (isset($item->realEstate->project->city)) { ?>
                            <p class="projectRegion"><?= Translation::t($item->realEstate->project->city, 'name') ?></p>
                            <?php } ?>
                        </div>
                        <div class="col-md-2 text-right">
                            <span class="text-red"><?= $item->realEstate->rating ?></span>
                        </div>
                    </div>
                <?php } ?>
                <hr />
                <div class="row">
                    <div class="col-md-3 col-xs-3 text-center">
                        <?php $userAvatar = isset($item->createdBy) ? $item->createdBy->getAvatarUrl() : "@web/images/no_avatar.png"; ?>
                        <?= Html::img($userAvatar, ['alt' => 'user picture', 'style' => 'max-width: 100%;', 'class' => 'reviewUserAvatar']) ?>
                    </div>
                    <div class="col-md-9 col-xs-9">
                        <p class="userName"><?= isset($item->createdBy) ? $item->createdBy->getFullName() : \Yii::t('app', 'Пользователь удален') ?></p>
                        <p class="date"><?= date('d.m.Y | H:i', $item->created_at) ?></p>
                    </div>
                </div>
                <div class="reviewItems row">
                    <div class="col-md-12">
                        <?php if ($item->pluses) { ?>
                            <p><div class="plus"><span>+</span></div> <?= $item->pluses ?></p>
                        <?php } ?>
                        <?php if ($item->minuses) { ?>
                            <p><div class="minus"><span>-</span></div> <?= $item->minuses ?></p>
                        <?php } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="title"><?= \Yii::t('app', 'Отзыв') ?>:</p>
                        <div class="review">
                            <?= $item->review ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>