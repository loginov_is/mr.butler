<?php

use yii\bootstrap\Html;
?>
<div class="modal fade" id="bookingFormModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= Yii::t('app', 'Забронировать объект') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <?= Html::label(Yii::t('app', 'Номер телефона'), 'bookingFormModal-phone', ['class' => 'col-md-5 col-md-offset-1 control-label']) ?>
                                <div class="col-md-5">
                                    <?= Html::textInput('phone', !Yii::$app->user->isGuest ? Yii::$app->user->identity->phone : null, ['class' => 'form-control', 'id' => 'bookingFormModal-phone']) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= Html::label(Yii::t('app', 'Комментарий к заказу'), 'bookingFormModal-comment', ['class' => 'col-md-5 col-md-offset-1 control-label']) ?>
                                <div class="col-md-5">
                                    <?= Html::textarea('comment', '', ['class' => 'form-control', 'id' => 'bookingFormModal-comment']) ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"><?= Yii::t('app', 'Забронировать') ?></button>
            </div>
        </div>
    </div>
</div>