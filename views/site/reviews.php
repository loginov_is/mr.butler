<?php
$this->title = \Yii::t('app', 'Отзывы');
$this->params['breadcrumbs'][] = $this->title;

use app\widgets\BackgroundWidget;
use yii\helpers\Html;
?>

<?=
BackgroundWidget::widget([
    'content' => '<h1 class="text-center">' . $this->title . '</h1>',
    'htmlClass' => 'site-dynamic-page',
    'page' => $model
])
?>
<div class="container">
    <div class="row reviews-content">
        <?php
        $itemsCount = count($reviews);
        $a = 0;
        foreach ($reviews as $key => $item) {

            if ($a >= 6) {
                break;
            }

            echo $this->render('_review_item', [
                'item' => $item
            ]);
            unset($reviews[$key]);
            $a++;
        }
        ?>
    </div>
    <?php if ($reviews) { ?>
        <div class="row" id="viewMoreLinkBlock">
            <div class="col-md-12 text-center viewAllItems">
                <div>
                    <?= Html::img('@web/images/view_more_plus.png', ['alt' => 'view more']); ?>
                </div>
                <div>
                    <a href="" data-shown="promotionsHidden" data-removed="viewMoreLinkBlock" class="viewMoreLink"><?= \Yii::t('app', 'Показать все отзывы'); ?></a>
                </div>
                <div>
                    <span class="viewMoreSummary"><?= \Yii::t('app', 'Показано {x} из {y}', ['x' => 6, 'y' => $itemsCount]) ?></span>
                </div>
            </div>
        </div>
        <div class="hidden" id="promotionsHidden">
            <?php
            foreach ($reviews as $key => $item) {
                echo $this->render('_review_item', [
                    'item' => $item
                ]);
            }
            ?>
        </div>
    <?php } ?>
    <div class="row marginTop20">
        <div class="col-md-12 col-xs-12 text-center">
            <?=
            Html::a(\Yii::t('app', 'Оставить свой отзыв'), ['profile/reviews'], [
                'class' => 'btn btn-default btnAdnvaceYellow'
            ]);
            ?>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-12">
            <h3><?= \Yii::t('app', 'Видеоотзывы') ?></h3>
        </div>
    </div>
    <?php if ($videoReviews) { ?>
        <div class="row">
            <?php foreach ($videoReviews as $review) { ?>
                <div class="col-md-4 videoReviewsItem">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?= $review->youtube_id ?>"></iframe>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>