<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model SignupForm */

use app\models\SignupForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\widgets\BackgroundWidget;

$this->params['breadcrumbs'][] = $this->title;
?>
<?=
BackgroundWidget::widget([
    'content' => '<h1 class="text-center">' . $this->title . '</h1>',
    'htmlClass' => 'site-dynamic-page',
    'page' => $page
])
?>
<div class="site-signup" style="margin-top: 20px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-xs-12">
                <p><?= Yii::t('app', 'Пожалуйста, заполните следующие поля, чтобы зарегистрироваться') ?>:</p>
                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->errorSummary([$model]) ?>

                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'first_name') ?>
                <?= $form->field($model, 'last_name') ?>
                <?= $form->field($model, 'patronymic') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'password_repeat')->passwordInput() ?>

                <?=
                $form->field($model, 'account_type')->radioList([
                    SignupForm::ACCOUNT_TYPE_RENTER => Yii::t('app', 'Снять жилье'), // Арендатор
                    SignupForm::ACCOUNT_TYPE_LANDLORD => Yii::t('app', 'Сдать недвижимость в доверительное управление'), // Арендодатель
                ])->label(false)
                ?>

                <?= $form->field($model, 'accept_rules')->checkbox() ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Зарегистрироваться'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
