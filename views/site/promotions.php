<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use app\widgets\PromotionsWidget;
use app\widgets\BackgroundWidget;

$this->params['breadcrumbs'][] = $this->title;
?>
<?=
BackgroundWidget::widget([
    'content' => '<h1 class="text-center">' . $this->title . '</h1>',
    'htmlClass' => 'site-dynamic-page',
    'page' => $model
])
?>
<div class="site-promotions">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?= $model->content ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= PromotionsWidget::widget() ?>
            </div>
        </div>
    </div>
</div>
