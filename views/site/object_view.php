<?php

use app\models\Booking;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use kartik\widgets\StarRating;
//use kartik\widgets\DatePicker;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\bootstrap\Tabs;
use app\widgets\BookingWidget;
use app\models\Lookup;
use app\models\Translation;
use app\models\UserLine;
use app\models\Reviews;

/* @var $this yii\web\View */
/* @var $model app\models\RealEstate */

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Популярые объекты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="realEstateContainer">
    <div class="projectPhotoSlider">
        <?php
        $sliderPhotos = $model->getProject()->one()->getProjectSliderPhotos()->all();
        foreach ($sliderPhotos as $photo) {
            echo Html::img($photo->src);
        }
        ?>
    </div>
    <?php
    $this->registerJs("$('.projectPhotoSlider').fotorama({
            nav: 'thumbs',
            fit: 'cover',
            width: '100%',
            height: 600,
            thumbwidth: 220,
            thumbheight: 100,
            thumbmargin: 25,
            navwidth: 960
        });");
    ?>

    <div class="container realEstatePage">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="row">
                    <div class="col-md-12 objectViewRating">
                        <?=
                        StarRating::widget([
                            'name' => 'rating_1',
                            'value' => $model->rating,
                            'pluginOptions' => [
                                'disabled' => true,
                                'showClear' => false,
                                'size' => 'xs',
                                'showCaption' => false,
                            ],
                        ]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 objectViewRating">
                        <strong><?= $model->rating ?></strong>/5 <?= $model->getRatingTitle() ?> (<?= \Yii::t('app', 'оценок') ?>: <?= $model->getReviewsCount() ?>)
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="objectTitle"><?= $this->title ?></h2>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="monthlyPrice text-center">
                    THB <?= $model->monthly_price ?><br />
                    <p class="priceType"><?= Yii::t('app', 'месяц') ?></p>
                </div>
                <div class="dailyPrice text-center">
                    THB <?= $model->daily_price ?><br />
                    <p class="priceType"><?= Yii::t('app', 'сутки') ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="bottomLine"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="promoteBlock visible-lg-block">
                    <strong><?= Yii::t('app', 'Бронируйте скорее этот вариант') ?>, <span class="text-danger"><?= Yii::t('app', '{x} человек', ['x' => rand(2, 25)]) ?></span> <?= Yii::t('app', 'смотрят это объект вместе с тобой') ?></strong>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="realEstateInfo">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="property" title="<?= Yii::t('app', 'Тип жилья') ?>"><i><?= Html::img('@web/images/house-icon.png') ?></i> <?= Yii::t('app', Lookup::item('RealEstateTypes', $model->type_id)) ?></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="property" title="<?= Yii::t('app', 'Количество гостей') ?>"><i><?= Html::img('@web/images/guest-icon.png') ?></i><?= Yii::t('app', 'Гостей') . ': ' . $model->residents ?></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="property" title="<?= Yii::t('app', 'Количество спален') ?>"><i><?= Html::img('@web/images/door-icon.png') ?></i> <?= Yii::t('app', 'Спален') . ': ' . $model->bedrooms ?></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="property" title="<?= Yii::t('app', 'Бассейн') ?>"><i><?= Html::img('@web/images/pool-icon.png') ?></i><?= $model->pool ? Yii::t('app', 'Есть бассейн') : Yii::t('app', 'Нет бассейна') ?></div>
                                </div>
                                <div class="col-md-2">
                                    <div class="property" title="<?= Yii::t('app', 'Площадь') ?>"><i><?= Html::img('@web/images/square-icon.png') ?></i> <?= $model->square ?></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="property" title="<?= Yii::t('app', 'Количество кроватей') ?>"><i><?= Html::img('@web/images/bed-icon.png') ?></i> <?= Yii::t('app', 'Кроватей') . ': ' . $model->residents ?></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="property" title="<?= Yii::t('app', 'Расстояние до моря') ?>"><i><?= Html::img('@web/images/anchor-icon.png') ?></i><?= Yii::t('app', 'Расстояние до моря') . ': ' . $model->to_the_sea ?></div>
                                </div>
                                <div class="col-md-5">
                                    <div class="property" title="<?= Yii::t('app', 'Свободные даты') ?>"><i><?= Html::img('@web/images/calendar-icon.png') ?></i><?= Yii::t('app', 'Свободные даты') ?> <a href="#" class="showEstateCalendarButton"><span class="glyphicon glyphicon-triangle-bottom pull-right" aria-hidden="true"></span></a></div>
                                    <div class="datesCalendar">
                                        <?php
                                        $booking = json_encode(
                                                $model->getBooking()->select(['start_date', 'end_date'])
                                                        ->andWhere(['<>', 'status', Booking::STATUS_NOT_PAYED])
                                                        ->andWhere(['<>', 'status', Booking::STATUS_CANCELED])
                                                        ->asArray()
                                                        ->all()
                                        );
                                        echo DatePicker::widget([
                                            'name' => 'from_date',
                                            'value' => '',
                                            'inline' => true,
                                            'clientOptions' => [
                                                'beforeShowDay' => new JsExpression("
                                                function(date) {
                                                    var available = 1;
                                                    var booking = $.parseJSON('" . $booking . "');
                                                    var timestamp = (Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()) / 1000) + 86400;

                                                    for(var i = 0; i < booking.length; i++) {
                                                        if(timestamp >= booking[i].start_date && timestamp <= booking[i].end_date) {
                                                            available = 0;
                                                            break;
                                                        }
                                                    }
                                                    return [1, available ? '' : 'highlight', available ? '" . Yii::t('app', 'Свободно') . "' : '" . Yii::t('app', 'Занято') . "'];
                                                }
                                            "),
                                            ],
                                            'containerOptions' => [
                                                'class' => '',
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="#" class="getFullInfo"><?= Yii::t('app', 'Посмотреть полное описание') ?></a>
                                </div>
                            </div>
                            <div class="fullInfo hidden">
                                <?php $comfort = $model->getAllComfortAsArray(); ?>
                                <?php if ($comfort) { ?>
                                    <div class="row realEstateComfort">
                                        <div class="col-md-12">
                                            <ul>
                                                <?php foreach ($comfort as $item) { ?>
                                                    <li class="propertyComfort">
                                                        <?= $item ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php $projectDescription = Translation::t($project, 'description'); ?>
                                <?php if ($projectDescription) { ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3><?= Yii::t('app', 'Описание проекта') ?></h3>
                                            <?= $projectDescription ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $description = Translation::t($model, 'description'); ?>
                <?php if ($description) { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h3><?= Yii::t('app', 'Описание') ?></h3>
                            <div>
                                <?php //@todo выбор описания в зависимости от языка сайта  ?>
                                <?= $description ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php $photos = $model->getAllPhotosAsArray(); ?>
                <?php if ($photos) { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h3><?= Yii::t('app', 'Фотогалерея объекта') ?></h3>
                            <div class="real-estate-gallery">
                                <?php
                                foreach ($photos as $photo) {
                                    echo Html::img('@web' . $photo);
                                }
                                ?>
                            </div>
                            <?php $this->registerJs("$('.real-estate-gallery').fotorama({nav: 'thumbs', fit: 'cover', width: '100%', thumbwidth: 200, thumbheight: 120, thumbmargin: 15});"); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php $videos = $model->getVideos()->all(); ?>
                <?php if ($videos) { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h3><?= Yii::t('app', 'Видеотур объекта') ?></h3>
                            <?php foreach ($videos as $video) { ?>
                                <div class="row" style="margin-bottom: 20px;">
                                    <div class="col-md-12">
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <iframe class="embed-responsive-item" src="<?= $video->getYoutubeFullUrl($video->youtube_id) ?>"></iframe>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($map) { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h3><?= Yii::t('app', 'Объект на карте') ?></h3>

                            <?php
                            echo Tabs::widget([
                                'items' => [
                                    [
                                        'label' => Yii::t('app', 'Карта'),
                                        'content' => '<div id="map" style="width: 100%; height: 400px; margin-bottom: 20px;"></div>',
                                    ],
                                    [
                                        'label' => Yii::t('app', 'Панорама'),
                                        'content' => '<div id="map2" style="width: 100%; height: 400px; margin-bottom: 20px;"></div>',
                                    ],
                                ],
                                'options' => ['tag' => 'div', 'class' => 'mapButtons'],
                                'itemOptions' => ['tag' => 'div'],
                                'headerOptions' => ['class' => 'my-class'],
                                'clientOptions' => ['collapsible' => false],
                            ]);
                            ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3><?= Yii::t('app', 'Отзывы') ?></h3>
                    </div>
                </div>
                <?php
                $a = 0;
                $itemsCount = count($reviews);
                foreach ($reviews as $key => $item) {

                    if ($a >= 5) {
                        break;
                    }

                    echo $this->render('_object_review', [
                        'model' => $item
                    ]);
                    unset($reviews[$key]);
                    $a++;
                }
                ?>
                <?php if ($reviews) { ?>
                    <div class="row" id="viewMoreLinkBlock">
                        <div class="col-md-12 text-center viewAllItems">
                            <div>
                                <?= Html::img('@web/images/view_more_plus.png', ['alt' => 'view more']); ?>
                            </div>
                            <div>
                                <a href="" data-shown="reviewsHidden" data-removed="viewMoreLinkBlock" class="viewMoreLink"><?= \Yii::t('app', 'Показать все отзывы'); ?></a>
                            </div>
                            <div>
                                <span class="viewMoreSummary"><?= \Yii::t('app', 'Показано {x} из {y}', ['x' => 5, 'y' => $itemsCount]) ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="hidden" id="reviewsHidden">
                        <?php
                        foreach ($reviews as $key => $item) {
                            echo $this->render('_object_review', [
                                'model' => $item
                            ]);
                        }
                        ?>
                    </div>
                <?php } ?>
            </div>
            <div class="col-md-3">
                <?php if (!Yii::$app->user->isGuest && !UserLine::findOne(['created_by' => Yii::$app->user->identity->id, 'type_id' => UserLine::TYPE_ID_OBJECT_ADDED_TO_USERLINE, 'model_id' => $model->id])) { ?>
                    <div class="row">
                        <div class="col-md-12" style="padding: 0;">
                            <a class="userLine" href="<?= Url::to(['user-line/create', 'modelId' => $model->id, 'typeId' => UserLine::TYPE_ID_OBJECT_ADDED_TO_USERLINE]) ?>"><i class="fa fa-plus"></i> <?= Yii::t('app', 'сохранить этот объект в строку пользователя') ?></a>
                        </div>
                    </div>
                <?php } ?>
                <?php if (!Yii::$app->user->isGuest) { ?>
                    <div class="row rowOffset">
                        <div class="col-md-12 realEstateBooking">
                            <?= BookingWidget::widget(['realEstateId' => $model->id]); ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3><?= Yii::t('app', 'Информация об оплате') ?></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 realEstatePayment">
                        <div class="row">
                            <div class="col-md-12 heading">
                                <?= Yii::t('app', 'Стоимость') ?>
                            </div>
                        </div>
                        <div class="row summary">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p><?= Yii::t('app', 'Сутки') ?></p>
                                <p><?= Yii::t('app', 'Месяц') ?></p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                <p class="bold">THB <?= $model->daily_price ?></p>
                                <p class="bold">THB <?= $model->monthly_price ?></p>
                            </div>
                        </div>
                        <div class="row fullInfo">
                            <div class="col-md-12">
                                <p><?= Yii::t('app', 'Возвратный депозит') ?>:</p>
                                <p class="bold"><?= Yii::t('app', 'Сумма в выбранной валюте') ?></p>
                                <p><?= Yii::t('app', 'Отмена бронирования') ?>:</p>
                                <p class="bold"><?= \app\models\RealEstate::getCancelBookingList($model->cancel_booking_type) ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($similiarRealEstate) { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h3><?= Yii::t('app', 'Похожие предложения') ?></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 noPadding">
                            <?php foreach ($similiarRealEstate as $realEstate): ?>
                                <div class="similiarRealEstate">
                                    <div class="photo similiarGallery">
                                        <?php
                                        $photos = $realEstate->getRealEstatePhotos()->all();
                                        foreach ($photos as $photo) {
                                            ?>
                                            <div class="real-estate-photo">
                                                <?= Html::a(Html::img('@web' . $photo->src), ['real-estate/view', 'id' => $realEstate->id]) ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="info">
                                        <?= Html::a($realEstate->title, ['real-estate/view', 'id' => $realEstate->id]) ?>
                                        <p><span class="price">$<?= $realEstate->daily_price ?></span> <?= Yii::t('app', 'сутки') ?></p>
                                        <p><span class="price">$<?= $realEstate->monthly_price ?></span> <?= Yii::t('app', 'месяц') ?></p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <?php $this->registerJs("$('.similiarGallery').fotorama({nav: false, fit: 'cover', width: '100%', height: 200});"); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div class="hidden">-->
<?= $this->render('_object_view_booking_modal') ?>
<!--</div>-->

