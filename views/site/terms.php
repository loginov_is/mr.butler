<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use app\models\Page;
use app\widgets\BackgroundWidget;

$this->params['breadcrumbs'][] = $this->title;
?>
<?=
BackgroundWidget::widget([
    'content' => '<h1 class="text-center">' . $this->title . '</h1>',
    'htmlClass' => 'site-dynamic-page',
    'page' => $model
])
?>
<div class="site-terms">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?= $model->content ?>
            </div>
        </div>
    </div>
</div>
