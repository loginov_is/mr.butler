<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\widgets\BackgroundWidget;

$this->params['breadcrumbs'][] = $this->title;
?>
<?=
BackgroundWidget::widget([
    'content' => '<h1 class="text-center">' . $this->title . '</h1>',
    'htmlClass' => 'site-dynamic-page',
    'page' => $page
])
?>
<div class="site-login" style="margin-top: 20px;">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h1><?= Yii::t('app', 'Войти с помощью') ?>:</h1>
                                <?=
                                AuthChoice::widget([
                                    'baseAuthUrl' => ['site/auth'],
                                    'popupMode' => true,
                                ])
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

                                <?php
                                if (($errors = Yii::$app->session->getFlash('error', null, true)) !== null) {
                                    foreach ($errors as $error) {
                                        echo Html::tag('div', $error, ['class' => 'alert alert-danger']);
                                    }
                                }
                                ?>

                                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                                <?= $form->field($model, 'email') ?>

                                <?= $form->field($model, 'password')->passwordInput() ?>

                                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                                <div class="form-group">
                                    <?= Html::submitButton(Yii::t('app', 'Войти'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                                    <?= Html::a(Yii::t('app', 'Зарегистрироваться'), ['/site/signup'], ['class' => 'btn btn-success']) ?>
                                </div>

                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>