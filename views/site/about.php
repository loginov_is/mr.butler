<?php

use app\widgets\MotivationBlocks;
use app\models\MotivationPicture;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use app\widgets\ReviewsWidget;
use app\widgets\ContactsWidget;
use app\models\Translation;

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="whoIsMrbutler" id="whoIsMrButler">
    <div class="container">
        <?=
        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
        <div class="row">
            <div class="col-md-12">
                <h1 class="whoIsMrbutlerTitle text-center"><?= Translation::t($pageAbout, 'who_is_mr_butler_title') ?></h1>
            </div>
        </div>
        <?= MotivationBlocks::widget(['type' => MotivationPicture::TYPE_WHO_IS_MR_BUTLER]) ?>
        <div class="row whoIsMrbutlerDescription">
            <div class="col-md-12">
                <?= nl2br(Translation::t($pageAbout, 'who_is_mr_butler_promotions')) ?>
            </div>
        </div>
    </div>
</div>
<div class="wannaGetToRent" id="wannaGetToRent">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="wannaGetToRentTitle text-center"><?= Translation::t($pageAbout, 'wanna_get_to_rent_title') ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-center"><?= nl2br(Translation::t($pageAbout, 'wanna_get_to_rent_promotions')) ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-xs-12">
                <?= Html::a(\Yii::t('app', 'Передать недвижимость в управление'), ['/real-estate/add'], ['class' => 'realEstateSubmitButton link']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-xs-12">
                <ul class="files">
                    <li>
                        <?= Html::img('@web/images/file_link.jpg', ['class' => 'filePresentationImage']) ?>
                        <?= Html::a(\Yii::t('app', 'Кейс'), 'http://www.youtube.com/watch?v=jrPZhT4lAtA', ['class' => 'filePresentation']) ?>
                    </li>
                    <li>
                        <?= Html::img('@web/images/file_link.jpg', ['class' => 'filePresentationImage']) ?>
                        <?= Html::a(\Yii::t('app', 'Кейс'), 'http://www.youtube.com/watch?v=jrPZhT4lAtA', ['class' => 'filePresentation']) ?>
                    </li>
                    <li>
                        <?= Html::img('@web/images/file_link.jpg', ['class' => 'filePresentationImage']) ?>
                        <?= Html::a(\Yii::t('app', 'Кейс'), 'http://www.youtube.com/watch?v=jrPZhT4lAtA', ['class' => 'filePresentation']) ?>
                    </li>
                    <li>
                        <?= Html::a(\Yii::t('app', 'Видео презентация'), 'http://www.youtube.com/watch?v=jrPZhT4lAtA', ['class' => 'videoPresentation']) ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="wannaRent" id="wannaRent">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="wannaRentTitle text-center"><?= Translation::t($pageAbout, 'wanna_rent_title') ?></h1>
            </div>
        </div>
        <div class="row" style="margin-bottom: 50px;">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p class="description">
                            <?= nl2br(Translation::t($pageAbout, 'wanna_rent_promotions')) ?>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= Html::a(\Yii::t('app', 'Забронировать'), ['/site/search'], ['class' => 'realEstateSubmitButton link']) ?>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="embed-responsive embed-responsive-16by9 wannaRentVideoBlock">
                    <iframe class="embed-responsive-item" src="<?= $pageAbout->wanna_rent_video ?>"></iframe>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="partners" id="partners">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="partnersTitle text-center"><?= Translation::t($pageAbout, 'partners_title') ?></h1>
                <h4 class="text-center"><?= nl2br(Translation::t($pageAbout, 'partners_promotions')) ?></h4>
            </div>
        </div>
        <div class="row pageAboutSlider visible-lg-block">
            <?php foreach ($partners as $partner) { ?>
                <div class="col-md-3 pageAboutSlide text-center">
                    <div class="pageAboutSlideImage">
                        <?= Html::a(Html::img('@web/upload/partner/' . $partner->image, ['class' => 'slideItem', 'title' => Translation::t($partner, 'name')])) ?>
                    </div>
                    <p class="description text-center" style="margin-top: 10px;">
                        <?= nl2br(Translation::t($partner, 'description')) ?>
                    </p>
                </div>
            <?php } ?>
        </div>
        <a class="leftArrow visible-lg-block"><img src="<?= Yii::getAlias('@web/images/about_left_arrow.png') ?>"></a>
        <a class="rightArrow visible-lg-block"><img src="<?= Yii::getAlias('@web/images/about_right_arrow.png') ?>"></a>
        <div class="row hidden-lg">
            <?php foreach ($partners as $partner) { ?>
                <div class="col-md-3 col-sm-12 col-xs-12 pageAboutSlide text-center" style="margin-bottom: 20px;">
                    <div class="pageAboutSlideImage center-block">
                        <?= Html::a(Html::img('@web/upload/partner/' . $partner->image, ['class' => 'slideItem', 'title' => Translation::t($partner, 'name')])) ?>
                    </div>
                    <p class="description text-center" style="margin-top: 10px;">
                        <?= nl2br(Translation::t($partner, 'description')) ?>
                    </p>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="team" id="team">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="teamTitle text-center"><?= Translation::t($pageAbout, 'team_title') ?></h1>
                <h4 class="text-center"><?= nl2br(Translation::t($pageAbout, 'team_promotions')) ?></h4>
            </div>
        </div>
        <div class="row pageAboutSlider visible-lg-block">
            <?php foreach ($team as $mate) { ?>
                <div class="col-md-3 pageAboutSlide text-center">
                    <div class="pageAboutSlideImage">
                        <?= Html::a(Html::img('@web/upload/team/' . $mate->image, ['class' => 'slideItem', 'title' => Translation::t($mate, 'name')])) ?>
                    </div>
                    <p class="teamName text-center">
                        <?= Translation::t($mate, 'name') ?>
                    </p>
                    <p class="teamData text-center">
                        <?= $mate->phone ?>
                    </p>
                    <p class="teamData text-center">
                        <?= $mate->email ?>
                    </p>
                    <p class="teamData text-center">
                        <?= Translation::t($mate, 'position') ?>
                    </p>
                </div>
            <?php } ?>
        </div>
        <a class="leftArrow visible-lg-block"><img src="<?= Yii::getAlias('@web/images/about_left_arrow.png') ?>"></a>
        <a class="rightArrow visible-lg-block"><img src="<?= Yii::getAlias('@web/images/about_right_arrow.png') ?>"></a>
        <div class="row hidden-lg">
            <?php foreach ($team as $mate) { ?>
                <div class="col-md-3 col-sm-12 col-xs-12 pageAboutSlide text-center" style="margin-bottom: 20px;">
                    <div class="pageAboutSlideImage center-block">
                        <?= Html::a(Html::img('@web/upload/team/' . $mate->image, ['class' => 'slideItem', 'title' => Translation::t($mate, 'name')])) ?>
                    </div>
                    <p class="teamName center-block text-center">
                        <?= Translation::t($mate, 'name') ?>
                    </p>
                    <p class="teamData center-block text-center">
                        <?= $mate->phone ?>
                    </p>
                    <p class="teamData center-block text-center">
                        <?= $mate->email ?>
                    </p>
                    <p class="teamData center-block text-center">
                        <?= Translation::t($mate, 'position') ?>
                    </p>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="pressAboutUs" id="pressAboutUs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="pressAboutUsTitle text-center"><?= Translation::t($pageAbout, 'press_title') ?></h1>
            </div>
        </div>
        <div class="row pressAboutUsItems">
            <?php foreach ($pressArticles as $pressArticle) { ?>
                <div class="col-md-6">
                    <div class="pressAboutUsItem">
                        <div class="row">
                            <div class="col-md-4">
                                <?= Html::a(Html::img('@web/upload/press/' . $pressArticle->press->image, ['class' => 'pressAboutUsImage'])) ?>
                            </div>
                            <div class="col-md-8">
                                <p class="date"><?= $pressArticle->dateField ?></p>
                                <p class="link">
                                    <?= Html::a(Translation::t($pressArticle, 'title'), $pressArticle->url, ['target' => '_blank']) ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?= ReviewsWidget::widget() ?>
<?= ContactsWidget::widget() ?>