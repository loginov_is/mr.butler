<?php

use yii\helpers\Html;
use kartik\widgets\StarRating;
?>
<div class="row objectReview">
    <div class="col-md-1 col-xs-2" style="padding-top: 20px;">
        <?php $userAvatar = $model->createdBy->getAvatarUrl(); ?>
        <?= Html::img($userAvatar, ['class' => 'review_avatar']) ?>
    </div>
    <div class="col-md-11 col-xs-10">
        <div class="row">
            <div class="col-md-6 col-xs-12 viewObjectRatingBlock" style="padding-top: 20px;">
                <span class="bold"><?= \Yii::t('app', 'Оценка') ?>: <span class="rating"><?= $model->rating ?></span></span> 
            </div>
            <div class="col-md-6 col-xs-12 text-right viewObjectRatingBlock">
                <?=
                StarRating::widget([
                    'name' => 'rating_1',
                    'value' => $model->rating,
                    'pluginOptions' => ['disabled' => true, 'showClear' => false, 'size' => 'xs'],
                ]);
                ?>
            </div>
        </div>
        <hr />
        <div class="row reviewItems">
            <div class="col-md-12">
                <p class="userName"><?= $model->createdBy->getFullName() ?></p>
                <p class="date"><?= date('d.m.Y | H:i', $model->created_at) ?></p>
                <br />
                <?= nl2br($model->review) ?>
                <?php if ($model->pluses) { ?>
                    <br /><br />
                    <p><div class="plus"><span>+</span></div> <span class="bold"><?= \Yii::t('app', 'Преимущества') ?>:</span></p>
                    <?= nl2br($model->pluses) ?>
                <?php } ?>
                <?php if ($model->minuses) { ?>
                    <br /><br />
                    <p><div class="minus"><span>-</span></div> <span class="bold"><?= \Yii::t('app', 'Недостатки') ?>:</span></p>
                    <?= nl2br($model->minuses) ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>