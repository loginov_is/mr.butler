<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use app\widgets\MotivationBlocks;
use app\models\MotivationPicture;
use app\widgets\BackgroundWidget;

$this->params['breadcrumbs'][] = $this->title;
?>
<?=
BackgroundWidget::widget([
    'content' => '<h1 class="text-center">' . $this->title . '</h1>',
    'htmlClass' => 'site-dynamic-page',
    'page' => $model
])
?>
<div class="site-payments-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center title"><?= \Yii::t('app', 'Варианты оплаты') ?></h3>
            </div>
        </div>
        <?= MotivationBlocks::widget(['type' => MotivationPicture::TYPE_PAY_METHODS]) ?>        
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center title"><?= \Yii::t('app', 'Наши реквизиты') ?></h3>
            </div>
        </div>
        <div class="row model-content">
            <div class="col-md-12">
                <?= $model->content ?>
            </div>
        </div>
    </div>
</div>