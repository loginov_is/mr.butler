<?php
/* @var $this \yii\web\View */

use app\widgets\TopSearchForm;
use app\widgets\MotivationBlocks;
use app\models\MotivationPicture;
use app\widgets\ReviewsWidget;
use app\widgets\BackgroundWidget;

?>

<?=
BackgroundWidget::widget([
    'content' => $this->render('_site_index_title'),
    'htmlClass' => 'site-dynamic-page',
    'page' => $model
])
?>
<?= TopSearchForm::widget(); ?>
<div class="container">
    <?= MotivationBlocks::widget(['type' => MotivationPicture::TYPE_HOW_IT_WORK]) ?>
</div>
<?= ReviewsWidget::widget() ?>
<?= MotivationBlocks::widget(['type' => MotivationPicture::TYPE_BOTTOM_LAYOUT_MOTIVATION]) ?>