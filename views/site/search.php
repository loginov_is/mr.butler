<?php

use app\widgets\TopSearchForm;
use yii\helpers\Html;
use app\widgets\BackgroundWidget;

$this->params['breadcrumbs'][] = $this->title;
?>
<?=
BackgroundWidget::widget([
    'content' => '<h1 class="text-center">' . $this->title . '</h1>',
    'htmlClass' => 'site-dynamic-page',
    'page' => $page
])
?>
<div class="container" style="margin-top: 20px;">
    <div class="row">
        <div class="col-md-12">
            <?= TopSearchForm::widget() ?>
            <hr />
        </div>
    </div>
    <div class="row siteSearchSortBlock">
        <div class="col-md-2 col-md-offset-7 col-xs-12">
            <h4><?= \Yii::t('app', 'Сортировка') ?></h4>
        </div>
        <div class="col-md-3 col-xs-12">
            <?=
            Html::dropDownList('', 1, [
                1 => Yii::t('app', 'по популярности'),
                2 => Yii::t('app', 'от дорогих к дешевым'),
                3 => Yii::t('app', 'ближе к морю'),
                4 => Yii::t('app', 'по рейтингу'),
                    ], ['class' => 'siteSearchSortDropDown form-control'])
            ?>
        </div>
    </div>
    <div class="row">
        <?php if ($model) { ?>
            <?php
            $a = 0;
            foreach ($model as $key => $item) {
                if ($a >= 6) {
                    break;
                }

                echo $this->render('//default/_real_estate_item', ['model' => $item, 'hidden' => false]);

                unset($model[$key]);
                $a++;
            }
            ?>
            <?php $this->registerJs("$('.real-estate-photos').fotorama({nav: false, fit: 'cover', width: 360, height: 283});"); ?>
        <?php } else { ?>
            <p><?= \Yii::t('app', 'По вашему запросу ничего не найдено.') ?></p>
        <?php } ?>
    </div>
    <?php if ($model) { ?>
        <div class="row" id="searchItemsMoreLink">
            <div class="col-md-4 col-xs-0"></div>
            <div class="col-md-4 col-xs-0">
                <?=
                Html::button(\Yii::t('app', 'Показать все доступные предложения'), [
                    'class' => 'realEstateSubmitButton viewMoreLink',
                    'data-shown' => 'searchItemsHidden',
                    'data-removed' => 'searchItemsMoreLink'
                ])
                ?>
            </div>
            <div class="col-md-4 col-xs-0"></div>
        </div>
        <div class="hidden row" id="searchItemsHidden">
            <?php
            foreach ($model as $item) {
                echo $this->render('//default/_real_estate_item', ['model' => $item, 'hidden' => false]);
            }
            ?>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <h3><?= \Yii::t('app', 'Предлагаем посмотреть в других районах города') ?></h3>
        </div>
    </div>
</div>