<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Ticket;
use app\models\User;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Тикеты (тех. поддержка)');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            [
                'attribute' => 'created_at',
                'value' => function($row) {
                    return date('d/m/Y H:i', $row->created_at);
                }
            ],
            [
                'attribute' => 'created_by',
                'filter' => User::find()->select(['email', 'id'])->indexBy('id')->column(),
                'value' => function ($row) {
                    if (isset($row->createdBy->email)) {
                        return $row->createdBy->email;
                    } else {
                        return $row->created_by;
                    }
                }
            ],
            [
                'attribute' => 'viewed_admin',
                'filter' => Ticket::getAdminViewedList(),
                'value' => function ($row) {
                    return Ticket::getAdminViewedList($row['viewed_admin']);
                }
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {delete}', 'options' => ['style' => 'width:60px']],
        ],
    ]);
    ?>

</div>
