<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Добавить участника');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Команда'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
