<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Изменить участника: ') . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Команда'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Изменить');
?>
<div class="team-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
