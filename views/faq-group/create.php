<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Добавить группу');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Группы FAQ'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
