<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Изменить группу');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Группы FAQ'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Изменить');
?>
<div class="faq-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
