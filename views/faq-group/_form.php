<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Language;

?>
<?php $form = ActiveForm::begin() ?>
<?= $form->errorSummary([$model]) ?>
<div class="row">
    <div class="col-md-12">
        <?php echo $form->field($model, 'name')->textInput(); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php
        echo $form->field($model, 'language_id')->dropDownList(
                Language::find()->select(['code', 'id'])->indexBy('id')->column(), [
            'prompt' => \Yii::t('app', 'Выберите язык'),
            'class' => 'form-control'
                ]
        );
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php echo $form->field($model, 'active')->checkbox(); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=
        Html::submitButton(($model->getIsNewRecord()) ? \Yii::t('app', 'Создать') : \Yii::t('app', 'Сохранить'), [
            'class' => 'btn btn-success'
        ]);
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>