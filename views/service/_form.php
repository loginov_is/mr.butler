<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use app\models\Lookup;
use app\models\ServicePrice;

/* @var $this yii\web\View */
/* @var $model app\models\Service */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-form">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?= Yii::t('app', 'Услуга') ?></div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin(); ?>
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        <?php if(!$model->isNewRecord) { ?>
                        <?= $form->field($model, 'price_type_id')->dropDownList(Lookup::items('ServicePriceType')) ?>
                        <?= $form->field($model, 'pricePerMan')->textInput() ?>
                        <?php } ?>
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <?php if(!$model->isNewRecord) { ?>
            <div class="panel panel-default serviceGroupPrices">
                <div class="panel-heading"><?= Yii::t('app', 'Стоимость услуг') ?></div>
                <div class="panel-body">
                    <?= GridView::widget([
                        'dataProvider' => $servicePriceDataProvider,
                        'columns' => [
                            'people_min',
                            'people_max',
                            'price',
                            [
                                'label' => Yii::t('app', 'Действия'),
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return Html::a(Yii::t('app', 'Изменить'), '#', ['class' => 'servicePriceUpdateButton']) . ' ' . Html::a(Yii::t('app', 'Удалить'), ['service/delete-service-price', 'id' => $model->id], ['class' => 'servicePriceDeleteButton']);
                                },
                            ],
                        ],
                    ]); ?>
                    <?= Html::a(Yii::t('app', 'Добавить услугу'), '#', ['class' => 'btn btn-primary servicePriceCreateButton']) ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?= $this->render('_update_service_price_create_modal', ['service' => $model]); ?>
<?= $this->render('_update_service_price_update_modal'); ?>