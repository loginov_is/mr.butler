<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Lookup;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Дополнительные услуги');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить услугу'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
                'id',
            'name',
            [
                'attribute' => 'price_type_id',
                'value' => function($model) {
                    return Lookup::item('ServicePriceType', $model->price_type_id);
                },
            ],

            [
                'class' => \yii\grid\ActionColumn::className(),
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
