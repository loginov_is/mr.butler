<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Html;
?>
<div class="modal fade" id="servicePriceCreateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= Yii::t('app', 'Добавление стоимости услуг') ?></h4>
            </div>
            <?php $form = ActiveForm::begin([
                'action' => ['service/create-service-price'],
                'options' => [
                    'class' => "form-horizontal",
                ],
            ]); ?>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= Html::label(Yii::t('app', 'Минимальное количество людей:'), null, ['class' => 'col-md-6 control-label']) ?>
                                <div class="col-md-6">
                                    <?= Html::textInput('ServicePrice[people_min]', null, ['class' => 'form-control']) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= Html::label(Yii::t('app', 'Максимальное количество людей:'), null, ['class' => 'col-md-6 control-label']) ?>
                                <div class="col-md-6">
                                    <?= Html::textInput('ServicePrice[people_max]', null, ['class' => 'form-control']) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= Html::label(Yii::t('app', 'Стоимость:'), 'bookingFormModal-phone', ['class' => 'col-md-6 control-label']) ?>
                                <div class="col-md-6">
                                    <?= Html::textInput('ServicePrice[price]', null, ['class' => 'form-control']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <?= Html::submitInput(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
                </div>
                <?= Html::hiddenInput('ServicePrice[service_id]', $service->id) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>