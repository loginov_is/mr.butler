<?php

use yii\helpers\Html;
use app\models\Translation;
use app\models\Lookup;
?>
<div class="col-md-4 <?= $hidden ? 'realEstateHidden': '' ?>" style="margin-bottom: 20px;">
    <div class="realEstateItemBlock">
        <div class="appartamentRating"><?= $model->rating ?> <?= $model->getRatingTitle() ?></div>
        <div class="itemPhotos">
            <?php $photos = $model->getRealEstatePhotos()->orderBy('main, index')->all(); ?>
            <?php if ($photos) { ?>
                <div class="appartamentPhoto real-estate-photos">
                    <?php foreach ($photos as $photo) { ?>
                        <div class="real-estate-photo">
                            <?= Html::img('@web' . $photo->src) ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } else { ?>
                <?= Html::img('@web/images/no-photo.jpg', ['class' => 'noPhoto']) ?>
            <?php } ?>
        </div>
        <div class="appartamentDescription">
            <p><span class="price">$<?= $model->daily_price ?></span> ночь</p>
            <p><span class="price">$<?= $model->monthly_price ?></span> месяц</p>
            <?= Html::a(Translation::t($model, 'title'), $model->getUrl()) ?>
            <p class="type"><?= Lookup::item(Lookup::TYPE_REAL_ESTATE_TYPES, $model->type_id) ?></p>
        </div>                                                                                                                                          
    </div>
</div>