<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success" role="alert" style="margin-bottom: 20px; margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <?= Yii::$app->session->getFlash('success') ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger" role="alert" style="margin-bottom: 20px; margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <?= Yii::$app->session->getFlash('error') ?>
    </div>
<?php } ?>


