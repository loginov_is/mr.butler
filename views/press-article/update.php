<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Изменить статью прессы: ') . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пресса о нас'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Изменить');
?>
<div class="press-article-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
