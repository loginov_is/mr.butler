<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Пресса о нас');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="press-article-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить статью прессы'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'press_id',
                'value' => function ($row) {
                    return $row->press->title;
                }
            ],
            'title',
            'url',
            'dateField',
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}', 'options' => ['style' => 'width:60px']],
        ],
    ]);
    ?>

</div>
