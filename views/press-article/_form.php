<?php

use app\widgets\Translation;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Press;
use kartik\widgets\DatePicker;
?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
<?= $form->errorSummary([$model]) ?>
<div class="row">
    <div class="col-md-12">
        <?php
        echo $form->field($model, 'press_id')->dropDownList(
                Press::find()->select(['title', 'id'])->indexBy('id')->column(), [
            'prompt' => \Yii::t('app', 'Выберите прессу'),
            'class' => 'form-control'
                ]
        );
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=
        Translation::widget([
            'field' => $form->field($model, 'title')->textInput(),
        ])
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'url')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=
        $form->field($model, 'dateField')->widget(
                DatePicker::className(), [
            'type' => DatePicker::TYPE_INPUT,
            'language' => 'ru',
            'options' => [
                'placeholder' => 'dd/mm/yyyy',
                'class' => 'form-control fieldAdvance',
            ],
            'pluginOptions' => [
                'format' => 'dd/mm/yyyy',
                'todayHighlight' => true,
            ]
                ]
        )
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=
        Html::submitButton(($model->getIsNewRecord()) ? \Yii::t('app', 'Создать') : \Yii::t('app', 'Сохранить'), [
            'class' => 'btn btn-success'
        ]);
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>