<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Добавить статью прессы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пресса о нас'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="press-article-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
