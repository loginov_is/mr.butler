<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Настройки сайта');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-options-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'feedbackEmails' => $feedbackEmails,
        'ticketEmails' => $ticketEmails,
        'siteImageFormModel' => $siteImageFormModel,
    ])
    ?>

</div>