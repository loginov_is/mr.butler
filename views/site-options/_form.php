<?php

/* @var $siteImageFormModel app\models\SiteImageForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\FeedbackEmail;
use app\models\SiteImage;
?>

<div class="language-form">
    <?= $this->render('//default/flash') ?>
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?= \Yii::t('app', 'Социальные сети'); ?></div>
                <div class="panel-body">
                    <?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'vkontakte')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'youtube')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?= \Yii::t('app', 'Email для отправки обратной связи'); ?></div>
                <div class="panel-body">
                    <div class="feedbackEmailsBlock-<?= FeedbackEmail::TYPE_FEEDBACK ?>">
                        <?php
                        foreach ($feedbackEmails as $feedbackEmail) {
                            echo $this->render('_feedback_email_item', [
                                'model' => $feedbackEmail
                            ]);
                        }
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <?= Html::textInput('email', '', ['class' => 'form-control cloneEmailField-' . FeedbackEmail::TYPE_FEEDBACK]) ?>
                        </div>
                        <div class="col-md-4">
                            <?=
                            Html::button(\Yii::t('app', 'Добавить Email'), [
                                'class' => 'btn btn-success cloneEmailSubmit',
                                'data-url' => Url::to(['/site-options/create-feedback-email']),
                                'data-type' => FeedbackEmail::TYPE_FEEDBACK
                            ])
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><?= \Yii::t('app', 'Email для отправки тикет уведомлений'); ?></div>
                <div class="panel-body">
                    <div class="feedbackEmailsBlock-<?= FeedbackEmail::TYPE_TICKET ?>">
                        <?php
                        foreach ($ticketEmails as $ticketEmail) {
                            echo $this->render('_feedback_email_item', [
                                'model' => $ticketEmail
                            ]);
                        }
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <?= Html::textInput('email', '', ['class' => 'form-control cloneEmailField-' . FeedbackEmail::TYPE_TICKET]) ?>
                        </div>
                        <div class="col-md-4">
                            <?=
                            Html::button(\Yii::t('app', 'Добавить Email'), [
                                'class' => 'btn btn-success cloneEmailSubmit',
                                'data-url' => Url::to(['/site-options/create-feedback-email']),
                                'data-type' => FeedbackEmail::TYPE_TICKET
                            ])
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <?php
    $siteImageForm = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ])
    ?>
    <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?= \Yii::t('app', 'Фоновые изображения'); ?></div>
                <div class="panel-body">
                    <?= Html::img('@web/upload/' . SiteImage::findOne(['type'=>'background_block_default'])->src, ['style' => 'max-width: 100%;']) ?>
                    <?= $siteImageForm->field($siteImageFormModel, 'images[background_block_default]')->fileInput()->label(Yii::t('app', 'Изображение по умолчанию')) ?>

                    <?= Html::img('@web/upload/' . SiteImage::findOne(['type'=>'background_block_contacts'])->src, ['style' => 'max-width: 100%;']) ?>
                    <?= $siteImageForm->field($siteImageFormModel, 'images[background_block_contacts]')->fileInput()->label(Yii::t('app', 'Изображение для страницы Контакты')) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
