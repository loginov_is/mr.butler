<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="row" id="feedbackEmail-<?= $model->type ?>-<?= $model->id ?>" style="margin-bottom: 10px;">
    <div class="col-md-8">
        <?= Html::textInput('', $model->email, ['class' => 'form-control', 'disabled' => true]) ?>
    </div>
    <div class="col-md-4">
        <?=
        Html::button('<i class="fa fa-remove"></i>', [
            'class' => 'btn btn-default cloneEmailRemove',
            'data-id' => $model->id,
            'data-type' => $model->type,
            'data-url' => Url::to(['/site-options/delete-feedback-email'])
        ])
        ?>
    </div>
</div>