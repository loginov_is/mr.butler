<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Menu;
use app\helpers\ContentHelper;
$this->title = Yii::t('app', 'Меню');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<p>
    <?= Html::a(Yii::t('app', 'Добавить меню'), ['create'], ['class' => 'btn btn-success']) ?>
</p>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                [
                    'attribute' => 'page_id',
                    'value' => function ($data) {
                        return $data->page->title;
                    }
                ],
                [
                    'attribute' => 'active',
                    'value' => function ($data) {
                        return ContentHelper::getActiveListData($data->active);
                    }
                ],
                [
                    'attribute' => 'language_id',
                    'value' => function ($data) {
                        return $data->language->code;
                    }
                ],
                [
                    'attribute' => 'order',
                    'format' => 'html',
                    'value' => function ($data) use ($type, $language, $dataProvider) {
                        return $this->render('_orderButtons', [
                                    'id' => $data->id,
                                    'type' => $type,
                                    'languageId' => $language->id,
                        ]);
                    }
                        ],
                        [
                            'class' => \yii\grid\ActionColumn::className(),
                            'template' => '{update} {delete}',
                        ],
                    ],
                ])
                ?>