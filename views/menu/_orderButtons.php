<?php

use yii\helpers\Html;

echo Html::a('<span class="glyphicon glyphicon-upload"></span>', ['/menu/set-menu-order-up', 'id' => $id, 'typeId' => $type, 'languageId' => $languageId], ['style' => 'margin-right:10px;']);
echo Html::a('<span class="glyphicon glyphicon-download"></span>', ['/menu/set-menu-order-down', 'id' => $id, 'typeId' => $type, 'languageId' => $languageId]);
