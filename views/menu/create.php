<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Добавить меню');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Меню'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
