<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Изменить запись');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Бухгалтерия'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Изменить');
?>
<div class="accounting-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
