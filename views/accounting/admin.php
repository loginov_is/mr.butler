<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;
use app\models\Accounting;
use app\models\Lookup;
use app\models\Translation;

$this->title = Yii::t('app', 'Бухгалтерия');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index">

    <h1><?= Html::encode($this->title) ?></h1>
<p>
    <?= Html::a(Yii::t('app', 'Создать запись'), ['create'], ['class' => 'btn btn-success']) ?>
</p>
    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'amount',
            'created_at:datetime',
            [
                'attribute' => 'created_by',
                'filter' => User::find()->select(['concat (id,". ",email)', 'id'])->indexBy('id')->column(),
                'value' => function ($row) {
            if (isset($row->createdBy->email)) {
                return $row->createdBy->email;
            } else {
                return $row->created_by;
            }
        }
            ],
            [
                'attribute' => 'user_id',
                'filter' => User::find()->select(['concat (id,". ",email)', 'id'])->indexBy('id')->column(),
                'value' => function ($row) {
            if (isset($row->user->email)) {
                return $row->user->email;
            } else {
                return $row->user_id;
            }
        }
            ],
            [
                'attribute' => 'lookup_id',
                'filter' => Lookup::items(Lookup::TYPE_ACCOUNTING_TEMPLATE, true),
                'value' => function ($row) {
                    if (isset($row->lookup->name)) {
                        return $row->lookup->name;
                    } else {
                        return $row->lookup_id;
                    }
                }
            ],
            [
                'attribute' => 'operation',
                'filter' => Accounting::getOperationList(),
                'value' => function ($row) {
                    return Accounting::getOperationList($row->operation);
                }
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {update}', 'options' => ['style' => 'width:60px']],
        ],
    ]);
    ?>

</div>
