<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Accounting;
use app\models\Lookup;
use app\widgets\AdminLogsWidget;
?>
<?php $form = ActiveForm::begin() ?>
<?= $form->errorSummary([$model]) ?>
<div class="row">
    <div class="col-md-12">
        <?php
        echo $form->field($model, 'user_id')->dropDownList(
                User::find()->select(['concat (id,". ",email)', 'id'])->indexBy('id')->column(), [
            'prompt' => \Yii::t('app', 'Выберите пользователя'),
            'class' => 'form-control',
            'disabled' => !$model->isNewRecord
                ]
        );
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php
        echo $form->field($model, 'operation')->dropDownList(
                Accounting::getOperationList(), [
            'prompt' => \Yii::t('app', 'Выберите операцию'),
            'class' => 'form-control',
            'disabled' => !$model->isNewRecord
                ]
        );
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php
        echo $form->field($model, 'lookup_id')->dropDownList(
                Lookup::items(Lookup::TYPE_ACCOUNTING_TEMPLATE, true), [
            'prompt' => \Yii::t('app', 'Выберите шаблон'),
            'class' => 'form-control'
                ]
        );
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'amount')->textInput(['disabled' => !$model->isNewRecord]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=
        Html::submitButton(($model->getIsNewRecord()) ? \Yii::t('app', 'Создать') : \Yii::t('app', 'Сохранить'), [
            'class' => 'btn btn-success'
        ]);
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?=
AdminLogsWidget::widget([
    'className' => $model::className(),
    'modelId' => $model->id
])
?>
