<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Создать запись');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Бухгалтерия'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accounting-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
