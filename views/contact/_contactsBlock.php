<?php
/* @var $this yii\web\View */
/* @var $model \app\models\Contact */

use yii\helpers\Html;
use app\models\SiteImage;
?>

<div class="carousel-wrapper">
    <div class="contactsBlock" style="background-image: url(<?= Yii::getAlias('@web/upload/' . SiteImage::findOne(['type'=>'background_block_contacts'])->src) ?>);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?= Html::encode($this->title) ?></h1>
                </div>
            </div>
            <div class="row contactsPanel">
                <div class="col-md-6 col-xs-12">
                    <div class="row">
                        <div class="col-md-2 images text-center col-xs-3 item">
                            <?= Html::img('@web/images/contacts_map.png', ['alt' => 'Map']); ?>
                        </div>
                        <div class="col-md-10 col-xs-9 itemText">
                            <?= nl2br($model->address); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 images text-center col-xs-3 item">
                            <?= Html::img('@web/images/contacts_phone.png', ['alt' => 'Phone']); ?>
                        </div>
                        <div class="col-md-10 col-xs-9 itemText">
                            <p class="link"><?= $model->phone; ?></p>
                            <small>(<?= \Yii::t('app', 'русский, английский') ?>)</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 images text-center col-xs-3 item">
                            <?= Html::img('@web/images/contacts_email.png', ['alt' => 'Email']); ?>
                        </div>
                        <div class="col-md-10 col-xs-9 itemText">
                            <p class="link"><?= $model->email; ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 images text-center col-xs-3 item">
                            <?= Html::img('@web/images/contacts_skype.png', ['alt' => 'Skype']); ?>
                        </div>
                        <div class="col-md-10 col-xs-9 itemText">
                            <p class="link"><?= $model->skype; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12 hidden-xs" style="padding: 20px;">
                    <div class=map" id="map" style="min-height: 350px;">
                    </div>             
                </div>
            </div>
            <div class="row visible-xs yMapXs">
                <div class="col-md-12 map">
                </div>
            </div>
        </div>
    </div>
</div>