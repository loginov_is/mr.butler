<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contact */

$this->title = Yii::t('app', 'Изменить контакт');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Управление контактами'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Изменить');
?>
<div class="contact-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
