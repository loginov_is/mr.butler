<?php
/* @var $this yii\web\View */
/* @var $model \app\models\Contact */


$this->title = Yii::t('app', 'Контакты');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-page">
    <?= $this->render('_contactsBlock', ['model' => $model]); ?>
</div>