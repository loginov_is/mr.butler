<?php

use app\widgets\Translation;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Lookup;

/* @var $this yii\web\View */
/* @var $model app\models\Lookup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lookup-form">
    <?php $form = ActiveForm::begin() ?>
    <?= $form->errorSummary([$model]) ?>
    <?=
    Translation::widget([
        'field' => $form->field($model, 'name')->textInput(['maxlength' => true]),
    ])
    ?>
    <?=
    $form->field($model, 'type')->dropDownList(
            Lookup::types(), [
        'prompt' => \Yii::t('app', 'Выберите тип'),
        'class' => 'form-control'
            ]
    )
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
