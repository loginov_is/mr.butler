<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\widgets\Translation;

/* @var $this yii\web\View */
/* @var $model app\models\PageAbout */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary([$model]) ?>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?= \Yii::t('app', 'Кто такой Mr Butler?') ?></div>
                <div class="panel-body">
                    <?=
                    Translation::widget([
                        'field' => $form->field($model, 'who_is_mr_butler_title')->textInput(),
                    ])
                    ?>
                    <?=
                    Translation::widget([
                        'field' => $form->field($model, 'who_is_mr_butler_promotions')->textArea(),
                    ])
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?= \Yii::t('app', 'Хотите сдать свою недвижимость в Таиланде в доверительное управление?') ?></div>
                <div class="panel-body">
                    <?=
                    Translation::widget([
                        'field' => $form->field($model, 'wanna_get_to_rent_title')->textInput(),
                    ])
                    ?>
                    <?=
                    Translation::widget([
                        'field' => $form->field($model, 'wanna_get_to_rent_promotions')->textArea(),
                    ])
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?= \Yii::t('app', 'Вы хотите снять недвижимость в Таиланде для себя и своих близих?') ?></div>
                <div class="panel-body">
                    <?=
                    Translation::widget([
                        'field' => $form->field($model, 'wanna_rent_title')->textInput(),
                    ])
                    ?>
                    <?=
                    Translation::widget([
                        'field' => $form->field($model, 'wanna_rent_promotions')->textarea(),
                    ])
                    ?>
                    <?= $form->field($model, 'wanna_rent_video')->textInput() ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?= \Yii::t('app', 'Партнеры') ?></div>
                <div class="panel-body">
                    <?=
                    Translation::widget([
                        'field' => $form->field($model, 'partners_title')->textInput(),
                    ])
                    ?>
                    <?=
                    Translation::widget([
                        'field' => $form->field($model, 'partners_promotions')->textArea(),
                    ])
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?= \Yii::t('app', 'Команда') ?></div>
                <div class="panel-body">
                    <?=
                    Translation::widget([
                        'field' => $form->field($model, 'team_title')->textInput(),
                    ])
                    ?>
                    <?=
                    Translation::widget([
                        'field' => $form->field($model, 'team_promotions')->textArea(),
                    ])
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?= \Yii::t('app', 'Пресса о нас') ?></div>
                <div class="panel-body">
                    <?=
                    Translation::widget([
                        'field' => $form->field($model, 'press_title')->textInput(),
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
