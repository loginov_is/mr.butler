<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PageAbout */

$this->title = Yii::t('app', 'Изменить страницу "О нас"');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cтраницы'), 'url' => ['/page/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
