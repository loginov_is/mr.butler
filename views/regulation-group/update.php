<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RegulationGroup */

$this->title = Yii::t('app', 'Изменить группу') . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Управление группами правил'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Изменить');
?>
<div class="regulation-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
