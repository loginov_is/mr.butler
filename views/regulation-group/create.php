<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RegulationGroup */

$this->title = Yii::t('app', 'Добавить группу правил');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Управление группами правил'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="regulation-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
