<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Добавить город');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Города'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
