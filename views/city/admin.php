<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Country;

$this->title = Yii::t('app', 'Города');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить город'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            [
                'attribute' => 'country_id',
                'filter' => Country::find()->select(['name', 'id'])->indexBy('id')->column(),
                'value' => function ($data) {
            return $data->country->name;
        }
            ],
            [
                'class' => \yii\grid\ActionColumn::className(),
                'template' => '{update} {delete}',
            ],
        ],
    ])
    ?>
</div>