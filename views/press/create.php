<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Добавить прессу');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пресса'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="press-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
