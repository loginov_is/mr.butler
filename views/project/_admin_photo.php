<?php

use yii\helpers\Html;
?>

<?php $mainClass = ($model->main) ? "photo-item-main" : ""; ?>

<li id="' . $model->id . '" class="photo-item <?= $mainClass ?>">
    <?= Html::a(Html::img('@web' . $model->src, ['class' => '']), '@web' . $model->src, ['class' => 'photo-item-gallery thumbnail']) ?>
    <div class="text-center">
        <?php
        if (!$model->main) {
            echo Html::a(\Yii::t('app', 'Сделать главной'), ['project-photo/set-main', 'id' => $model->id], ['class' => 'photo-setmain btn btn-primary btn-xs']);
        } else {
            echo Html::button(\Yii::t('app', 'Главное фото'), ['class' => 'photo-setmain btn btn-success btn-xs', 'disabled' => true]);
        }

        if (!$model->head_slider) {
            echo Html::a(\Yii::t('app', 'Показывать в слайдере'), ['project-photo/set-slider', 'id' => $model->id, 'show' => 1], ['class' => 'btn btn-warning btn-xs', 'style' => 'margin-left:20px;']);
        } else {
            echo Html::a(\Yii::t('app', 'Не показывать в слайдере'), ['project-photo/set-slider', 'id' => $model->id, 'show' => 0], ['class' => 'btn btn-warning btn-xs', 'style' => 'margin-left:20px;']);
        }

        echo Html::a(\Yii::t('app', 'Удалить'), ['project-photo/delete', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger', 'style' => 'margin-left:20px;']);
        ?>
    </div>
</li>
<hr />
