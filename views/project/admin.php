<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\City;
use app\models\Region;

$this->title = Yii::t('app', 'Проекты');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<p>
    <?= Html::a(Yii::t('app', 'Добавить проект'), ['create'], ['class' => 'btn btn-success']) ?>
</p>
<?=
GridView::widget([
    'filterModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'name',
        [
            'attribute' => 'city_id',
            'filter' => City::find()->select(['name', 'id'])->indexBy('id')->column(),
            'value' => function ($data) {
        if (isset($data->city->name)) {
            return $data->city->name;
        } else {
            return $data->city_id;
        }
    }
        ],
        [
            'attribute' => 'region_id',
            'filter' => Region::find()->select(['name', 'id'])->indexBy('id')->column(),
            'value' => function ($data) {
        if (isset($data->region->name)) {
            return $data->region->name;
        } else {
            return $data->region_id;
        }
    }
        ],
        [
            'class' => \yii\grid\ActionColumn::className(),
            'template' => '{update} {delete}',
        ],
    ],
])
?>