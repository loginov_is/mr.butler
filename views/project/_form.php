<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\City;
use app\models\Region;
use app\models\Lookup;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php
    $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ])
    ?>

    <?= $form->errorSummary([$model]) ?>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(City::find()->all(), 'id', 'name'), [
                        'prompt' => '- ' . $model->getAttributeLabel('city_id') . ' -',
                        'data' => [
                            'ajax-option' => true,
                            'target' => Html::getInputId($model, 'region_id'),
                        ],
                    ])
                    ?>
                </div>
                <div class="col-md-6">
                    <?=
                    $form->field($model, 'region_id')->dropDownList(ArrayHelper::map(Region::find()->where(['city_id' => $model->city_id])->all(), 'id', 'name'), [
                        'prompt' => '- ' . $model->getAttributeLabel('region_id') . ' -',
                        'data' => [
                            'url' => Url::to(['/project/ajax-region']),
                        ],
                    ])
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?=
                    $form->field($model, 'description')->widget(yii\imperavi\Widget::className(), [
                        'options' => [
                            'lang' => 'ru',
                            'minHeight' => 200,
                            'imageUpload' => Url::to([
                                'image/upload',
                                'type' => Inflector::camel2id(StringHelper::basename(get_class($model))),
                                'id' => $model->isNewRecord ? 'tmp' : $model->id
                            ]),
                            /* 'fileUpload' => Url::to([
                              'file/upload',
                              'type' => Inflector::camel2id(StringHelper::basename(get_class($model))),
                              'id' => $model->isNewRecord ? 'new' : $model->id
                              ]), */
                            'uploadImageFields' => [
                                Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                            ],
                            'convertImageLinks' => true,
                            'convertVideoLinks' => true,
                            //'imageGetJson' => Url::to(['/admin/gallery/json-list']),
                            //'imageManagerJson' => Url::to(['/admin/gallery/json-list']),
                            //'fileManagerJson' => Url::to(['/admin/gallery/json-list']),
                            'imageUploadCallback' => $model->isNewRecord ? new \yii\web\JsExpression(<<<JS
                function(image, json) {
                    $(".page-form")
                        .find('form')
                        .append($('<input type="hidden" name="images[]" value="" />').val($(image).attr("src")));
                }
JS
                                    ) : false,
                        //'definedLinks' => '/defined-links.json', // @todo Action to site pages
                        ],
                        'plugins' => [
                            'fullscreen',
                            'table',
                            'video',
                            'imagemanager',
                            'filemanager',
                            'definedlinks',
                            'fontsize',
                            'fontfamily',
                            'fontcolor',
                        //'clips',
                        ]
                    ])
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'map')->hiddenInput(['maxlength' => true, 'class' => 'hiddenCoords']) ?>
                    <div id="map" style="width: 100%; height: 400px; margin-bottom: 20px;"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <p class="text-bold"><?= \Yii::t('app', 'Фотографии (размер фото от 10кб до 10мб)') ?></p>
                        </div>
                        <div class="panel-body">
                            <ul id="sortable" style="padding-left: 0px;">
                                <?php
                                $photos = $model->getProjectPhotos()->all();
                                foreach ($photos as $photo) {
                                    echo $this->render('_admin_photo', [
                                        'model' => $photo
                                    ]);
                                }
                                ?>
                            </ul>

                            <?= $form->field($model, 'images[]')->fileInput(['multiple' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <?= $form->field($model, 'comfort_ids')->checkBoxList(Lookup::items(Lookup::TYPE_HOUSING_COMFORT, true), ['multiple' => true]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
