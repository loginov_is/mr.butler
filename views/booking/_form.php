<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use app\models\Lookup;
use app\models\RealEstate;
use app\models\User;
use app\models\Booking;

/* @var $this yii\web\View */
/* @var $model app\models\Booking */
/* @var $form yii\widgets\ActiveForm */

$services = $model->getServiceOrders()->all();
?>

<div class="booking-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')->dropDownList([
        Booking::STATUS_NOT_PAYED => Booking::STATUS_NOT_PAYED,
        Booking::STATUS_PREPAYED => Booking::STATUS_PREPAYED,
        Booking::STATUS_PAYED => Booking::STATUS_PAYED,
        Booking::STATUS_FINISHED => Booking::STATUS_FINISHED,
        Booking::STATUS_CANCELED => Booking::STATUS_CANCELED,
    ]) ?>

    <div class="form-group">
        <?= Html::activeLabel($model, 'real_estate_id') ?>
        <?= Html::textInput(null, RealEstate::findOne($model->real_estate_id)->title, ['class' => 'form-control', 'disabled' => 'disabled']) ?>
    </div>

    <div class="form-group">
        <?= Html::activeLabel($model, 'source_type') ?>
        <?= Html::textInput(null, $model->source_type, ['class' => 'form-control', 'disabled' => 'disabled']) ?>
    </div>

    <?= $form->field($model, 'user_phone')->textInput() ?>

    <?=
    $form->field($model, 'start_date')->widget(
        DatePicker::className(), [
            'type' => DatePicker::TYPE_INPUT,
            'language' => Yii::$app->language,
            'options' => [
                'placeholder' => 'mm/dd/yyyy',
                'class' => 'form-control',
                'value' => date('m/d/Y', $model->start_date),
            ],
            'pluginOptions' => [
                'format' => 'mm/dd/yyyy',
                'todayHighlight' => true,
            ]
        ]
    )
    ?>

    <?=
    $form->field($model, 'end_date')->widget(
        DatePicker::className(), [
            'type' => DatePicker::TYPE_INPUT,
            'language' => Yii::$app->language,
            'options' => [
                'placeholder' => 'mm/dd/yyyy',
                'class' => 'form-control',
                'value' => date('m/d/Y', $model->end_date),
            ],
            'pluginOptions' => [
                'format' => 'mm/dd/yyyy',
                'todayHighlight' => true,
            ]
        ]
    )
    ?>

    <?= $form->field($model, 'rent_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?=
    $form->field($model, 'end_date')->widget(
        DatePicker::className(), [
            'type' => DatePicker::TYPE_INPUT,
            'language' => Yii::$app->language,
            'options' => [
                'placeholder' => 'mm/dd/yyyy',
                'class' => 'form-control',
                'value' => date('m/d/Y', $model->created_at),
            ],
            'pluginOptions' => [
                'format' => 'mm/dd/yyyy',
                'todayHighlight' => true,
            ]
        ]
    )
    ?>

    <div class="form-group">
        <?= Html::activeLabel($model, 'created_by') ?>
        <?= Html::textInput(null, User::findOne($model->created_by)->first_name . ' ' . User::findOne($model->created_by)->last_name, ['class' => 'form-control', 'disabled' => 'disabled']) ?>
    </div>

    <?php if($services) { ?>
        <div class="form-group">
            <div class="panel panel-default">
                <div class="panel-heading"><?= Yii::t('app', 'Дополнительные услуги') ?></div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th class="text-right"><?= Yii::t('app', 'Название услуги') ?></th>
                            <th class="text-center"><?= Yii::t('app', 'Количество человек') ?></th>
                            <th class="text-center"><?= Yii::t('app', 'Стоимость') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($services as $service) { ?>
                            <tr>
                                <td class="text-right"><?= $service->getServiceName() ?></td>
                                <td class="text-center"><?= $service->people_count ?></td>
                                <td class="text-center"><?= $service->price ?>$</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>



    <?php ActiveForm::end(); ?>

</div>
