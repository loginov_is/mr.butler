<?php

/* @var $model app\models\Booking */

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Lookup;
use app\models\RealEstate;
use app\models\User;
use app\models\Booking;

$this->title = Yii::t('app', 'Бронирование');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<?=
GridView::widget([
    'filterModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        [
            'attribute' => 'status',
            'value' => $model->status,
            'filter' => [
                Booking::STATUS_NOT_PAYED => Booking::STATUS_NOT_PAYED,
                Booking::STATUS_PREPAYED => Booking::STATUS_PREPAYED,
                Booking::STATUS_PAYED => Booking::STATUS_PAYED,
                Booking::STATUS_FINISHED => Booking::STATUS_FINISHED,
                Booking::STATUS_CANCELED => Booking::STATUS_CANCELED,
            ],
        ],
        [
            'attribute' => 'real_estate_id',
            'filter' => RealEstate::find()->select(['title'])->column(),
            'value' => function($model) {
                return RealEstate::findOne($model->real_estate_id)->title;
            }
        ],
        [
            'attribute' => 'created_at',
            'value' => function($model) {
                return date('d.m.Y', $model->end_date);
            }
        ],
        [
            'attribute' => 'start_date',
            'value' => function($model) {
                return date('d.m.Y', $model->start_date);
            }
        ],
        [
            'attribute' => 'end_date',
            'value' => function($model) {
                return date('d.m.Y', $model->end_date);
            }
        ],
        'user_phone',
        [
            'attribute' => 'created_by',
            'filter' => User::find()->select(['last_name'])->column(),
            'value' => function($model) {
                $user = User::findOne($model->created_by);
                return $user->first_name . ' ' . $user->last_name;
            }
        ],
        'comment',
        'rent_price',
        [
            'class' => \yii\grid\ActionColumn::className(),
            'template' => '{update} {delete}',
        ],
    ],
])
?>