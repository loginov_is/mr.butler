<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Изменить платежную систему: ') . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Платежные системы'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Изменить');
?>
<div class="payment-system-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
