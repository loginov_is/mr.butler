<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Добавить платежную систему');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Платежные системы'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-system-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
