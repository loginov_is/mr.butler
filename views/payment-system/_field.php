<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="row" style="margin-top: 5px;" id="field-<?= $model->id ?>">
    <div class="col-md-8">
        <?= Html::textInput('', $model->title, ['class' => 'form-control', 'disabled' => true]) ?>
    </div>
    <div class="col-md-4">
        <button class="btn btn-default removePaymentField" data-url="<?= Url::to(['payment-system/remove-field']) ?>" data-id="<?= $model->id ?>">
            <i class="fa fa-remove"></i>
        </button>
    </div>
</div>