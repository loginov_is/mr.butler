<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Платежные системы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-system-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить платежную систему'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            [
                'attribute' => 'fields',
                'format' => 'raw',
                'value' => function ($row) {
                    $content = '';
                    $content .= Html::beginTag('div', ['class' => 'row']);
                    $content .= Html::beginTag('div', ['class' => 'col-md-8']);
                    $content .= Html::textInput('', '', ['class' => 'cloneField-' . $row->id . ' form-control']);
                    $content .= Html::endTag('div');
                    $content .= Html::beginTag('div', ['class' => 'col-md-4']);
                    $content .= Html::button(\Yii::t('app', 'Добавить поле'), [
                                'class' => 'btn btn-success clonePaymentFieldSubmit',
                                'data-url' => Url::to(['/payment-system/create-field']),
                                'data-system-id' => $row->id
                    ]);
                    $content .= Html::endTag('div');
                    $content .= Html::endTag('div');
                    $content .= Html::beginTag('div', ['class' => 'row']);
                    $content .= Html::beginTag('div', ['class' => 'col-md-12']);
                    $content .= Html::beginTag('div', ['class' => 'paymentSystemFieldsBlock-' . $row->id]);
                    foreach ($row->paymentSystemFields as $field) {
                        $content .= $this->render('_field', ['model' => $field]);
                    }
                    $content .= Html::endTag('div');
                    $content .= Html::endTag('div');
                    $content .= Html::endTag('div');
                    return $content;
                }
                    ],
                    [
                        'attribute' => 'image',
                        'format' => 'html',
                        'value' => function ($row) {
                            return Html::img('@web/upload/payment_system/' . $row->image, ['style' => 'height: 100px;']);
                        }
                            ],
                            [
                                'attribute' => 'language_id',
                                'format' => 'html',
                                'value' => function ($row) {
                                    return isset($row->language->name) ? $row->language->name : $row->language_id;
                                }
                            ],
                            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}', 'options' => ['style' => 'width:60px']],
                        ],
                    ]);
                    ?>

</div>
