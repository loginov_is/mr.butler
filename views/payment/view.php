<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\ListView;
use app\models\Payment;
$this->title = \Yii::t('app', 'Просмотр запроса на выплату от {date}', ['date' => date('d/m/Y H:i', $model->created_at)]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Запросы на выплату'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Просмотр');
?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-12 col-xs-12">
            <div class="row">
                <div class="col-md-12">
                    <?= $this->render('//default/_top_banner', ['title' => $this->title]) ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="row">
                                <div class="col-md-6 text-left">
                                    <p class="text-muted"><i class="fa fa-user"></i> <?= isset($model->createdBy->email) ? $model->createdBy->email : \Yii::t('app', 'Неопределено') ?></p>
                                </div>
                                <div class="col-md-6 text-right">
                                    <p class="text-muted"><i class="fa fa-globe"></i> <?= date('d/m/Y H:i', $model->created_at) ?></p>
                                </div>
                            </div>  
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="bold"><?= \Yii::t('app', 'Сумма') ?>: THB <?= $model->amount ?></p>
                                    <?php if ($model->requisites) { ?>
                                        <?= $model->requisites ?>
                                    <?php } else { ?>
                                        <p><?= \Yii::t('app', 'Реквизиты не указаны') ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($model->status_id == Payment::STATUS_NO_REQUISITES) { ?>
                                <div class="alert alert-danger"><?= \Yii::t('app', 'Операции невозможны. Пользователь не подтвердил запрос') ?></div>
                            <?php } else if ($model->status_id == Payment::STATUS_COMPLETE) { ?>
                                <div class="alert alert-success"><?= \Yii::t('app', 'Операции выполнена. Со счета пользователя списаны средства') ?></div>
                            <?php } else if ($model->status_id == Payment::STATUS_DENIED) { ?>
                                <div class="alert alert-warning"><?= \Yii::t('app', 'Операции отклонена. Средства возвращены на счет пользователя') ?></div>
                            <?php } else if ($model->status_id == Payment::STATUS_PENDING) { ?>
                                <?php
                                echo Html::a(\Yii::t('app', 'Выполнено'), '#', [
                                    'class' => 'btn btn-success bootbox',
                                    'bootbox-url' => Url::to(['payment/complete/', 'id' => $model->id]),
                                    'bootbox-message' => \Yii::t('app', 'Подтверждение выполнения запроса. Со счета пользователя будут списаны указанные средства')
                                ]);
                                ?>
                                <?php
                                echo Html::a(\Yii::t('app', 'Отклонено'), '#', [
                                    'class' => 'btn btn-danger bootbox',
                                    'bootbox-url' => Url::to(['payment/denied/', 'id' => $model->id]),
                                    'bootbox-message' => \Yii::t('app', 'Подтверждение отклонения запроса. На счет пользователя будут возвращены указанные средства')
                                ]);
                                ?> 
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->errorSummary([$commentModel]) ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($commentModel, 'message')->textarea(['class' => 'form-control', 'rows' => 5]) ?>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-3 col-xs-12">
                            <?=
                            Html::submitButton(\Yii::t('app', 'Отправить'), [
                                'class' => 'btn btn-default realEstateSubmitButton'
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item'],
                'itemView' => function ($model, $key, $index, $widget) {
            return $this->render('_profile_comment', ['model' => $model]);
        },
            ])
            ?>
        </div>
    </div>
</div>