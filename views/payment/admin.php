<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Payment;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Запросы на выплату');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'amount',
            [
                'attribute' => 'created_at',
                'value' => function($row) {
                    return date('d/m/Y H:i', $row->created_at);
                }
            ],
            [
                'attribute' => 'created_by',
                'value' => function ($row) {
                    if (isset($row->createdBy->email)) {
                        return $row->createdBy->email;
                    } else {
                        return $row->created_by;
                    }
                }
            ],
            [
                'attribute' => 'payment_system_id',
                'format' => 'html',
                'value' => function ($row) {
                    if (isset($row->paymentSystem->title)) {
                        return $row->paymentSystem->title;
                    } else {
                        return Html::tag('span', \Yii::t('app', 'Удалена'), ['class' => 'text-muted']);
                    }
                }
                    ],
                    [
                        'attribute' => 'status_id',
                        'filter' => Payment::getList(),
                        'value' => function ($row) {
                            return Payment::getList($row->status_id);
                        }
                    ],
                    ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {delete}', 'options' => ['style' => 'width:60px']],
                ],
            ]);
            ?>

</div>
