<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin() ?>
<?= $form->errorSummary([$model]) ?>
<div class="row">
    <div class="col-md-12">
        <?php echo $form->field($model, 'name')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=
        Html::submitButton(($model->getIsNewRecord()) ? \Yii::t('app', 'Создать') : \Yii::t('app', 'Сохранить'), [
            'class' => 'btn btn-success'
        ]);
        ?>
    </div>
</div>
<?php if (!$model->getIsNewRecord()) { ?>
    <div class="row" style="margin-top: 20px;">
        <div class="col-md-12">
            <h1><?php echo \Yii::t('app', 'Сортировка изображений'); ?></h1>
            <?php
            echo Html::beginTag('table', ['class' => 'table table-striped table-bordered']);
            $a = 0;
            foreach ($model->albumImages as $image) {
                $a++;

                echo Html::beginTag('tr');
                echo Html::beginTag('td');
                echo Html::img('@web/upload/' . $image->image, ['alt' => $image->image, 'class' => 'gridImage']);
                echo Html::endTag('td');
                echo Html::beginTag('td');
                $buttonUp = Html::a('<span class="glyphicon glyphicon-upload"></span>', ['/album/set-image-order-up', 'imageId' => $image->id, 'albumId' => $model->id]);
                $buttonDown = Html::a('<span class="glyphicon glyphicon-download"></span>', ['/album/set-image-order-down', 'imageId' => $image->id, 'albumId' => $model->id]);

                if ($a == 1) {
                    echo $buttonDown;
                } else if ($a == count($model->albumImages)) {
                    echo $buttonUp;
                } else {
                    echo $buttonDown;
                    echo $buttonUp;
                }
                echo Html::endTag('td');
                echo Html::endTag('tr');
            }
            echo Html::endTag('table');
            ?>
        </div>
    </div>
<?php } ?>
<?php ActiveForm::end(); ?>