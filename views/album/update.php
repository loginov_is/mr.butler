<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Изменить альбом');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Альбомы'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Изменить');
?>
<div class="album-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
