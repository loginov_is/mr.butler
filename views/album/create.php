<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Добавить альбом');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Альбомы'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="album-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
