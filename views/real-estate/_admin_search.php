<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RealEstateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="real-estate-search">

    <?php $form = ActiveForm::begin([
        'action' => ['admin'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'project_id') ?>

    <?= $form->field($model, 'status_id') ?>

    <?= $form->field($model, 'type_id') ?>

    <?= $form->field($model, 'house') ?>

    <?php // echo $form->field($model, 'flat_number') ?>

    <?php // echo $form->field($model, 'square') ?>

    <?php // echo $form->field($model, 'floor') ?>

    <?php // echo $form->field($model, 'bedrooms') ?>

    <?php // echo $form->field($model, 'bathrooms') ?>

    <?php // echo $form->field($model, 'furnishings') ?>

    <?php // echo $form->field($model, 'pool') ?>

    <?php // echo $form->field($model, 'description_ru') ?>

    <?php // echo $form->field($model, 'description_en') ?>

    <?php // echo $form->field($model, 'information') ?>

    <?php // echo $form->field($model, 'owner_name') ?>

    <?php // echo $form->field($model, 'owner_email') ?>

    <?php // echo $form->field($model, 'owner_phone') ?>

    <?php // echo $form->field($model, 'owner_info') ?>

    <?php // echo $form->field($model, 'contacts_name') ?>

    <?php // echo $form->field($model, 'contacts_email') ?>

    <?php // echo $form->field($model, 'contacts_phone') ?>

    <?php // echo $form->field($model, 'contacts_info') ?>

    <?php // echo $form->field($model, 'quota') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'no_photo') ?>

    <?php // echo $form->field($model, 'to_the_sea') ?>

    <?php // echo $form->field($model, 'client_price') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
