<?php

use yii\helpers\Html;
?>

<?php $mainClass = ($model->main) ? "photo-item-main" : ""; ?>

<li id="' . $model->id . '" class="photo-item <?= $mainClass ?>">
    <?= Html::a(Html::img('@web' . $model->src, ['class' => '']), '@web' . $model->src, ['class' => 'photo-item-gallery thumbnail']) ?>
    <div class="text-center">
        <?php
        if (!$model->main) {
            echo Html::a(\Yii::t('app', 'Сделать главной'), ['real-estate-photo/set-main', 'id' => $model->id], ['class' => 'photo-setmain btn btn-primary btn-xs']);
        } else {
            echo Html::button(\Yii::t('app', 'Главное фото'), ['class' => 'photo-setmain btn btn-success btn-xs', 'disabled' => true]);
        }

        echo Html::a(\Yii::t('app', 'Удалить'), ['real-estate-photo/delete', 'id' => $model->id], ['class' => 'btn btn-xs btn-danger', 'style' => 'margin-left:20px;']);
        ?>
    </div>
</li>
<hr />
