<?php
/* @var $this yii\web\View */

use app\widgets\MotivationBlocks;
use app\models\MotivationPicture;
use app\widgets\BackgroundWidget;

$this->params['breadcrumbs'][] = $this->title;
?>

<?php
echo BackgroundWidget::widget([
    'content' => MotivationBlocks::widget(['type' => MotivationPicture::TYPE_REAL_ESTATE_ADD]),
    'htmlClass' => 'site-dynamic-page',
    'page' => $page
]);
?>

<div class="container realEstateAdd">
    <div class="row">
        <div class="col-md-12">
            <?= $this->render('//default/flash') ?>
            <?= $page->content ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center text-uppercase"><?= \Yii::t('app', 'Предложите нам свою недвижимость в доверительное управление'); ?></h3>
        </div>
    </div>
    <?php
    echo $this->render('_form', [
        'model' => $model
    ])
    ?>
</div>
<?= MotivationBlocks::widget(['type' => MotivationPicture::TYPE_BOTTOM_LAYOUT_MOTIVATION]) ?>