<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use app\models\Lookup;
use yii\jui\AutoComplete;
use app\models\Project;
use kartik\file\FileInput;
use kartik\widgets\SwitchInput;
?>
<?php
$form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ])
?>
<?= $form->errorSummary($model) ?>
<div class="row">
    <div class="col-md-12">
        <div class="realEstateAddFormBlock">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <p class="text-bold"><?= \Yii::t('app', 'Тип недвижимости') ?></p>
                        </div>
                        <div class="panel-body">
                            <?= $form->field($model, 'type_id')->radioList(Lookup::items(Lookup::TYPE_REAL_ESTATE_TYPES)) ?>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <p class="text-bold"><?= \Yii::t('app', 'Тип размещения') ?></p>
                        </div>
                        <div class="panel-body">
                            <?= $form->field($model, 'housing_type_id')->radioList(Lookup::items(Lookup::TYPE_HOUSING_TYPE)) ?>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <p class="text-bold"><?= \Yii::t('app', 'Подробности') ?></p>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'project_id')->hiddenInput()->label(false)->error(false) ?>
                                    <?=
                                    $form->field($model, 'project_autocomplete')->widget(
                                            AutoComplete::className(), [
                                        'clientOptions' => [
                                            'minLength' => 2,
                                            'source' => Url::to(['/project/json-list']),
                                            'select' => new JsExpression(<<<JS
function (event, ui) {
    $('#realestate-project_autocomplete').val(ui.item.label);
    $('#realestate-project_id').val(ui.item.value);
    return false;
}
JS
                                            ),
                                            'change' => new JsExpression(<<<JS
function( event, ui ) {
    $( "#realestate-project_id" ).val( ui.item? ui.item.value : 0 );
}
JS
                                            ),
                                        ],
                                        'options' => [
                                            'class' => 'form-control'
                                        ]
                                    ])
                                    ?>
                                </div>
                            </div>
                            <small><?= \Yii::t('app', 'Начните писать название проекта на английском и выберите проект из списка') ?></small>
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'to_the_sea')->textInput() ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'client_price')->textInput() ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?= $form->field($model, 'residents')->textInput() ?>
                                        </div>
                                    </div>
                                    <br class="hidden-xs"/>
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <?= $form->field($model, 'square')->textInput() ?>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <?= $form->field($model, 'floor')->textInput() ?>
                                        </div>
                                    </div>
                                    <br class="hidden-xs"/>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?= $form->field($model, 'furnishings')->dropDownList(Lookup::items(Lookup::TYPE_REAL_ESTATE_FURNISHINGS)) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?= $form->field($model, 'flat_number')->textInput() ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <?= $form->field($model, 'bedrooms')->textInput() ?>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <?= $form->field($model, 'bathrooms')->textInput() ?>
                                        </div>
                                    </div>
                                    <small><?= \Yii::t('app', '0 спален, если студия') ?></small>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?= $form->field($model, 'pool')->dropDownList(Lookup::items(Lookup::TYPE_REAL_ESTATE_POOL)) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'description')->textarea(['rows' => 5, 'style' => 'margin-bottom:0px;']) ?>
                                    <small><?= \Yii::t('app', 'Объявление появится на сайте после модерации') ?></small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'information')->textarea(['rows' => 5, 'style' => 'margin-bottom:0px;']) ?>
                                    <small><?= \Yii::t('app', 'Не отображается на сайте. При перепродаже контракты от застройщика, пожалуйста, укажите график платежей') ?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <p class="text-bold"><?= \Yii::t('app', 'Контактная иформация') ?></p>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <p style="margin-bottom: 43px;"><?= \Yii::t('app', 'Владелец') ?></p>
                                    <hr/>
                                    <?= $form->field($model, 'owner_name')->textInput() ?>
                                    <?= $form->field($model, 'owner_email')->textInput() ?>
                                    <?= $form->field($model, 'owner_phone')->textInput() ?>
                                    <?= $form->field($model, 'owner_info')->textarea() ?>
                                    <?= $form->field($model, 'quota')->radioList(Lookup::items(Lookup::TYPE_REAL_ESTATE_QOUTA)) ?>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <p><?= \Yii::t('app', 'Контактное лицо в Таиланде') ?></p>
                                    <hr/>
                                    <?= $form->field($model, 'contacts_name')->textInput() ?>
                                    <?= $form->field($model, 'contacts_email')->textInput() ?>
                                    <?= $form->field($model, 'contacts_phone')->textInput() ?>
                                    <?= $form->field($model, 'contacts_info')->textarea() ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <p class="text-bold"><?= \Yii::t('app', 'Фотографии (размер фото от 10кб до 10мб)') ?></p>
                        </div>
                        <div class="panel-body">
                            <?= $form->field($model, 'images[]')->fileInput(['multiple' => true]) ?>
                            <small><?= \Yii::t('app', 'Если у Вас нет фотографий или 3d изображений объекта, мы свяжемся с Вами.') ?></small>
                            <?= $form->field($model, 'no_photo')->checkbox() ?>
                        </div>
                    </div>
                    <?=
                    Html::submitButton(($model->getIsNewRecord()) ? \Yii::t('app', 'Отправить') : \Yii::t('app', 'Сохранить'), [
                        'class' => 'btn btn-default realEstateSubmitButton'
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>