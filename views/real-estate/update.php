<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Page */

$this->title = Yii::t('app', 'Изменить объект') . ' "' . $model->title . '"';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Объекты'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $model->title;
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="real-estate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_admin_form', [
        'model' => $model,
    ]) ?>

</div>
