<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use app\models\Project;
use kartik\file\FileInput;
use kartik\widgets\SwitchInput;
use app\models\Lookup;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use app\widgets\Translation;
use app\models\User;
use app\helpers\ContentHelper;
use app\models\RealEstate;
?>

<div class="real-estate-form">
    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                ],
            ])
    ?>
    <?= $form->errorSummary($model) ?>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <p class="text-bold"><?= \Yii::t('app', 'Тип недвижимости') ?></p>
                        </div>
                        <div class="panel-body">
                            <?= $form->field($model, 'type_id')->radioList(Lookup::items(Lookup::TYPE_REAL_ESTATE_TYPES)) ?>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <p class="text-bold"><?= \Yii::t('app', 'Тип размещения') ?></p>
                        </div>
                        <div class="panel-body">
                            <?= $form->field($model, 'housing_type_id')->radioList(Lookup::items(Lookup::TYPE_HOUSING_TYPE)) ?>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <p class="text-bold"><?= \Yii::t('app', 'Цена') ?></p>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <?= $form->field($model, 'client_price')->textInput() ?>
                                </div>
                                <div class="col-md-4">
                                    <?= $form->field($model, 'daily_price')->textInput() ?>
                                </div>
                                <div class="col-md-4">
                                    <?= $form->field($model, 'monthly_price')->textInput() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <p class="text-bold"><?= \Yii::t('app', 'Подробности') ?></p>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <?=
                                    $form->field($model, 'status_id')->dropDownList(ContentHelper::getActiveListData(), [
                                        'prompt' => '- ' . $model->getAttributeLabel('status_id') . ' -'
                                    ])
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?=
                                    $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(User::find()->all(), 'id', 'email'), [
                                        'prompt' => '- ' . $model->getAttributeLabel('user_id') . ' -'
                                    ])
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?=
                                    $form->field($model, 'project_id')->dropDownList(ArrayHelper::map(Project::find()->all(), 'id', 'name'), [
                                        'prompt' => '- ' . $model->getAttributeLabel('project_id') . ' -'
                                    ])
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?=
                                    $form->field($model, 'cancel_booking_type')->dropDownList(RealEstate::getCancelBookingList(), [
                                        'prompt' => '- ' . $model->getAttributeLabel('cancel_booking_type') . ' -'
                                    ])
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'to_the_sea')->textInput() ?>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?= $form->field($model, 'residents')->textInput() ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <?= $form->field($model, 'square')->textInput() ?>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <?= $form->field($model, 'floor')->textInput() ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?= $form->field($model, 'furnishings')->dropDownList(Lookup::items(Lookup::TYPE_REAL_ESTATE_FURNISHINGS)) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?= $form->field($model, 'flat_number')->textInput() ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <?= $form->field($model, 'bedrooms')->textInput() ?>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <?= $form->field($model, 'bathrooms')->textInput() ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?= $form->field($model, 'pool')->dropDownList(Lookup::items(Lookup::TYPE_REAL_ESTATE_POOL)) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?=
                                    Translation::widget([
                                        'field' => $form->field($model, 'title')->textInput(),
                                    ])
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tab-content">
                                        <div id="description_ru" class="tab-pane active">
                                            <?=
                                            $form->field($model, 'description')->widget(yii\imperavi\Widget::className(), [
                                                'options' => [
                                                    'lang' => 'ru',
                                                    'minHeight' => 200,
                                                    'imageUpload' => Url::to([
                                                        'image/upload',
                                                        'type' => Inflector::camel2id(StringHelper::basename(get_class($model))),
                                                        'id' => $model->isNewRecord ? 'tmp' : $model->id
                                                    ]),
                                                    /* 'fileUpload' => Url::to([
                                                      'file/upload',
                                                      'type' => Inflector::camel2id(StringHelper::basename(get_class($model))),
                                                      'id' => $model->isNewRecord ? 'new' : $model->id
                                                      ]), */
                                                    'uploadImageFields' => [
                                                        Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                                    ],
                                                    'convertImageLinks' => true,
                                                    'convertVideoLinks' => true,
                                                    //'imageGetJson' => Url::to(['/admin/gallery/json-list']),
                                                    //'imageManagerJson' => Url::to(['/admin/gallery/json-list']),
                                                    //'fileManagerJson' => Url::to(['/admin/gallery/json-list']),
                                                    'imageUploadCallback' => $model->isNewRecord ? new \yii\web\JsExpression(<<<JS
                function(image, json) {
                    $(".page-form")
                        .find('form')
                        .append($('<input type="hidden" name="images[]" value="" />').val($(image).attr("src")));
                }
JS
                                                            ) : false,
                                                //'definedLinks' => '/defined-links.json', // @todo Action to site pages
                                                ],
                                                'plugins' => [
                                                    'fullscreen',
                                                    'table',
                                                    'video',
                                                    'imagemanager',
                                                    'filemanager',
                                                    'definedlinks',
                                                    'fontsize',
                                                    'fontfamily',
                                                    'fontcolor',
                                                //'clips',
                                                ]
                                            ])
                                            ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'information')->textarea(['rows' => 5]) ?>
                                </div>
                            </div>
                            <?=
                            Html::submitButton(($model->getIsNewRecord()) ? \Yii::t('app', 'Создать') : \Yii::t('app', 'Сохранить'), [
                                'class' => 'btn btn-success'
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <p class="text-bold"><?= \Yii::t('app', 'Контактная иформация') ?></p>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <p><?= \Yii::t('app', 'Владелец') ?></p>
                                            <hr/>
                                            <?= $form->field($model, 'owner_name')->textInput() ?>
                                            <?= $form->field($model, 'owner_email')->textInput() ?>
                                            <?= $form->field($model, 'owner_phone')->textInput() ?>
                                            <?= $form->field($model, 'owner_info')->textarea() ?>
                                            <?= $form->field($model, 'quota')->radioList(Lookup::items(Lookup::TYPE_REAL_ESTATE_QOUTA)) ?>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <p><?= \Yii::t('app', 'Контактное лицо в Таиланде') ?></p>
                                            <hr/>
                                            <?= $form->field($model, 'contacts_name')->textInput() ?>
                                            <?= $form->field($model, 'contacts_email')->textInput() ?>
                                            <?= $form->field($model, 'contacts_phone')->textInput() ?>
                                            <?= $form->field($model, 'contacts_info')->textarea() ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <p class="text-bold"><?= \Yii::t('app', 'Фотографии (размер фото от 10кб до 10мб)') ?></p>
                                </div>
                                <div class="panel-body">
                                    <ul id="sortable" style="padding-left: 0px;">
                                        <?php
                                        $photos = $model->getRealEstatePhotos()->all();
                                        foreach ($photos as $photo) {
                                            echo $this->render('_admin_photo', [
                                                'model' => $photo
                                            ]);
                                        }
                                        ?>
                                    </ul>

                                    <?= $form->field($model, 'images[]')->fileInput(['multiple' => true]) ?>
                                    <?= $form->field($model, 'no_photo')->checkbox() ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <?= $form->field($model, 'comfort_ids')->checkBoxList(Lookup::items(Lookup::TYPE_HOUSING_COMFORT, true), ['multiple' => true]) ?>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>