<?php

use app\helpers\ContentHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Lookup;
use app\models\Project;
use app\models\User;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Объекты');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="real-estate-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить объект'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'user_id',
                'filter' => User::find()->select(['email', 'id'])->indexBy('id')->column(),
                'value' => function ($model) {
            /** @var \app\models\Page $model */
            return ContentHelper::userLinkById($model->user_id);
        },
                'format' => 'raw',
            ],
            [
                'attribute' => 'project_id',
                'filter' => Project::find()->select(['name', 'id'])->indexBy('id')->column(),
                'value' => function($model) {
            return Project::findOne($model->project_id)->name;
        },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status_id',
                'value' => function($model) {
                    return $model->status_id ? Yii::t('app', 'Активна') : Yii::t('app', 'Не активна');
                },
                'format' => 'raw',
                'filter' => Html::dropDownList('Выбрать', null, ['1' => Yii::t('app', 'Активна'), '0' => Yii::t('app', 'Не активна')], ['class' => 'form-control']),
            ],
            'client_price',
            'daily_price',
            'monthly_price',
            'rating',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
