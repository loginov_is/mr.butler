<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Translation;
?>

<div class="guidebook container">
    <div class="row">
        <div class="col-md-12">
            <h1><?= Translation::t($regionModel, 'name') ?> / <?= Translation::t($regionModel->city, 'name') ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= Translation::t($regionModel, 'description') ?>
        </div>
    </div>
    <?php if ($galleryImages) { ?>
        <div class="row">
            <div class="col-md-12">
                <h3><?= \Yii::t('app', 'Фотогалерея района') ?>:</h3>
                <div class="region-gallery">
                    <?php
                    foreach ($galleryImages as $image) {
                        echo Html::img('@web/upload/' . $image->image, ['alt' => $image->image]);
                    }
                    ?>
                </div>
                <?php $this->registerJs("$('.region-gallery').fotorama({width: '100%', height: 600, nav: false, fit: 'cover'});"); ?>
            </div>
        </div>
    <?php } ?>
    <?php if ($regionModel->videos) { ?>
        <div class="row">
            <div class="col-md-12">
                <h3><?= \Yii::t('app', 'Видео района') ?>:</h3>
            </div>
        </div>
        <?php foreach ($regionModel->videos as $video) { ?>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="<?= $video->getYoutubeFullUrl($video->youtube_id) ?>"></iframe>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
    <?php if ($sightImages) { ?>
        <div class="row">
            <div class="col-md-12">
                <h3><?= \Yii::t('app', 'Основные достопримечательности') ?>:</h3>
                <div class="region-sights">
                    <?php foreach ($sightImages as $image) { ?>
                        <div class="region-sights-photo">
                            <?= Html::img('@web/upload/' . $image->image, ['alt' => $image->image]); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <h3><?= \Yii::t('app', 'На карте') ?></h3>
            <?php if (!empty($similiarRegions)) { ?>
                <p>
                    <?= \Yii::t('app', '{region} граничит с районами', ['region' => $regionModel->name]) ?>
                    <?php for ($i = 0; $i < count($similiarRegions); $i++): ?>
                        <span class="nearRegions">
                            <?= Html::a($similiarRegions[$i]->name, [
                                'guidebook/search',
                                'GuidebookForm[cityId]' => $similiarRegions[$i]->city_id,
                                'GuidebookForm[regionId]' => $similiarRegions[$i]->id,
                                'GuidebookForm[price]' => '50,1000',
                                'GuidebookForm[housingType]' => 1,
                                'GuidebookForm[residents]' => 1,
                                'GuidebookForm[priceType]' => 'daily_price',
                                'GuidebookForm[sameRegions]' => 0,
                                'GuidebookForm[sights]' => 0,
                            ]) ?>
                        </span>
                        <!-- Запятую можно вынести в CSS как :after { content: ","; } -->
                        <?php if ($i < count($similiarRegions) - 1) echo ','; ?>
                    <?php endfor; ?>
                </p>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="map" style="width: 100%; height: 400px; margin-bottom: 20px;"></div> 
        </div>
    </div>
    <?php if (!empty($similiarRegions)) { ?>
        <div class="row">
            <div class="col-md-12">
                <h3><?= \Yii::t('app', 'Похожие районы') ?></h3>
            </div>
        </div>
        <div class="row">
            <?php foreach ($similiarRegions as $similiarRegion): ?>
                <div class="col-md-4">
                    <div class="sameRegionAvatar">
                        <?php if($similiarRegion->avatar) { ?>
                        <?= Html::img('@web/upload/' . $similiarRegion->avatar) ?>
                        <?php } else { ?>
                        <?= Html::img('@web/images/no-photo.jpg') ?>
                        <?php } ?>
                    </div>
                    <div class="sameRegionDescription">
                        <?= Html::a($similiarRegion->name, [
                            'guidebook/search',
                            'GuidebookForm[cityId]' => $similiarRegion->city_id,
                            'GuidebookForm[regionId]' => $similiarRegion->id,
                            'GuidebookForm[price]' => '50,1000',
                            'GuidebookForm[housingType]' => 1,
                            'GuidebookForm[residents]' => 1,
                            'GuidebookForm[priceType]' => 'daily_price',
                            'GuidebookForm[sameRegions]' => 0,
                            'GuidebookForm[sights]' => 0,
                        ]) ?>
                        <div>
                            <?= $similiarRegion->description ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <h3><?= \Yii::t('app', 'Посмотреть предложения (объекты) в этом районе') ?></h3>
        </div>
    </div>
    <div class="row">
        <?php
        $counter = 0;
        foreach ($realEstateModel as $item) {
            $hidden = false;
            if($counter > 2) {
                $hidden = true;
            }
            echo $this->render('//default/_real_estate_item', ['model' => $item, 'hidden' => $hidden]);
            $counter++;
        }
        ?>
        <?php $this->registerJs("$('.real-estate-photos').fotorama({nav: false, fit: 'cover', width: 360, height: 283});"); ?>
    </div>
    <?php if (count($realEstateModel) > 3) { ?>
        <div class="row">
            <div class="col-md-12 text-center viewAllItems">
                <div>
                    <?= Html::img('@web/images/view_more_plus.png', ['alt' => 'view more']); ?>
                </div>
                <div>
                    <a href="" class="viewMoreLink"><?= \Yii::t('app', 'Посмотреть все предложения в этом районе'); ?></a>
                </div>
                <div>
                    <span class="viewMoreSummary"><?= \Yii::t('app', 'Показано {x} из {y}', ['x' => (count($realEstateModel) > 3 ? 3 : count($realEstateModel)), 'y' => count($realEstateModel)]) ?></span>
                </div>
            </div>
        </div>
    <?php } ?>
</div>