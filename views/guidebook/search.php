<?php
/* @var $this yii\web\View */
/* @var $regionModel app\models\Region */
/* @var $realEstateModel app\models\RealEstate */
/* @var $galleryImages array */
/* @var $sightImages array */

use app\models\Page;
use app\widgets\BackgroundWidget;
use app\widgets\GuidebookSearch;
use app\models\Region;
use app\models\Translation;

$this->title = Translation::t($regionModel, 'name');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Гид по районам'), 'url' => ['guidebook/index']];
$this->params['breadcrumbs'][] = $this->title;
//$this->params['carousel-block'] = GuidebookSearch::widget();
?>

<?= GuidebookSearch::widget() ?>


<?php
if(!empty($regionModel)) {
    echo $this->render('_region', [
        'regionModel' => $regionModel,
        'realEstateModel' => $realEstateModel,
        'similiarRegions' => $similiarRegions,
        'galleryImages' => $galleryImages,
        'sightImages' => $sightImages,
    ]);
}
?>
