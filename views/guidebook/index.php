<?php

/* @var $this yii\web\View */
/* @var $regions \app\models\Region[] */

use app\widgets\GuidebookSearch;
use app\widgets\BackgroundWidget;

$this->params['breadcrumbs'][] = $this->title;
?>

<?= GuidebookSearch::widget() ?>
