<?php

use yii\helpers\Html;
use app\models\Translation;
?>
<div class="guidebook container">
    <div class="row">
        <div class="col-md-12">
            <h1><?= Translation::t($model, 'name') ?> / <?= Translation::t($model->city, 'name') ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= Translation::t($model, 'description') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3><?= \Yii::t('app', 'Фотогалерея района') ?>:</h3>
            <?php var_dump($model->getImageAlbums(\app\models\ImageAlbumToRegion::TYPE_ID_PHOTOALBUMS)) ?>
        </div>
    </div>
    <?php if ($model->videos) { ?>
        <div class="row">
            <div class="col-md-12">
                <h3><?= \Yii::t('app', 'Видео района') ?>:</h3>
            </div>
        </div>
        <?php foreach ($model->videos as $video) { ?>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="<?= $video->getYoutubeFullUrl($video->youtube_id) ?>"></iframe>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
    <?php if ($sightItems) { ?>
        <div class="row">
            <div class="col-md-12">
                <h3><?= \Yii::t('app', 'Основные достопримечательности') ?>:</h3>
            </div>
        </div>
        <?=
        dosamigos\gallery\Gallery::widget([
            'items' => $sightItems,
        ])
        ?>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <h3><?= \Yii::t('app', 'На карте') ?></h3>
            <?php if (count($similiarRegions) != 0): ?>
                <p>
                    <?= \Yii::t('app', '{region} граничит с районами', ['region' => $model->name]) ?>
                    <?php for ($i = 0; $i < count($similiarRegions); $i++): ?>
                        <span class="nearRegions">
                            <?= Html::a($similiarRegions[$i]->name, ['guidebook/region', 'id' => $similiarRegions[$i]->id]) ?>
                        </span>
                        <!-- Запятую можно вынести в CSS как :after { content: ","; } -->
                        <?php if ($i < count($similiarRegions) - 1) echo ','; ?>
                    <?php endfor; ?>
                </p>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="map" style="width: 100%; height: 400px; margin-bottom: 20px;"></div> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3><?= \Yii::t('app', 'Похожие районы') ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= Html::img('@web/images/sameRegion.jpg', ['alt' => 'region name', 'class' => 'guidbookImage']); ?>
            <div class="sameRegionDescription">
                <a href="" >Banglampoo</a>
                <p>Khao San Road Исторический Уличная Еда Hidden Treasures</p>
            </div>
        </div>
        <div class="col-md-4">
            <?= Html::img('@web/images/sameRegion.jpg', ['alt' => 'region name', 'class' => 'guidbookImage']); ?>
            <div class="sameRegionDescription">
                <a href="" >Banglampoo</a>
                <p>Khao San Road Исторический Уличная Еда Hidden Treasures</p>
            </div>
        </div>
        <div class="col-md-4">
            <?= Html::img('@web/images/sameRegion.jpg', ['alt' => 'region name', 'class' => 'guidbookImage']); ?>
            <div class="sameRegionDescription">
                <a href="" >Banglampoo</a>
                <p>Khao San Road Исторический Уличная Еда Hidden Treasures</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3><?= \Yii::t('app', 'Посмотреть предложения (объекты) в этом районе') ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= Html::img('@web/images/flat_view_demo.jpg', ['alt' => 'appartaments', 'class' => 'guidbookImage']); ?>
            <div class="appartamentDescription">
                <p><span class="price">$50</span> ночь</p>
                <p><span class="price">$350</span> месяц</p>
            </div>
        </div>
        <div class="col-md-4">
            <?= Html::img('@web/images/flat_view_demo.jpg', ['alt' => 'appartaments', 'class' => 'guidbookImage']); ?>
            <div class="appartamentDescription">
                <p><span class="price">$50</span> ночь</p>
                <p><span class="price">$350</span> месяц</p>
            </div>
        </div>
        <div class="col-md-4">
            <?= Html::img('@web/images/flat_view_demo.jpg', ['alt' => 'appartaments', 'class' => 'guidbookImage']); ?>
            <div class="appartamentDescription">
                <p><span class="price">$50</span> ночь</p>
                <p><span class="price">$350</span> месяц</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center viewAllItems">
            <div>
                <?= Html::img('@web/images/view_more_plus.png', ['alt' => 'view more']); ?>
            </div>
            <div>
                <a href="" class="viewMoreLink"><?= \Yii::t('app', 'Посмотреть все предложения в этом районе'); ?></a>
            </div>
            <div>
                <span class="viewMoreSummary"><?= \Yii::t('app', 'Показано {x} из {y}', ['x' => 5, 'y' => 10]) ?></span>
            </div>
        </div>
    </div>
</div>