<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use app\models\Accounting;
use app\helpers\ContentHelper;

$this->title = ($type == Accounting::OPERATION_MINUS_BALANCE) ? \Yii::t('app', 'Исходящие счета') : \Yii::t('app', 'Входящие счета');
?>
<?= $this->render('_mobile_profile_menu') ?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_menu') ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('//profile/_balance_menu') ?>
                    <div class="profileTable hidden-xs">
                        <?=
                        GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                [
                                    'attribute' => 'lookup_id',
                                    'header' => \Yii::t('app', 'Тип операции'),
                                    'value' => function ($row) {
                                        if (isset($row->lookup->name)) {
                                            return $row->lookup->name;
                                        } else {
                                            return null;
                                        }
                                    }
                                ],
                                [
                                    'attribute' => 'amount',
                                    'value' => function ($row) {
                                        return ContentHelper::getHumanAmount($row->amount, 'THB');
                                    }
                                ],
                                'created_at:datetime'
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="profileTable visible-xs">
                        <?=
                        GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                [
                                    'attribute' => 'lookup_id',
                                    'header' => \Yii::t('app', 'Тип операции'),
                                    'value' => function ($row) {
                                        if (isset($row->lookup->name)) {
                                            return $row->lookup->name;
                                        } else {
                                            return null;
                                        }
                                    }
                                ],
                                [
                                    'attribute' => 'amount',
                                    'value' => function ($row) {
                                        return ContentHelper::getHumanAmount($row->amount, 'THB');
                                    }
                                ],
                            ],
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>