<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\PaymentSystem;
use yii\grid\GridView;
use app\models\Payment;
use app\helpers\ContentHelper;

$this->title = \Yii::t('app', 'Заказать вывод средств');
?>
<?= $this->render('_mobile_profile_menu') ?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_menu') ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="panel panel-default panel-white">
                        <div class="panel-body">
                            <?= $this->render('//profile/_balance_menu') ?>
                            <div class="balanceBlock">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12">
                                        <p class="bold"><?= \Yii::t('app', 'Текущий баланс:') ?></p>
                                    </div>
                                    <div class="col-md-9 col-xs-12">
                                        <p class="bold"><span class="userBalance"><?= ContentHelper::getHumanAmount($user->getBalance(), 'THB') ?></span></p>
                                    </div>
                                </div>
                                <?php if ($user->getFrozenBalance()) { ?>
                                    <div class="row">
                                        <div class="col-md-3 col-xs-12">
                                            <p class="bold"><?= \Yii::t('app', 'Замороженный баланс:') ?></p>
                                        </div>
                                        <div class="col-md-9 col-xs-12">
                                            <p class="bold"><span class="userBalance"><?= ContentHelper::getHumanAmount($user->getFrozenBalance(), 'THB') ?></span></p>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($user->getBalance()) { ?>
                                    <?php $form = ActiveForm::begin(); ?>
                                    <?= $form->errorSummary([$model]) ?>
                                    <?= $this->render('//default/flash') ?>
                                    <div class="row">
                                        <div class="col-md-3 col-xs-12">
                                            <p class="bold"><?= \Yii::t('app', 'Сумма вывода') ?></p>
                                        </div>
                                        <div class="col-md-9 col-xs-12">
                                            <?= $form->field($model, 'amount')->textInput(['style' => 'width: 150px;', 'placeholder' => \Yii::t('app', 'Введите сумму')]) ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-xs-12">
                                            <p class="bold"><?= \Yii::t('app', 'Вывод через:') ?></p>
                                        </div>
                                        <div class="col-md-9 col-xs-12">
                                            <?=
                                            $form->field($model, 'payment_system_id')->radioList(
                                                    PaymentSystem::find()->select(['title', 'id'])->where(['language_id' => Yii::$app->languageInfo->id])->indexBy('id')->column(), [
                                                'item' => function ($index, $label, $name, $checked, $value) {
                                                    $check = $checked ? ' checked="checked"' : '';
                                                    $radioModel = PaymentSystem::findOne($value);
                                                    $image = Html::img('@web/upload/payment_system/' . $radioModel->image, ['style' => 'height: 50px;']);
                                                    return "<div class='text-center radio-inline-block'><label class=\"form__param\">$image<br/><input type=\"radio\" name=\"$name\" value=\"$value\"$check></label></div>";
                                                }])
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-xs-0">

                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <?=
                                                    Html::submitButton(\Yii::t('app', 'Вывести'), [
                                                        'class' => 'btn btn-default realEstateSubmitButton'
                                                    ]);
                                                    ?>
                                                </div>
                                            </div>
                                            <?php ActiveForm::end(); ?>
                                        <?php } else { ?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="alert alert-danger"><?= \Yii::t('app', 'Недостаточно средств на балансе') ?></p>    
                                                </div>
                                            </div>                                   
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profileTable hidden-xs">
                        <h3><?= \Yii::t('core', 'Мои запросы на выплату') ?></h3>
                        <?=
                        GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                [
                                    'attribute' => 'amount',
                                    'value' => function ($row) {
                                        return ContentHelper::getHumanAmount($row->amount, 'THB');
                                    }
                                ],
                                [
                                    'attribute' => 'created_at',
                                    'value' => function($row) {
                                        return date('d/m/Y H:i', $row->created_at);
                                    }
                                ],
                                [
                                    'attribute' => 'payment_system_id',
                                    'format' => 'html',
                                    'value' => function ($row) {
                                        if (isset($row->paymentSystem->title)) {
                                            return $row->paymentSystem->title;
                                        } else {
                                            return Html::tag('span', \Yii::t('app', 'Удалена'), ['class' => 'text-muted']);
                                        }
                                    }
                                        ],
                                        [
                                            'attribute' => 'status_id',
                                            'value' => function ($row) {
                                                return Payment::getList($row->status_id);
                                            }
                                        ],
                                        [
                                            'header' => '',
                                            'format' => 'html',
                                            'options' => ['class' => 'text-center'],
                                            'value' => function($row) {
                                        $content = Html::a(\Yii::t('app', 'Просмотр'), ['profile/payment-view', 'id' => $row->id], ['class' => 'btn btn-xs btn-success']);
                                        if ($row->status_id == Payment::STATUS_NO_REQUISITES) {
                                            $content .= Html::tag('br');
                                            $content .= Html::a(\Yii::t('app', 'Подтвердить'), ['profile/confirm-payment', 'id' => $row->id], [
                                                        'class' => 'btn btn-xs btn-primary',
                                                        'style' => 'margin-top:5px'
                                            ]);
                                        }

                                        return $content;
                                    }
                                        ],
                                    ],
                                ]);
                                ?>
                            </div>
                            <div class="profileTable visible-xs">
                                <h3><?= \Yii::t('core', 'Мои запросы на выплату') ?></h3>
                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'columns' => [
                                        [
                                            'format' => 'html',
                                            'attribute' => 'amount',
                                            'value' => function ($row) {
                                                return Html::a(ContentHelper::getHumanAmount($row->amount, 'THB'), ['profile/payment-view', 'id' => $row->id]);
                                            }
                                                ],
                                                [
                                                    'attribute' => 'status_id',
                                                    'value' => function ($row) {
                                                        return Payment::getList($row->status_id);
                                                    }
                                                ],
                                            ],
                                        ]);
                                        ?>
            </div>
        </div>
    </div>
</div>