<?php

use yii\helpers\Html;
use app\models\Accounting;
?>
<div class="row balance_menu" style="margin-bottom: 20px;">
    <div class="col-md-4 col-xs-12 text-center balanceMenuItem">
        <?=
        Html::a(\Yii::t('app', 'Входящие счета'), ['/profile/balance', 'type' => Accounting::OPERATION_PLUS_BALANCE], [
            'class' => (Yii::$app->controller->action->id == 'balance' && Yii::$app->request->get('type') == Accounting::OPERATION_PLUS_BALANCE) ? 'active' : '',
        ])
        ?>
    </div>
    <div class="col-md-4 col-xs-12 text-center balanceMenuItem">
        <?=
        Html::a(\Yii::t('app', 'Исходящие счета'), ['/profile/balance', 'type' => Accounting::OPERATION_MINUS_BALANCE], [
            'class' => (Yii::$app->controller->action->id == 'balance' && Yii::$app->request->get('type') == Accounting::OPERATION_MINUS_BALANCE) ? 'active' : '',
        ])
        ?>
    </div>
    <div class="col-md-4 col-xs-12 text-center balanceMenuItem">
        <?=
        Html::a(\Yii::t('app', 'Заказать вывод средств'), ['/profile/my-balance'], [
            'class' => (Yii::$app->controller->action->id == 'my-balance') ? 'active' : '',
        ])
        ?>
    </div>
</div>