<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Translation;
use app\models\UserLine;
use app\models\GuidebookForm;

$this->title = \Yii::t('app', 'Просмотренные объекты недвижимости');
?>
<?= $this->render('_mobile_profile_menu') ?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_menu') ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->render('//default/_top_banner', ['title' => $this->title]) ?>
                            <div class="arrows">
                                <a class="viewedObjectsLeft"><?= Html::img('@web/images/profile_left_arrow.png') ?></a>
                                <a class="viewedObjectsRight"><?= Html::img('@web/images/profile_right_arrow.png') ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <?php if ($viewedObjects) { ?>
                            <div class="viewedObjectsCarousel hidden-xs">
                                <?php foreach ($viewedObjects as $object) { ?>
                                    <div class="col-md-4 viewedObjectItem">
                                        <?php $photos = $object->getRealEstatePhotos()->orderBy('main, index')->all(); ?>
                                        <?php if ($photos) { ?>
                                            <div class="viewedObjectPhoto">
                                                <?php foreach ($photos as $photo) { ?>
                                                    <?= Html::img('@web' . $photo->src, ['alt' => 'appartaments', 'class' => 'likedRegionImage']) ?>
                                                <?php } ?>
                                            </div>
                                        <?php } else { ?>
                                            <?= Html::img('@web/images/no-photo.jpg', ['class' => 'likedRegionImage']) ?>
                                        <?php } ?>
                                        <div class="appartamentDescription">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p><span class="price">$<?= $object->daily_price ?></span> ночь</p>
                                                    <p><span class="price">$<?= $object->monthly_price ?></span> месяц</p>
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <?= Html::a(\Yii::t('app', 'Забронировать'), ['site/object/', 'id' => $object->id], ['class' => 'btn btn-default btn-xs selectedObjectBtn pull-right']) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="viewedObjectsMobile visible-xs-block">
                                <?php foreach ($viewedObjects as $object) { ?>
                                    <div class="col-md-4 viewedObjectItem">
                                        <?php $photos = $object->getRealEstatePhotos()->orderBy('main, index')->all(); ?>
                                        <?php if ($photos) { ?>
                                            <div class="viewedObjectPhoto">
                                                <?php foreach ($photos as $photo) { ?>
                                                    <?= Html::img('@web' . $photo->src, ['alt' => 'appartaments']) ?>
                                                <?php } ?>
                                            </div>
                                        <?php } else { ?>
                                            <?= Html::img('@web/images/no-photo.jpg') ?>
                                        <?php } ?>
                                        <div class="appartamentDescription">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p><span class="price">$<?= $object->daily_price ?></span> ночь</p>
                                                    <p><span class="price">$<?= $object->monthly_price ?></span> месяц</p>
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <?= Html::a(\Yii::t('app', 'Забронировать'), ['site/object/', 'id' => $object->id], ['class' => 'btn btn-default btn-xs selectedObjectBtn pull-right']) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <?php $this->registerJs("$('.viewedObjectPhoto').fotorama({nav: false, fit: 'cover', width: '100%', minheight: 150});"); ?>
                        <?php } else { ?>
                            <div class="col-md-6 col-xs-12 text-center">
                                <?= Html::a(\Yii::t('app', 'Популярные объявления'), ['site/search/'], ['class' => 'realEstateSubmitButton']) ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->render('//default/_top_banner', ['title' => \Yii::t('app', 'Понравившиеся объекты недвижимости')]) ?>
                            <div class="arrows">
                                <a class="userObjectsLeft"><?= Html::img('@web/images/profile_left_arrow.png') ?></a>
                                <a class="userObjectsRight"><?= Html::img('@web/images/profile_right_arrow.png') ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <?php if ($userLineObjects) { ?>
                            <div class="userObjectsCarousel hidden-xs">
                                <?php foreach ($userLineObjects as $object) { ?>
                                    <div class="col-md-4 viewedObjectItem">
                                        <?php $photos = $object->getRealEstatePhotos()->orderBy('main, index')->all(); ?>
                                        <?php if ($photos) { ?>
                                            <div class="viewedObjectPhoto">
                                                <?php foreach ($photos as $photo) { ?>
                                                    <?= Html::img('@web' . $photo->src, ['alt' => 'appartaments', 'class' => 'likedRegionImage']) ?>
                                                <?php } ?>
                                            </div>
                                        <?php } else { ?>
                                            <?= Html::img('@web/images/no-photo.jpg', ['class' => 'likedRegionImage']) ?>
                                        <?php } ?>
                                        <div class="appartamentDescription">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p><span class="price">$<?= $object->daily_price ?></span> ночь</p>
                                                    <p><span class="price">$<?= $object->monthly_price ?></span> месяц</p>
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <?= Html::a(\Yii::t('app', 'Забронировать'), ['site/object/', 'id' => $object->id], ['class' => 'btn btn-default btn-xs selectedObjectBtn pull-right']) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="viewedObjectsMobile visible-xs-block">
                                <?php foreach ($userLineObjects as $object) { ?>
                                    <div class="col-md-4 viewedObjectItem">
                                        <?php $photos = $object->getRealEstatePhotos()->orderBy('main, index')->all(); ?>
                                        <?php if ($photos) { ?>
                                            <div class="viewedObjectPhoto">
                                                <?php foreach ($photos as $photo) { ?>
                                                    <?= Html::img('@web' . $photo->src, ['alt' => 'appartaments']) ?>
                                                <?php } ?>
                                            </div>
                                        <?php } else { ?>
                                            <?= Html::img('@web/images/no-photo.jpg') ?>
                                        <?php } ?>
                                        <div class="appartamentDescription">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p><span class="price">$<?= $object->daily_price ?></span> ночь</p>
                                                    <p><span class="price">$<?= $object->monthly_price ?></span> месяц</p>
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <?= Html::a(\Yii::t('app', 'Забронировать'), ['site/object/', 'id' => $object->id], ['class' => 'btn btn-default btn-xs selectedObjectBtn pull-right']) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <?php $this->registerJs("$('.viewedObjectPhoto').fotorama({nav: false, fit: 'cover', width: '100%', minheight: 150});"); ?>
                        <?php } else { ?>
                            <div class="col-md-6 col-md-offset-3 col-xs-12 text-center" style="margin-bottom: 20px;">
                                <?= Html::a(\Yii::t('app', 'Популярные объявления'), ['site/search/'], ['class' => 'realEstateSubmitButton']) ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->render('//default/_top_banner', ['title' => \Yii::t('app', 'Понравившиеся районы')]) ?>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <?php if ($userLineRegions) { ?>
                            <?php foreach ($userLineRegions as $region) { ?>
                                <div class="col-md-4">
                                    <a href="<?= Url::to(['user-line/delete', 'modelId' => $region->id, 'typeId' => UserLine::TYPE_ID_REGION_ADDED_TO_USERLINE]) ?>" class="removeLikedRegion"><i class="fa fa-remove"></i></a>
                                    <?php if ($region->avatar) { ?>
                                        <?= Html::img('@web/upload/' . $region->avatar, ['alt' => $region->name, 'class' => 'likedRegionImage']) ?>
                                    <?php } else { ?>
                                        <?= Html::img('@web/images/no-photo.jpg', ['alt' => $region->name, 'class' => 'likedRegionImage']) ?>
                                    <?php } ?>
                                    <div class="sameRegionDescription">
                                        <?php
                                        $minPrice = GuidebookForm::MIN_FORM_PRICE;
                                        $maxPrice = GuidebookForm::MAX_FORM_PRICE;
                                        echo Html::a($region->name, [
                                            'guidebook/search',
                                            'GuidebookForm[cityId]' => $region->city_id,
                                            'GuidebookForm[regionId]' => $region->id,
                                            'GuidebookForm[price]' => "$minPrice,$maxPrice",
                                            'GuidebookForm[housingType]' => 1,
                                            'GuidebookForm[residents]' => 1,
                                            'GuidebookForm[priceType]' => 'daily_price',
                                            'GuidebookForm[sameRegions]' => 0,
                                            'GuidebookForm[sights]' => 0,
                                        ])
                                        ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php } else { ?>
                            <div class="col-md-6 col-md-offset-3 col-xs-12 text-center" style="margin-bottom: 20px;">
                            <?= Html::a(\Yii::t('app', 'Гид по районам'), ['guidbook/search/'], ['class' => 'realEstateSubmitButton']) ?>
                            </div>
<?php } ?>
                    </div>

                </div>

                <!--                    <div class="row regulationLinks">
                                        <div class="col-md-3 col-xs-5">
                                            <p><?= \Yii::t('app', 'Поделиться') ?>:</p>
                                        </div>
                                        <div class="col-md-9 col-xs-7">
                
                                        </div>
                                    </div>-->
            </div>
        </div>
    </div>
</div>
</div>
</div>