<?php

/* @var $model app\models\Booking */

use yii\helpers\Html;

$this->title = \Yii::t('app', 'Оплата недвижимости');
?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_menu') ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->render('//default/_top_banner', ['title' => $this->title]) ?>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-12 col-xs-12">
                            <?php if($model) { ?>
                                <?php foreach($model as $booking) { ?>
                                        <?= $this->render('_saved_objects_items', ['booking' => $booking]) ?>
                                <?php } ?>
                            <?php } else { ?>
                                <?= Yii::t('app', 'Нет информации.') ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>