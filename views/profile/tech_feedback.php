<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\grid\GridView;

$this->title = \Yii::t('app', 'Написать в тех. поддержку');
?>
<?= $this->render('_mobile_profile_menu') ?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_menu') ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->render('//default/_top_banner', ['title' => $this->title]) ?>
                        </div>
                    </div>
                    <?php $form = ActiveForm::begin(); ?>
                    <?= $form->errorSummary([$model]) ?>
                    <?= $this->render('//default/flash') ?>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-center" style="margin-bottom: 20px; margin-top: 20px;"><?= \Yii::t('app', 'Прежде чем написать нам, возможно, вы сможете получить ответы в разделе "{x}"', ['x' => Html::a(\Yii::t('app', 'Помощь'), ['regulation/index'])]) ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'title')->textInput(['class' => 'form-control']) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'message')->textarea(['class' => 'form-control', 'rows' => 5]) ?>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-3 col-xs-12">
                                    <?=
                                    Html::submitButton(\Yii::t('app', 'Отправить'), [
                                        'class' => 'btn btn-default realEstateSubmitButton'
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="profileTable hidden-xs">
                <h3><?= \Yii::t('core', 'Мои сообщения') ?></h3>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'title',
                        'created_at:datetime',
                        [
                            'header' => '',
                            'format' => 'raw',
                            'options' => ['class' => 'text-center'],
                            'value' => function($row) {
                        $content = Html::a(\Yii::t('app', 'Просмотр'), ['profile/ticket-view', 'id' => $row->id], ['class' => 'btn btn-xs btn-success']);
                        $content .= Html::tag('br');
                        $content .= Html::a(\Yii::t('app', 'Удалить'), '#', [
                                    'class' => 'btn btn-xs btn-danger bootbox',
                                    'style' => 'margin-top: 5px',
                                    'bootbox-url' => Url::to(['profile/ticket-delete/', 'id' => $row->id]),
                                    'bootbox-message' => \Yii::t('app', 'Подтверждение удаления')
                        ]);
                        return $content;
                    }
                        ],
                    ],
                ]);
                ?>
            </div>
            <div class="profileTable visible-xs">
                <h3><?= \Yii::t('core', 'Мои сообщения') ?></h3>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'attribute' => 'title',
                            'format' => 'html',
                            'value' => function ($row) {
                                return Html::a($row->title, ['profile/ticket-view', 'id' => $row->id]);
                            }
                                ],
                            ],
                        ]);
                        ?>
            </div>
        </div>
    </div>
</div>