<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <p class="text-muted"><i class="fa fa-user"></i> <?= isset($model->createdBy->email) ? $model->createdBy->email : \Yii::t('app', 'Неопределено') ?></p>
                    </div>
                    <div class="col-md-6 text-right">
                        <p class="text-muted"><i class="fa fa-globe"></i> <?= date('d/m/Y H:i', $model->created_at) ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= nl2br($model->message) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <?php
                        if ($model->created_by == Yii::$app->user->id) {
                            echo Html::a(\Yii::t('app', 'Удалить'), '#', [
                                'class' => 'btn btn-danger bootbox',
                                'bootbox-url' => Url::to(['profile/payment-comment-delete/', 'id' => $model->id]),
                                'bootbox-message' => \Yii::t('app', 'Подтверждение удаления')
                            ]);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>