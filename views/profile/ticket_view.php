<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->title = \Yii::t('app', 'Просмотр тикета от {date}', ['date' => date('d/m/Y H:i', $model->created_at)]);
?>
<?= $this->render('_mobile_profile_menu')?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_menu') ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="row">
                <div class="col-md-12">
                    <?= $this->render('//default/_top_banner', ['title' => $this->title]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li>
                            <?= Html::a(\Yii::t('app', 'Написать в тех. поддержку'), ['profile/tech-feedback']) ?>
                        </li>
                        <li class="active">
                            <?= $this->title ?>
                        </li>
                    </ol>
                </div>
            </div>
            <br />
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="row">
                                <div class="col-md-6 text-left">
                                    <p class="text-muted"><i class="fa fa-user"></i> <?= isset($model->createdBy->email) ? $model->createdBy->email : \Yii::t('app', 'Неопределено') ?></p>
                                </div>
                                <div class="col-md-6 text-right">
                                    <p class="text-muted"><i class="fa fa-globe"></i> <?= date('d/m/Y H:i', $model->created_at) ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4><?= $model->title ?></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= nl2br($model->message) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->errorSummary([$commentModel]) ?>
            <?= $this->render('//default/flash') ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($commentModel, 'message')->textarea(['class' => 'form-control', 'rows' => 5]) ?>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-3 col-xs-12">
                            <?=
                            Html::submitButton(\Yii::t('app', 'Отправить'), [
                                'class' => 'btn btn-default realEstateSubmitButton'
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item'],
                'itemView' => function ($model, $key, $index, $widget) {
            return $this->render('_ticket_comment', ['model' => $model]);
        },
            ])
            ?>
        </div>
    </div>
</div>