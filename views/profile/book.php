<?php

/** @var $model \app\models\BookingForm */
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use app\models\Lookup;
use app\models\BookingForm;

$this->title = \Yii::t('app', 'Бронирование');
?>
<?= $this->render('_mobile_profile_menu')?>
<div class="container bookingPage">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('//profile/_menu') ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php $form = ActiveForm::begin([
                        'action' => ['profile/book', 'scenario' => BookingForm::SCENARIO_STEP_3]
                    ]); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Выберите необходимые услуги</h3>
                            <table class="table">
                                <thead>
                                    <th><?= Yii::t('app', 'Тип услуги') ?></th>
                                    <th><?= Yii::t('app', 'Цена') ?></th>
                                    <th><?= Yii::t('app', 'Кол-во человек') ?></th>
                                    <th><?= Yii::t('app', 'Выбрать') ?></th>
                                </thead>
                                <tbody>
                                <?php foreach($services as $service) { ?>
                                    <tr>
                                        <td>
                                            <?= $service->name ?>
                                        </td>
                                        <td class="text-center">
                                            $ <span class="servicePrice"><?= $service->getServicePrice(1) ?></span>
                                        </td>
                                        <td>
                                            <?= Html::dropDownList($model->formName() . '[services][' . $service->id . '][people_count]', null, [
                                                    '1' => 1,
                                                    '2' => 2,
                                                    '3' => 3,
                                                    '4' => 4,
                                                    '5' => 5,
                                                    '6' => 6,
                                                    '7' => 7,
                                                    '8' => 8,
                                                    '9' => 9,
                                                    '10' => 10,
                                                ], ['class' => 'form-control serviceList', 'data-service-id' => $service->id]) ?>
                                        </td>
                                        <td class="text-center">
                                            <?= Html::checkbox($model->formName() . '[services][' . $service->id . '][selected]') ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right" style="margin-bottom: 20px;">
                            <h3><?= Yii::t('app', 'Итого') ?>: <strong>$ <span class="totalServicesPrice">0</span></strong></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 pull-left">
<!--                            --><?//= Html::submitInput(Yii::t('app', 'Сохранить для оплаты'), ['class' => 'btn btn-default realEstateSubmitButton', 'name' => $model->formName() . '[submitSave]']) ?>
                        </div>
                        <div class="col-md-3 pull-right">
<!--                            --><?//= Html::submitInput(Yii::t('app', 'Оплатить'), ['class' => 'btn btn-default realEstateSubmitButton', 'name' => $model->formName() . '[submitPay]']) ?>
                            <?= Html::submitInput(Yii::t('app', 'Забронировать'), ['class' => 'btn btn-default realEstateSubmitButton', 'name' => $model->formName() . '[submitBook]']) ?>
                        </div>
                    </div>
                    <?= $form->field($model, 'realEstateId')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'startDate')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'endDate')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'phone')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'comment')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'price')->hiddenInput()->label(false) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
