<?php

use yii\helpers\Html;

$this->title = \Yii::t('app', 'Подписки');
?>
<?= $this->render('_mobile_profile_menu')?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_menu') ?>
                </div>
            </div> 
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->render('//default/_top_banner', ['title' => $this->title]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <?php if ($model->news_subscription) { ?>
                                <p style="margin-bottom: 20px; margin-top: 20px;"><?= \Yii::t('app', 'Вы подписаны на рассылку новостей') ?></p>
                                <div class="row">
                                    <div class="col-md-3 col-xs-12">
                                        <?=
                                        Html::a(\Yii::t('app', 'Отписаться'), '#', [
                                            'class' => 'btn btn-default realEstateSubmitButton bootbox',
                                            'bootbox-message' => \Yii::t('app', 'Подтверждение действия'),
                                            'bootbox-url' => \yii\helpers\Url::to(['profile/subscriptions', 'action' => true, 'value' => 0])
                                        ])
                                        ?>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <p style="margin-bottom: 20px; margin-top: 20px;"><?= \Yii::t('app', 'Подписаться на рассылку новостей') ?></p>
                                <div class="row">
                                    <div class="col-md-3 col-xs-12">
                                        <?=
                                        Html::a(\Yii::t('app', 'Подписаться'), '#', [
                                            'class' => 'btn btn-default realEstateSubmitButton bootbox',
                                            'bootbox-message' => \Yii::t('app', 'Подтверждение действия'),
                                            'bootbox-url' => \yii\helpers\Url::to(['profile/subscriptions', 'action' => true, 'value' => 1])
                                        ])
                                        ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>