<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="panel panel-default">
    <div class="panel-body">
        <h4><?= Html::a($model->title, ['profile/ticket-view', 'id' => $model->id]) ?></h4>
        <p class="text-muted"><i class="fa fa-globe"></i> <?= date('d/m/Y H:i', $model->created_at) ?></p>
        <div class="text-right">
            <?= Html::a(\Yii::t('app', 'Просмотр'), ['profile/ticket-view', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
            <?=
            Html::a(\Yii::t('app', 'Удалить'), '#', [
                'class' => 'btn btn-danger bootbox',
                'bootbox-url' => Url::to(['profile/ticket-delete/', 'id' => $model->id]),
                'bootbox-message' => \Yii::t('app', 'Подтверждение удаления')
            ])
            ?>
        </div>
    </div>
</div>