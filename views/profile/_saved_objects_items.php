<?php

/* @var $booking app\models\Booking */

use app\models\Booking;
use yii\helpers\Url;
use yii\bootstrap\Html;
use app\models\Lookup;
use app\models\Translation;

?>

<?php
    $realEstate = $booking->getRealEstate()->one();
    $services = $booking->getServiceOrders()->all(); ?>
<div class="panel panel-default panel-white">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3 col-xs-12 text-center">
                <?php $photos = $realEstate->getRealEstatePhotos()->orderBy('main, index')->all(); ?>
                <?php if ($photos) { ?>
                    <div class="selectedObjectPhotos">
                        <?php foreach ($photos as $photo) { ?>
<!--                            <div class="real-estate-photo">-->
                                <?= Html::img('@web' . $photo->src) ?>
<!--                            </div>-->
                        <?php } ?>
                    </div>
                <?php } else { ?>
                    <?= Html::img('@web/images/no-photo.jpg', ['class' => 'noPhoto']) ?>
                <?php } ?>
            </div>
            <?php $this->registerJs("$('.selectedObjectPhotos').fotorama({nav: false, fit: 'cover', width: 173, height: 130});"); ?>
            <div class="col-md-5 col-xs-6">
                <p><?= Html::a($realEstate->title, ['site/object', 'id' => $realEstate->id], ['class' => 'link-red']) ?></p>
                <p><?= \Yii::t('app', 'Бронирование № {x}', ['x' => $booking->id]) ?></p>
                <p><?= \Yii::t('app', 'Добавлено в {x}', ['x' => Yii::$app->formatter->asDate($booking->created_at, 'php:H:i, d.m.Y')]) ?></p>
                <br />
                <p class="text-muted"><?= \Yii::t('app', 'Заезд: {x}', ['x' => Yii::$app->formatter->asDate($booking->start_date, 'php:d.m.Y')]) ?></p>
                <p class="text-muted"><?= \Yii::t('app', 'Выезд: {x}', ['x' => Yii::$app->formatter->asDate($booking->end_date, 'php:d.m.Y')]) ?></p>
            </div>
            <div class="col-md-4 col-xs-12">
                <p class="price"><?= $booking->getTotalPrice() ?>$ <span class="status text-muted">(<?= Translation::t($booking, 'status') ?>)</span></p>

                    <?php if($booking->status == Booking::STATUS_NOT_PAYED) { ?>
                        <?php if($realEstate->isAvailableForBooking($booking->start_date, $booking->end_date)) { ?>
                            <?= Html::a(\Yii::t('app', 'Оплатить'), ['profile/pay', 'id' => $booking->id], [
                                'class' => 'btn btn-default btn-xs toggleRealEstateServices profileRealEstateBtn',
                            ]) ?>
                        <?php } else { ?>
                            <span class="text-danger"><?= Yii::t('app', 'Объект на текущие даты забронирован.') ?></span>
                        <?php } ?>

                    <?= Html::a(\Yii::t('app', 'Отменить'), '#', [
                    'class' => 'btn btn-default btn-xs selectedObjectBtn bootbox profileRealEstateBtn',
                    'bootbox-url' => Url::to(['booking/cancel-book', 'id' => $booking->id]),
                    'bootbox-message' => \Yii::t('app', 'Отменить бронирование?'),
                    ]) ?>

                    <?php if($services) { ?>
                        <?= Html::a(\Yii::t('app', 'Дополнительные услуги'), '#', [
                            'class' => 'btn btn-default btn-xs toggleRealEstateServices profileRealEstateBtn',
                        ]) ?>
                    <?php } ?>

                <?php } ?>
            </div>
        </div>
        <?php if($services) { ?>
        <div class="row">
            <div class="col-md-9 col-md-offset-3 realEstateServices" style="display: none;">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-right"><?= Yii::t('app', 'Название услуги') ?></th>
                            <th class="text-center"><?= Yii::t('app', 'Количество человек') ?></th>
                            <th class="text-center"><?= Yii::t('app', 'Стоимость') ?></th>
                            <th class="text-center"><?= Yii::t('app', 'Отменить') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($services as $service) { ?>
                            <tr>
                                <td class="text-right"><?= $service->getServiceName() ?></td>
                                <td class="text-center"><?= $service->people_count ?></td>
                                <td class="text-center"><?= $service->price ?>$</td>
                                <td class="text-center"><?= Html::a('<span class="glyphicon glyphicon-remove"></span>', ['booking/ajax-delete-service-order', 'id' => $service->id], ['class' => 'serviceOrderDelete']) ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php } ?>
    </div>
</div>