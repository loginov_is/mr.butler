<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\ListView;
use kartik\rating\StarRating;
use yii\grid\GridView;
use app\models\Reviews;

$this->title = \Yii::t('app', 'Просмотр отзыва от {date}', ['date' => date('d/m/Y H:i', $model->created_at)]);
?>
<?= $this->render('_mobile_profile_menu')?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_menu') ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->errorSummary([$model]) ?>
                <?= $this->render('//default/flash') ?>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->render('//default/_top_banner', ['title' => $this->title]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ol class="breadcrumb">
                                <li>
                                    <?= Html::a(\Yii::t('app', 'Отзывы'), ['profile/reviews']) ?>
                                </li>
                                <li class="active">
                                    <?= $this->title ?>
                                </li>
                            </ol>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default panel-white">
                        <div class="panel-body">
                            <p class="bold"><?= \Yii::t('app', 'Выберите объект, о котором хотите оставить отзыв') ?></p>
                            <?= $form->field($model, 'review')->textArea() ?>
                            <?= $form->field($model, 'video_review')->textInput(['placeholder' => \Yii::t('app', 'Введите ссылку на youtube видео')]) ?>
                        </div>
                    </div>
                    <div class="panel panel-default panel-white">
                        <div class="panel-body">
                            <p class="bold"><?= \Yii::t('app', 'Недостатки и преимущества') ?></p>
                            <br />
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'minuses')->textarea(['rows' => 4, 'placeholder' => \Yii::t('app', 'Опишите недостатки арендованной недвижимости')]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'pluses')->textarea(['rows' => 4, 'placeholder' => \Yii::t('app', 'Опишите преимущества арендованной недвижимости')]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default panel-white">
                        <div class="panel-body">
                            <?=
                            $form->field($model, 'rating')->widget(StarRating::classname(), [
                                'pluginOptions' => ['size' => 'xs', 'step' => \app\models\Reviews::RATING_STEP]
                            ]);
                            ?>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-3 col-xs-12">
                                    <?=
                                    Html::submitButton(\Yii::t('app', 'Отправить'), [
                                        'class' => 'btn btn-default realEstateSubmitButton'
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>