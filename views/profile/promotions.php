<?php

use yii\helpers\Html;
use app\widgets\PromotionsWidget;

$this->title = \Yii::t('app', 'Акции и скидки');
?>
<?= $this->render('_mobile_profile_menu')?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_menu') ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->render('//default/_top_banner', ['title' => $this->title]) ?>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-12 col-xs-12">
                            <?= PromotionsWidget::widget() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>