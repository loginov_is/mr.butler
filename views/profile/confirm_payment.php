<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\ListView;
use app\models\PaymentSystem;
use app\helpers\ContentHelper;

$this->title = \Yii::t('app', 'Подтверждение запроса на выплату от {date}', ['date' => date('d/m/Y H:i', $model->created_at)]);
?>
<?= $this->render('_mobile_profile_menu')?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_menu') ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="panel panel-default panel-white">
                        <div class="panel-body">
                            <?= Html::beginForm('', 'post') ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <ol class="breadcrumb">
                                        <li>
                                            <?= Html::a(\Yii::t('app', 'Мой баланс'), ['profile/my-balance']) ?>
                                        </li>
                                        <li class="active">
                                            <?= $this->title ?>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-12">
                                    <p class=""><?= \Yii::t('app', 'Дата создания') ?>: <?= date('d/m/Y H:i', $model->created_at) ?></p>
                                    <p class=""><?= \Yii::t('app', 'Сумма') ?>: <?= ContentHelper::getHumanAmount($model->amount, 'THB') ?></p>
                                </div>
                            </div>
                            <?php if ($fields) { ?>
                                <p class="bold"><?= \Yii::t('app', 'Заполните обязательные поля') ?>:</p>
                                <?php foreach ($fields as $field) { ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="bold"><?= $field->title ?></p>
                                            <?= Html::textInput('field[' . $field->id . ']', '', ['class' => 'form-control payment_field_required']) ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-4 col-xs-12">
                                    <?=
                                    Html::submitButton(\Yii::t('app', 'Подтвердить запрос'), [
                                        'class' => 'btn btn-default realEstateSubmitButton confirmPayment'
                                    ]);
                                    ?>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <?=
                                    Html::a(\Yii::t('app', 'Отменить запрос'), '#', [
                                        'class' => 'bootbox realEstateSubmitButton',
                                        'bootbox-url' => Url::to(['profile/delete-payment/', 'id' => $model->id]),
                                        'bootbox-message' => \Yii::t('app', 'Подтверждение отмены запроса на выплату')
                                    ]);
                                    ?>
                                </div>
                            </div>
                            <?= Html::endForm() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>