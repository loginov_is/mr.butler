<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\ListView;
use kartik\rating\StarRating;
use yii\grid\GridView;
use app\models\Reviews;

$this->title = \Yii::t('app', 'Отзывы');
?>
<?= $this->render('_mobile_profile_menu') ?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_menu') ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->errorSummary([$model]) ?>
                <?= $this->render('//default/flash') ?>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->render('//default/_top_banner', ['title' => $this->title]) ?>
                        </div>
                    </div>
                    <?php if ($reviewsItems) { ?>
                        <p class="text-center bold" style="margin-bottom: 20px; margin-top: 20px;"><?= \Yii::t('app', 'Уважаемый(ая), {username}! Оставьте отзыв об арендованной недвижимости', ['username' => $user->first_name]) ?></p>
                        <div class="panel panel-default panel-white">
                            <div class="panel-body">
                                <p class="bold"><?= \Yii::t('app', 'Выберите объект, о котором хотите оставить отзыв') ?></p>
                                <?= $form->field($model, 'real_estate_id')->dropDownList($reviewsItems)->label(false) ?>
                                <?= $form->field($model, 'review')->textArea() ?>
                                <?= $form->field($model, 'video_review')->textInput(['placeholder' => \Yii::t('app', 'Введите ссылку на youtube видео')]) ?>
                            </div>
                        </div>
                        <div class="panel panel-default panel-white">
                            <div class="panel-body">
                                <p class="bold"><?= \Yii::t('app', 'Недостатки и преимущества') ?></p>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'minuses')->textarea(['rows' => 4, 'placeholder' => \Yii::t('app', 'Опишите недостатки арендованной недвижимости')]) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'pluses')->textarea(['rows' => 4, 'placeholder' => \Yii::t('app', 'Опишите преимущества арендованной недвижимости')]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default panel-white">
                            <div class="panel-body">
                                <?=
                                $form->field($model, 'rating')->widget(StarRating::classname(), [
                                    'pluginOptions' => ['size' => 'xs', 'step' => \app\models\Reviews::RATING_STEP]
                                ]);
                                ?>
                                <div class="row" style="margin-top: 20px;">
                                    <div class="col-md-3 col-xs-12">
                                        <?=
                                        Html::submitButton(\Yii::t('app', 'Отправить'), [
                                            'class' => 'btn btn-default realEstateSubmitButton'
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="profileTable hidden-xs">
                    <h3><?= \Yii::t('core', 'Мои отзывы') ?></h3>
                    <?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'real_estate_id',
                                'value' => function ($row) {
                                    if (isset($row->realEstate->title)) {
                                        return $row->realEstate->title;
                                    } else {
                                        return \Yii::t('app', 'Неопределено');
                                    }
                                }
                            ],
                            [
                                'attribute' => 'created_at',
                                'value' => function($row) {
                                    return date('d/m/Y H:i', $row->created_at);
                                }
                            ],
                            [
                                'attribute' => 'status_id',
                                'value' => function ($row) {
                                    return Reviews::getList($row->status_id);
                                }
                            ],
                            [
                                'header' => '',
                                'format' => 'html',
                                'value' => function($row) {
                                    return Html::a(\Yii::t('app', 'Редактировать'), ['profile/review-update', 'id' => $row->id], ['class' => 'btn btn-xs btn-success']);
                                }
                                    ],
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="profileTable visible-xs">
                            <h3><?= \Yii::t('core', 'Мои отзывы') ?></h3>
                            <?=
                            GridView::widget([
                                'dataProvider' => $dataProvider,
                                'columns' => [
                                    [
                                        'attribute' => 'real_estate_id',
                                        'format' => 'html',
                                        'value' => function ($row) {
                                            if (isset($row->realEstate->title)) {
                                                return Html::a($row->realEstate->title, ['profile/review-update', 'id' => $row->id]);
                                            } else {
                                                return \Yii::t('app', 'Неопределено');
                                            }
                                        }
                                            ],
                                            [
                                                'attribute' => 'status_id',
                                                'value' => function ($row) {
                                                    return Reviews::getList($row->status_id);
                                                }
                                            ],
                                            [
                                                'header' => '',
                                                'format' => 'html',
                                                'value' => function($row) {
                                                    return Html::a(\Yii::t('app', 'Редактировать'), ['profile/review-update', 'id' => $row->id], ['class' => 'btn btn-xs btn-success']);
                                                }
                                                    ],
                                                ],
                                            ]);
                                            ?>
                                        </div>
                                    <?php } else { ?>
                                        <div class="alert alert-warning" style="margin-top: 20px;">
                                            <?= \Yii::t('app', 'Вы можете добавлять комментарии только к забронированным объектам') ?>
                                        </div>
                                    <?php } ?>
        </div>
    </div>
</div>