<?php

/* @var $model app\models\Booking */

use yii\helpers\Html;

$this->title = \Yii::t('app', 'Арендованные объекты недвижимости');
?>
<?= $this->render('_mobile_profile_menu')?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_menu') ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->render('//default/_top_banner', ['title' => $this->title]) ?>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-12 col-xs-12">
                            <?php if($model) { ?>
                                <p><?= \Yii::t('app', 'Предстоящее проживание') ?>:</p>
                                <?php foreach($model as $booking) { ?>
                                    <?php if($booking->start_date > time()) { ?>
                                        <?= $this->render('_selected_objects_items', ['booking' => $booking, 'previousOjects' => false]) ?>
                                    <?php } ?>
                                <?php } ?>

                                <p><?= \Yii::t('app', 'Предыдущее проживание') ?>:</p>
                                <?php foreach($model as $booking) { ?>
                                    <?php if($booking->end_date <= time()) { ?>
                                        <?= $this->render('_selected_objects_items', ['booking' => $booking, 'previousOjects' => true]) ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } else { ?>
                                <?= Yii::t('app', 'Нет информации.') ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>