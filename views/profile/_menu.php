<?php

use yii\bootstrap\Tabs;
use app\models\Reviews;
use app\models\Accounting;
?>

<?php

$reviewsCount = Reviews::getUserReviews(Yii::$app->user->id, true);
$reviewsLabel = ($reviewsCount) ? \Yii::t('app', 'Отзывы') : \Yii::t('app', 'Отзывы') . ' (!)';
$class = isset($mobileMenu) ? 'aboutLeftMenu' : "nav nav-pills nav-stacked leftMenu";
$items = [
    [
        'label' => \Yii::t('app', 'Мой профиль'),
        'url' => ['profile/index'],
        'active' => Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'index'
    ],
//    [
//        'label' => \Yii::t('app', 'Оплатить'),
//        'url' => ['profile/object-payment'],
//        'active' => false,
//        'visible' => Yii::$app->user->can('renter')
//    ],
    [
        'label' => \Yii::t('app', 'Мои объекты'),
        'url' => ['profile/my-objects'],
        'active' => Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'my-objects',
        'visible' => Yii::$app->user->can('landlord')
    ],
    [
        'label' => \Yii::t('app', 'Мои баланс'),
        'url' => ['profile/my-balance'],
        'active' => Yii::$app->controller->id == 'profile' && in_array(Yii::$app->controller->action->id, ['my-balance', 'payment-view', 'confirm-payment', 'balance']),
        'visible' => Yii::$app->user->can('landlord')
    ],
//    [
//        'label' => \Yii::t('app', 'Контрольная проверка объекта'),
//        'url' => ['profile/control-check-objects'],
//        'active' => Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'control-check-objects',
//        'visible' => Yii::$app->user->can('landlord')
//    ],
    [
        'label' => $reviewsLabel,
        'url' => ['profile/reviews'],
        'active' => Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'reviews',
        'visible' => Yii::$app->user->can('renter') || Yii::$app->user->can('landlord')
    ],
    [
        'label' => \Yii::t('app', 'Подписки'),
        'url' => ['profile/subscriptions'],
        'active' => Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'subscriptions',
        'visible' => Yii::$app->user->can('renter') || Yii::$app->user->can('landlord')
    ],
//    [
//        'label' => \Yii::t('app', 'Дополнительные услуги'),
//        'url' => ['profile/advance-services'],
//        'active' => Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'advance-services',
//        'active' => false,
//        'visible' => Yii::$app->user->can('renter')
//    ],
    [
        'label' => \Yii::t('app', 'Написать в тех. поддержку'),
        'url' => ['profile/tech-feedback'],
        'active' => Yii::$app->controller->id == 'profile' && in_array(Yii::$app->controller->action->id, ['tech-feedback', 'ticket-view']),
        'visible' => Yii::$app->user->can('renter') || Yii::$app->user->can('landlord')
    ],
    [
        'label' => \Yii::t('app', 'Условия и конфиденциальность'),
        'url' => ['profile/terms'],
        'active' => Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'terms',
        'visible' => Yii::$app->user->can('renter') || Yii::$app->user->can('landlord')
    ],
    [
        'label' => \Yii::t('app', 'Акции и скидки'),
        'url' => ['profile/promotions'],
        'active' => Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'promotions',
        'visible' => Yii::$app->user->can('renter') || Yii::$app->user->can('landlord')
    ],
    [
        'label' => \Yii::t('app', 'Просмотренные объекты'),
        'url' => ['profile/viewed-objects'],
        'active' => Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'viewed-objects',
        'visible' => Yii::$app->user->can('renter')
    ],
    [
        'label' => \Yii::t('app', 'Выбранные объекты'),
        'url' => ['profile/selected-objects'],
        'active' => Yii::$app->controller->id == 'profile' && Yii::$app->controller->action->id == 'selected-objects',
        'visible' => Yii::$app->user->can('renter')
    ],
];


if (isset($mobileMenu)) {
    echo yii\widgets\Menu::widget([
        'options' => ['class' => $class],
        'items' => $items
    ]);
} else {
    echo Tabs::widget([
        'options' => ['class' => $class],
        'items' => $items
    ]);
}
?>