<?php

/** @var $model \app\models\User */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use app\models\Lookup;

$this->title = \Yii::t('app', 'Мой профиль');
?>
<?php
$form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]);
?>
<?= $this->render('_mobile_profile_menu')?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_menu') ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->render('//default/_top_banner', ['title' => $this->title]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->errorSummary([$model]) ?>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-4 col-xs-12">
                            <?= $form->field($model, 'first_name')->textInput(['class' => 'form-control fieldAdvance']) ?>
                            <?= $form->field($model, 'last_name')->textInput(['class' => 'form-control fieldAdvance']) ?>
                            <?= $form->field($model, 'phone')->textInput(['class' => 'form-control fieldAdvance']) ?>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <label class="control-label" for="user-birthday"><?= $model->getAttributeLabel('birthday') ?></label>
                            <?=
                            DatePicker::widget([
                                'model' => $model,
                                'dateFormat' => 'dd/MM/y',
                                'attribute' => 'birthdayField',
                                'options' => [
                                    'class' => 'form-control fieldAdvance'
                                ],
                                'language' => Yii::$app->language,
                            ]);
                            ?>
                            <div style="margin-top: 20px;">
                                <?= $form->field($model, 'payment_method')->radioList(Lookup::items(Lookup::TYPE_USER_PAYMENT_METHOD)) ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12 text-center">
                            <div class="row">
                                <div class="col-md-12 text-center">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center" style="overflow: hidden;">
                                    <?= Html::img($model->avatarUrl, ['alt' => 'User avatar', 'class' => 'profileNoAvatar thumbnail']) ?>
                                    <?= $form->field($model, 'avatar')->fileInput() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-xs-12">
                            <?=
                            Html::submitButton(($model->getIsNewRecord()) ? \Yii::t('app', 'Отправить') : \Yii::t('app', 'Сохранить'), [
                                'class' => 'btn btn-default realEstateSubmitButton'
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>