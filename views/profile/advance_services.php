<?php

use yii\helpers\Html;

$this->title = \Yii::t('app', 'Дополнительные услуги');
?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_menu') ?>
                </div>
            </div> 
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->render('//default/_top_banner', ['title' => $this->title]) ?>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <p class="bold"><?= Yii::t('app', 'Выберите необходимые услуги') ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>