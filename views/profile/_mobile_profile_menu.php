<?php

use yii\helpers\Html;
?>
<div class="pageAbout visible-xs">
    <div class="leftMenu">
        <div class="leftMenuButton">
            <i class="fa fa-navicon"></i>
        </div>
        <div class="menu" style="margin-top: 20px;">
            <?=
            $this->render('_menu', [
                'mobileMenu' => 'true'
            ])
            ?>
        </div>
    </div>
</div>