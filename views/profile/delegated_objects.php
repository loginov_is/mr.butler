<?php

/* @var $model app\models\Booking */

use yii\helpers\Html;
use yii\jui\DatePicker;
use app\models\Booking;
use yii\web\JsExpression;

$this->title = \Yii::t('app', 'Переданные объекты в доверительное управление');
?>
<?= $this->render('_mobile_profile_menu')?>
<div class="container">
    <div class="row profileBlock">
        <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $this->render('_menu') ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->render('//default/_top_banner', ['title' => $this->title]) ?>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-12 col-xs-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default" style="background: #fff;">
                                        <div class="panel-body">
                                            <div class="profileCalendars">
                                                <div class="row">
                                                    <div class="col-md-8 col-md-offset-2">
                                                        <?= Html::dropDownList(null, null, \yii\helpers\ArrayHelper::map($model, 'id', 'title'), ['class' => 'form-control profileCalendarSelector']) ?>
                                                    </div>
                                                </div>
                                                <?php foreach($model as $realEstate) { ?>
                                                    <div class="row hidden" data-key="<?= $realEstate->id ?>">
                                                        <div class="col-md-12">
                                                            <div class="profileCalendar center-block">
                                                            <?php
                                                            $booking = json_encode(
                                                                $realEstate->getBooking()->select(['start_date', 'end_date'])
                                                                    ->andWhere(['<>', 'status', Booking::STATUS_NOT_PAYED])
                                                                    ->andWhere(['<>', 'status', Booking::STATUS_CANCELED])
                                                                    ->asArray()
                                                                    ->all()
                                                            );
                                                            echo DatePicker::widget([
                                                                'name'  => 'from_date',
                                                                'value'  => '',
                                                                'inline' => true,
                                                                'clientOptions' => [
                                                                    'beforeShowDay' => new JsExpression("
                                                                        function(date) {
                                                                            var available = 1;
                                                                            var booking = $.parseJSON('" . $booking . "');
                                                                            var timestamp = (Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()) / 1000) + 86400;

                                                                            for(var i = 0; i < booking.length; i++) {
                                                                                if(timestamp >= booking[i].start_date && timestamp <= booking[i].end_date) {
                                                                                    available = 0;
                                                                                    break;
                                                                                }
                                                                            }
                                                                            return [1, available ? '' : 'highlight', available ? '" . Yii::t('app', 'Свободно') . "' : '" . Yii::t('app', 'Занято') . "'];
                                                                        }
                                                                    "),
                                                                ],
                                                                'containerOptions' => [
                                                                    'class' => '',
                                                                ],
                                                            ]);
                                                            ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="row" style="margin-top: 20px;">
                                                    <div class="col-md-4 col-xs-12 text-center">
                                                        <i class="calendarLegend"></i> <?= Yii::t('app', 'Свободно') ?>
                                                    </div>
                                                    <div class="col-md-4 col-xs-12 text-center">
                                                        <i class="calendarLegend" style="background: #d1d1d1;"></i> <?= Yii::t('app', 'Забронировано') ?>
                                                    </div>
                                                    <div class="col-md-4 col-xs-12 text-center">
                                                        <i class="calendarLegend" style="background: #bdd610;"></i> <?= Yii::t('app', 'Сдано') ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if($model) { ?>
                                <?php foreach($model as $realEstate) { ?>

                                    <div class="panel panel-default panel-white  <?= $cssClass ?>">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-3 col-xs-12 text-center">
                                                    <?php if($realEstate) { ?>
                                                        <?php $photos = $realEstate->getRealEstatePhotos()->orderBy('main, index')->all(); ?>
                                                        <?php if ($photos) { ?>
                                                            <div class="myObjectPhotos" style="max-width: 100%;">
                                                                <?php foreach ($photos as $photo) { ?>
                                                                    <?= Html::img('@web' . $photo->src) ?>
                                                                <?php } ?>
                                                            </div>
                                                        <?php } else { ?>
                                                            <?= Html::img('@web/images/no-photo.jpg', ['class' => 'noPhoto']) ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                                <?php $this->registerJs("$('.myObjectPhotos').fotorama({nav: false, fit: 'cover', width: '100%', minheight: 150});"); ?>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <div class="visible-xs" style="margin-bottom: 20px;"></div>
                                                    <?php if($realEstate) { ?>
                                                        <p><?= Html::a($realEstate->title, ['site/object', 'id' => $realEstate->id], ['class' => 'link-red']) ?></p>
                                                    <?php } ?>
                                                    <br />
                                                    <p><?= \Yii::t('app', 'Стоимость за сутки: {x}', ['x' => $realEstate->daily_price]) ?></p>
                                                    <p><?= \Yii::t('app', 'Стоимость за месяц: {x}', ['x' => $realEstate->monthly_price]) ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>
<!--                                <p>--><?//= \Yii::t('app', 'Предстоящее проживание') ?><!--:</p>-->
<!--                                --><?php //foreach($model as $booking) { ?>
<!--                                    --><?php //if($booking->start_date > time()) { ?>
<!--                                        --><?//= $this->render('_selected_objects_items', ['booking' => $booking, 'previousOjects' => false]) ?>
<!--                                    --><?php //} ?>
<!--                                --><?php //} ?>
<!---->
<!--                                <p>--><?//= \Yii::t('app', 'Предыдущее проживание') ?><!--:</p>-->
<!--                                --><?php //foreach($model as $booking) { ?>
<!--                                    --><?php //if($booking->end_date <= time()) { ?>
<!--                                        --><?//= $this->render('_selected_objects_items', ['booking' => $booking, 'previousOjects' => true]) ?>
<!--                                    --><?php //} ?>
<!--                                --><?php //} ?>
                            <?php } else { ?>
                                <?= Yii::t('app', 'Нет информации.') ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>