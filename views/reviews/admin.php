<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Reviews;
use app\models\User;
use app\models\RealEstate;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Отзывы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить отзыв'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'real_estate_id',
                'filter' => RealEstate::find()->select(['title', 'id'])->indexBy('id')->column(),
                'format' => 'html',
                'value' => function ($row) {
            if (isset($row->realEstate->title)) {
                return $row->realEstate->title;
            } else {
                return \Yii::t('app', 'Неопределено');
            }
        }
            ],
            [
                'attribute' => 'created_by',
                'filter' => User::find()->select(['email', 'id'])->indexBy('id')->column(),
                'value' => function ($row) {
            if (isset($row->createdBy->email)) {
                return $row->createdBy->email;
            } else {
                return \Yii::t('app', 'Неопределено');
            }
        }
            ],
            [
                'attribute' => 'status_id',
                'filter' => Reviews::getList(),
                'value' => function ($row) {
                    return Reviews::getList($row->status_id);
                }
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}', 'options' => ['style' => 'width:60px']],
        ],
    ]);
    ?>

</div>
