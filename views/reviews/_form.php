<?php

use app\widgets\Translation;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\RealEstate;
use app\models\User;
use app\models\Reviews;
use kartik\widgets\StarRating;
?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
<?= $form->errorSummary([$model]) ?>
<div class="row">
    <div class="col-md-12">
        <?=
        $form->field($model, 'status_id')->dropDownList(
                Reviews::getList(), [
            'prompt' => \Yii::t('app', 'Выберите статус'),
            'class' => 'form-control'
                ]
        );
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=
        $form->field($model, 'real_estate_id')->dropDownList(
                RealEstate::find()->select(['title', 'id'])->indexBy('id')->column(), [
            'prompt' => \Yii::t('app', 'Выберите объект'),
            'class' => 'form-control'
                ]
        );
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=
        $form->field($model, 'created_by')->dropDownList(
                User::find()->select(['email', 'id'])->indexBy('id')->column(), [
            'prompt' => \Yii::t('app', 'Выберите пользователя'),
            'class' => 'form-control'
                ]
        );
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'review')->textarea(['rows' => 5]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'video_review')->textInput() ?>
    </div>
</div>
<?php if (!$model->isNewRecord && $model->youtube_id) { ?>
    <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="<?= "https://www.youtube.com/embed/" . $model->youtube_id; ?>"></iframe>
    </div>
<?php } ?>
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'pluses')->textarea(['rows' => 5]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'minuses')->textarea(['rows' => 5]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=
        $form->field($model, 'rating')->widget(StarRating::classname(), [
            'pluginOptions' => ['size' => 'xs', 'step' => \app\models\Reviews::RATING_STEP]
        ]);
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=
        Html::submitButton(($model->getIsNewRecord()) ? \Yii::t('app', 'Создать') : \Yii::t('app', 'Сохранить'), [
            'class' => 'btn btn-success'
        ]);
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>