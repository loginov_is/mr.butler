<?php

use app\widgets\Translation;
use yii\helpers\Html;
use app\models\City;
use app\models\Video;
use app\models\Region;
use yii\widgets\ActiveForm;
use app\models\ImageAlbumToRegion;
use app\models\ImageAlbum;
use yii\helpers\Url;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use app\models\Lookup;
?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
<div class="panel panel-default">
    <div class="panel-heading"><?= \Yii::t('app', 'Основная информация'); ?></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <?=
                Translation::widget([
                    'field' => $form->field($model, 'name')->textInput(),
                ])
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?=
                $form->field($model, 'city_id')->dropDownList(
                        City::find()->select(['name', 'id'])->indexBy('id')->column(), [
                    'prompt' => \Yii::t('app', 'Выберите город'),
                    'class' => 'form-control'
                        ]
                );
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php if ($model->avatar) echo Html::img('@web/upload/' . $model->avatar); ?>
                <?= $form->field($model, 'avatar')->fileInput(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?=
                $form->field($model, 'description')->widget(yii\imperavi\Widget::className(), [
                    'options' => [
                        'lang' => 'ru',
                        'minHeight' => 200,
                        'imageUpload' => Url::to([
                            'image/upload',
                            'type' => Inflector::camel2id(StringHelper::basename(get_class($model))),
                            'id' => $model->isNewRecord ? 'tmp' : $model->id
                        ]),
                        /* 'fileUpload' => Url::to([
                          'file/upload',
                          'type' => Inflector::camel2id(StringHelper::basename(get_class($model))),
                          'id' => $model->isNewRecord ? 'new' : $model->id
                          ]), */
                        'uploadImageFields' => [
                            Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                        ],
                        'convertImageLinks' => true,
                        'convertVideoLinks' => true,
                        //'imageGetJson' => Url::to(['/admin/gallery/json-list']),
                        //'imageManagerJson' => Url::to(['/admin/gallery/json-list']),
                        //'fileManagerJson' => Url::to(['/admin/gallery/json-list']),
                        'imageUploadCallback' => $model->isNewRecord ? new \yii\web\JsExpression(<<<JS
                function(image, json) {
                    $(".page-form")
                        .find('form')
                        .append($('<input type="hidden" name="images[]" value="" />').val($(image).attr("src")));
                }
JS
                                ) : false,
                    //'definedLinks' => '/defined-links.json', // @todo Action to site pages
                    ],
                    'plugins' => [
                        'fullscreen',
                        'table',
                        'video',
                        'imagemanager',
                        'filemanager',
                        'definedlinks',
                        'fontsize',
                        'fontfamily',
                        'fontcolor',
                    //'clips',
                    ]
                ])
                ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <?php if (!$model->isNewRecord) { ?>
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><?= \Yii::t('app', 'Фотоальбомы района'); ?></div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-7 col-xs-7">
                                            <?=
                                            Html::dropDownList(
                                                    'photoalbums', false, ImageAlbum::find()->select(['name', 'id'])->indexBy('id')->column(), [
                                                'prompt' => \Yii::t('app', 'Выберите фотоальбом'),
                                                'class' => 'form-control selectAlbum-' . ImageAlbumToRegion::TYPE_ID_PHOTOALBUMS
                                                    ]
                                            );
                                            ?>
                                        </div>
                                        <div class="col-md-5 col-xs-5">
                                            <?=
                                            Html::submitButton(\Yii::t('app', 'Добавить'), [
                                                'class' => 'btn btn-default addAlbum',
                                                'url' => \Yii::$app->getUrlManager()->createUrl(['/region/set-region-album']),
                                                'typeId' => ImageAlbumToRegion::TYPE_ID_PHOTOALBUMS,
                                                'regionId' => $model->id
                                            ]);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="albumBlock-<?= ImageAlbumToRegion::TYPE_ID_PHOTOALBUMS; ?>">
                                        <?php
                                        foreach ($model->getImageAlbums(ImageAlbumToRegion::TYPE_ID_PHOTOALBUMS) as $album) {
                                            echo $this->render('_album_item', [
                                                'model' => $album,
                                                'regionId' => $model->id,
                                                'typeId' => ImageAlbumToRegion::TYPE_ID_PHOTOALBUMS
                                            ]);
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <?= $form->field($model, 'comfort_ids')->checkBoxList(Lookup::items(Lookup::TYPE_REGION_CONFORM, true), ['multiple' => true]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><?= \Yii::t('app', 'Достопримечательности района'); ?></div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-7 col-xs-7">
                                            <?=
                                            Html::dropDownList(
                                                    'photoalbums', false, ImageAlbum::find()->select(['name', 'id'])->indexBy('id')->column(), [
                                                'prompt' => \Yii::t('app', 'Выберите фотоальбом'),
                                                'class' => 'form-control selectAlbum-' . ImageAlbumToRegion::TYPE_ID_SIGHT
                                                    ]
                                            );
                                            ?>
                                        </div>
                                        <div class="col-md-5 col-xs-5">
                                            <?=
                                            Html::submitButton(\Yii::t('app', 'Добавить'), [
                                                'class' => 'btn btn-default addAlbum',
                                                'url' => \Yii::$app->getUrlManager()->createUrl(['/region/set-region-album']),
                                                'typeId' => ImageAlbumToRegion::TYPE_ID_SIGHT,
                                                'regionId' => $model->id
                                            ]);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="albumBlock-<?= ImageAlbumToRegion::TYPE_ID_SIGHT; ?>">
                                        <?php
                                        foreach ($model->getImageAlbums(ImageAlbumToRegion::TYPE_ID_SIGHT) as $album) {
                                            echo $this->render('_album_item', [
                                                'model' => $album,
                                                'regionId' => $model->id,
                                                'typeId' => ImageAlbumToRegion::TYPE_ID_SIGHT
                                            ]);
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><?= \Yii::t('app', 'Видео района'); ?></div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-7 col-xs-7">
                                            <?=
                                            Html::dropDownList(
                                                    'videos', false, Video::find()->select(['name', 'id'])->indexBy('id')->column(), [
                                                'prompt' => \Yii::t('app', 'Выберите видео'),
                                                'class' => 'form-control selectVideo'
                                                    ]
                                            );
                                            ?>
                                        </div>
                                        <div class="col-md-5 col-xs-5">
                                            <?=
                                            Html::submitButton(\Yii::t('app', 'Добавить'), [
                                                'class' => 'btn btn-default addVideo',
                                                'url' => \Yii::$app->getUrlManager()->createUrl(['/region/set-region-video']),
                                                'regionId' => $model->id
                                            ]);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="videoBlock">
                                        <?php
                                        foreach ($model->videos as $video) {
                                            echo $this->render('_video_item', [
                                                'model' => $video,
                                                'regionId' => $model->id
                                            ]);
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><?= \Yii::t('app', 'Граничащие районы'); ?></div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-7 col-xs-7">
                                            <?=
                                            Html::dropDownList(
                                                    'childRegions', false, Region::find()->select(['name', 'id'])->indexBy('id')->column(), [
                                                'prompt' => \Yii::t('app', 'Выберите регион'),
                                                'class' => 'form-control selectChildRegion'
                                                    ]
                                            );
                                            ?>
                                        </div>
                                        <div class="col-md-5 col-xs-5">
                                            <?=
                                            Html::submitButton(\Yii::t('app', 'Добавить'), [
                                                'class' => 'btn btn-default addChildRegion',
                                                'url' => \Yii::$app->getUrlManager()->createUrl(['/region/set-region-child']),
                                                'regionId' => $model->id
                                            ]);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="childRegionBlock">
                                        <?php
                                        foreach ($model->children as $region) {
                                            echo $this->render('_region_child_item', [
                                                'parentId' => $model->id,
                                                'model' => $region
                                            ]);
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="col-md-5">
        <?= $form->field($model, 'map')->hiddenInput(['maxlength' => true, 'class' => 'hiddenCoords']) ?>
        <div id="map" style="width: 100%; height: 400px; margin-bottom: 20px;"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=
        Html::submitButton(($model->getIsNewRecord()) ? \Yii::t('app', 'Создать') : \Yii::t('app', 'Сохранить'), [
            'class' => 'btn btn-success'
        ]);
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>