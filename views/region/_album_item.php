<?php

use yii\helpers\Html;
?>

<?php if ($model) { ?>
    <div class="row" id="album-<?= $typeId; ?>-<?= $model->id; ?>">
        <hr />
        <div class="col-md-10 col-xs-10">
            <?= Html::a($model->name, ['/album/update', 'id' => $model->id]); ?>
        </div>
        <div class="col-md-2 col-xs-2">
            <?=
            Html::button('<i class="fa fa-minus"></i>', [
                'class' => 'btn btn-default removeAlbum',
                'albumId' => $model->id,
                'regionId' => $regionId,
                'typeId' => $typeId,
                'title' => \Yii::t('app', 'Удалить'),
                'url' => \Yii::$app->getUrlManager()->createUrl(['/region/unset-region-album']),
            ]);
            ?>
        </div>
        <hr />
    </div>
<?php } ?>
