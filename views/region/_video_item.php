<?php

use yii\helpers\Html;
?>

<?php if ($model) { ?>
    <div class="row" id="video-<?= $model->id; ?>">
        <hr />
        <div class="col-md-10 col-xs-10">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="<?= $model->getYoutubeFullUrl($model->youtube_id); ?>"></iframe>
            </div>
        </div>
        <div class="col-md-2 col-xs-2">
            <?=
            Html::button('<i class="fa fa-minus"></i>', [
                'class' => 'btn btn-default removeVideo',
                'videoId' => $model->id,
                'regionId' => $regionId,
                'title' => \Yii::t('app', 'Удалить'),
                'url' => \Yii::$app->getUrlManager()->createUrl(['/region/unset-region-video']),
            ]);
            ?>
        </div>
        <hr />
    </div>
<?php } ?>