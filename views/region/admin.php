<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('app', 'Регионы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить регион'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            [
                'attribute' => 'city_id',
                'value' => function ($data) {
                    if (isset($data->city->name)) {
                        return $data->city->name;
                    } else {
                        return $data->city_id;
                    }
                }
            ],
            [
                'class' => \yii\grid\ActionColumn::className(),
                'template' => '{update} {delete}',
            ],
        ],
    ])
    ?>
</div>