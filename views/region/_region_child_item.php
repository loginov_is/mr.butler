<?php

use yii\helpers\Html;
?>

<?php if ($model) { ?>
    <div class="row" id="region-<?= $parentId ?>-child-<?= $model->id; ?>">
        <hr />
        <div class="col-md-10 col-xs-10">
            <?= Html::a($model->name, ['/region/update', 'id' => $model->id]); ?>
        </div>
        <div class="col-md-2 col-xs-2">
            <?=
            Html::button('<i class="fa fa-minus"></i>', [
                'class' => 'btn btn-default removeRegionChild',
                'childId' => $model->id,
                'regionId' => $parentId,
                'title' => \Yii::t('app', 'Удалить'),
                'url' => \Yii::$app->getUrlManager()->createUrl(['/region/unset-region-child']),
            ]);
            ?>
        </div>
        <hr />
    </div>
<?php } ?>
