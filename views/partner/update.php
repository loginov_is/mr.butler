<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Изменить партнера: ') . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Партнеры'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Изменить');
?>
<div class="partner-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
