<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Добавить партнера');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Партнеры'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
