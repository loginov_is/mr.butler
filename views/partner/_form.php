<?php

use app\widgets\Translation;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
<?= $form->errorSummary([$model]) ?>
<div class="row">
    <div class="col-md-12">
        <?=
        Translation::widget([
            'field' => $form->field($model, 'name')->textInput(),
        ])
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=
        Translation::widget([
            'field' => $form->field($model, 'description')->textArea(),
        ])
        ?>
    </div>
</div>
<?php if ($model->isNewRecord) { ?>
    <div class="row">
        <div class="col-md-12">
            <?php echo $form->field($model, 'image')->fileInput() ?>
        </div>
    </div>
<?php } ?>
<div class="row">
    <div class="col-md-12">
        <?=
        Html::submitButton(($model->getIsNewRecord()) ? \Yii::t('app', 'Создать') : \Yii::t('app', 'Сохранить'), [
            'class' => 'btn btn-success'
        ]);
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>