<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\widgets\Translation;
?>
<?php $form = ActiveForm::begin() ?>
<?= $form->errorSummary([$model]) ?>
<div class="row">
    <div class="col-md-12">
        <?php echo $form->field($model, 'code')->textInput(); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=
        Translation::widget([
            'field' => $form->field($model, 'description')->textInput(),
        ])
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?=
        Html::submitButton(($model->getIsNewRecord()) ? \Yii::t('app', 'Создать') : \Yii::t('app', 'Сохранить'), [
            'class' => 'btn btn-success'
        ]);
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>