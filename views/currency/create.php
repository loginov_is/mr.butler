<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Добавить валюту');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Валюты'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
