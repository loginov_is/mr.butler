<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Изменить валюту: ') . ' ' . $model->code;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Валюты'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Изменить');
?>
<div class="currency-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
