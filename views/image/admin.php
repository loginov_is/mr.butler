<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
$this->title = Yii::t('app', 'Изображения');
$this->params['breadcrumbs'][] = $this->title;
?>


<h1><?= Html::encode($this->title) ?></h1>

<p>
    <?= Html::a(Yii::t('app', 'Добавить альбом'), ['create'], ['class' => 'btn btn-success']) ?>
</p>
    <div class="col-md-12">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                [
                    'attribute' => 'image',
                    'format' => 'raw',
                    'value' => function($data) {
                        return Html::img($data->imageUrl, ['alt' => $data->image, 'class' => 'gridImage', 'style' => 'max-width:300px;max-height:300px;']);
                    },
                        ],
                        [
                            'attribute' => 'albums',
                            'format' => 'html',
                            'value' => function($data) {
                                $albums = $data->getImageAlbums();
                                if ($albums) {
                                    $content = "";
                                    foreach ($albums as $album) {
                                        $content .= Html::beginTag('div', ['class' => 'row']);
                                        $content .= Html::beginTag('div', ['class' => 'col-md-10']);
                                        $content .= Html::a($album->name, ['/album/update', 'id' => $album->id]);
                                        $content .= Html::endTag('div');
                                        $content .= Html::beginTag('div', ['class' => 'col-md-2']);
                                        $content .= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['/image/unset-image-relation', 'imageId' => $data->id, 'albumId' => $album->id]);
                                        $content .= Html::endTag('div');
                                        $content .= Html::endTag('div');
                                    }
                                    return $content;
                                }
                            },
                                ],
                                [
                                    'header' => \Yii::t('core', 'Закрепить за альбомом'),
                                    'options' => [
                                        'style' => 'width: 300px;'
                                    ],
                                    'format' => 'raw',
                                    'value' => function ($data) use ($albums) {
                                if ($albums) {
                                    $content = Html::beginForm(['/image/set-image-relation'], 'post');
                                    $content .= Html::beginTag('div', ['class' => 'row']);
                                    $content .= Html::beginTag('div', ['class' => 'col-md-6']);
                                    $content .= Html::hiddenInput('ImageToAlbum[image_id]', $data->id);
                                    $content .= Html::dropDownList('ImageToAlbum[album_id]', false, ArrayHelper::map($albums, 'id', 'name'), ['class' => 'form-control']);
                                    $content .= Html::endTag('div');
                                    $content .= Html::beginTag('div', ['class' => 'col-md-6']);
                                    $content .= Html::submitButton(\Yii::t('app', 'Закрепить'), ['class' => 'btn btn-default']);
                                    $content .= Html::endTag('div');
                                    $content .= Html::endTag('div');
                                    $content .= Html::endForm();
                                    return $content;
                                }
                            }
                                ],
                                [
                                    'class' => \yii\grid\ActionColumn::className(),
                                    'template' => '{delete}',
                                ],
                            ],
                        ])
                        ?>