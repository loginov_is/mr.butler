<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Добавить изображение');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Изображения'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
