<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Carousel;
use app\widgets\LanguageWidget;
use app\widgets\RegistrationWidget;
use app\widgets\HeaderSocialLinksWidget;
use app\widgets\CurrencyWidget;
use app\widgets\FooterFeedbackWidget;
use app\widgets\AuthWidget;
use app\widgets\UserTopNavigationWidget;
use app\widgets\BottomMenuWidget;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\GalleryAsset;

AppAsset::register($this);
GalleryAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <?php $this->registerCssFile(Yii::getAlias('@web') . '/lib/font-awesome-4.4.0/css/font-awesome.min.css'); ?>
        <?php
        $this->registerJsFile(Yii::getAlias('@web') . '/js/main.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->registerJsFile('http://maps.google.com/maps/api/js?sensor=false', ['depends' => [\yii\web\JqueryAsset::className()]]);
        ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div class="wrap">
            <div class="topLane">
                <div class="container">
                    <div class="row topLaneNav text-center">
                        <div class="col-md-3 col-xs-0"></div>
                        <div class="col-md-1 item col-xs-3 topFlags">
                            <?= LanguageWidget::widget() ?>
                        </div>
                        <div class="col-md-1 col-xs-3 item currencyDropDownBlock">
                            <?= CurrencyWidget::widget() ?>
                        </div>
                        <div class="col-md-3 col-xs-5 item itemLast">
                            <?= HeaderSocialLinksWidget::widget(); ?>
                        </div>
                        <div class="col-md-4 col-xs-0"></div>
                    </div>
                </div>
            </div>
            <div class="topSlide">
                <div class="navbar-wrapper">
                    <?php
                    NavBar::begin([
                        'brandLabel' => Html::img('@web/images/main_logo.png', ['alt' => 'Logo']),
                        'brandUrl' => Yii::$app->homeUrl,
                        'options' => [
                            'class' => 'navbar-inverse navbar-fixed-top navbarAdvance',
                        ],
                    ]);
                    echo Nav::widget([
                        'options' => ['class' => 'navbar-nav navbar-right topMenu',],
                        'items' => [
                            [
                                'label' => '<i class="fa fa-cog"></i> ' . \Yii::t('app', 'Администрирование'),
                                'url' => ['/admin/index'],
                                'visible' => Yii::$app->user->can('admin'),
                            ],
                            [
                                'label' => '<i class="fa fa-building-o"></i> ' . \Yii::t('app', 'Передать недвижимость в управление'),
                                'url' => ['/real-estate/add']
                            ],
                            [
                                'label' => '<i class="fa fa-question-circle"></i> ' . \Yii::t('app', 'Помощь'),
                                'url' => ['/faq/index']
                            ],
                            [
                                'label' => '<i class="fa fa-user"></i> ' . \Yii::t('app', 'Регистрация'),
                                'url' => ['/site/signup'],
                                'visible' => Yii::$app->user->isGuest,
                                'linkOptions' => [
                                    'class' => 'showRegistrModal'
                                ],
                            ],
                            Yii::$app->user->isGuest ?
                                    [
                                'label' => '<i class="fa fa-lock"></i> ' . \Yii::t('app', 'Войти'),
                                'url' => ['/site/login'],
                                'linkOptions' => [
                                    'class' => 'showLoginModal'
                                ]
                                    ] :
                                    [
                                'label' => '<i class="fa fa-lock"></i> ' . \Yii::t('app', 'Выйти') . ' (' . Yii::$app->user->identity->email . ')',
                                'url' => ['/site/logout'],
                                'linkOptions' => ['data-method' => 'post']
                                    ],
                        ],
                        'encodeLabels' => false
                    ]);
                    NavBar::end();
                    ?>
                </div>
                <!--                --><?php //Carousel::begin()  ?>
                <?= UserTopNavigationWidget::widget() ?>
                <?php
                    $class = '';
                    if (Yii::$app->controller->id == 'profile') {
                        $class = 'hidden';
                    }
                ?>
                <div class="carousel-wrapper <?= $class ?>">
                    <div class="container">
                        <?=
                        Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ])
                        ?>
                    </div>
                    <?= isset($this->params['carousel-block']) ? $this->params['carousel-block'] : '' ?>
                </div>
                <!--                --><?php //Carousel::end()  ?>
            </div>
            <div class="mainContent">
                <?= $content ?>
            </div>
        </div>
        <footer class="footer">
            <?= FooterFeedbackWidget::widget(); ?>
            <div class="container footerNav">
                <div class="row">
                    <div class="col-md-2 col-xs-12 footerLogoBlock">
                        <?= Html::img('@web/images/logo_black.png', ['alt' => 'Logo']); ?>
                    </div>
                    <div class="col-md-10 col-xs-12">
                        <?= BottomMenuWidget::widget() ?>
                    </div>
                </div>
            </div>
            <div class="bottomFooter">
                <p class="text-center">&copy; Mr.Butler <?= date('Y') ?></p>
            </div>
        </footer>
        <?= RegistrationWidget::widget() ?>
        <?= AuthWidget::widget() ?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
