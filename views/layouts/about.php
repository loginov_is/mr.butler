<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\LanguageWidget;
use app\widgets\RegistrationWidget;
use app\widgets\HeaderSocialLinksWidget;
use app\widgets\CurrencyWidget;
use app\widgets\FooterFeedbackWidget;
use app\widgets\AuthWidget;
use app\widgets\BottomMenuWidget;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;
use app\assets\GalleryAsset;

AppAsset::register($this);
GalleryAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <?php $this->registerCssFile(Yii::getAlias('@web') . '/lib/font-awesome-4.4.0/css/font-awesome.min.css'); ?>
        <?php
        $this->registerJsFile(Yii::getAlias('@web') . '/js/main.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->registerJsFile('http://maps.google.com/maps/api/js?sensor=false', ['depends' => [\yii\web\JqueryAsset::className()]]);
        ?>
    </head>
    <body class="pageAbout">
        <?php $this->beginBody() ?>
        <div class="wrap">
            <div class="topLane">
                <div class="container">
                    <div class="row topLaneNav text-center">
                        <div class="col-md-1 item col-xs-3 topFlags">
                            <?= LanguageWidget::widget() ?>
                        </div>
                        <div class="col-md-1 col-xs-3 item currencyDropDownBlock">
                            <?= CurrencyWidget::widget() ?>
                        </div>
                        <div class="col-md-3 col-xs-5 item itemLast">
                            <?= HeaderSocialLinksWidget::widget(); ?>
                        </div>
                        <div class="col-md-3 col-xs-1"></div>
                        <div class="col-md-4 col-xs-0"></div>
                    </div>
                </div>
            </div>
            <div class="topSlide">
                <div class="navbar-wrapper">
                    <?php
                    NavBar::begin([
                        'options' => [
                            'class' => 'navbar-inverse navbar-fixed-top navbarAdvance',
                        ],
                    ]);
                    echo Nav::widget([
                        'options' => ['class' => 'navbar-nav navbar-left topMenu',],
                        'items' => [
                            [
                                'label' => '<i class="fa fa-cog"></i> ' . \Yii::t('app', 'Администрирование'),
                                'url' => ['/admin/index'],
                                'visible' => Yii::$app->user->can('admin'),
                            ],
                            [
                                'label' => '<i class="fa fa-building-o"></i> ' . \Yii::t('app', 'Передать недвижимость в управление'),
                                'url' => ['/real-estate/add']
                            ],
                            [
                                'label' => '<i class="fa fa-question-circle"></i> ' . \Yii::t('app', 'Помощь'),
                                'url' => ['/faq/index']
                            ],
                            [
                                'label' => '<i class="fa fa-user"></i> ' . \Yii::t('app', 'Регистрация'),
                                'url' => ['/site/signup'],
                                'visible' => Yii::$app->user->isGuest,
                                'linkOptions' => [
                                    'class' => 'showRegistrModal'
                                ],
                            ],
                            Yii::$app->user->isGuest ?
                                    [
                                'label' => '<i class="fa fa-lock"></i> ' . \Yii::t('app', 'Войти'),
                                'url' => ['/site/login'],
                                'linkOptions' => [
                                    'class' => 'showLoginModal'
                                ]
                                    ] :
                                    [
                                'label' => '<i class="fa fa-lock"></i> ' . \Yii::t('app', 'Выйти') . ' (' . Yii::$app->user->identity->email . ')',
                                'url' => ['/site/logout'],
                                'linkOptions' => ['data-method' => 'post']
                                    ],
                        ],
                        'encodeLabels' => false
                    ]);
                    NavBar::end();
                    ?>
                </div>
                <div class="leftMenu">
                    <div class="leftMenuButton">
                        <i class="fa fa-navicon"></i>
                    </div>
                    <div class="logo text-center">
                        <?= Html::a(Html::img('@web/images/main_logo.png', ['alt' => 'Logo']), ['/']); ?>
                    </div>
                    <div class="menu">
                        <?=
                        yii\widgets\Menu::widget([
                            'items' => [
                                [
                                    'label' => '<i class="fa fa-question"></i> ' . \Yii::t('app', 'Кто такой Mr Butler'),
                                    'url' => '#whoIsMrButler',
                                ],
                                [
                                    'label' => '<i class="fa fa-home"></i> ' . \Yii::t('app', 'Как сдать недвижимость в доверительное управление'),
                                    'url' => '#wannaGetToRent',
                                ],
                                [
                                    'label' => '<i class="fa fa-building"></i> ' . \Yii::t('app', 'Как снять недвижимость'),
                                    'url' => '#wannaRent',
                                ], [
                                    'label' => '<i class="fa fa-suitcase"></i> ' . \Yii::t('app', 'Партнеры'),
                                    'url' => '#partners',
                                ],
                                [
                                    'label' => '<i class="fa fa-users"></i> ' . \Yii::t('app', 'Команда'),
                                    'url' => '#team',
                                ],
                                [
                                    'label' => '<i class="fa fa-file"></i> ' . \Yii::t('app', 'Пресса о нас'),
                                    'url' => '#pressAboutUs',
                                ],
                                [
                                    'label' => '<i class="fa fa-eye"></i> ' . \Yii::t('app', 'Отзывы'),
                                    'url' => '#reviews',
                                ],
                                [
                                    'label' => '<i class="fa fa-clipboard"></i> ' . \Yii::t('app', 'Контакты'),
                                    'url' => '#contacts',
                                ],
                            ],
                            'options' => [
                                'class' => 'aboutLeftMenu'
                            ],
                            'encodeLabels' => false
                        ])
                        ?>
                    </div>
                </div>
                <div class="pageAboutContent">
                    <?= $content ?>
                </div>
            </div>
        </div>
        <footer class="footer">
            <?= FooterFeedbackWidget::widget(); ?>
            <div class="container footerNav">
                <div class="row">
                    <div class="col-md-2 col-xs-12 footerLogoBlock">
                        <?= Html::img('@web/images/logo_black.png', ['alt' => 'Logo']); ?>
                    </div>
                    <div class="col-md-10 col-xs-12">
                        <?= BottomMenuWidget::widget() ?>
                    </div>
                </div>
            </div>
            <div class="bottomFooter">
                <p class="text-center">&copy; Mr.Butler <?= date('Y') ?></p>
            </div>
        </footer>
        <?= RegistrationWidget::widget() ?>
        <?= AuthWidget::widget() ?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
     