<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\widgets\BackgroundWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Page */
$this->params['breadcrumbs'][] = $this->title;
?>
<?=
BackgroundWidget::widget([
    'content' => '<h1 class="text-center">' . $this->title . '</h1>',
    'htmlClass' => 'site-dynamic-page',
    'page' => $model
])
?>
<div class="page-view">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?= $model->content ?>
            </div>
        </div>
    </div>
</div>
