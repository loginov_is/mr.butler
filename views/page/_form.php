<?php

use app\models\ImageAlbum;
use app\models\Language;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Lookup;

/* @var $this yii\web\View */
/* @var $model app\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">
    <?php
    $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ])
    ?>

    <?= $form->errorSummary([$model]) ?>

    <div class="row">
        <div class="col-lg-2">
            <?=
            $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(Language::find()->all(), 'id', 'name'), [
                'prompt' => '- ' . $model->getAttributeLabel('language_id') . ' -'
            ])
            ?>
        </div>
        <?php if (!$model->system) { ?>
            <div class="col-lg-2">
                <?=
                $form->field($model, 'type_id')->dropDownList(Lookup::items(Lookup::TYPE_PAGE_TYPE), [
                    'prompt' => '- ' . $model->getAttributeLabel('type_id') . ' -'
                ])
                ?>
            </div>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?php
// @todo Generate slug
// value.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
            ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true, 'readonly' => !$model->isNewRecord]) ?>
        </div>
    </div>

    <?=
    $form->field($model, 'content')->widget(yii\imperavi\Widget::className(), [
        'options' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'imageUpload' => Url::to([
                'image/upload',
                'type' => Inflector::camel2id(StringHelper::basename(get_class($model))),
                'id' => $model->isNewRecord ? 'tmp' : $model->id
            ]),
            /* 'fileUpload' => Url::to([
              'file/upload',
              'type' => Inflector::camel2id(StringHelper::basename(get_class($model))),
              'id' => $model->isNewRecord ? 'new' : $model->id
              ]), */
            'uploadImageFields' => [
                Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
            ],
            'convertImageLinks' => true,
            'convertVideoLinks' => true,
            //'imageGetJson' => Url::to(['/admin/gallery/json-list']),
            //'imageManagerJson' => Url::to(['/admin/gallery/json-list']),
            //'fileManagerJson' => Url::to(['/admin/gallery/json-list']),
            'imageUploadCallback' => $model->isNewRecord ? new \yii\web\JsExpression(<<<JS
                function(image, json) {
                    $(".page-form")
                        .find('form')
                        .append($('<input type="hidden" name="images[]" value="" />').val($(image).attr("src")));
                }
JS
                    ) : false,
        //'definedLinks' => '/defined-links.json', // @todo Action to site pages
        ],
        'plugins' => [
            'fullscreen',
            'table',
            'video',
            'imagemanager',
            'filemanager',
            'definedlinks',
            'fontsize',
            'fontfamily',
            'fontcolor',
        //'clips',
        ]
    ])
    ?>

    <?=
    $form->field($model, 'preview')->widget(yii\imperavi\Widget::className(), [
        'options' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'imageUpload' => Url::to([
                'image/upload',
                'type' => Inflector::camel2id(StringHelper::basename(get_class($model))),
                'id' => $model->isNewRecord ? 'tmp' : $model->id
            ]),
            /* 'fileUpload' => Url::to([
              'file/upload',
              'type' => Inflector::camel2id(StringHelper::basename(get_class($model))),
              'id' => $model->isNewRecord ? 'new' : $model->id
              ]), */
            'uploadImageFields' => [
                Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
            ],
            'convertImageLinks' => true,
            'convertVideoLinks' => true,
            //'imageGetJson' => Url::to(['/admin/gallery/json-list']),
            //'imageManagerJson' => Url::to(['/admin/gallery/json-list']),
            //'fileManagerJson' => Url::to(['/admin/gallery/json-list']),
            'imageUploadCallback' => $model->isNewRecord ? new \yii\web\JsExpression(<<<JS
                function(image, json) {
                    $(".page-form")
                        .find('form')
                        .append($('<input type="hidden" name="images[]" value="" />').val($(image).attr("src")));
                }
JS
                    ) : false,
        //'definedLinks' => '/defined-links.json', // @todo Action to site pages
        ],
        'plugins' => [
            'fullscreen',
            'table',
            'video',
            'imagemanager',
            'filemanager',
            'definedlinks',
            'fontsize',
            'fontfamily',
            'fontcolor',
        //'clips',
        ]
    ])
    ?>

    <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'album_id')->dropDownList(ArrayHelper::map(ImageAlbum::find()->all(), 'id', 'name'), [
        'prompt' => '- ' . $model->getAttributeLabel('album_id') . ' -'
    ])
    ?>

    <?= $form->field($model, 'slider_type')->dropDownList([
        0 => Yii::t('app', 'Слайдер'),
        1 => Yii::t('app', 'Изображение по умолчанию'),
        2 => Yii::t('app', 'Фоновое изображение'),
    ]) ?>

    <?php if($model->background_image) echo Html::img('@web/upload/' . $model->background_image, ['style' => 'max-width: 300px;']); ?>
    <?= $form->field($model, 'uploadedFile')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
