<?php

use app\helpers\ContentHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Страницы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить страницу'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'language_id',
                'value' => function ($model) {
                    /** @var \app\models\Page $model */
                    return "[{$model->language->code}] {$model->language->name}";
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'title',
                'value' => function ($model) {
                    /** @var \app\models\Page $model */
                    return "{$model->title} [{$model->slug}]";
                },
                'format' => 'raw',
            ],
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'created_by',
                'value' => function ($model) {
                    /** @var \app\models\Page $model */
                    return ContentHelper::userLinkById($model->created_by);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'updated_by',
                'value' => function ($model) {
                    /** @var \app\models\Page $model */
                    return ContentHelper::userLinkById($model->updated_by);
                },
                'format' => 'raw',
            ],
            'system',
            // 'meta_keywords',
            // 'meta_description',
            // 'content:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
