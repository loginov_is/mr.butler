<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin() ?>
<?= $form->errorSummary([$model]) ?>
<div class="row">
    <div class="col-md-12">
        <?php echo $form->field($model, 'name')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php echo $form->field($model, 'url')->textInput() ?>
    </div>
</div>
<?php if (!$model->isNewRecord && $model->youtube_id) { ?>
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-12">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="<?= $model->getYoutubeFullUrl($model->youtube_id); ?>"></iframe>
            </div>
        </div>
    </div>
<?php } ?>
<div class="row">
    <div class="col-md-12">
        <?=
        Html::submitButton(($model->getIsNewRecord()) ? \Yii::t('app', 'Создать') : \Yii::t('app', 'Сохранить'), [
            'class' => 'btn btn-success'
        ]);
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>