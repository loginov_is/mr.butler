<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RegulationGroup */

$this->title = Yii::t('app', 'Изменить видео');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Видео'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Изменить');
?>
<div class="video-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
