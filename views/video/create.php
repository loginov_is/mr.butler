<?php

use yii\helpers\Html;
$this->title = Yii::t('app', 'Добавить видео');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Видео'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
