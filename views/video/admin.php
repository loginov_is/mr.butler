<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = Yii::t('app', 'Видео');
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить группу'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                'name',
                'url',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{update} {delete}',
                ],
            ],
        ])
        ?>