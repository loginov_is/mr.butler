<?php

namespace app\rbac;

use yii\rbac\Rule;

class RentOwnerRule extends Rule
{
    public $name = 'isRentOwner';

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        return isset($params['rent']) ? $params['rent']->created_by == $user : false;
    }
}
