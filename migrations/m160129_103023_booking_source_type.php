<?php

use yii\db\Migration;

class m160129_103023_booking_source_type extends Migration
{
    public function safeUp() {
        $this->delete('{{%lookup}}', ['type' => 'BookingSource']);
        $this->dropColumn('{{%booking}}', 'source_type_id');
        $this->addColumn('{{%booking}}', 'source_type', $this->string()->notNull());
    }

    public function safeDown() {
        $this->batchInsert('{{%lookup}}', ['type', 'code', 'name', 'sort_order'], [
            ['BookingSource', 0, '����', 1],
            ['BookingSource', 1, '������� ���������� �������', 2],
        ]);
        $this->dropColumn('{{%booking}}', 'source_type');
        $this->addColumn('{{%booking}}', 'source_type_id', $this->bigInteger(20)->notNull()->defaultValue(0));
    }
}
