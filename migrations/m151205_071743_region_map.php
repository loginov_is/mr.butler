<?php

use yii\db\Migration;

class m151205_071743_region_map extends Migration {

    public function safeUp() {
        $this->addColumn('{{%region}}', 'map', $this->string(255)->notNull()->defaultValue(0));
    }

    public function safeDown() {
        $this->dropColumn('{{%region}}', 'map');
    }

}
