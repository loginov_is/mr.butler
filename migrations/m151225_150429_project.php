<?php

use yii\db\Migration;

class m151225_150429_project extends Migration {

    public function safeUp() {
        $this->addColumn('{{%project}}', 'map', $this->string(255)->notNull());
        $this->createIndex('uk-project-name', '{{%project}}', 'name', true);
    }

    public function safeDown() {
        $this->dropIndex('uk-project-name', '{{%project}}');
        $this->dropColumn('{{%project}}', 'map');
    }

}
