<?php

use yii\db\Migration;

class m160114_104735_user_subscription extends Migration {

    public function safeUp() {
        $this->addColumn('{{%user}}', 'news_subscription', $this->integer(1)->notNull()->defaultValue(0));
        $this->createIndex('uk-user-news_subscription', '{{%user}}', 'news_subscription');
    }

    public function safeDown() {
        $this->dropIndex('uk-user-news_subscription', '{{%user}}');
        $this->dropColumn('{{%user}}', 'news_subscription');
    }

}
