<?php

use yii\db\Migration;

class m160113_190551_region_comfort extends Migration
{
    public function safeUp() {
        $this->batchInsert('{{%lookup}}', ['type', 'code', 'name', 'sort_order'], [
            ['RegionComfort', 6, '������������ ���������', 6],
            ['RegionComfort', 7, '�����', 7],
            ['RegionComfort', 8, '������ ����/����', 8],
        ]);
    }

    public function safeDown() {
        $this->delete('{{%lookup}}', ['code' => [6, 7, 8]]);
    }
}
