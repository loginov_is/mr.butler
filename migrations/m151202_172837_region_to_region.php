<?php

use yii\db\Migration;

class m151202_172837_region_to_region extends Migration {

    public function safeUp() {
        $this->createTable('{{%region_to_region}}', [
            'parent_id' => $this->bigInteger(20)->notNull(),
            'child_id' => $this->bigInteger(20)->notNull(),
        ]);

        $this->createIndex('uk-region_to_region-parent_id', '{{%region_to_region}}', 'parent_id');
        $this->createIndex('uk-region_to_region-child_id', '{{%region_to_region}}', 'child_id');
        $this->addForeignKey('fk-region_to_region-parent_id-region-id', '{{%region_to_region}}', 'parent_id', '{{%region}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-region_to_region-child_id-region-id', '{{%region_to_region}}', 'child_id', '{{%region}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk-region_to_region-child_id-region-id', '{{%region_to_region}}');
        $this->dropForeignKey('fk-region_to_region-parent_id-region-id', '{{%region_to_region}}');
        $this->dropIndex('uk-region_to_region-child_id', '{{%region_to_region}}');
        $this->dropIndex('uk-region_to_region-parent_id', '{{%region_to_region}}');
    }

}
