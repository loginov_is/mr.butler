<?php

use yii\db\Migration;

class m151016_130126_menu_language extends Migration {

    public function safeUp() {
        $this->addColumn('{{%menu}}', 'language_id', $this->integer(11)->notNull());
        $this->createIndex('ak_menu_language_id', '{{%menu}}', 'language_id');
        $this->addForeignKey('fk_menu_language_id', '{{%menu}}', 'language_id', '{{%language}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk_menu_language_id', '{{%menu}}');
        $this->dropTable('{{%menu}}');
    }

}
