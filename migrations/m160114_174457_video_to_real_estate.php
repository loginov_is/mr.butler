<?php

use yii\db\Migration;

class m160114_174457_video_to_real_estate extends Migration
{
    public function safeUp() {
       $this->createTable('{{%video_to_real_estate}}', [
            'real_estate_id' => $this->bigInteger()->notNull(),
            'video_id' => $this->bigInteger()->notNull()
        ]);
    }

    public function safeDown() {
        $this->dropTable('{{%video_to_real_estate}}');
    }
}
