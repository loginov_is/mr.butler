<?php

use yii\db\Migration;

class m151226_052709_project_description extends Migration {

    public function safeUp() {
        $this->addColumn('{{%project}}', 'description', $this->text());
    }

    public function safeDown() {
        $this->dropColumn('{{%project}}', 'description');
    }

}
