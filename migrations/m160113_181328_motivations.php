<?php

use yii\db\Migration;

class m160113_181328_motivations extends Migration {

    public function safeUp() {
        $this->batchInsert('{{%motivation_picture}}', ['type', 'image', 'title', 'order'], [
            ['HowItWork', 'give_order.png', 'Вы оставляете заявку на сайте', 1],
            ['HowItWork', 'agent.png', 'Наш агент свяжется с Вами и уточнит детали', 2],
            ['HowItWork', 'house.png', 'Вы выбираете дом своей мечты!', 3],
            ['HowItWork', 'documents.png', 'Мы готовим пакет документов для оформления в собственность', 4],
            ['HowItWork', 'money.png', 'Вы вносите залог', 5],
            ['HowItWork', 'finish_result.png', 'Получаете пакет документов и ключи от вашего дома мечты!', 6],
        ]);
    }

    public function safeDown() {
        $this->delete('{{%motivation_picture}}', ['type' => 'HowItWork']);
    }

}
