<?php

use yii\db\Migration;

class m160117_124122_booking extends Migration
{
    public function safeUp() {
        $this->createTable('{{%booking}}', [
            'id' => $this->bigInteger(20)->notNull(),
            'status_id' => $this->bigInteger(20)->notNull()->defaultValue(0),
            'real_estate_id' => $this->bigInteger(20)->notNull(),
            'source_type_id' => $this->bigInteger(20)->notNull()->defaultValue(0),
            'user_phone' => $this->integer(11)->notNull()->defaultValue(0),
            'start_date' => $this->integer(11)->notNull()->defaultValue(0),
            'end_date' => $this->integer(11)->notNull()->defaultValue(0),
            'rent_price' => $this->bigInteger(20)->notNull()->defaultValue(0),
            'comment' => $this->text()->notNull(),
            'created_at' => $this->integer(11)->notNull()->defaultValue(0),
            'created_by' => $this->bigInteger(20)->notNull(),
        ]);

        $this->createIndex('uk-booking-real_estate_id', '{{%booking}}', 'real_estate_id');
        $this->createIndex('uk-booking-start_date', '{{%booking}}', 'start_date');
        $this->createIndex('uk-booking-end_date', '{{%booking}}', 'end_date');
        $this->addForeignKey('fk-booking-created_by-user-id', '{{%booking}}', 'created_by', '{{%user}}', 'id');
        //$this->addForeignKey('fk-booking-real_estate_id-real_estate-id', '{{%booking}}', 'real_estate_id', '{{%real_estate}}', 'id');
    }

    public function safeDown() {
        $this->dropTable('{{%booking}}');
    }
}
