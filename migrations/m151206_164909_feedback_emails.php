<?php

use yii\db\Migration;

class m151206_164909_feedback_emails extends Migration {

    public function safeUp() {
        $this->createTable('{{%feedback_email}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string(255)->notNull()
        ]);
        
        $this->createIndex('uk-feedback_email-email', '{{%feedback_email}}', 'email');
    }

    public function safeDown() {
        $this->dropIndex('uk-feedback_email-email', '{{%feedback_email}}');
        $this->dropTable('{{%feedback_email}}');
    }

}
