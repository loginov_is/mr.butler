<?php

use yii\db\Migration;

class m160119_214047_payment_comment extends Migration {

    public function safeUp() {
        $this->createTable('{{%payment_comment}}', [
            'id' => $this->primaryKey(),
            'payment_id' => $this->integer(11)->notNull(),
            'message' => $this->text()->notNull(),
            'created_at' => $this->integer(11),
            'viewed_user' => $this->integer(1)->notNull()->defaultValue(0),
            'created_by' => $this->bigInteger(20)->notNull()
        ]);

        $this->createIndex('uk-payment_comment-viewed_user', '{{%payment_comment}}', 'viewed_user');
        $this->createIndex('uk-payment_comment-payment_id', '{{%payment_comment}}', 'payment_id');
        $this->createIndex('uk-payment_comment-created_at', '{{%payment_comment}}', 'created_at');
        $this->addForeignKey('fk-payment_comment-payment_id-payment-id', '{{%payment_comment}}', 'payment_id', '{{%payment}}', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('uk-payment_comment-created_by', '{{%payment_comment}}', 'created_by');
        $this->addForeignKey('fk-payment_comment-creted_by-user-id', '{{%payment_comment}}', 'created_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk-payment_comment-creted_by-user-id', '{{%payment_comment}}');
        $this->dropIndex('uk-payment_comment-created_by', '{{%payment_comment}}');
        $this->dropForeignKey('fk-payment_comment-payment_id-payment-id', '{{%payment_comment}}');
        $this->dropIndex('uk-payment_comment-created_at', '{{%payment_comment}}');
        $this->dropIndex('uk-payment_comment-payment_id', '{{%payment_comment}}');
        $this->dropIndex('uk-payment_comment-viewed_user', '{{%payment_comment}}');
        $this->dropTable('{{%payment_comment}}');
    }

}
