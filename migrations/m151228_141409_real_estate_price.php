<?php

use yii\db\Migration;

class m151228_141409_real_estate_price extends Migration {

    public function safeUp() {
        $this->addColumn('{{%real_estate}}', 'daily_price', $this->float()->notNull()->defaultValue(0));
        $this->addColumn('{{%real_estate}}', 'monthly_price', $this->float()->notNull()->defaultValue(0));
        $this->createIndex('uk-real_estate-daily_price', '{{%real_estate}}', 'daily_price');
        $this->createIndex('uk-real_estate-monthly_price', '{{%real_estate}}', 'monthly_price');
    }

    public function safeDown() {
        $this->dropIndex('uk-real_estate-monthly_price', '{{%real_estate}}');
        $this->dropIndex('uk-real_estate-daily_price', '{{%real_estate}}');
        $this->dropColumn('{{%real_estate}}', 'monthly_price');
        $this->dropColumn('{{%real_estate}}', 'daily_price');
    }

}
