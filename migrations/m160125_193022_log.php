<?php

use yii\db\Migration;

class m160125_193022_log extends Migration {

    public function safeUp() {
        $this->createTable('{{%user_log}}', [
            'id' => $this->primaryKey(),
            'created_by' => $this->integer(20)->notNull(),
            'created_at' => $this->integer(20)->notNull(),
            'class_name' => $this->string(50)->notNull(),
            'model_id' => $this->integer(11)->notNull(),
            'is_admin' => $this->integer(1)->notNull()->defaultValue(1)
        ]);

        $this->createIndex('uk-user_log-created_at', '{{%user_log}}', 'created_at');
        $this->createIndex('uk-user_log-model_id', '{{%user_log}}', 'model_id');
        $this->createIndex('uk-user_log-is_admin', '{{%user_log}}', 'is_admin');
        $this->createIndex('uk-user_log-class_name', '{{%user_log}}', 'class_name');
    }

    public function safeDown() {
        $this->dropIndex('uk-user_log-class_name', '{{%user_log}}');
        $this->dropIndex('uk-user_log-is_admin', '{{%user_log}}');
        $this->dropIndex('uk-user_log-model_id', '{{%user_log}}');
        $this->dropIndex('uk-user_log-created_at', '{{%user_log}}');
        $this->dropTable('{{%user_log}}');
    }

}
