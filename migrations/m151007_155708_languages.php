<?php

use yii\db\Migration;

class m151007_155708_languages extends Migration
{
    public function up()
    {
        $this->createTable('{{%language}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(5)->notNull(), // ru, en, en-GB
            'name' => $this->string(100)->notNull(),
        ]);

        $this->createIndex('uk-language-code', '{{%language}}', 'code', true);

        $this->batchInsert('{{%language}}', ['code', 'name'], [
            ['ru', 'Русский'],
            ['en', 'English'],
        ]);
    }

    public function down()
    {
        $this->dropIndex('uk-language-code', '{{%language}}');
        $this->dropTable('{{%language}}');
    }
}
