<?php

use yii\db\Migration;

class m151227_110009_real_estate_photo_main extends Migration
{
    public function safeUp() {
        $this->addColumn('{{%real_estate_photo}}', 'main', $this->integer(11)->notNull()->defaultValue(0));
    }

    public function safeDown() {
        $this->dropColumn('{{%real_estate_photo}}', 'main');
    }
}
