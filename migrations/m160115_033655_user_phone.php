<?php

use yii\db\Migration;

class m160115_033655_user_phone extends Migration {

    public function safeUp() {
        $this->dropColumn('{{%user}}', 'phone');
        $this->addColumn('{{%user}}', 'phone', $this->bigInteger(20)->defaultValue(NULL));
    }

    public function safeDown() {
        $this->dropColumn('{{%user}}', 'phone');
        $this->addColumn('{{%user}}', 'phone', $this->integer(11)->defaultValue(NULL));
    }

}
