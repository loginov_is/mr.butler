<?php

use yii\db\Migration;

class m160113_175823_motivations extends Migration {

    public function safeUp() {
        $this->createTable('{{%motivation_picture}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(50)->notNull(),
            'image' => $this->string(255)->notNull(),
            'title' => $this->text()->notNull(),
            'order' => $this->integer(11)->notNull(),
        ]);

        $this->createIndex('uk-motivation_picture-order', 'motivation_picture', 'order');
    }

    public function safeDown() {
        $this->dropIndex('uk-motivation_picture-order', 'motivation_picture');
        $this->dropTable('{{%motivation_picture}}');
    }

}
