<?php

use yii\db\Migration;
use app\models\Language;

class m160125_041100_page extends Migration {

    public function safeUp() {
        foreach (Language::find()->all() as $language) {
            $this->batchInsert('{{%page}}', ['created_at', 'language_id', 'slug', 'system', 'title'], [
                [time(), $language->id, 'guidebook_index', 1, 'Гид по районам'],
            ]);
        }
    }

    public function safeDown() {
        
    }

}
