<?php

use yii\db\Migration;

class m160122_193110_service extends Migration
{
    public function safeUp() {
        $this->createTable('{{%service}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert('{{%service}}', ['name'], [
            ['Трансфер из аэропорта'],
            ['Заказать страховку'],
            ['Аренда транспорта'],
            ['Экскурсии со скидкой'],
            ['Заказать шоппинг'],
        ]);
    }

    public function safeDown() {
        //$this->dropTable('{{%service}}');
    }
}
