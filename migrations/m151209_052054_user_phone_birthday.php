<?php

use yii\db\Migration;

class m151209_052054_user_phone_birthday extends Migration {

    public function safeUp() {
        $this->addColumn('{{%user}}', 'phone', $this->integer(11)->notNull()->defaultValue(0));
        $this->addColumn('{{%user}}', 'birthday', $this->integer(11)->notNull()->defaultValue(0));
        $this->addColumn('{{%user}}', 'avatar', $this->string(255));
    }

    public function safeDown() {
        $this->dropColumn('{{%user}}', 'avatar');
        $this->dropColumn('{{%user}}', 'birthday');
        $this->dropColumn('{{%user}}', 'phone');
    }

}
