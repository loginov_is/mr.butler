<?php

use yii\db\Migration;

class m160113_222204_ticket extends Migration {

    public function safeUp() {
        $this->createTable('{{%ticket}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'message' => $this->text()->notNull(),
            'created_by' => $this->bigInteger(20)->notNull(),
            'created_at' => $this->integer(11),
            'viewed_admin' => $this->integer(1)->notNull()->defaultValue(0)
        ]);

        $this->createIndex('uk-ticket-created_by', '{{%ticket}}', 'created_by');
        $this->createIndex('uk-ticket-created_at', '{{%ticket}}', 'created_at');
        $this->createIndex('uk-ticket-viewed_admin', '{{%ticket}}', 'viewed_admin');
        $this->addForeignKey('fk-ticket-creted_by-user-id', '{{%ticket}}', 'created_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%ticket_comment}}', [
            'id' => $this->primaryKey(),
            'ticket_id' => $this->integer(11)->notNull(),
            'message' => $this->text()->notNull(),
            'created_at' => $this->integer(11),
            'viewed_user' => $this->integer(1)->notNull()->defaultValue(0)
        ]);

        $this->createIndex('uk-ticket-viewed_user', '{{%ticket_comment}}', 'viewed_user');
        $this->createIndex('uk-ticket-ticket_id', '{{%ticket_comment}}', 'ticket_id');
        $this->createIndex('uk-ticket-created_at', '{{%ticket_comment}}', 'created_at');
        $this->addForeignKey('fk-ticket_comment-ticket_id-ticket-id', '{{%ticket_comment}}', 'ticket_id', '{{%ticket}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk-ticket_comment-ticket_id-ticket-id', '{{%ticket_comment}}');
        $this->dropIndex('uk-ticket-created_at', '{{%ticket_comment}}');
        $this->dropIndex('uk-ticket-ticket_id', '{{%ticket_comment}}');
        $this->dropIndex('uk-ticket-viewed_user', '{{%ticket_comment}}');
        $this->dropTable('{{%ticket_comment}}');
        $this->dropForeignKey('fk-ticket-creted_by-user-id', '{{%ticket}}');
        $this->dropIndex('uk-ticket-viewed_admin', '{{%ticket}}');
        $this->dropIndex('uk-ticket-created_at', '{{%ticket}}');
        $this->dropIndex('uk-ticket-created_by', '{{%ticket}}');
        $this->dropTable('{{%ticket}}');
    }

}
