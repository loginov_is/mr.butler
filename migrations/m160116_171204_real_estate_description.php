<?php

use yii\db\Migration;

class m160116_171204_real_estate_description extends Migration {

    public function safeUp() {
        $this->dropColumn('{{%real_estate}}', 'description_ru');
        $this->dropColumn('{{%real_estate}}', 'description_en');
        $this->addColumn('{{%real_estate}}', 'description', $this->text()->defaultValue(NULL));
    }

    public function safeDown() {
        $this->dropColumn('{{%real_estate}}', 'description');
        $this->addColumn('{{%real_estate}}', 'description_en', $this->text()->defaultValue(NULL));
        $this->addColumn('{{%real_estate}}', 'description_ru', $this->text()->defaultValue(NULL));
    }

}
