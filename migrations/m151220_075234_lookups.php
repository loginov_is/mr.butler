<?php

use yii\db\Migration;

class m151220_075234_lookups extends Migration {

    public function safeUp() {
            $this->batchInsert('{{%lookup}}', ['type', 'code', 'name', 'sort_order'], [
            ['RealEstatePriceRage', 0, 'За сутки', 1],
            ['RealEstatePriceRage', 1, 'За месяц', 2]
        ]);
        
    }

    public function safeDown() {
         $this->delete('{{%lookup}}', ['type' => 'RealEstatePriceRage']);
    }

}
