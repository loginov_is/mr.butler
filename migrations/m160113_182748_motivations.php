<?php

use yii\db\Migration;

class m160113_182748_motivations extends Migration {

    public function safeUp() {
        $this->batchInsert('{{%motivation_picture}}', ['type', 'image', 'title', 'order'], [
            ['BottomLayoutMotivation', 'security.png', 'Доверие и безопасность', 1],
            ['BottomLayoutMotivation', 'payments.png', 'Надежные платежи', 2],
            ['BottomLayoutMotivation', 'garantire.png', 'Гарантия сохранности имущества', 3],
            ['BottomLayoutMotivation', 'service.png', 'Высокий сервис', 4],
        ]);
    }

    public function safeDown() {
        $this->delete('{{%motivation_picture}}', ['type' => 'PayMethods']);
    }

}
