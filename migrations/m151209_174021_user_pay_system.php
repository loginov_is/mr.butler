<?php

use yii\db\Migration;
use app\models\Lookup;

class m151209_174021_user_pay_system extends Migration {

    public function safeUp() {
        $this->addColumn('{{%user}}', 'payment_method', $this->integer(11)->notNull()->defaultValue(1));
        $this->batchInsert('{{lookup}}', ['type', 'code', 'name', 'sort_order'], [
            [Lookup::TYPE_USER_PAYMENT_METHOD, 1, 'Банковская карта', 1],
            [Lookup::TYPE_USER_PAYMENT_METHOD, 2, 'Безналичный расчет', 2],
        ]);
    }

    public function safeDown() {
        $this->delete('{{%lookup}}', ['type' => Lookup::TYPE_USER_PAYMENT_METHOD]);
        $this->dropColumn('{{%user}}', 'payment_method');
    }

}
