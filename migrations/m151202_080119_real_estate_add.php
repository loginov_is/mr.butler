<?php

use yii\db\Migration;

class m151202_080119_real_estate_add extends Migration {

    public function safeUp() {
        $this->addColumn('{{%real_estate}}', 'to_the_sea', $this->integer(11)->notNull());
        $this->addColumn('{{%real_estate}}', 'client_price', $this->float()->notNull());
        $this->createIndex('uk-real_estate-client_price', '{{%real_estate}}', 'client_price', false);
    }

    public function safeDown() {
        $this->dropIndex('uk-real_estate-client_price', '{{%real_estate}}');
        $this->dropColumn('{{%real_estate}}', 'client_price');
        $this->dropColumn('{{%real_estate}}', 'to_the_sea');
    }

}
