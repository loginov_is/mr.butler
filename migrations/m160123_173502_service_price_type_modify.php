<?php

use yii\db\Migration;

class m160123_173502_service_price_type_modify extends Migration
{
    public function safeUp() {
        $this->dropColumn('{{%service_price}}', 'people_min');
        $this->dropColumn('{{%service_price}}', 'people_max');

        $this->addColumn('{{%service_price}}', 'people_min', $this->integer());
        $this->addColumn('{{%service_price}}', 'people_max', $this->integer());

        $this->addColumn('{{%service_price}}', 'price_type_id', $this->integer()->defaultValue(0));
        $this->addColumn('{{%service}}', 'price_type_id', $this->integer()->defaultValue(0));
    }

    public function safeDown() {
        $this->dropColumn('{{%service_price}}', 'people_min');
        $this->dropColumn('{{%service_price}}', 'people_max');

        $this->addColumn('{{%service_price}}', 'people_min', $this->integer()->notNull());
        $this->addColumn('{{%service_price}}', 'people_max', $this->integer()->notNull());

        $this->dropColumn('{{%service_price}}', 'price_type_id');
        $this->dropColumn('{{%service}}', 'price_type_id');
    }
}
