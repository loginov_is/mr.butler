<?php

use yii\db\Migration;

class m151014_083315_locations extends Migration {

    public function safeUp() {
        $this->createTable('{{%country}}', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string(255)->notNull()
        ]);

        $this->createTable('{{%city}}', [
            'id' => $this->bigPrimaryKey(),
            'country_id' => $this->bigInteger(11)->notNull(),
            'name' => $this->string(255)->notNull()
        ]);

        $this->createTable('{{%region}}', [
            'id' => $this->bigPrimaryKey(),
            'city_id' => $this->bigInteger(11)->notNull(),
            'name' => $this->string(255)->notNull()
        ]);

        $this->createIndex('ak_city_country_id', '{{%city}}', 'country_id');
        $this->addForeignKey('fk_city_country_id', '{{%city}}', 'country_id', '{{%country}}', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('ak_region_city_id', '{{%region}}', 'city_id');
        $this->addForeignKey('fk_region_city_id', '{{%region}}', 'city_id', '{{%city}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk_region_city_id', '{{%region}}');
        $this->dropForeignKey('fk_city_country_id', '{{%city}}');
        $this->dropTable('{{%region}}');
        $this->dropTable('{{%city}}');
        $this->dropTable('{{%country}}');
    }

}
