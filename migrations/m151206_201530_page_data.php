<?php

use yii\db\Migration;
use app\models\Language;

class m151206_201530_page_data extends Migration {

    public function safeUp() {

        foreach (Language::find()->all() as $language) {
            $this->batchInsert('{{%page}}', ['created_at', 'language_id', 'slug', 'system', 'title'], [
                [time(), $language->id, 'site_index', 1, 'Главная'],
                [time(), $language->id, 'real-estate_add', 1, 'Передать недвижимость в управление'],
                [time(), $language->id, 'faq_index', 1, 'Помощь'],
                [time(), $language->id, 'site_about', 1, 'О нас'],
                [time(), $language->id, 'regulation_index', 1, 'Правила'],
                [time(), $language->id, 'contacts', 1, 'Контакты']
            ]);
        }
    }

    public function safeDown() {
        
    }

}
