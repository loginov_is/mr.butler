<?php

use yii\db\Migration;

class m151204_062830_valuta extends Migration {

    public function safeUp() {
        $this->createTable('{{%currency}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(3)->notNull(),
            'description' => $this->string(50)->notNull()
        ]);

        $this->createIndex('uk-currency-code', '{{%currency}}', 'code');

        $this->batchInsert('{{%currency}}', ['code', 'description'], [
            ['RUB', 'Российский рубль'],
            ['USD', 'Доллар США'],
            ['EUR', 'Евро'],
            ['THB', 'Тайский Бат'],
        ]);
    }

    public function safeDown() {
        $this->dropIndex('uk-currency-code', '{{%currency}}');
        $this->dropTable('{{$currency}}');
    }

}
