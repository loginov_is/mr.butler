<?php

use yii\db\Migration;

class m151013_153752_user_additional extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%user}}', 'username');
        $this->addColumn('{{%user}}', 'first_name', $this->string(255)->notNull());
        $this->addColumn('{{%user}}', 'last_name', $this->string(255)->notNull());
        $this->addColumn('{{%user}}', 'patronymic', $this->string(255)->defaultValue(null));
    }

    public function down()
    {
        echo "m151013_153752_user_additional cannot be reverted.\n";

        return false;
    }
}
