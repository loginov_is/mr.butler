<?php

use yii\db\Migration;

class m160119_184511_payment extends Migration {

    public function safeUp() {
        $this->createTable('{{%payment}}', [
            'id' => $this->primaryKey(),
            'created_by' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'status_id' => $this->integer(11)->notNull(),
            'payment_system_id' => $this->integer(11)->notNull(),
            'amount' => $this->float()->notNull(),
            'requisites' => $this->text()
        ]);

        $this->createIndex('uk-payment-created_by', '{{%payment}}', 'created_by');
        $this->createIndex('uk-payment-created_at', '{{%payment}}', 'created_at');
        $this->createIndex('uk-payment-status_id', '{{%payment}}', 'status_id');
        $this->createIndex('uk-payment-payment_system_id', '{{%payment}}', 'payment_system_id');
        $this->createIndex('uk-payment-amount', '{{%payment}}', 'amount');
    }

    public function safeDown() {
        $this->dropIndex('uk-payment-amount', '{{%payment}}');
        $this->dropIndex('uk-payment-payment_system_id', '{{%payment}}');
        $this->dropIndex('uk-payment-status_id', '{{%payment}}');
        $this->dropIndex('uk-payment-created_at', '{{%payment}}');
        $this->dropIndex('uk-payment-created_by', '{{%payment}}');
        $this->dropTable('{{%payment}}');
    }

}
