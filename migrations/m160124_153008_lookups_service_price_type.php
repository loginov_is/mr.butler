<?php

use yii\db\Migration;

class m160124_153008_lookups_service_price_type extends Migration
{
    public function safeUp() {
        $this->batchInsert('{{%lookup}}', ['type', 'code', 'name', 'sort_order'], [
            ['ServicePriceType', 0, 'За группу людей', 1],
            ['ServicePriceType', 1, 'За человека', 2],
        ]);
    }

    public function safeDown() {
        $this->delete('{{%lookup}}', ['type' => 'ServicePriceType']);
    }
}
