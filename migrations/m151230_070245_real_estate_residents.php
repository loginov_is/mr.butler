<?php

use yii\db\Migration;

class m151230_070245_real_estate_residents extends Migration
{
    public function safeUp() {
        $this->addColumn('{{%real_estate}}', 'residents', $this->integer()->notNull()->defaultValue(0));
    }

    public function safeDown() {
        $this->dropColumn('{{%real_estate}}', 'residents');
    }
}
