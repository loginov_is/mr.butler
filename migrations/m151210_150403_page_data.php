<?php

use yii\db\Migration;
use app\models\Language;

class m151210_150403_page_data extends Migration {

    public function safeUp() {
        foreach (Language::find()->all() as $language) {
            $this->batchInsert('{{%page}}', ['created_at', 'language_id', 'slug', 'system', 'title'], [
                [time(), $language->id, 'site_terms', 1, 'Условия и конфеденциальность'],
                [time(), $language->id, 'site_promotions', 1, 'Акции и скидки'],
            ]);
        }
    }

    public function safeDown() {
        
    }

}
