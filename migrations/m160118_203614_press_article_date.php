<?php

use yii\db\Migration;

class m160118_203614_press_article_date extends Migration {

    public function safeUp() {
        $this->addColumn('{{%press_article}}', 'publish_date', $this->integer(11)->notNull());
    }

    public function safeDown() {
        $this->dropColumn('{{%press_article}}', 'publish_date');
    }

}
