<?php

use yii\db\Migration;

class m151019_172841_page_album extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%page}}', 'album_id', $this->bigInteger()->defaultValue(null));

        $this->createIndex('ak_page_album_id', '{{%page}}', 'album_id');

        $this->addForeignKey('fk_page_album_id', '{{%page}}', 'album_id', '{{%image_album}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_page_album_id', '{{%page}}');
        $this->dropColumn('{{%page}}', 'album_id');
    }
}
