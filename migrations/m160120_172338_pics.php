<?php

use yii\db\Migration;

class m160120_172338_pics extends Migration {

    public function safeUp() {
        $this->batchInsert('{{%motivation_picture}}', ['type', 'image', 'title', 'order'], [
            ['PayMethods', 'pay_methods_visa.png', 'Банковскими картами', 1],
            ['PayMethods', 'pay_methods_paypal.png', 'Paypal', 2],
            ['PayMethods', 'pay_methods_cash.png', 'Наличными', 3],
        ]);
    }

    public function safeDown() {
        
    }

}
