<?php

use yii\db\Migration;

class m160122_195219_service_price extends Migration
{
    public function safeUp() {
        $this->createTable('{{%service_price}}', [
            'id' => $this->primaryKey(),
            'service_id' => $this->integer()->notNull(),
            'people_min' => $this->integer()->notNull(),
            'people_max' => $this->integer()->notNull(),
            'price' => $this->bigInteger()->notNull(),
        ]);

        $this->addForeignKey('fk-service_price-service_id-service-id', '{{%service_price}}', 'service_id', '{{%service}}', 'id');
    }

    public function safeDown() {
        $this->dropTable('{{%service_price}}');
    }
}
