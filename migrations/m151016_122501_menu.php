<?php

use yii\db\Migration;

class m151016_122501_menu extends Migration {

    public function safeUp() {
        $this->createTable('{{%menu}}', [
            'id' => $this->bigPrimaryKey(),
            'page_id' => $this->integer(11)->notNull(),
            'type_id' => $this->integer(11)->notNull(),
            'active' => $this->integer(1)->notNull()->defaultValue(1),
            'order' => $this->integer(11)->notNull()->defaultValue(0),
        ]);

        $this->createIndex('ak_menu_page_id', '{{%menu}}', 'page_id');
        $this->createIndex('ak_menu_type_id', '{{%menu}}', 'type_id');
        $this->createIndex('ak_menu_active', '{{%menu}}', 'active');
        $this->createIndex('ak_menu_order', '{{%menu}}', 'order');
        $this->addForeignKey('fk_menu_page_id', '{{%menu}}', 'page_id', '{{%page}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk_menu_page_id', '{{%menu}}');
        $this->dropTable('{{%menu}}');
    }

}
