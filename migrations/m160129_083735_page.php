<?php

use yii\db\Migration;
use app\models\Language;

class m160129_083735_page extends Migration
{
    public function safeUp() {
        foreach (Language::find()->all() as $language) {
            $this->batchInsert('{{%page}}', ['created_at', 'language_id', 'slug', 'system', 'title'], [
                [time(), $language->id, 'site_search', 1, 'Популярные объявления']
            ]);
        }
    }

    public function safeDown() {
        
    }
}
