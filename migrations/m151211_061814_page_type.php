<?php

use yii\db\Migration;
use app\models\Lookup;

class m151211_061814_page_type extends Migration {

    public function safeUp() {
        $this->addColumn('{{%page}}', 'type_id', $this->integer(11)->notNull()->defaultValue(1));
        $this->createIndex('uk-page-type_id', '{{%page}}', 'type_id');
        $this->batchInsert('{{lookup}}', ['type', 'code', 'name', 'sort_order'], [
            [Lookup::TYPE_PAGE_TYPE, 1, 'Страница', 1],
            [Lookup::TYPE_PAGE_TYPE, 2, 'Акции и скидки', 2],
        ]);
    }

    public function safeDown() {
        $this->delete('{{%lookup}}', ['type' => Lookup::TYPE_PAGE_TYPE]);
        $this->dropIndex('uk-page-type_id', '{{%user}}');
        $this->dropColumn('{{%user}}', 'type_id');
    }

}
