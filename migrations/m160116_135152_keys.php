<?php

use yii\db\Migration;

class m160116_135152_keys extends Migration {

    public function safeUp() {
        $this->addForeignKey('fk-real_estate_photo_real_estate_id_real_estate_id', '{{%real_estate_photo}}', 'real_estate_id', '{{%real_estate}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-project_photo_project_id_project_id', '{{%project_photo}}', 'project_id', '{{%project}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk-project_photo_project_id_project_id', '{{%project_photo}}');
        $this->dropForeignKey('fk-real_estate_photo_real_estate_id_real_estate_id', '{{%real_estate_photo}}');
    }

}
