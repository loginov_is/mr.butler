<?php

use yii\db\Migration;

class m151007_155737_pages extends Migration
{
    public function up()
    {
        $this->createTable('{{%page}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'language_id' => $this->integer()->notNull(),
            'slug' => $this->string(255)->notNull(), // read-only
            'system' => $this->boolean()->notNull()->defaultValue(false), // нельзя удалять если системная
            'meta_keywords' => $this->string(160)->defaultValue(null),
            'meta_description' => $this->string(160)->defaultValue(null),
            'title' => $this->string(255)->notNull(),
            'content' => $this->text()->defaultValue(null),
        ]);

        $this->addForeignKey('fk-page-language_id-language-id', '{{%page}}', 'language_id', '{{%language}}', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('uk-page-language_id-slug', '{{%page}}', ['language_id', 'slug'], true);

        /*$user_id = (new \yii\db\Query())
            ->select('id')
            ->from('{{%user}}')
            ->where(['email' => Yii::$app->params['systemEmail']])
            ->scalar();

        $language_id = (new \yii\db\Query())
            ->select('id')
            ->from('{{%language}}')
            ->where(['code' => 'ru'])
            ->scalar();

        $this->batchInsert('{{%page}}', [
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'language_id',
            'system',
            'slug',
            'title',
        ], [
            [time(), time(), $user_id, $user_id, $language_id, 1, 'about', 'О нас'],
        ]);*/
    }

    public function down()
    {
        $this->dropForeignKey('fk-page-language_id-language-id', '{{%page}}');
        $this->dropIndex('uk-page-language_id-slug', '{{%page}}');
        $this->dropTable('{{%page}}');
    }
}
