<?php

use yii\db\Migration;

class m151014_094257_faq extends Migration {

    public function safeUp() {
        $this->createTable('{{%faq_group}}', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string(255)->notNull(),
            'language_id' => $this->integer()->notNull(),
            'active' => $this->integer(1)->notNull()->defaultValue(1)
        ]);

        $this->createTable('{{%faq}}', [
            'id' => $this->bigPrimaryKey(),
            'faq_group_id' => $this->bigInteger()->notNull(),
            'question' => $this->text()->notNull(),
            'answer' => $this->text()->notNull(),
            'active' => $this->integer(1)->notNull()->defaultValue(1)
        ]);

        $this->createIndex('ak_faq_group_language_id', '{{%faq_group}}', 'language_id');
        $this->createIndex('ak_faq_group_active', '{{%faq_group}}', 'active');
        $this->addForeignKey('fk_faq_group_language_id', '{{%faq_group}}', 'language_id', '{{%language}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('ak_faq_language_id', '{{%faq}}', 'faq_group_id');
        $this->createIndex('ak_faq_active', '{{%faq}}', 'active');
        $this->addForeignKey('fk_faq_faq_group_id', '{{%faq}}', 'faq_group_id', '{{%faq_group}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk_faq_faq_group_id', '{{%faq}}');
        $this->dropForeignKey('fk_faq_group_language_id', '{{%faq_group}}');
        $this->dropTable('{{%faq}}');
        $this->dropTable('{{%faq_group}}');
    }

}
