<?php

use yii\db\Migration;
use app\models\Language;

class m160128_193400_pages extends Migration {

    public function safeUp() {
        foreach (Language::find()->all() as $language) {
            $this->batchInsert('{{%page}}', ['created_at', 'language_id', 'slug', 'system', 'title'], [
                [time(), $language->id, 'site_login', 1, 'Авторизация'],
                [time(), $language->id, 'site_signup', 1, 'Регистрация'],
            ]);
        }
    }

    public function safeDown() {
        
    }

}
