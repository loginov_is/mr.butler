<?php

use yii\db\Migration;

class m160118_204836_press_article_date extends Migration {

    public function safeUp() {
        $this->addColumn('{{%press_article}}', 'created_at', $this->integer(11)->defaultValue(0));
        $this->addColumn('{{%press_article}}', 'updated_at', $this->integer(11)->defaultValue(0));
    }

    public function safeDown() {
        $this->dropColumn('{{%press_article}}', 'updated_at');
        $this->dropColumn('{{%press_article}}', 'created_at');
    }

}
