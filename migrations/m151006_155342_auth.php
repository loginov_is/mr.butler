<?php

use yii\db\Migration;

class m151006_155342_auth extends Migration
{
    public function up()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->bigPrimaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'username' => $this->string(255)->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string(255)->notNull(),
            'password_reset_token' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'status' => $this->smallInteger(6)->notNull()->defaultValue(10),
        ]);

        $this->createIndex('uk-user-email', '{{%user}}', 'email', true);

        $this->createTable('{{%auth}}', [
            'id' => $this->bigPrimaryKey(),
            'user_id' => $this->bigInteger()->notNull(),
            'source' => $this->string(255)->notNull(),
            'source_id' => $this->string(255)->notNull(),
        ]);

        $this->createIndex('ak_auth_user_id', '{{%auth}}', 'user_id');
        $this->addForeignKey('fk_auth_user_id_user_id', '{{%auth}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk-auth-user_id-user-id', '{{%auth}}');
        $this->dropTable('{{%auth}}');
        $this->dropTable('{{%user}}');
    }
}
