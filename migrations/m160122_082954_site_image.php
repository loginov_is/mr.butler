<?php

use yii\db\Migration;

class m160122_082954_site_image extends Migration
{
    public function safeUp() {
        $this->createTable('{{%site_image}}',  [
            'id' => $this->primaryKey(),
            'type' => $this->string()->notNull(),
            'src' => $this->string(),
        ]);
        $this->batchInsert('{{%site_image}}', ['type', 'src'], [
            ['background_block_default', 'background_block_default.png'],
            ['background_block_contacts', 'background_block_contacts.png'],
        ]);
        $this->createIndex('uk-site_image-type', '{{%site_image}}', 'type');
    }

    public function safeDown() {
        $this->dropTable('{{%site_image}}');
    }
}
