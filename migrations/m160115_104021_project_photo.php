<?php

use yii\db\Migration;

class m160115_104021_project_photo extends Migration
{
    public function safeUp() {
        $this->createTable('{{%project_photo}}', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer(11)->notNull(),
            'src' => $this->string(255)->notNull(),
            'index' => $this->integer(11)->notNull()->defaultValue(0),
            'main' => $this->integer(11)->notNull()->defaultValue(0),
            'head_slider' => $this->integer(11)->notNull()->defaultValue(0),
        ]);

        $this->createTable('{{%real_estate_photo}}', [
            'id' => $this->primaryKey(),
            'real_estate_id' => $this->integer(11)->notNull(),
            'src' => $this->string(255)->notNull()
        ]);
        $this->addColumn('{{%real_estate_photo}}', 'main', $this->integer(11)->notNull()->defaultValue(0));
        $this->addColumn('{{%real_estate_photo}}', 'index', $this->integer(11)->notNull()->defaultValue(0));
    }

    public function safeDown() {
        $this->dropTable('{{%project_photo}}');
    }
}
