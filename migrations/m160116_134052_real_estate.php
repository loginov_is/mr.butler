<?php

use yii\db\Migration;

class m160116_134052_real_estate extends Migration {

    public function safeUp() {
        $this->addColumn('{{%real_estate}}', 'title', $this->string(255)->notNull());
        $this->dropColumn('{{%real_estate}}', 'house');
    }

    public function safeDown() {
        $this->addColumn('{{%real_estate}}', 'house', $this->string(50)->notNull());
        $this->dropColumn('{{%real_estate}}', 'title');
    }

}
