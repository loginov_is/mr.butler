<?php

use yii\db\Migration;

class m151201_101032_lookups extends Migration {

    public function up() {
        $this->batchInsert('{{%lookup}}', ['type', 'code', 'name', 'sort_order'], [
            ['RealEstateTypes', 1, 'Кондо', 1],
            ['RealEstateTypes', 2, 'Вилла', 2],
            ['RealEstateTypes', 3, 'Таунхаус', 3],
        ]);

        $this->batchInsert('{{%lookup}}', ['type', 'code', 'name', 'sort_order'], [
            ['RealEstateQuota', 1, 'Тайская', 1],
            ['RealEstateQuota', 2, 'Иностранная', 2],
            ['RealEstateQuota', 3, 'Компания', 3],
        ]);
    }

    public function down() {
        $this->delete('{{%lookup}}', ['type' => 'RealEstateQuota']);
        $this->delete('{{%lookup}}', ['type' => 'RealEstateTypes']);
    }

}
