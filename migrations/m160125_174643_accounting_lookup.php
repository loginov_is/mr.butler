<?php

use yii\db\Migration;

class m160125_174643_accounting_lookup extends Migration
{

    public function safeUp()
    {
                    $this->batchInsert('{{%lookup}}', ['type', 'code', 'name', 'sort_order'], [
            ['AccountingTemplate', 1, 'Пополнение баланса с арендованной недвижимости', 1],
            ['AccountingTemplate', 2, 'Снисание с баланса за коммунальные услуги (вода, свет)', 2]
        ]);
    }

    public function safeDown()
    {
         $this->delete('{{%lookup}}', ['type' => 'AccountingTemplate']);
    }

}
