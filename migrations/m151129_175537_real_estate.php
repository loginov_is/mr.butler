<?php

use yii\db\Migration;

class m151129_175537_real_estate extends Migration {

    public function safeUp() {
        $this->createTable('{{%real_estate}}', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer(11)->notNull(),
            'status_id' => $this->integer(11)->notNull(),
            'type_id' => $this->integer(11)->notNull(),
            'house' => $this->string(50)->notNull(),
            'flat_number' => $this->integer(11)->notNull(),
            'square' => $this->float()->notNull(),
            'floor' => $this->integer(11)->notNull(),
            'bedrooms' => $this->integer(11)->notNull(),
            'bathrooms' => $this->integer(11)->notNull(),
            'furnishings' => $this->integer(11)->notNull(),
            'pool' => $this->integer(11)->notNull(),
            'description_ru' => $this->text(),
            'description_en' => $this->text(),
            'information' => $this->text(),
            'owner_name' => $this->string(50)->notNull(),
            'owner_email' => $this->string(50)->notNull(),
            'owner_phone' => $this->string(50)->notNull(),
            'owner_info' => $this->string(255)->notNull(),
            'contacts_name' => $this->string(50)->notNull(),
            'contacts_email' => $this->string(50)->notNull(),
            'contacts_phone' => $this->string(50)->notNull(),
            'contacts_info' => $this->string(255)->notNull(),
            'quota' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ]);

        $this->createIndex('uk-real_estate-project_id', '{{%real_estate}}', 'project_id', false);
        $this->createIndex('uk-real_estate-status_id', '{{%real_estate}}', 'status_id', false);

        $this->createTable('{{%real_estate_photo}}', [
            'id' => $this->primaryKey(),
            'real_estate_id' => $this->integer(11)->notNull(),
            'src' => $this->string(255)->notNull()
        ]);

        $this->createIndex('uk-real_estate_photo-real_estate_id', '{{%real_estate_photo}}', 'real_estate_id', false);
        $this->addForeignKey('fk-real_estate_photo-real_estate_id-real_estate-id', '{{%real_estate_photo}}', 'real_estate_id', '{{%real_estate}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk-real_estate_photo-real_estate_id-real_estate-id', '{{%real_estate_photo}}');
        $this->dropIndex('uk-real_estate_photo-real_estate_id', '{{%real_estate_photo}}');
        $this->dropTable('{{%real_estate_photo}}');
        $this->dropIndex('uk-real_estate-status_id', '{{%real_estate}}');
        $this->dropIndex('uk-real_estate-project_id', '{{%real_estate}}');
        $this->dropTable('{{%real_estate}}');
    }

}
