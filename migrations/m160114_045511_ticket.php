<?php

use yii\db\Migration;

class m160114_045511_ticket extends Migration {

    public function safeUp() {
        $this->addColumn('{{%ticket_comment}}', 'created_by', $this->bigInteger(20)->notNull());
        $this->createIndex('uk-ticket_comment-created_by', '{{%ticket_comment}}', 'created_by');
        $this->addForeignKey('fk-ticket_comment-creted_by-user-id', '{{%ticket_comment}}', 'created_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk-ticket_comment-creted_by-user-id', '{{%ticket_comment}}');
        $this->dropIndex('uk-ticket_comment-created_by', '{{%ticket_comment}}');
        $this->dropTable('{{%ticket_comment}}');
    }

}
