<?php

use yii\db\Migration;

class m160119_183117_user_balance extends Migration {

    public function safeUp() {
        $this->addColumn('{{%user}}', 'balance', $this->float()->defaultValue(0));
        $this->addColumn('{{%user}}', 'frozen_balance', $this->float()->defaultValue(0));
    }

    public function safeDown() {
        $this->dropColumn('{{%user}}', 'frozen_balance');
        $this->dropColumn('{{%user}}', 'balance');
    }

}
