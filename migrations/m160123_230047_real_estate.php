<?php

use yii\db\Migration;

class m160123_230047_real_estate extends Migration {

    public function safeUp() {
        $this->addColumn('{{%real_estate}}', 'cancel_booking_type', $this->integer(11)->notNull()->defaultValue(0));
    }

    public function safeDown() {
        $this->dropColumn('{{%real_estate}}', 'cancel_booking_type');
    }

}
