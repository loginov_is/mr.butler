<?php

use yii\db\Migration;

class m151227_090952_project_furnish extends Migration {

    public function safeUp() {
        $this->createTable('{{%project_to_lookup}}', [
            'project_id' => $this->integer(11)->notNull(),
            'lookup_id' => $this->integer(11)->notNull(),
        ]);

        $this->createIndex('uk-project_to_lookup-project_id', '{{%project_to_lookup}}', 'project_id');
        $this->createIndex('uk-project_to_lookup-lookup_id', '{{%project_to_lookup}}', 'lookup_id');
        $this->addForeignKey('fk-project_to_lookup-project_id-lookup-id', '{{%project_to_lookup}}', 'project_id', '{{%project}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-project_to_lookup-lookup_id-lookup-id', '{{%project_to_lookup}}', 'lookup_id', '{{%lookup}}', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%real_estate_to_lookup}}', [
            'real_estate_id' => $this->integer(11)->notNull(),
            'lookup_id' => $this->integer(11)->notNull(),
        ]);

        $this->createIndex('uk-real_estate_to_lookup-real_estate_id', '{{%real_estate_to_lookup}}', 'real_estate_id');
        $this->createIndex('uk-real_estate_to_lookup-lookup_id', '{{%real_estate_to_lookup}}', 'lookup_id');
        $this->addForeignKey('fk-real_estate_to_lookup-real_estate_id-real_estate-id', '{{%real_estate_to_lookup}}', 'real_estate_id', '{{%real_estate}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-real_estate_to_lookup-lookup_id-lookup-id', '{{%real_estate_to_lookup}}', 'lookup_id', '{{%lookup}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk-real_estate_to_lookup-lookup_id-lookup-id', '{{%real_estate_to_lookup}}');
        $this->dropForeignKey('fk-real_estate_to_lookup-real_estate_id-real_estate-id', '{{%real_estate_to_lookup}}');
        $this->dropIndex('uk-real_estate_to_lookup-lookup_id', '{{%real_estate_to_lookup}}');
        $this->dropIndex('uk-real_estate_to_lookup-real_estate_id', '{{%real_estate_to_lookup}}');
        $this->dropTable('{{%real_estate_to_lookup}}');
        $this->dropForeignKey('fk-project_to_lookup-lookup_id-lookup-id', '{{%project_to_lookup}}');
        $this->dropForeignKey('fk-project_to_lookup-project_id-lookup-id', '{{%project_to_lookup}}');
        $this->dropIndex('uk-project_to_lookup-lookup_id', '{{%project_to_lookup}}');
        $this->dropIndex('uk-project_to_lookup-project_id', '{{%project_to_lookup}}');
        $this->dropTable('{{%project_to_lookup}}');
    }

}
