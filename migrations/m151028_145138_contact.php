<?php

use yii\db\Migration;

class m151028_145138_contact extends Migration
{
    public function up()
    {
        $this->createTable('{{%contact}}', [
            'id' => $this->primaryKey(),
            'language_id' => $this->integer()->notNull(),
            'address' => $this->string(255)->notNull(),
            'phone' => $this->string(255)->notNull(),
            'email' => $this->string(100)->notNull(),
            'skype' => $this->string(100)->notNull(),
            'map' => $this->string(255)->notNull(),
        ]);

        $this->addForeignKey('fk_contact_language_id', '{{%contact}}', 'language_id', '{{%language}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_contact_language_id', '{{%contact}}');

        $this->dropTable('{{%contact}}');
    }
}
