<?php

use yii\db\Migration;

class m160128_184546_motivation_pics extends Migration {

    public function safeUp() {
        $this->batchInsert('{{%motivation_picture}}', ['type', 'image', 'title', 'order'], [
            ['RealEstateAdd', 'rent_key.png', 'Надежность', 1],
            ['RealEstateAdd', 'rent_arrow.png', 'Достижение цели', 2],
            ['RealEstateAdd', 'rent_comfort.png', 'Удобство', 3],
        ]);
    }

    public function safeDown() {
        $this->delete('{{%motivation_picture}}', ['type' => 'RealEstateAdd']);
    }

}
