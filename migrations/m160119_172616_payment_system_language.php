<?php

use yii\db\Migration;

class m160119_172616_payment_system_language extends Migration {

    public function safeUp() {
        $this->addColumn('{{%payment_system}}', 'language_id', $this->integer(11)->notNull());
        $this->createIndex('uk-payment_system-language_id', '{{%payment_system}}', 'language_id');
        $this->addForeignKey('fk-payment_system-language_id-language-id', '{{%payment_system}}', 'language_id', '{{%language}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk-payment_system-language_id-language-id', '{{%payment_system}}');
        $this->dropIndex('uk-payment_system-language_id', '{{%payment_system}}');
        $this->dropTable('{{%payment_system}}');
    }

}
