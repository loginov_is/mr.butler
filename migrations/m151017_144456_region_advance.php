<?php

use yii\db\Migration;

class m151017_144456_region_advance extends Migration {

    public function safeUp() {
        $this->addColumn('{{%region}}', 'description', $this->text());

        $this->createTable('{{%video}}', [
            'id' => $this->bigPrimaryKey(),
            'url' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull()
        ]);

        $this->createTable('{{%video_to_region}}', [
            'region_id' => $this->bigInteger()->notNull(),
            'video_id' => $this->bigInteger()->notNull()
        ]);

        $this->createIndex('ak_video_to_region_region_id', '{{%video_to_region}}', 'region_id');
        $this->createIndex('ak_video_to_region_video_id', '{{%video_to_region}}', 'video_id');
        $this->addForeignKey('fk_video_to_region_region_id', '{{%video_to_region}}', 'region_id', '{{%region}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_video_to_region_video_id', '{{%video_to_region}}', 'video_id', '{{%video}}', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%image_album_to_region}}', [
            'region_id' => $this->bigInteger()->notNull(),
            'album_id' => $this->bigInteger()->notNull()
        ]);

        $this->createIndex('ak_image_album_to_region_region_id', '{{%image_album_to_region}}', 'region_id');
        $this->createIndex('ak_image_album_to_region_album_id', '{{%image_album_to_region}}', 'album_id');
        $this->addForeignKey('fk_image_album_to_region_region_id', '{{%image_album_to_region}}', 'region_id', '{{%region}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_image_album_to_region_album_id', '{{%image_album_to_region}}', 'album_id', '{{%image_album}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk_image_album_to_region_album_id', '{{%image_album_to_region}}');
        $this->dropForeignKey('fk_image_album_to_region_region_id', '{{%image_album_to_region}}');
        $this->dropTable('{{%image_album_to_region}}');
        $this->dropForeignKey('fk_video_to_region_video_id', '{{%video_to_region}}');
        $this->dropForeignKey('fk_video_to_region_region_id', '{{%fk_video_to_region_region_id}}');
        $this->dropTable('{{%video_to_region}}');
        $this->dropTable('{{%video}}');
        $this->dropColumn('{{%region}}', 'description');
    }

}
