<?php

use yii\db\Migration;

class m151129_183357_real_estate_user_id extends Migration {

    public function safeUp() {
        $this->addColumn('{{%real_estate}}', 'user_id', $this->bigInteger(20)->notNull());
        $this->createIndex('uk-real_estate-user_id', '{{%real_estate}}', 'user_id', false);
        $this->addForeignKey('fk-real_estate-user_id-user-id', '{{%real_estate}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk-real_estate-user_id-user-id', '{{%real_estate}}');
        $this->dropIndex('uk-real_estate-user_id', '{{%real_estate}}');
        $this->dropColumn('{{%real_estate}}', 'user_id');
    }

}
