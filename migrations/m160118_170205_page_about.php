<?php

use yii\db\Migration;

class m160118_170205_page_about extends Migration {

    public function safeUp() {
        $this->createTable('{{%page_about}}', [
            'id' => $this->primaryKey(),
            'who_is_mr_butler_title' => $this->string(255)->notNull(),
            'who_is_mr_butler_promotions' => $this->text(),
            'wanna_get_to_rent_title' => $this->string(255)->notNull(),
            'wanna_get_to_rent_promotions' => $this->text(),
            'wanna_rent_title' => $this->string(255)->notNull(),
            'wanna_rent_promotions' => $this->text(),
            'wanna_rent_video' => $this->string(255)->notNull(),
            'partners_title' => $this->string(255)->notNull(),
            'partners_promotions' => $this->text(),
            'team_title' => $this->string(255)->notNull(),
            'team_promotions' => $this->text(),
            'press_title' => $this->string(255)->notNull(),
            'press_promotions' => $this->text(),
        ]);

        $this->insert('{{%page_about}}', [
            'who_is_mr_butler_title' => 'Кто такой Mr Butler',
            'who_is_mr_butler_promotions' => 'Очевидно, что разрабатывая бизнес-план, вы преследуете вполне-конкретные цели. И разумеется вы полагаете, что ваша компания будет развиваться и процветать из года в год.

                                                Чтобы вам было проще, мы составили таблиу предполагаемых достижений, которую вам осталось только заполнить

                                                Для начала четко определитесь в какой сфере, нише, микронише вы намерены стать лидером. Определите направления, в которых вы имеете неоспоримые преимущества и реальные шансы выделиться.
                                                ',
            'wanna_get_to_rent_title' => 'Хотите сдать свою недвижимость в Таиланде в доверительное управление?',
            'wanna_get_to_rent_promotions' => 'Свяжитесь с нами и начните получать
доход от аренды прямо сейчас',
            'wanna_rent_title' => 'Вы хотите снять недвижимость в Таиланде для себя и своих близих?',
            'wanna_rent_promotions' => 'Выберите свой дом в Таиланде на время отпуска. Забронируйте его и наслаждайтесь отдыхом в комфортных условиях без лишних хлопот',
            'partners_title' => 'Партнеры',
            'partners_promotions' => 'Нам помогают развиваться и становиться мощнее:',
            'team_title' => 'Команда',
            'team_promotions' => 'С вами работают:',
            'press_title' => 'Пресса о нас',
        ]);
    }

    public function safeDown() {
        $this->execute("truncate table {{%page_about}}");
        $this->dropTable('{{%page_about}}');
    }

}
