<?php

use yii\db\Migration;

class m151013_141414_order_rework extends Migration {

    public function safeUp() {
        $this->dropColumn('{{%image}}', 'order');
        $this->addColumn('{{%image_to_album}}', 'order', $this->integer(11)->notNull()->defaultValue(0));
    }

    public function safeDown() {
        $this->addColumn('{{%image}}', 'order', $this->integer(11)->notNull()->defaultValue(0));
        $this->dropColumn('{{%image_to_album}}', 'order');
    }

}
