<?php

use yii\db\Migration;

class m151007_155700_system_user extends Migration
{
    private $email;

    public function init()
    {
        parent::init();
        $this->email = Yii::$app->params['systemEmail'];
    }

    public function up()
    {
        $this->insert('{{%user}}', [
            'username' => 'system',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => '',
            'password_reset_token' => '',
            'email' => $this->email,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    public function down()
    {
        $this->delete('{{%user}}', ['email' => $this->email]);
    }
}
