<?php

use yii\db\Migration;

class m160118_210106_partner extends Migration {

    public function safeUp() {
        $this->createTable('{{%partner}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'image' => $this->string(255)->notNull(),
            'description' => $this->text()
        ]);
    }

    public function safeDown() {
        $this->dropTable('{{%partner}}');
    }

}
