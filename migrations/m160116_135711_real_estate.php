<?php

use yii\db\Migration;

class m160116_135711_real_estate extends Migration {

    public function safeUp() {
        $this->dropColumn('{{%real_estate}}', 'contacts_info');
        $this->dropColumn('{{%real_estate}}', 'owner_info');
        $this->addColumn('{{%real_estate}}', 'contacts_info', $this->string(255)->defaultValue(NULL));
        $this->addColumn('{{%real_estate}}', 'owner_info', $this->string(255)->defaultValue(NULL));
    }

    public function safeDown() {
        $this->dropColumn('{{%real_estate}}', 'owner_info');
        $this->dropColumn('{{%real_estate}}', 'contacts_info');
        $this->addColumn('{{%real_estate}}', 'owner_info', $this->string(255)->notNull());
        $this->addColumn('{{%real_estate}}', 'contacts_info', $this->string(255)->notNull());
    }

}
