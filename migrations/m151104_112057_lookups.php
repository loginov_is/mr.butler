<?php

use yii\db\Migration;

class m151104_112057_lookups extends Migration
{
    public function up()
    {
        $this->createTable('{{%lookup}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(128)->notNull(),
            'code' => $this->integer()->notNull(),
            'name' => $this->string(128)->notNull(),
            'sort_order' => $this->smallInteger()->notNull()->defaultValue(0)
        ]);

        $this->batchInsert('{{%lookup}}', ['type', 'code', 'name', 'sort_order'], [
            ['PostStatus', 1, 'Черновик', 1],
            ['PostStatus', 2, 'Опубликовано', 2],
            ['PostStatus', 3, 'Архив', 3],
            ['PostStatus', 4, 'Удалено', 4],

            ['HousingType', 1, 'Отдельная комната', 1],
            ['HousingType', 2, 'Дом или квартира', 2],
            ['HousingType', 3, 'Общая комната', 3],
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%lookup}}');
    }
}
