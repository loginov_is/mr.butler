<?php

use yii\db\Migration;

class m160111_143059_region_to_lookup extends Migration
{
    public function safeUp() {
        $this->createTable('{{%region_to_lookup}}', [
            'region_id' => $this->integer(11)->notNull(),
            'lookup_id' => $this->integer(11)->notNull(),
        ]);
        $this->batchInsert('{{%lookup}}', ['type', 'code', 'name', 'sort_order'], [
            ['RegionComfort', 1, '�������� ������', 1],
            ['RegionComfort', 2, '���������� � ��������', 2],
            ['RegionComfort', 3, '������ ����', 3],
            ['RegionComfort', 4, '������ ����������', 4],
            ['RegionComfort', 5, '�����������', 5],
        ]);
    }

    public function safeDown() {
        $this->dropTable('{{%region_to_lookup}}');
        $this->delete('{{%lookup}}', ['type' => 'RegionComfort']);
    }
}
