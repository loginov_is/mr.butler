<?php

use yii\db\Migration;

class m160123_100241_service_order extends Migration
{
    public function safeUp() {
        $this->createTable('{{%service_order}}', [
            'id' => $this->primaryKey(),
            'booking_id' => $this->bigInteger()->notNull(),
            'service_id' => $this->integer()->notNull(),
            'people_count' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-service_order-service_id-service-id', '{{%service_order}}', 'service_id', '{{%service}}', 'id');
        $this->addForeignKey('fk-service_order-booking_id-booking-id', '{{%service_order}}', 'booking_id', '{{%booking}}', 'id');
        $this->createIndex('uk-service_order-booking_id', '{{%service_order}}', 'booking_id');
    }

    public function safeDown() {
        $this->dropTable('{{%service_order}}');
    }
}
