<?php

use yii\db\Migration;

class m151204_094530_feedback extends Migration {

    public function safeUp() {
        $this->createTable('{{%feedback}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(11),
            'user_id' => $this->integer(11)->defaultValue(0)->notNull(),
            'email' => $this->string(50)->notNull(),
            'phone' => $this->string(50)->notNull(),
            'name' => $this->string(50)->notNull(),
            'is_viewed' => $this->integer(1)->notNull()->defaultValue(0)
        ]);

        $this->createIndex('uk-feedback-created_at', '{{%feedback}}', 'created_at');
        $this->createIndex('uk-feedback-user_id', '{{%feedback}}', 'user_id');
        $this->createIndex('uk-feedback-is_viewed', '{{%feedback}}', 'is_viewed');
    }

    public function safeDown() {
        $this->dropIndex('uk-feedback-is_viewed', '{{%feedback}}');
        $this->dropIndex('uk-feedback-user_id', '{{%feedback}}');
        $this->dropIndex('uk-feedback-created_at', '{{%feedback}}');
        $this->dropTable('{{%feedback}}');
    }

}
