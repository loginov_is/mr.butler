<?php

use yii\db\Migration;

class m151203_174841_site_options extends Migration {

    public function safeUp() {
        $this->createTable('{{%site_option}}', [
            'id' => $this->primaryKey(),
            'facebook' => $this->string(255),
            'vkontakte' => $this->string(255),
            'youtube' => $this->string(255),
        ]);

        $this->insert('{{%site_option}}', [
            'facebook' => 'https://www.facebook.com/MrButler-1497463147217631',
            'vkontakte' => 'http://vk.com/butler.estate',
            'youtube' => 'https://www.youtube.com/channel/UCGDkWwUAVSgQ5xtiwI0iatQ'
                ]
        );
    }

    public function safeDown() {
        $this->dropTable('{{%site_option}}');
    }

}
