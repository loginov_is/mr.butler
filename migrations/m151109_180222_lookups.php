<?php

use yii\db\Migration;

class m151109_180222_lookups extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%lookup}}', ['type', 'code', 'name', 'sort_order'], [
            ['HousingComfort', 1, 'Кухня', 1],
            ['HousingComfort', 2, 'Интернет', 2],
            ['HousingComfort', 3, 'TV', 3],
            ['HousingComfort', 4, 'Туалетные принадлежности', 4],
            ['HousingComfort', 5, 'Кондиционер', 5],
            ['HousingComfort', 6, 'Стиральная машинка', 6],
            ['HousingComfort', 7, 'Бесплатная парковкая', 7],
            ['HousingComfort', 8, 'Wi-Fi', 8],
            ['HousingComfort', 9, 'Кабельное телевидение', 9],
            ['HousingComfort', 10, 'Можно с питомцами', 10],
            ['HousingComfort', 11, 'Можно курить', 11],
            ['HousingComfort', 12, 'Подходит для семей с детьми', 12],
            ['HousingComfort', 13, 'Подходит для проведения вечеринок', 13],
            ['HousingComfort', 14, 'Подходит для людей с ограниченными возможностями', 14],
            ['HousingComfort', 15, 'Камин', 15],
            ['HousingComfort', 16, 'Вахтер', 16],
            ['HousingComfort', 17, 'Бассейн коммунальный', 17],
            ['HousingComfort', 18, 'Бассейн частный', 18],
            ['HousingComfort', 19, 'Собственный пляж', 19],
            ['HousingComfort', 20, 'Сауна', 20],
            ['HousingComfort', 21, 'Тренажерный зал', 21],
            ['HousingComfort', 22, 'Джакузи', 22],
            ['HousingComfort', 23, 'Душ', 23],
            ['HousingComfort', 24, 'Ванная', 24],
            ['HousingComfort', 25, 'Сейф', 25],
            ['HousingComfort', 26, 'Фен', 26],
            ['HousingComfort', 27, 'Чайник', 27],
            ['HousingComfort', 28, 'Микроволновка', 28],
            ['HousingComfort', 29, 'Детская площадка', 29],
        ]);

    }

    public function down()
    {
        $this->delete('{{%lookup}}', ['type' => 'HousingComfort']);
    }
}
