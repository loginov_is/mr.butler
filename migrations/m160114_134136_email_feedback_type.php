<?php

use yii\db\Migration;

class m160114_134136_email_feedback_type extends Migration {

    public function safeUp() {
        $this->addColumn('{{%feedback_email}}', 'type', $this->integer(11)->notNull());
        $this->createIndex('uk-feedback_email-type', '{{%feedback_email}}', 'type');
    }

    public function safeDown() {
        $this->dropIndex('uk-feedback_email-type', '{{%feedback_email}}');
        $this->dropTable('{{%feedback_email}}');
    }

}
