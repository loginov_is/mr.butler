<?php

use yii\db\Migration;
use app\models\Language;

class m160120_173251_page extends Migration {

    public function safeUp() {

        foreach (Language::find()->all() as $language) {
            $this->batchInsert('{{%page}}', ['created_at', 'language_id', 'slug', 'system', 'title'], [
                [time(), $language->id, 'site_payments', 1, 'Способы оплаты'],
            ]);
        }
    }

    public function safeDown() {
        
    }

}
