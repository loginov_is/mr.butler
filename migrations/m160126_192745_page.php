<?php

use yii\db\Migration;
use app\models\Language;

class m160126_192745_page extends Migration {

    public function safeUp() {
        foreach (Language::find()->all() as $language) {
            $this->batchInsert('{{%page}}', ['created_at', 'language_id', 'slug', 'system', 'title'], [
                [time(), $language->id, 'site_reviews', 1, 'Отзывы'],
            ]);
        }
    }

    public function safeDown() {
        
    }

}
