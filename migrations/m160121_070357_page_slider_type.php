<?php

use yii\db\Migration;

class m160121_070357_page_slider_type extends Migration
{
    public function safeUp() {
        $this->addColumn('{{%page}}', 'slider_type', $this->integer()->defaultValue(1));
        $this->addColumn('{{%page}}', 'background_image', $this->string(128));
    }

    public function safeDown() {
        $this->dropColumn('{{%page}}', 'slider_type');
        $this->dropColumn('{{%page}}', 'background_image');
    }
}
