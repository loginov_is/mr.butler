<?php

use yii\db\Migration;

class m151017_151757_video_advance extends Migration {

    public function safeUp() {
        $this->addColumn('{{%video}}', 'youtube_id', $this->string(255)->notNull());
    }

    public function safeDown() {
        $this->dropColumn('{{%video}}', 'youtube_id');
    }

}
