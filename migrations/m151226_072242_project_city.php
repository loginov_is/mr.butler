<?php

use yii\db\Migration;

class m151226_072242_project_city extends Migration {

    public function safeUp() {
        $this->addColumn('{{%project}}', 'city_id', $this->integer(11)->notNull());
        $this->addColumn('{{%project}}', 'region_id', $this->integer(11)->notNull());
        $this->createIndex('uk-project-city_id', '{{%project}}', 'city_id');
        $this->createIndex('uk-project-region_id', '{{%project}}', 'region_id');
    }

    public function safeDown() {
        $this->dropIndex('uk-project-region_id', '{{%project}}');
        $this->dropIndex('uk-project-city_id', '{{%project}}');
        $this->dropColumn('{{%project}}', 'region_id');
        $this->dropColumn('{{%project}}', 'city_id');
    }

}
