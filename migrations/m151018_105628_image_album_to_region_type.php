<?php

use yii\db\Migration;

class m151018_105628_image_album_to_region_type extends Migration {

    public function safeUp() {
        $this->addColumn('{{%image_album_to_region}}', 'type_id', $this->integer(11)->notNull()->defaultValue(0));
        $this->createIndex('ak_image_album_to_region_type_id', '{{%image_album_to_region}}', 'type_id');
    }

    public function safeDown() {
        $this->dropColumn('{{%image_album_to_region}}', 'type_id');
    }

}
