<?php

use yii\db\Migration;

class m151202_082943_project extends Migration {

    public function safeUp() {
        $this->createTable('{{%project}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk-real_estate-project_id-project-id', '{{%real_estate}}', 'project_id', '{{%project}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk-real_estate-project_id-project-id', '{{%real_estate}}');
        $this->dropTable('{{%project}}');
    }

}
