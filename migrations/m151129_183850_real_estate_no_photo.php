<?php

use yii\db\Migration;

class m151129_183850_real_estate_no_photo extends Migration
{
    public function safeUp() {
        $this->addColumn('{{%real_estate}}', 'no_photo', $this->integer(1)->notNull()->defaultValue(0));
        $this->createIndex('uk-real_estate-no_photo', '{{%real_estate}}', 'no_photo', false);
    }

    public function safeDown() {
        $this->dropIndex('uk-real_estate-no_photo', '{{%real_estate}}');
        $this->dropColumn('{{%real_estate}}', 'no_photo');
    }
}
