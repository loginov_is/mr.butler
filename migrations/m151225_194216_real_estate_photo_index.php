<?php

use yii\db\Migration;

class m151225_194216_real_estate_photo_index extends Migration
{
    public function safeUp() {
        $this->addColumn('{{%real_estate_photo}}', 'index', $this->integer(11)->notNull()->defaultValue(0));
    }

    public function safeDown() {
        $this->dropColumn('{{%real_estate_photo}}', 'index');
    }
}
