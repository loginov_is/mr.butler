<?php

use yii\db\Migration;

class m160118_194229_team extends Migration {

    public function safeUp() {
        $this->createTable('{{%team}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'image' => $this->string(255)->notNull(),
            'phone' => $this->string(50)->notNull(),
            'email' => $this->string(50)->notNull(),
            'position' => $this->string(50)->notNull(),
        ]);

        $this->createTable('{{%press}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50)->notNull(),
            'image' => $this->string(255)->notNull(),
        ]);
        
        $this->createTable('{{%press_article}}', [
            'id' => $this->primaryKey(),
            'press_id' => $this->integer(11)->notNull(),
            'title' => $this->string(255)->notNull(),
            'url' => $this->string(255)->notNull(),
        ]);
        
        $this->createIndex('uk-press_article-press_id', '{{%press_article}}', 'press_id');
        $this->addForeignKey('fk-press_article-press_id-press-id', '{{%press_article}}', 'press_id', '{{%press}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk-press_article-press_id-press-id', '{{%press_article}}');
        $this->dropIndex('uk-press_article-press_id', '{{%press_article}}');
        $this->dropTable('{{%press_article}}');
        $this->dropTable('{{%press}}');
        $this->dropTable('{{%team}}');
    }

}
