<?php

use yii\db\Migration;

class m151027_151629_regulations extends Migration
{
    public function up()
    {
        $this->createTable('{{%regulation_group}}', [
            'id' => $this->primaryKey(),
            'language_id' => $this->integer()->notNull(),
            'title' => $this->string(255)->notNull(),
            'sort_order' => $this->smallInteger()->notNull()->defaultValue(0),
        ]);

        $this->createTable('{{%regulation}}', [
            'id' => $this->primaryKey(),
            'group_id' => $this->integer()->notNull(),
            'title' => $this->string(255)->notNull(),
            'content' => $this->text()->notNull(),
            'sort_order' => $this->smallInteger()->notNull()->defaultValue(0),
        ]);

        $this->addForeignKey('fk_regulation_group_language_id', '{{%regulation_group}}', 'language_id', '{{%language}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_regulation_regulation_group_id', '{{%regulation}}', 'group_id', '{{%regulation_group}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_regulation_regulation_group_id', '{{%regulation}}');
        $this->dropForeignKey('fk_regulation_group_language_id', '{{%regulation_group}}');

        $this->dropTable('{{%regulation}}');
        $this->dropTable('{{%regulation_group}}');
    }
}
