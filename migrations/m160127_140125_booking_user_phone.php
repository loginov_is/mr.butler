<?php

use yii\db\Migration;

class m160127_140125_booking_user_phone extends Migration
{
    public function safeUp() {
        $this->dropColumn('{{%booking}}', 'user_phone');
        $this->addColumn('{{%booking}}', 'user_phone', $this->string());
    }

    public function safeDown() {
        $this->dropColumn('{{%booking}}', 'user_phone');
        $this->addColumn('{{%booking}}', 'user_phone', $this->integer());
    }
}
