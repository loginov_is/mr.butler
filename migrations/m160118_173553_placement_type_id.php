<?php

use yii\db\Migration;

class m160118_173553_placement_type_id extends Migration
{
    public function safeUp() {
        $this->addColumn('{{%real_estate}}', 'housing_type_id', $this->bigInteger(20)->defaultValue(0));
    }

    public function safeDown() {
        $this->dropColumn('{{%real_estate}}', 'housing_type_id');
    }
}
