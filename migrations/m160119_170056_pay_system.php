<?php

use yii\db\Migration;

class m160119_170056_pay_system extends Migration {

    public function safeUp() {
        $this->createTable('{{%payment_system}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50)->notNull(),
            'image' => $this->string(255)->notNull()
        ]);

        $this->createTable('{{%payment_system_field}}', [
            'id' => $this->primaryKey(),
            'payment_system_id' => $this->integer(11)->notNull(),
            'title' => $this->string(255)->notNull(),
        ]);
        
        $this->createIndex('uk-payment_system_field-payment_system_id', '{{%payment_system_field}}', 'payment_system_id');
        $this->addForeignKey('fk-payment_system_field-payment_system_id-payment_system-id', '{{%payment_system_field}}', 'payment_system_id', '{{%payment_system}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk-payment_system_field-payment_system_id-payment_system-id', '{{%payment_system_field}}');
        $this->dropIndex('uk-payment_system_field-payment_system_id', '{{%payment_system_field}}');
        $this->dropTable('{{%payment_system_field}}');
        $this->dropTable('{{%payment_system}}');
    }

}
