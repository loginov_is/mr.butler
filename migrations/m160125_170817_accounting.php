<?php

use yii\db\Migration;

class m160125_170817_accounting extends Migration {

    public function safeUp() {
        $this->createTable('{{%accounting}}', [
            'id' => $this->primaryKey(),
            'created_by' => $this->bigInteger(20)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'user_id' => $this->bigInteger(20)->notNull(),
            'lookup_id' => $this->integer(11)->notNull(),
            'amount' => $this->float()->notNull(),
            'operation' => $this->integer(11)->notNull(),
        ]);

        $this->createIndex('uk-accounting-created_at', '{{%accounting}}', 'created_at');
        $this->createIndex('uk-accounting-user_id', '{{%accounting}}', 'user_id');
        $this->createIndex('uk-accounting-amount', '{{%accounting}}', 'amount');
    }

    public function safeDown() {
        $this->dropIndex('uk-accounting-amount', '{{%accounting}}');
        $this->dropIndex('uk-accounting-user_id', '{{%accounting}}');
        $this->dropIndex('uk-accounting-created_at', '{{%accounting}}');
        $this->dropTable('{{%accounting}}');
    }

}
