<?php

use yii\db\Migration;

class m160129_100338_booking_status extends Migration
{
    public function safeUp() {
        $this->delete('{{%lookup}}', ['type' => 'BookingStatus']);
        $this->dropColumn('{{%booking}}', 'status_id');
        $this->addColumn('{{%booking}}', 'status', $this->string()->notNull());
    }

    public function safeDown() {
        $this->batchInsert('{{%lookup}}', ['type', 'code', 'name', 'sort_order'], [
            ['BookingStatus', 0, 'Забронировано', 1],
            ['BookingStatus', 1, 'Оплачено', 2],
        ]);
        $this->dropColumn('{{%booking}}', 'status');
        $this->addColumn('{{%booking}}', 'status_id', $this->bigInteger(20)->notNull()->defaultValue(0));
    }
}
