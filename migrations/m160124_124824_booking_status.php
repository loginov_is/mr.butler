<?php

use yii\db\Migration;

class m160124_124824_booking_status extends Migration
{
    public function safeUp() {
        $this->batchInsert('{{%lookup}}', ['type', 'code', 'name', 'sort_order'], [
            ['BookingStatus', 0, 'Забронировано', 1],
            ['BookingStatus', 1, 'Оплачено', 2],
        ]);
        $this->batchInsert('{{%lookup}}', ['type', 'code', 'name', 'sort_order'], [
            ['BookingSource', 0, 'Сайт', 1],
            ['BookingSource', 1, 'Внешние букинговые системы', 2],
        ]);
    }

    public function safeDown() {
        $this->delete('{{%lookup}}', ['type' => 'BookingStatus']);
        $this->delete('{{%lookup}}', ['type' => 'BookingSource']);
    }
}
