<?php

use yii\db\Migration;

class m151011_071601_slider extends Migration {

    public function up() {
        $this->createTable('{{%image}}', [
            'id' => $this->bigPrimaryKey(),
            'image' => $this->string(255)->notNull(),
            'order' => $this->integer(11)->notNull()->defaultValue(0)
        ]);

        $this->createTable('{{%image_album}}', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);

        $this->createTable('{{%image_to_album}}', [
            'image_id' => $this->bigInteger()->notNull(),
            'album_id' => $this->bigInteger()->notNull(),
        ]); 

        $this->createIndex('ak_image_to_album_image_id', '{{%image_to_album}}', 'image_id');
        $this->createIndex('ak_image_to_album_album_id', '{{%image_to_album}}', 'album_id');

        $this->addForeignKey('fk_image_to_album_image_id', '{{%image_to_album}}', 'image_id', '{{%image}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_image_to_album_album_id', '{{%image_to_album}}', 'album_id', '{{%image_album}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down() {
        $this->dropForeignKey('fk_image_to_album_album_id', '{{%image_to_album}}');
        $this->dropForeignKey('fk_image_to_album_image_id', '{{%image_to_album}}');
        $this->dropTable('{{%image_to_album}}');
        $this->dropTable('{{%image_album}}');
        $this->dropTable('{{%image}}');
    }

}
