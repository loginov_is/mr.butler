<?php

use yii\db\Migration;

class m160123_165929_reviews extends Migration {

    public function safeUp() {
        $this->createTable('{{%reviews}}', [
            'id' => $this->primaryKey(),
            'real_estate_id' => $this->integer(11)->notNull(),
            'status_id' => $this->integer(11)->notNull(),
            'youtube_id' => $this->string(255),
            'created_by' => $this->bigInteger(20)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'review' => $this->text()->notNull(),
            'video_review' => $this->string(255),
            'minuses' => $this->text(),
            'pluses' => $this->text(),
            'rating' => $this->float()->notNull()->defaultValue(5),
        ]);

        $this->createIndex('uk-reviews-real_estate_id', '{{%reviews}}', 'real_estate_id');
        $this->createIndex('uk-reviews-status_id', '{{%reviews}}', 'status_id');
        $this->createIndex('uk-reviews-created_by', '{{%reviews}}', 'created_by');
        $this->createIndex('uk-reviews-created_at', '{{%reviews}}', 'created_at');
        $this->createIndex('uk-reviews-rating', '{{%reviews}}', 'rating');
        $this->addForeignKey('fk-reviews-real_estate_id-real_estate-id', '{{%reviews}}', 'real_estate_id', '{{%real_estate}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-reviews-created_by-user-id', '{{%reviews}}', 'created_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk-reviews-created_by-user-id', '{{%reviews}}');
        $this->dropForeignKey('fk-reviews-real_estate_id-real_estate-id', '{{%reviews}}');
        $this->dropIndex('uk-reviews-rating', '{{%reviews}}');
        $this->dropIndex('uk-reviews-created_at', '{{%reviews}}');
        $this->dropIndex('uk-reviews-created_by', '{{%reviews}}');
        $this->dropIndex('uk-reviews-status_id', '{{%reviews}}');
        $this->dropIndex('uk-reviews-real_estate_id', '{{%reviews}}');
        $this->dropTable('{{%reviews}}');
    }

}
