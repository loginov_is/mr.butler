<?php

use yii\db\Migration;

class m151211_172059_page_preview extends Migration {

    public function safeUp() {
        $this->addColumn('{{%page}}', 'preview', $this->text()->defaultValue(null));
    }

    public function safeDown() {
        $this->dropColumn('{{%page}}', 'preview');
    }

}
