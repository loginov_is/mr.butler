<?php

use yii\db\Migration;

class m160117_115556_mot_pics extends Migration {

    public function safeUp() {
        $this->batchInsert('{{%motivation_picture}}', ['type', 'image', 'title', 'order'], [
            ['WhoIsMrButler', 'site_about_reliability.png', 'Надежность', 1],
            ['WhoIsMrButler', 'site_about_devotion.png', 'Преданность, способность хранить тайну', 2],
            ['WhoIsMrButler', 'site_about_aspiration.png', 'Стремление сделать жизнь клиента и его семьи максимально комфортной', 3],
            ['WhoIsMrButler', 'site_about_tact.png', 'Тактичность, ненавязчивость, соблюдение профессиональной дистанции', 4],
            ['WhoIsMrButler', 'site_about_punctuality.png', 'Пунктуальность', 5],
            ['WhoIsMrButler', 'site_about_attention_to_detail.png', 'Внимание к деталям', 6],
        ]);
    }

    public function safeDown() {
        $this->delete('{{%motivation_picture}}', ['type' => 'WhoIsMrButler']);
    }

}
