<?php

use yii\db\Migration;

class m151201_134759_lookups extends Migration {

    public function up() {
        $this->batchInsert('{{%lookup}}', ['type', 'code', 'name', 'sort_order'], [
            ['RealEstateFurnishings', 0, 'Нет', 1],
            ['RealEstateFurnishings', 1, 'Частично', 2],
            ['RealEstateFurnishings', 2, 'Полностью', 3],
        ]);

        $this->batchInsert('{{%lookup}}', ['type', 'code', 'name', 'sort_order'], [
            ['RealEstatePool', 0, 'Нет', 1],
            ['RealEstatePool', 1, 'Общий', 2],
            ['RealEstatePool', 2, 'Личный', 3],
        ]);
    }

    public function down() {
        $this->delete('{{%lookup}}', ['type' => 'RealEstatePool']);
        $this->delete('{{%lookup}}', ['type' => 'RealEstateFurnishings']);
    }

}
