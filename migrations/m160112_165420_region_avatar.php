<?php

use yii\db\Migration;

class m160112_165420_region_avatar extends Migration
{
    public function safeUp() {
        $this->addColumn('{{%region}}', 'avatar', $this->string(255)->notNull());
    }

    public function safeDown() {
        $this->dropColumn('{{%region}}', 'avatar');
    }
}
