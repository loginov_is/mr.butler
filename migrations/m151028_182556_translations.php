<?php

use yii\db\Migration;

class m151028_182556_translations extends Migration {

    public function up() {
        $this->createTable('{{%translation}}', [
            'id' => $this->primaryKey(),
            'language_id' => $this->integer()->notNull(),
            'model' => $this->string(255)->notNull(),
            'model_id' => $this->integer()->notNull(),
            'column' => $this->string(100)->notNull(),
            'value' => $this->text(),
            'need_translate' => $this->boolean()->notNull()->defaultValue(true),
        ]);

        $this->createIndex('uk_language_id-model-model_id-column', '{{%translation}}', ['language_id', 'model', 'model_id', 'column'], true);
        $this->addForeignKey('fk_translation_language_id', '{{%translation}}', 'language_id', '{{%language}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down() {
        $this->dropForeignKey('fk_translation_language_id', '{{%translation}}');

        $this->dropTable('{{%translation}}');
    }

}
