<?php

use yii\db\Migration;
use app\models\Language;

class m160127_142126_guidebook_search_page extends Migration
{
    public function safeUp() {
        foreach (Language::find()->all() as $language) {
            $this->batchInsert('{{%page}}', ['created_at', 'language_id', 'slug', 'system', 'title'], [
                [time(), $language->id, 'guidebook_search', 1, 'Гид по районам'],
            ]);
        }
    }

    public function safeDown() {
        $this->delete('{{%page}}', ['slug' => 'guidebook_search']);
    }
}
