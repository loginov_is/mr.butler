<?php

use yii\db\Migration;

class m160114_071035_real_estate_rating extends Migration
{
    public function safeUp() {
        $this->addColumn('{{%real_estate}}', 'rating', $this->float()->notNull());
    }

    public function safeDown() {
        $this->dropColumn('{{%real_estate}}', 'rating');
    }
}
