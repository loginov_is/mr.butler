<?php

use yii\db\Migration;

class m160119_063941_user_line extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%user_line}}', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer()->notNull(),
            'model_id' => $this->bigInteger()->notNull(),
            'created_by' => $this->bigInteger()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex('uk-user_line-type_id', '{{%user_line}}', 'type_id', false);
        $this->createIndex('uk-user_line-model_id', '{{%user_line}}', 'model_id', false);
        $this->createIndex('uk-user_line-updated_at', '{{%user_line}}', 'updated_at', false);
        $this->addForeignKey('fk-user_line-created_by-user-id', '{{%user_line}}', 'created_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropTable('{{%user_line}}');
    }
}
