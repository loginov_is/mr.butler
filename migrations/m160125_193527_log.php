<?php

use yii\db\Migration;

class m160125_193527_log extends Migration {

    public function safeUp() {
        $this->addColumn('{{%user_log}}', 'action_id', $this->integer(11)->notNull());
    }

    public function safeDown() {
        $this->dropColumn('{{%user_log}}', 'action_id');
    }

}
