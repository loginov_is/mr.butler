<?php

namespace app\controllers;

use Yii;
use app\models\Faq;
use app\models\FaqGroup;
use yii\data\ActiveDataProvider;
use yii\helpers\StringHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Page;

class FaqController extends \yii\web\Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex($search = null) {
        $page = Page::findOne(['slug' => 'site_about', 'language_id' => Yii::$app->languageInfo->id]);
        $search = trim($search);
        $words = StringHelper::explode($search, ' ', true, true);
        $data = Faq::getData($words);
        $this->layout = '@app/views/layouts/main';
        return $this->render('index', [
                    'data' => $data,
                    'search' => $search,
                    'page' => $page
        ]);
    }

    public function actionAdmin() {
        $dataProvider = new ActiveDataProvider([
            'query' => Faq::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('admin', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new Faq();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }

        return $this->render('create', [
                    'model' => $model
        ]);
    }

    public function actionUpdate($id) {
        $model = Faq::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }

        return $this->render('update', [
                    'model' => $model
        ]);
    }

    public function actionDelete($id) {
        $model = Faq::findOne($id);
        Translation::deleteAll(['model' => $model::className(), 'model_id' => $id]);
        $model->delete();
        $this->redirect(['admin']);
    }

}
