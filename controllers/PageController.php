<?php

namespace app\controllers;

use app\helpers\ContentHelper;
use Yii;
use app\models\Page;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['view'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionAdmin() {
        $dataProvider = new ActiveDataProvider([
            'query' => Page::find(),
        ]);

        $this->layout = 'admin';

        return $this->render('admin', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        /**
         * @todo Load page by language
         */
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Page();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            ContentHelper::replaceImages($model);

            if ($model->uploadedFile = UploadedFile::getInstance($model, 'uploadedFile')) {
                $model->background_image = $model->uploadedFile->baseName . '-' . time() . '.' . $model->uploadedFile->extension;

                $dir = Yii::getAlias('@webroot/upload/') . '/' . $model->background_image;
                $model->uploadedFile->saveAs($dir);
                $model->save();
                \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
            }
        }

        $this->layout = 'admin';
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if ($model->uploadedFile = UploadedFile::getInstance($model, 'uploadedFile')) {
                $model->background_image = $model->uploadedFile->baseName . '-' . time() . '.' . $model->uploadedFile->extension;

                $dir = Yii::getAlias('@webroot/upload/') . '/' . $model->background_image;
                $model->uploadedFile->saveAs($dir);
                $model->save();
            }

            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }
        
        $this->layout = 'admin';
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);

        if ($model->system) {
            throw new NotFoundHttpException(\Yii::t('app', 'Невозможно удалить системную страницу'));
        } else {
            $model->delete();
            return $this->redirect(['admin']);
        }
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
