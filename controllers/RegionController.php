<?php

namespace app\controllers;

use app\models\Translation;
use Yii;
use app\models\Region;
use app\models\Video;
use app\models\VideoToRegion;
use app\models\RegionToRegion;
use yii\data\ActiveDataProvider;
use app\models\ImageAlbumToRegion;
use app\models\ImageAlbum;
use app\models\UserLine;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\helpers\ContentHelper;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

class RegionController extends Controller {
    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAdmin() {
        $dataProvider = new ActiveDataProvider([
            'query' => Region::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('admin', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new Region();
        $this->view->registerJs('Main.mapInit("' . ContentHelper::MAP_COORDS_DEFAULT . '", "Title")');
        $model->map = ContentHelper::MAP_COORDS_DEFAULT;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->avatar = UploadedFile::getInstance($model, 'avatar');
            if (!$model->upload()) {
                //uploading error
            }
            if ($model->avatar) {
                $model->avatar = $model->avatar->name;
                $model->save();
            }
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }

        return $this->render('create', [
                    'model' => $model
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $mapCoords = ($model->map) ? $model->map : ContentHelper::MAP_COORDS_DEFAULT;
        $this->view->registerJs('Main.mapInit("' . $mapCoords . '", "Title")');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->avatar = UploadedFile::getInstance($model, 'avatar');
            if (!$model->upload()) {
                //uploading error
            }
            if ($model->avatar) {
                $model->avatar = $model->avatar->name;
                $model->save();
            }
            Translation::loadTranslations($model);

            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }


        return $this->render('update', [
                    'model' => $model
        ]);
    }

    public function actionDelete($id) {
        $model = Region::findOne($id);
        Translation::deleteAll(['model' => $model::className(), 'model_id' => $id]);
        $model->delete();
        UserLine::deleteAll(['type_id' => UserLine::TYPE_ID_REGION_ADDED_TO_USERLINE, 'model_id' => $id]);
        $this->redirect(['admin']);
    }

    protected function findModel($id) {
        if (($model = Region::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSetRegionVideo() {
        if (Yii::$app->request->isAjax) {
            $videoId = Yii::$app->request->post('videoId');
            $regionId = Yii::$app->request->post('regionId');
            $model = new VideoToRegion();
            $model->region_id = $regionId;
            $model->video_id = $videoId;

            if ($model->save()) {
                $video = Video::findOne($videoId);
                echo $this->renderPartial('_video_item', [
                    'model' => $video,
                    'regionId' => $regionId
                ]);
            }
        }
    }

    public function actionUnsetRegionVideo() {
        if (Yii::$app->request->isAjax) {
            $videoId = Yii::$app->request->post('videoId');
            $regionId = Yii::$app->request->post('regionId');
            VideoToRegion::deleteAll('region_id = :regionId AND video_id = :videoId', [
                ':regionId' => $regionId,
                ':videoId' => $videoId
            ]);
        }
    }

    public function actionSetRegionAlbum() {
        if (Yii::$app->request->isAjax) {
            $albumId = Yii::$app->request->post('albumId');
            $regionId = Yii::$app->request->post('regionId');
            $typeId = Yii::$app->request->post('typeId');
            $model = new ImageAlbumToRegion();
            $model->region_id = $regionId;
            $model->album_id = $albumId;
            $model->type_id = $typeId;

            if ($model->save()) {
                $album = ImageAlbum::findOne($albumId);
                echo $this->renderPartial('_album_item', [
                    'model' => $album,
                    'regionId' => $regionId,
                    'typeId' => $typeId
                ]);
            }
        }
    }

    public function actionUnsetRegionAlbum() {
        if (Yii::$app->request->isAjax) {
            $albumId = Yii::$app->request->post('albumId');
            $regionId = Yii::$app->request->post('regionId');
            $typeId = Yii::$app->request->post('typeId');
            ImageAlbumToRegion::deleteAll('region_id = :regionId AND album_id = :albumId AND type_id = :typeId', [
                ':regionId' => $regionId,
                ':albumId' => $albumId,
                ':typeId' => $typeId
            ]);
        }
    }

    public function actionSetRegionChild() {
        if (Yii::$app->request->isAjax) {
            $childId = Yii::$app->request->post('childId');
            $regionId = Yii::$app->request->post('regionId');
            $model = new RegionToRegion();
            $model->parent_id = $regionId;
            $model->child_id = $childId;

            if ($model->save()) {
                $child = Region::findOne($childId);
                echo $this->renderPartial('_region_child_item', [
                    'parentId' => $regionId,
                    'model' => $child
                ]);
            }
        }
    }

    public function actionUnsetRegionChild() {
        if (Yii::$app->request->isAjax) {
            $childId = Yii::$app->request->post('childId');
            $regionId = Yii::$app->request->post('regionId');
            RegionToRegion::deleteAll('parent_id = :regionId AND child_id = :child_id', [
                ':regionId' => $regionId,
                ':child_id' => $childId
            ]);
        }
    }

}
