<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\PageAbout;
use app\models\Translation;

class PageAboutController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    public function actionUpdate() {
        $model = PageAbout::find()->one();

        if (!$model) {
            $model = new PageAbout();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Translation::loadTranslations($model);
            return $this->refresh();
        }

        return $this->render('update', [
                    'model' => $model
        ]);
    }

}
