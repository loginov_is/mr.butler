<?php

namespace app\controllers;

use Yii;
use app\models\Menu;
use yii\data\ActiveDataProvider;
use app\models\Language;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class MenuController extends \yii\web\Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAdmin($type, $language_id) {
        $dataProvider = new ActiveDataProvider([
            'query' => Menu::find()
                    ->where([
                        'type_id' => $type,
                        'language_id' => $language_id
                    ])
                    ->orderBy('order asc'),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $language = Language::findOne($language_id);
        return $this->render('admin', [
                    'dataProvider' => $dataProvider,
                    'type' => $type,
                    'language' => $language
        ]);
    }

    public function actionCreate() {
        $model = new Menu();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model
        ]);
    }

    public function actionUpdate($id) {
        $model = Menu::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model
        ]);
    }

    public function actionDelete($id) {
        $model = Menu::findOne($id);
        $model->delete();
        $this->redirect(['menu/admin']);
    }

    public function actionSetMenuOrderUp($id, $typeId, $languageId) {
        $currentItem = Menu::findOne($id);
        $currentOrder = $currentItem->order;
        $switchOrder = Menu::findBySql("SELECT m.id, m.order from {{%menu}} m WHERE m.type_id = :type AND m.language_id = :langId AND m.order < $currentOrder ORDER BY m.order desc", [
                    ':type' => $typeId,
                    ':langId' => $languageId
                ])->one();

        if ($switchOrder) {
            $currentItem->order = $switchOrder->order;
            $currentItem->save();
            $switchItem = Menu::findOne($switchOrder->id);
            $switchItem->order = $currentOrder;
            $switchItem->save();
        }

        $this->redirect(Yii::$app->request->referrer);
    }

    public function actionSetMenuOrderDown($id, $typeId, $languageId) {
        $currentItem = Menu::findOne($id);
        $currentOrder = $currentItem->order;
        $switchOrder = Menu::findBySql("SELECT m.id, m.order from {{%menu}} m WHERE m.type_id = :type AND m.language_id = :langId AND m.order > $currentOrder ORDER BY m.order asc", [
                    ':type' => $typeId,
                    ':langId' => $languageId
                ])->one();

        if ($switchOrder) {
            $currentItem->order = $switchOrder->order;
            $currentItem->save();
            $switchItem = Menu::findOne($switchOrder->id);
            $switchItem->order = $currentOrder;
            $switchItem->save();
        }

        $this->redirect(Yii::$app->request->referrer);
    }

}
