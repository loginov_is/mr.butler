<?php

namespace app\controllers;

use Yii;
use app\models\BookingSearch;
use app\models\Booking;
use app\models\RealEstate;
use app\models\Lookup;
use app\models\Service;
use app\models\ServiceOrder;

class BookingController extends \yii\web\Controller
{
    public function actionCancelBook($id) {
        $booking = Booking::findOne($id);
        if($booking) {
            //ServiceOrder::deleteAll(['booking_id' => $booking->id]);
            //$booking->delete();
            $booking->status = Booking::STATUS_CANCELED;
            $booking->save();
        }
        $this->redirect(['profile/selected-objects']);
    }

    public function actionAdmin() {
        $this->layout = 'admin';

        $searchModel = new BookingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id) {
        $this->layout = 'admin';

        $model = Booking::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $booking = Booking::findOne($id);
        if($booking) {
            ServiceOrder::deleteAll(['booking_id' => $booking->id]);
            $booking->delete();
        }
        return $this->redirect(['admin']);
    }

    public function actionAjaxDeleteServiceOrder($id) {
        $serviceOrder = ServiceOrder::findOne($id);
        if($serviceOrder) {
            $serviceOrder->delete();
        }
    }

    public function actionAjaxCheckAvailable($realEstateId, $startDate, $endDate) {
        $realEstate = RealEstate::findOne($realEstateId);
        if($realEstate) {
            if($realEstate->isAvailableForBooking(strtotime($startDate), strtotime($endDate))) {
                echo '1';
                return;
            }
        }
        echo '0';
    }

    public function actionAjaxGetServicePrice($serviceId, $peopleCount) {
        echo Service::findOne($serviceId)->getServicePrice($peopleCount);
    }
}
