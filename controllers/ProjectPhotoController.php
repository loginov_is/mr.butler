<?php

namespace app\controllers;

use app\models\ProjectPhoto;
use yii\filters\VerbFilter;
use \Yii;

class ProjectPhotoController extends \yii\web\Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    public function actionDelete($id) {
        $model = ProjectPhoto::findOne($id);
        if ($model && $model->delete()) {
            if (file_exists(Yii::getAlias('@webroot' . $model->src))) {
                $model->unsetImage(Yii::getAlias('@webroot' . $model->src));
            }
        }

        $this->redirect(['project/update/', 'id' => $model->project_id]);
    }

    public function actionSetMain($id) {
        $model = ProjectPhoto::findOne($id);
        if ($model) {
            $tmpModel = ProjectPhoto::find()->where(['main' => 1])->one();

            if ($tmpModel) {
                $tmpModel->main = 0;
                $tmpModel->save();
            }

            $model->main = 1;
            $model->save();
        }

        $this->redirect(['project/update/', 'id' => $model->project_id]);
    }

    public function actionSetSlider($id, $show) {
        $model = ProjectPhoto::findOne($id);
        if ($model) {
            $model->head_slider = $show;
            $model->save();
        }

        $this->redirect(['project/update/', 'id' => $model->project_id]);
    }

}
