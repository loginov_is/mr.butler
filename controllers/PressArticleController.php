<?php

namespace app\controllers;

use Yii;
use app\models\PressArticle;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Translation;

class PressArticleController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAdmin() {
        $dataProvider = new ActiveDataProvider([
            'query' => PressArticle::find(),
        ]);

        return $this->render('admin', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new PressArticle();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Translation::loadTranslations($model);
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        $model = PressArticle::findOne($id);
        Translation::deleteAll(['model' => $model::className(), 'model_id' => $id]);
        $model->delete();
        $this->redirect(['admin']);
    }

    protected function findModel($id) {
        if (($model = PressArticle::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
