<?php

namespace app\controllers;

use Yii;
use app\models\RegulationGroup;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Translation;

/**
 * RegulationGroupController implements the CRUD actions for RegulationGroup model.
 */
class RegulationGroupController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RegulationGroup models.
     * @return mixed
     */
    public function actionAdmin() {
        $dataProvider = new ActiveDataProvider([
            'query' => RegulationGroup::find(),
        ]);

        return $this->render('admin', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new RegulationGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new RegulationGroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing RegulationGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }
        
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RegulationGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = RegulationGroup::findOne($id);
        Translation::deleteAll(['model' => $model::className(), 'model_id' => $id]);
        $model->delete();
        $this->redirect(['admin']);
    }

    /**
     * Finds the RegulationGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RegulationGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = RegulationGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
