<?php

namespace app\controllers;

use Yii;
use app\models\Accounting;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\AccountingSearch;
use app\models\UserLog;

class AccountingController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAdmin() {
        $searchModel = new AccountingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new Accounting();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            UserLog::setLog($model::className(), $model->id, UserLog::ACTION_CREATE, UserLog::IS_ADMIN_TRUE);
            $model->setUserBalanceAction();
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
            $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $oldAmount = $model->amount;

        if ($model->load(Yii::$app->request->post())) {
            $model->amount = $oldAmount;

            if ($model->save()) {
                UserLog::setLog($model::className(), $model->id, UserLog::ACTION_UPDATE, UserLog::IS_ADMIN_TRUE);
                \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    protected function findModel($id) {
        if (($model = Accounting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
