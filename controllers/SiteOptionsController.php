<?php

namespace app\controllers;

use Yii;
use app\models\SiteOption;
use app\models\FeedbackEmail;
use app\models\SiteImage;
use app\models\SiteImageForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

class SiteOptionsController extends \yii\web\Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'except' => ['profile'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionUpdate() {
        $feedbackEmails = FeedbackEmail::find()->where('type = :type', [':type' => FeedbackEmail::TYPE_FEEDBACK])->all();
        $ticketEmails = FeedbackEmail::find()->where('type = :type', [':type' => FeedbackEmail::TYPE_TICKET])->all();
        $model = SiteOption::find()->one();
        $siteImageFormModel = new SiteImageForm();

        if (!$model) {
            $model = new SiteOption();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Изменения успешно сохранены'));
        }

        //Загрузка фоновых изображений
        $siteImageFormModel->load(Yii::$app->request->post());
        $backgroundImageDefault = UploadedFile::getInstanceByName('SiteImageForm[images][background_block_default]');
        $backgroundImageContacts = UploadedFile::getInstanceByName('SiteImageForm[images][background_block_contacts]');

        if($backgroundImageDefault) {
            $baseName = md5(time() . $backgroundImageDefault->baseName) . '.' . $backgroundImageDefault->extension;
            $backgroundImageDefault->saveAs(Yii::getAlias('@webroot/upload/') . $baseName);

            $siteImage = SiteImage::findOne(['type'=>'background_block_default']);
            $siteImage->src = $baseName;
            $siteImage->save();

            \Yii::$app->session->setFlash('bg_default_success', \Yii::t('app', 'Фоновое изображение по умолчанию установлено'));
        }

        if($backgroundImageContacts) {
            $baseName = md5(time() . $backgroundImageContacts->baseName) . '.' . $backgroundImageContacts->extension;
            $backgroundImageContacts->saveAs(Yii::getAlias('@webroot/upload/') . $baseName);

            $siteImage = SiteImage::findOne(['type'=>'background_block_contacts']);
            $siteImage->src = $baseName;
            $siteImage->save();

            \Yii::$app->session->setFlash('bg_contacts_success', \Yii::t('app', 'Фоновое изображение для страницы Контакты установлено'));
        }

        return $this->render('update', [
            'model' => $model,
            'feedbackEmails' => $feedbackEmails,
            'ticketEmails' => $ticketEmails,
            'siteImageFormModel' => $siteImageFormModel,
        ]);
    }

    public function actionCreateFeedbackEmail() {
        if (Yii::$app->request->isAjax) {
            $feedbackEmail = Yii::$app->request->post('feedbackEmail');
            $type = Yii::$app->request->post('type');
            $model = new FeedbackEmail();
            $model->email = $feedbackEmail;
            $model->type = $type;

            if ($model->save()) {
                echo $this->renderPartial('_feedback_email_item', [
                    'model' => $model
                ]);
            }
        }
    }

    public function actionDeleteFeedbackEmail() {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            $model = FeedbackEmail::findOne($id);

            if ($model && $model->delete()) {
                return true;
            }

            return false;
        }
    }

}
