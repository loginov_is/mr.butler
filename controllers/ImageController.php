<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use app\models\Image;
use app\models\ImageAlbum;
use \app\models\ImageToAlbum;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;

class ImageController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAdmin() {
        $albums = ImageAlbum::find()->all();
        $dataProvider = new ActiveDataProvider([
            'query' => Image::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('admin', [
                    'dataProvider' => $dataProvider,
                    'albums' => $albums
        ]);
    }

    public function actionSetImageRelation() {
        $model = new ImageToAlbum;
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            ImageToAlbum::resetAndGetMaxOrder($model->album_id);
        }

        $this->redirect(['image/admin']);
    }

    public function actionUnsetImageRelation($imageId, $albumId) {
        ImageToAlbum::deleteAll('image_id = :image AND album_id = :album', [
            ':image' => $imageId,
            ':album' => $albumId
        ]);

        ImageToAlbum::resetAndGetMaxOrder($albumId);
        $this->redirect(['image/admin']);
    }

    public function actionCreate() {
        $model = new Image();

        if (Yii::$app->request->isPost) {
            $model->uploadedFile = UploadedFile::getInstance($model, 'image');
            $model->image = $model->uploadedFile->baseName . '-' . time() . '.' . $model->uploadedFile->extension;

            if ($model->save()) {
                $dir = Yii::getAlias('@webroot/upload') . '/' . $model->image;
                $model->uploadedFile->saveAs($dir);
                \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
            }
        }

        return $this->render('create', [
                    'model' => $model
        ]);
    }

    public function actionDelete($id) {
        $model = Image::findOne($id);
        $model->delete();
        $this->redirect(['image/admin']);
    }

    /**
     * @param $type
     * @param $id
     * @param string $field
     * @return array
     * @todo Save to database?
     * @throws \yii\base\Exception
     */
    public function actionUpload($type, $id, $field = 'file') {
        $path = Yii::getAlias("@webroot/upload/{$type}/{$id}");
        $baseUrl = Yii::getAlias("@web/upload/{$type}/{$id}");

        if (!FileHelper::createDirectory($path)) {
            Yii::error("Can't create: {$path}");
        }

        $file = UploadedFile::getInstanceByName($field);
        $newName = md5(date('YmdHis') . $file->name) . '.' . $file->extension;
        $file->saveAs($path . '/' . $newName);

        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'filelink' => $baseUrl . '/' . $newName,
        ];
    }

}
