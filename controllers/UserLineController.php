<?php

namespace app\controllers;

use Yii;
use app\models\UserLine;

class UserLineController extends \yii\web\Controller {

    /**
     * Добавление объекта/района в строку пользователя
     *
     * @param $modelId
     * @param $typeId
     */
    public function actionCreate($modelId, $typeId) {
        if (!Yii::$app->user->isGuest) {
            $userId = Yii::$app->user->id;
            if (!$model = UserLine::findOne(['created_by' => $userId, 'type_id' => $typeId, 'model_id' => $modelId])) {
                $model = new UserLine();
                $model->type_id = $typeId;
                $model->model_id = $modelId;
                $model->created_by = $userId;
                $model->created_at = time();
            }
            $model->updated_at = time();
            $model->save();
        }

        $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Удаление объекта/района из строки пользователя
     *
     * @param $modelId
     * @param $typeId
     */
    public function actionDelete($modelId, $typeId) {
        if (!Yii::$app->user->isGuest) {
            $userId = Yii::$app->user->id;
            UserLine::deleteAll(['created_by' => $userId, 'type_id' => $typeId, 'model_id' => $modelId]);
        }

        $this->redirect(['profile/viewed-objects']);
    }

}
