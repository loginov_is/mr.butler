<?php

namespace app\controllers;

use Yii;
use app\models\Payment;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\PaymentSearch;
use app\models\PaymentComment;
use app\models\User;

class PaymentController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAdmin() {
        $searchModel = new PaymentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        $model = $this->findModel($id);
        $commentModel = new PaymentComment();
        $commentModel->payment_id = $id;
        $commentModel->viewed_user = 0;

        if ($commentModel->load(Yii::$app->request->post()) && $commentModel->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Комментарий успешно добавлен'));
        }

        $dataProvider = new ActiveDataProvider([
            'query' => PaymentComment::find()
                    ->where('payment_id = :id', [':id' => $id])
                    ->orderBy('id desc'),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('view', [
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                    'commentModel' => $commentModel
        ]);
    }

    public function actionDelete($id) {
        $model = Payment::findOne($id);

        if ($model->status_id == Payment::STATUS_NO_REQUISITES) {
            throw new NotFoundHttpException(\Yii::t('app', 'Невозможно удалить запрос. Пользователь не подтвердил свои намерения'));
        }

        $model->delete();
        $this->redirect(['admin']);
    }

    protected function findModel($id) {
        if (($model = Payment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPaymentCommentDelete($id) {
        $model = PaymentComment::findOne($id);

        if ($model) {
            $model->delete();
            $this->redirect(Yii::$app->request->referrer);
        } else {
            throw new NotFoundHttpException(\Yii::t('core', 'Данная страница не существует'));
        }
    }

    public function actionComplete($id) {
        $model = $this->findModel($id);
        $model->status_id = Payment::STATUS_COMPLETE;
        $user = User::findOne($model->created_by);

        if ($model->save()) {
            $user->minusFrozenBalance($model->amount);
        }

        $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDenied($id) {
        $model = $this->findModel($id);
        $model->status_id = Payment::STATUS_DENIED;
        $user = User::findOne($model->created_by);

        if ($model->save()) {
            $user->minusFrozenBalance($model->amount);
            $user->plusBalance($model->amount);
        }

        $this->redirect(Yii::$app->request->referrer);
    }

}
