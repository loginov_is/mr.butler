<?php

namespace app\controllers;

use Yii;
use app\models\PaymentSystem;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\PaymentSystemField;

class PaymentSystemController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAdmin() {
        $dataProvider = new ActiveDataProvider([
            'query' => PaymentSystem::find(),
        ]);

        return $this->render('admin', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new PaymentSystem();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->uploadedFile = UploadedFile::getInstance($model, 'image');
            $model->image = $model->uploadedFile->baseName . '-' . time() . '.' . $model->uploadedFile->extension;

            if ($model->save()) {
                $dir = Yii::getAlias('@webroot/upload/payment_system') . '/' . $model->image;
                $model->uploadedFile->saveAs($dir);
                \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->uploadedFile = UploadedFile::getInstance($model, 'image');
            $model->image = $model->uploadedFile->baseName . '-' . time() . '.' . $model->uploadedFile->extension;

            if ($model->save()) {
                $dir = Yii::getAlias('@webroot/upload/payment_system') . '/' . $model->image;
                $model->uploadedFile->saveAs($dir);
                \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    public function actionCreateField() {
        if (Yii::$app->request->isPost) {
            $title = Yii::$app->request->post('title');
            $paymentSystemId = Yii::$app->request->post('payment_system_id');
            $model = new PaymentSystemField();
            $model->title = $title;
            $model->payment_system_id = $paymentSystemId;

            if ($model->save()) {
                echo $this->renderPartial('_field', ['model' => $model]);
            }
        }
    }

    public function actionRemoveField() {
        if (Yii::$app->request->isPost) {
            $id = Yii::$app->request->post('id');
            $model = PaymentSystemField::findOne($id);

            if ($model->delete()) {
                return true;
            }

            return false;
        }
    }

    public function actionDelete($id) {
        $model = PaymentSystem::findOne($id);
        $model->delete();
        $this->redirect(['admin']);
    }

    protected function findModel($id) {
        if (($model = PaymentSystem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
