<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\models\User;
use app\models\Page;
use app\models\RealEstate;
use app\models\UserLine;
use yii\web\UploadedFile;
use app\models\Ticket;
use app\models\TicketComment;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use app\models\Payment;
use app\models\PaymentSystemField;
use app\models\PaymentComment;
use app\models\Booking;
use app\models\Reviews;
use app\models\BookingForm;
use app\models\Service;
use app\models\ServiceOrder;
use app\models\Accounting;

/**
 * ProfileController implements the CRUD actions.
 *///
class ProfileController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'except' => ['profile'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        /** @var User $model */
        $model = User::findOne(Yii::$app->user->id);

        $oldAvatar = $model->avatar;
        if ($model->load(Yii::$app->request->post())) {
            $model->avatar = UploadedFile::getInstance($model, 'avatar');
            if (!$model->avatar) {
                $model->avatar = $oldAvatar;
            }
            $model->upload();
            if ($model->save()) {
                return $this->refresh();
            }
        }
        //var_dump($model->errors);exit;

        return $this->render('index', [
                    'model' => $model,
        ]);
    }

    public function actionSubscriptions($action = false, $value = 0) {
        //Подписки
        $model = User::findOne(Yii::$app->user->id);

        if ($action) {
            $model->news_subscription = ($value) ? 1 : 0;
            $model->save();
        }

        return $this->render('subscriptions', [
                    'model' => $model
        ]);
    }

    public function actionReviews() {
        //Отзывы
        $dataProvider = new ActiveDataProvider([
            'query' => Reviews::find()
                    ->where('created_by = :user_id', [':user_id' => Yii::$app->user->id])
                    ->orderBy('created_at desc'),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        $user = User::findOne(Yii::$app->user->id);
        $model = new Reviews();
        $model->rating = Reviews::RATING_DEFAULT;
        $reviewsItems = $user->getBookedRealEstatesAsList();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //$todo Отправлять имейл оповещения админам о новом отзыве
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Отзыв будет добавлен после модерации'));
        }

        return $this->render('reviews', [
                    'user' => $user,
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                    'reviewsItems' => $reviewsItems
        ]);
    }

    public function actionReviewUpdate($id) {
        $model = Reviews::find()
                ->where('id = :id AND created_by = :user_id', [':id' => $id, 'user_id' => Yii::$app->user->id])
                ->one();
        if (!$model) {
            throw new NotFoundHttpException(\Yii::t('core', 'Данная страница не существует'));
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Отзыв будет добавлен после модерации'));
        }

        return $this->render('review_update', [
                    'model' => $model
        ]);
    }

    public function actionPromotions() {
        //Акции и скидки
        return $this->render('promotions', [
        ]);
    }

    public function actionSelectedObjects() {
        $model = Booking::find()
                ->andWhere(['created_by' => Yii::$app->user->identity->id])
                ->andWhere(['<>', 'status', Booking::STATUS_NOT_PAYED])
                ->andWhere(['<>', 'status', Booking::STATUS_CANCELED])
                ->all();

        //Выбранные объекты
        return $this->render('selected_objects', [
                    'model' => $model,
        ]);
    }

    public function actionMyObjects() {
        $model = RealEstate::findAll(['user_id' => Yii::$app->user->identity->id]);

        //Переданные объекты в управление
        return $this->render('delegated_objects', [
            'model' => $model,
        ]);
    }

    public function actionObjectPayment() {
        $model = Booking::find()
                ->andWhere(['created_by' => Yii::$app->user->identity->id])
                ->andWhere(['status' => Booking::STATUS_NOT_PAYED])
                ->andWhere(['>=', 'start_date', time()])
                ->all();

        //Объекты сохраненные для оплаты
        return $this->render('saved_objects', [
                    'model' => $model,
        ]);
    }

    public function actionViewedObjects() {
        //Просмотренные объекты
        $viewedObjects = Yii::$app->user->identity->getViewedObjects()->all();
        $userLineObjects = Yii::$app->user->identity->getUserLineObjects()->all();
        $userLineRegions = Yii::$app->user->identity->getUserLineRegions()->all();

        //Просмотренные объекты
        return $this->render('viewed_objects', [
                    'viewedObjects' => $viewedObjects,
                    'userLineObjects' => $userLineObjects,
                    'userLineRegions' => $userLineRegions,
        ]);
    }

    public function actionTechFeedback() {
        //Написать в тех. поддержку
        $model = new Ticket();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //$todo Отправлять имейл оповещения админам о новом тикете
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Сообщение успешно отправлено'));
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Ticket::find()
                    ->where('created_by = :user_id', [':user_id' => Yii::$app->user->id])
                    ->orderBy('created_at desc'),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('tech_feedback', [
                    'model' => $model,
                    'dataProvider' => $dataProvider
        ]);
    }

    public function actionTicketDelete($id) {
        //Удаление тикета
        $model = Ticket::find()
                ->where('id = :id AND created_by = :user_id', [':id' => $id, 'user_id' => Yii::$app->user->id])
                ->one();

        if ($model) {
            $model->delete();
            return $this->redirect(['tech-feedback']);
        } else {
            throw new NotFoundHttpException(\Yii::t('core', 'Данная страница не существует'));
        }
    }

    public function actionTicketView($id) {
        //Просмотр тикета
        $model = Ticket::find()
                ->where('id = :id AND created_by = :user_id', [':id' => $id, 'user_id' => Yii::$app->user->id])
                ->one();

        if ($model) {
            $commentModel = new TicketComment();
            $commentModel->ticket_id = $id;

            if ($commentModel->load(Yii::$app->request->post()) && $commentModel->save()) {
                //@todo Отправлять имейл оповещение админам о комменте к тикету
                \Yii::$app->session->setFlash('success', \Yii::t('app', 'Комментарий успешно добавлен'));
            }

            $dataProvider = new ActiveDataProvider([
                'query' => TicketComment::find()
                        ->where('ticket_id = :id', [':id' => $id])
                        ->orderBy('id desc'),
                'pagination' => [
                    'pageSize' => 5,
                ],
            ]);

            TicketComment::updateAll(['viewed_user' => TicketComment::VIEWED_TYPE_VIEWED], ['ticket_id' => $id]);
            return $this->render('ticket_view', [
                        'model' => $model,
                        'dataProvider' => $dataProvider,
                        'commentModel' => $commentModel
            ]);
        } else {
            throw new NotFoundHttpException(\Yii::t('core', 'Данная страница не существует'));
        }
    }

    public function actionTicketCommentDelete($id) {
        //Удаление комментария к тикету
        $model = TicketComment::find()
                ->where('id = :id AND created_by = :user_id', [':id' => $id, 'user_id' => Yii::$app->user->id])
                ->one();

        if ($model) {
            $model->delete();
            $this->redirect(Yii::$app->request->referrer);
        } else {
            throw new NotFoundHttpException(\Yii::t('core', 'Данная страница не существует'));
        }
    }

    public function actionTerms() {
        //Условия и конфиденциальность
        $page = Page::findOne(['slug' => 'site_terms', 'language_id' => Yii::$app->languageInfo->id]);
        return $this->render('terms', [
                    'page' => $page
        ]);
    }

    public function actionBalance($type) {
        if (!in_array($type, [Accounting::OPERATION_MINUS_BALANCE, Accounting::OPERATION_PLUS_BALANCE])) {
            throw new NotFoundHttpException(\Yii::t('core', 'Данная страница не существует'));
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Accounting::find()
                    ->where([
                        'user_id' => Yii::$app->user->id,
                        'operation' => $type,
                    ])
                    ->orderBy('created_at desc'),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('balance', [
                    'type' => $type,
                    'dataProvider' => $dataProvider
        ]);
    }

    public function actionMyBalance() {
        //Мой баланс
        $user = User::findOne(Yii::$app->user->id);
        $model = new Payment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $user->minusBalance($model->amount);
            $user->plusFozenBalance($model->amount);
            $this->redirect(['profile/confirm-payment', 'id' => $model->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Payment::find()
                    ->where('created_by = :user_id', [':user_id' => Yii::$app->user->id])
                    ->orderBy('created_at desc'),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('my_balance', [
                    'user' => $user,
                    'model' => $model,
                    'dataProvider' => $dataProvider
        ]);
    }

    public function actionConfirmPayment($id) {
        //Подтверждение запроса на выплату
        $model = Payment::find()->where(['id' => $id, 'created_by' => Yii::$app->user->id, 'status_id' => Payment::STATUS_NO_REQUISITES])->one();

        if (!$model) {
            throw new NotFoundHttpException(\Yii::t('core', 'Данная страница не существует'));
        }

        if (Yii::$app->request->post()) {
            if (Yii::$app->request->post('field')) {
                $model->requisites = "<p class='bold'>Реквизиты для выплаты</p>";
                foreach (Yii::$app->request->post('field') as $key => $field) {
                    $fieldTitle = PaymentSystemField::getFieldTitle($key);
                    $model->requisites .= "<p>$fieldTitle: $field</p>";
                }
            }

            $model->status_id = Payment::STATUS_PENDING;

            if ($model->save()) {
                \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запрос на выплату успешно подтвержден'));
                $this->redirect(['profile/my-balance']);
            }
        }

        $fields = PaymentSystemField::findAll(['payment_system_id' => $id]);
        return $this->render('confirm_payment', [
                    'model' => $model,
                    'fields' => $fields
        ]);
    }

    public function actionDeletePayment($id) {
        $model = Payment::find()->where(['id' => $id, 'created_by' => Yii::$app->user->id, 'status_id' => Payment::STATUS_NO_REQUISITES])->one();

        if (!$model) {
            throw new NotFoundHttpException(\Yii::t('core', 'Данная страница не существует'));
        }

        $user = User::findOne(Yii::$app->user->id);
        $amount = $model->amount;

        if ($model->delete()) {
            $user->plusBalance($amount);
            $user->minusFrozenBalance($amount);
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запрос на выплату успешно удален'));
            $this->redirect(['profile/my-balance']);
        }
    }

    public function actionPaymentView($id) {
        //Просмотр запроса на выплату
        $model = Payment::find()
                ->where('id = :id AND created_by = :user_id', [':id' => $id, 'user_id' => Yii::$app->user->id])
                ->one();

        if ($model) {
            $commentModel = new PaymentComment();
            $commentModel->payment_id = $id;

            if ($commentModel->load(Yii::$app->request->post()) && $commentModel->save()) {
                //@todo Отправлять имейл оповещение админам о комменте к выплате
                \Yii::$app->session->setFlash('success', \Yii::t('app', 'Комментарий успешно добавлен'));
            }

            $dataProvider = new ActiveDataProvider([
                'query' => PaymentComment::find()
                        ->where('payment_id = :id', [':id' => $id])
                        ->orderBy('id desc'),
                'pagination' => [
                    'pageSize' => 5,
                ],
            ]);

            PaymentComment::updateAll(['viewed_user' => PaymentComment::VIEWED_TYPE_VIEWED], ['payment_id' => $id]);
            return $this->render('payment_view', [
                        'model' => $model,
                        'dataProvider' => $dataProvider,
                        'commentModel' => $commentModel
            ]);
        } else {
            throw new NotFoundHttpException(\Yii::t('core', 'Данная страница не существует'));
        }
    }

    public function actionPaymentCommentDelete($id) {
        //Удаление комментария к запросу на выплату
        $model = PaymentComment::find()
                ->where('id = :id AND created_by = :user_id', [':id' => $id, 'user_id' => Yii::$app->user->id])
                ->one();

        if ($model) {
            $model->delete();
            $this->redirect(Yii::$app->request->referrer);
        } else {
            throw new NotFoundHttpException(\Yii::t('core', 'Данная страница не существует'));
        }
    }

    public function actionAdvanceServices() {
        //Дополнительные услуги
        return $this->render('advance_services', [
        ]);
    }

    public function actionBook($scenario) {
        $model = new BookingForm(['scenario' => $scenario]);

        switch ($model->scenario) {
            case BookingForm::SCENARIO_STEP_2: {
                    if ($model->load(Yii::$app->request->post())) {
                        $realEstate = RealEstate::find(['id' => $model->realEstateId])->one();

                        if ($realEstate->isAvailableForBooking($model->startDateTimestamp, $model->endDateTimestamp)) {
                            //Расчет стоимости аренды
                            $startDate = new \DateTime($model->startDate);
                            $endDate = new \DateTime($model->endDate);

                            $days = $startDate->diff($endDate)->format('%a');
                            $days += 1;

                            $rentMonths = floor($days / 30);
                            $rentDays = $days % 30;

                            $model->price = $realEstate->monthly_price * $rentMonths + $realEstate->daily_price * $rentDays;

                            //Дополнительные услуги
                            $services = Service::find()
                                    ->with('servicePrices')
                                    ->all();

                            return $this->render('book', [
                                        'model' => $model,
                                        'services' => $services,
                            ]);
                        } else {
                            Yii::$app->session->setFlash('booking_failed', Yii::t('app', 'Бронирование на данную дату недоступно.'));
                            $this->redirect(['site/object', 'id' => $model->realEstateId]);
                        }
                    }
                    break;
                }
            case BookingForm::SCENARIO_STEP_3: {
                    if ($model->load(Yii::$app->request->post())) {
                        //Нажали "Сохранить для оплаты"
                        if (isset($model->submitSave)) {
                            //Добавляем информацию о бронировании
                            $booking = new Booking();
                            $booking->status = Booking::STATUS_NOT_PAYED;
                            $booking->real_estate_id = $model->realEstateId;
                            $booking->source_type = Booking::SOURCE_TYPE_SITE;
                            $booking->user_phone = $model->phone;
                            $booking->start_date = $model->startDateTimestamp;
                            $booking->end_date = $model->endDateTimestamp;
                            $booking->rent_price = $model->price;
                            $booking->comment = $model->comment;
                            $booking->created_at = time();
                            $booking->created_by = Yii::$app->user->identity->id;
                            $booking->save();

                            //Подсчитываем стоимость дополнительных услуг
                            $servicesPrice = 0;
                            foreach ($model->services as $serviceId => $serviceInfo) {
                                if (isset($serviceInfo['selected'])) {
                                    //Суммируем стоимость услуг
                                    $service = Service::findOne($serviceId);
                                    $servicesPrice += $service->getServicePrice($serviceInfo['people_count']);

                                    //Добавляем выбранные услуги в базу данных
                                    $serviceOrder = new ServiceOrder();
                                    $serviceOrder->booking_id = $booking->id;
                                    $serviceOrder->service_id = $service->id;
                                    $serviceOrder->people_count = intval($serviceInfo['people_count']);
                                    $serviceOrder->price = $service->getServicePrice($serviceInfo['people_count']);
                                    $serviceOrder->save();
                                }
                            }

                            Yii::$app->session->setFlash('booking_success', Yii::t('app', 'Объект забронирован'));
                            $this->redirect(['profile/object-payment']);
                        }
                        //Нажали "Оплатить"
                        if (isset($model->submitPay)) {
                            //Добавляем информацию о бронировании
                            $booking = new Booking();
                            $booking->status = Booking::STATUS_PAYED;
                            $booking->real_estate_id = $model->realEstateId;
                            $booking->source_type = Booking::SOURCE_TYPE_SITE;
                            $booking->user_phone = $model->phone;
                            $booking->start_date = $model->startDateTimestamp;
                            $booking->end_date = $model->endDateTimestamp;
                            $booking->rent_price = $model->price;
                            $booking->comment = $model->comment;
                            $booking->created_at = time();
                            $booking->created_by = Yii::$app->user->identity->id;
                            $booking->save();

                            //Подсчитываем стоимость дополнительных услуг
                            $servicesPrice = 0;
                            foreach ($model->services as $serviceId => $serviceInfo) {
                                if (isset($serviceInfo['selected'])) {
                                    //Суммируем стоимость услуг
                                    $service = Service::findOne($serviceId);
                                    $servicesPrice += $service->getServicePrice($serviceInfo['people_count']);

                                    //Добавляем выбранные услуги в базу данных
                                    $serviceOrder = new ServiceOrder();
                                    $serviceOrder->booking_id = $booking->id;
                                    $serviceOrder->service_id = $service->id;
                                    $serviceOrder->people_count = intval($serviceInfo['people_count']);
                                    $serviceOrder->price = $service->getServicePrice($serviceInfo['people_count']);
                                    $serviceOrder->save();
                                }
                            }

                            Yii::$app->session->setFlash('booking_success', Yii::t('app', 'Объект забронирован'));
                            $this->redirect(['profile/selected-objects']);
                        }
                        //Нажали "Забронировать"
                        if (isset($model->submitBook)) {
                            //Добавляем информацию о бронировании
                            $booking = new Booking();
                            $booking->status = Booking::STATUS_BOOKED;
                            $booking->real_estate_id = $model->realEstateId;
                            $booking->source_type = Booking::SOURCE_TYPE_SITE;
                            $booking->user_phone = $model->phone;
                            $booking->start_date = $model->startDateTimestamp;
                            $booking->end_date = $model->endDateTimestamp;
                            $booking->rent_price = $model->price;
                            $booking->comment = $model->comment;
                            $booking->created_at = time();
                            $booking->created_by = Yii::$app->user->identity->id;
                            $booking->save();

                            //Подсчитываем стоимость дополнительных услуг
                            $servicesPrice = 0;
                            foreach ($model->services as $serviceId => $serviceInfo) {
                                if (isset($serviceInfo['selected'])) {
                                    //Суммируем стоимость услуг
                                    $service = Service::findOne($serviceId);
                                    $servicesPrice += $service->getServicePrice($serviceInfo['people_count']);

                                    //Добавляем выбранные услуги в базу данных
                                    $serviceOrder = new ServiceOrder();
                                    $serviceOrder->booking_id = $booking->id;
                                    $serviceOrder->service_id = $service->id;
                                    $serviceOrder->people_count = intval($serviceInfo['people_count']);
                                    $serviceOrder->price = $service->getServicePrice($serviceInfo['people_count']);
                                    $serviceOrder->save();
                                }
                            }

                            Yii::$app->session->setFlash('booking_success', Yii::t('app', 'Объект забронирован'));
                            $this->redirect(['profile/selected-objects']);
                        }
                    }
                }
        }
    }

}
