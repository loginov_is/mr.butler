<?php

namespace app\controllers;

use \Yii;
use app\models\RealEstatePhoto;
use yii\filters\VerbFilter;

class RealEstatePhotoController extends \yii\web\Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    public function actionDelete($id) {
        $model = RealEstatePhoto::findOne($id);
        if ($model && $model->delete()) {
            if (file_exists(Yii::getAlias('@webroot' . $model->src))) {
                $model->unsetImage(Yii::getAlias('@webroot' . $model->src));
            }
        }

        $this->redirect(['real-estate/update/', 'id' => $model->real_estate_id]);
    }

    public function actionSetMain($id) {
        $model = RealEstatePhoto::findOne($id);
        if ($model) {
            $tmpModel = RealEstatePhoto::find()->where(['main' => 1])->one();

            if ($tmpModel) {
                $tmpModel->main = 0;
                $tmpModel->save();
            }

            $model->main = 1;
            $model->save();
        }

        $this->redirect(['real-estate/update/', 'id' => $model->real_estate_id]);
    }

}
