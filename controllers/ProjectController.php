<?php

namespace app\controllers;

use Yii;
use app\models\Project;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\helpers\ContentHelper;
use yii\helpers\ArrayHelper;
use app\models\Region;
use yii\helpers\Html;
use app\models\Translation;
use yii\web\UploadedFile;
use app\models\ProjectSearch;
use yii\filters\AccessControl;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['json-list'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionJsonList() {
        $term = Yii::$app->request->get('term');
        $query = (new Query())->select(['name as label', 'id as value'])->from(Project::tableName())->where(['like', 'name', $term])->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $query;
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionAdmin() {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Project();
        $this->view->registerJs('Main.mapInit("' . ContentHelper::MAP_COORDS_DEFAULT . '", "Title")');
        $model->map = ContentHelper::MAP_COORDS_DEFAULT;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->images = UploadedFile::getInstances($model, 'images');

            if (!$model->upload()) {
                /** @todo Show error? */
            }
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $mapCoords = ($model->map) ? $model->map : ContentHelper::MAP_COORDS_DEFAULT;
        $this->view->registerJs('Main.mapInit("' . $mapCoords . '", "Title")');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->images = UploadedFile::getInstances($model, 'images');

            if (!$model->upload()) {
                /** @todo Show error? */
            }
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = Project::findOne($id);
        Translation::deleteAll(['model' => $model::className(), 'model_id' => $id]);
        $model->delete();
        $this->redirect(['admin']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAjaxRegion() {
        $model = new Project();
        $model->load(Yii::$app->request->post());
        $regions = Region::find()->where(['city_id' => $model->city_id])->all();

        $options = [
            'prompt' => '- ' . $model->getAttributeLabel('region_id') . ' -',
        ];
        return Html::renderSelectOptions('', ArrayHelper::map($regions, 'id', function ($model) {
                            return Translation::t($model, 'name');
                        }), $options);
    }

}
