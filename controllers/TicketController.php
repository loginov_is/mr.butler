<?php

namespace app\controllers;

use Yii;
use app\models\Ticket;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\TicketSearch;
use app\models\TicketComment;

class TicketController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAdmin() {
        $searchModel = new TicketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        $model = $this->findModel($id);
        $model->viewed_admin = Ticket::VIEWED_ADMIN_TRUE;
        $model->save();
        $commentModel = new TicketComment();
        $commentModel->ticket_id = $id;
        $commentModel->viewed_user = 0;

        if ($commentModel->load(Yii::$app->request->post()) && $commentModel->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Комментарий успешно добавлен'));
        }

        $dataProvider = new ActiveDataProvider([
            'query' => TicketComment::find()
                    ->where('ticket_id = :id', [':id' => $id])
                    ->orderBy('id desc'),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('view', [
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                    'commentModel' => $commentModel
        ]);
    }

    public function actionDelete($id) {
        $model = Ticket::findOne($id);
        $model->delete();
        $this->redirect(['admin']);
    }

    protected function findModel($id) {
        if (($model = Ticket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionTicketCommentDelete($id) {
        $model = TicketComment::findOne($id);

        if ($model) {
            $model->delete();
            $this->redirect(Yii::$app->request->referrer);
        } else {
            throw new NotFoundHttpException(\Yii::t('core', 'Данная страница не существует'));
        }
    }

}
