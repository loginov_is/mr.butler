<?php

namespace app\controllers;

use Yii;
use app\models\ImageAlbum;
use app\models\ImageToAlbum;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class AlbumController extends \yii\web\Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAdmin() {
        $dataProvider = new ActiveDataProvider([
            'query' => ImageAlbum::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('admin', [
                    'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreate() {
        $model = new ImageAlbum();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }

        return $this->render('create', [
                    'model' => $model
        ]);
    }

    public function actionUpdate($id) {
        $model = ImageAlbum::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        $model = ImageAlbum::findOne($id);
        $model->delete();
        $this->redirect(['album/admin']);
    }

    public function actionSetImageOrderDown($imageId, $albumId) {
        $relation = ImageToAlbum::findOne(['image_id' => $imageId, 'album_id' => $albumId]);

        if ($relation) {
            $currentOrder = $relation->order;
            $nextRelation = ImageToAlbum::findOne(['image_id' => $imageId, 'album_id' => $albumId, 'order' => $currentOrder++]);

            if ($nextRelation) {
                ImageToAlbum::updateAll(["order" => $nextRelation->order], "album_id = $albumId AND image_id = $imageId");
                ImageToAlbum::updateAll(["order" => $currentOrder], "album_id = $albumId AND image_id = $nextRelation->image_id");
            }
        }

        return $this->redirect(['update', 'id' => $albumId]);
    }

    public function actionSetImageOrderUp($imageId, $albumId) {
        $relation = ImageToAlbum::findOne(['image_id' => $imageId, 'album_id' => $albumId]);

        if ($relation) {
            $currentOrder = $relation->order;
            $nextRelation = ImageToAlbum::findOne(['image_id' => $imageId, 'album_id' => $albumId, 'order' => $currentOrder--]);

            if ($nextRelation) {
                ImageToAlbum::updateAll(["order" => $nextRelation->order], "album_id = $albumId AND image_id = $imageId");
                ImageToAlbum::updateAll(["order" => $currentOrder], "album_id = $albumId AND image_id = $nextRelation->image_id");
            }
        }

        return $this->redirect(['update', 'id' => $albumId]);
    }

}
