<?php

namespace app\controllers;

use Yii;
use app\models\Contact;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\helpers\ContentHelper;

/**
 * ContactController implements the CRUD actions for Contact model.
 */
class ContactController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['view'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contact models.
     * @return mixed
     */
    public function actionAdmin() {
        $dataProvider = new ActiveDataProvider([
            'query' => Contact::find(),
        ]);

        return $this->render('admin', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contact model.
     * @return mixed
     */
    public function actionView() {
        $this->layout = 'main';
        $model = Contact::find()->joinWith('language', true, 'RIGHT JOIN')->where(['{{%language}}.code' => Yii::$app->language])->one();
        $mapCoords = ($model->map) ? $model->map : 0;
        $this->view->registerJs('Main.mapInit("' . $mapCoords . '", "Title")');
        return $this->render('view', [
                    'model' => $model
        ]);
    }

    /**
     * Creates a new Contact model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Contact();
        $this->view->registerJs('Main.mapInit("' . ContentHelper::MAP_COORDS_DEFAULT . '", "Title")');
        $model->map = ContentHelper::MAP_COORDS_DEFAULT;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Contact model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $mapCoords = ($model->map) ? $model->map : ContentHelper::MAP_COORDS_DEFAULT;
        $this->view->registerJs('Main.mapInit("' . $mapCoords . '", "Title")');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Contact model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['admin']);
    }

    /**
     * Finds the Contact model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contact the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Contact::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
