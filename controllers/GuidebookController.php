<?php

namespace app\controllers;

use app\models\GuidebookForm;
use app\models\ImageAlbumToRegion;
use app\models\RealEstate;
use app\models\Region;
use app\models\Project;
use app\models\RegionToRegion;
use app\models\Translation;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\Page;

class GuidebookController extends Controller {

    public function actionIndex() {
        $regions = Region::find()->all();
        $page = Page::findOne(['slug' => 'guidebook_index', 'language_id' => Yii::$app->languageInfo->id]);
        return $this->render('index', [
                    'regions' => $regions,
                    'page' => $page
        ]);
    }

    public function actionSearch() {
        $form = new GuidebookForm();
        $regionModel = null;
        $realEstateModel = [];
        $similiarRegions = [];
        $galleryImages = [];
        $sightImages = [];

        if ($form->load(Yii::$app->request->get())) {
            //Интервал стоимости аренды
            $form->priceMin = strtok($form->price, ',');
            $form->priceMax = strtok(',');

            //Район
            $regionModel = $this->findModel($form->regionId);
            $sightItems = $this->sightsToGalleryArray($regionModel);

            if ($regionModel->map) {
                $this->view->registerJs('Main.mapInit("' . $regionModel->map . '", "' . $regionModel->name . '")');
            }

            //Недвижимость в районе
//            $realEstateModel = Project::find(['city_id' => $form->cityId, 'region_id' => $form->regionId])
//                    ->one()
//                    ->getRealEstates()
//                    ->andFilterWhere(['type_id' => $form->housingType])
//                    ->andFilterWhere(['>=', 'residents', $form->residents])
//                    ->andFilterWhere(['between', $form->priceType, $form->priceMin, $form->priceMax])
//                    ->all();

            $realEstateModel = Region::findOne($form->regionId)
                ->getRealEstates()
                ->andFilterWhere(['type_id' => $form->housingType])
                ->andFilterWhere(['>=', 'residents', $form->residents])
                ->andFilterWhere(['between', $form->priceType, $form->priceMin, $form->priceMax])
                ->all();

            //Похожие районы
            if ($form->sameRegions)
                $similiarRegions = $regionModel->getChildren()->all();

            //Галереи
            $galleryImages = $regionModel->getImages(ImageAlbumToRegion::TYPE_ID_PHOTOALBUMS);
            if ($form->sights)
                $sightImages = $regionModel->getImages(ImageAlbumToRegion::TYPE_ID_SIGHT);
        }

        /** @todo render not found */
        return $this->render('search', [
                    'regionModel' => $regionModel,
                    'realEstateModel' => $realEstateModel,
                    'similiarRegions' => $similiarRegions,
                    'galleryImages' => $galleryImages,
                    'sightImages' => $sightImages,
        ]);
    }

    public function actionAjaxRegion() {
        $model = new GuidebookForm();
        $model->load(Yii::$app->request->post());
        $regions = Region::find()->where(['city_id' => $model->cityId])->all();

        $options = [
            'prompt' => '- ' . $model->getAttributeLabel('regionId') . ' -',
        ];
        return Html::renderSelectOptions('', ArrayHelper::map($regions, 'id', function ($model) {
                            return Translation::t($model, 'name');
                        }), $options);
    }

    public function sightsToGalleryArray($model) {
        $items = [];

        foreach ($model->getImageAlbums(ImageAlbumToRegion::TYPE_ID_SIGHT) as $album) {
            foreach ($album->albumImages as $image) {
                $items[] = [
                    'url' => '@web/upload/' . $image->image
                ];
            }
        }

        return $items;
    }

    protected function findModel($id) {
        if (($model = Region::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
