<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Service;
use app\models\ServicePrice;

/**
 * ServiceController implements the CRUD actions for Service model.
 */
class ServiceController extends Controller
{
    public $layout = 'admin';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Service models.
     * @return mixed
     */
    public function actionAdmin()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Service::find(),
        ]);

        return $this->render('admin', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Service model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => Service::findOne($id),
        ]);
    }

    /**
     * Creates a new Service model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Service();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Service model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = Service::findOne($id);
        $servicePriceDataProvider = new ActiveDataProvider([
            'query' => ServicePrice::find()
                ->andWhere(['service_id' => $model->id])
                ->andWhere(['price_type_id' => ServicePrice::TYPE_PER_GROUP]),
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->price_type_id == ServicePrice::TYPE_PER_MAN) {
                if($servicePrice = ServicePrice::findOne(['service_id' => $model->id, 'price_type_id' => ServicePrice::TYPE_PER_MAN])) {
                    $servicePrice->price = $model->pricePerMan;
                } else {
                    $servicePrice = new ServicePrice();
                    $servicePrice->service_id = $model->id;
                    $servicePrice->price = $model->pricePerMan;
                    $servicePrice->price_type_id = ServicePrice::TYPE_PER_MAN;
                }
                $servicePrice->save();
            }
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            if($model->price_type_id == ServicePrice::TYPE_PER_MAN) {
                $model->pricePerMan = $model->getServicePrice();
            }
            return $this->render('update', [
                'model' => $model,
                'servicePriceDataProvider' => $servicePriceDataProvider,
            ]);
        }
    }

    /**
     * Deletes an existing Service model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        ServicePrice::deleteAll(['service_id' => $id]);
        Service::findOne($id)->delete();

        return $this->redirect(['admin']);
    }

    public function actionUpdateServicePrice() {
        $data = Yii::$app->request->post();
        if($data) {
            $servicePrice = ServicePrice::findOne($data['id']);
            if($servicePrice) {
                $servicePrice->load($data);
                $servicePrice->save();
            }
           return $this->redirect(['service/update', 'id' => $servicePrice->service_id]);
        }
        return $this->redirect(['service/admin']);
    }

    public function actionCreateServicePrice() {
        $data = Yii::$app->request->post();
        if($data) {
            $servicePrice = new ServicePrice();
            $servicePrice->load($data);
            $servicePrice->price_type_id = ServicePrice::TYPE_PER_GROUP;
            $servicePrice->save();

            $service = Service::findOne($servicePrice->service_id);
            $service->price_type_id = ServicePrice::TYPE_PER_GROUP;
            $service->save();

            return $this->redirect(['service/update', 'id' => $servicePrice->service_id]);
        }
        return $this->redirect(['service/admin']);
    }

    public function actionDeleteServicePrice($id) {
        $servicePrice = ServicePrice::findOne($id);
        if($servicePrice) {
            $serviceId = $servicePrice->service_id;
            $servicePrice->delete();
            return $this->redirect(['service/update', 'id' => $serviceId]);
        }
        return $this->redirect(['service/admin']);
    }

    public function actionAjaxGetServicePrice($id) {
        $servicePrice = ServicePrice::findOne($id);
        if($servicePrice) {
            $data = json_encode($servicePrice->toArray());
            echo $data;
        }
    }
}
