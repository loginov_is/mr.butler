<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\models\RealEstate;
use app\models\RealEstateSearch;
use app\models\RealEstatePhoto;
use app\models\UserLine;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use app\models\Translation;
use app\helpers\ContentHelper;
use app\models\Page;

class RealEstateController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['add'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAdd() {
        $page = Page::findOne(['slug' => 'real-estate_add', 'language_id' => Yii::$app->languageInfo->id]);
        $model = new RealEstate(['scenario' => RealEstate::SCENARIO_USER_CREATE]);
        $model->user_id = (Yii::$app->user->id) ? Yii::$app->user->id : 0;
        $model->status_id = ContentHelper::ITEM_STATUS_NOT_ACTIVE;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->images = UploadedFile::getInstances($model, 'images');

            if (!$model->upload()) {
                /** @todo Show error? */
            }

            \Yii::$app->session->setFlash('success', 'Заявка успешно отправлена. Ваша недвижимость будет добавлена после модерации администратором.');
        }

        return $this->render('add', [
                    'model' => $model,
                    'page' => $page
        ]);
    }

    /**
     * Lists all RealEstate models.
     * @return mixed
     */
    public function actionAdmin() {
        $this->layout = 'admin';

        $searchModel = new RealEstateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $this->layout = 'admin';
        $model = new RealEstate(['scenario' => RealEstate::SCENARIO_ADMIN_CREATE]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->images = UploadedFile::getInstances($model, 'images');

            if (!$model->upload()) {
                /** @todo Show error? */
            }
            return $this->redirect(['update', 'id' => $model->id]);
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing RealEstate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $this->layout = 'admin';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Translation::loadTranslations($model);
            $model->images = UploadedFile::getInstances($model, 'images');

            if (!$model->upload()) {
                /** @todo Show error? */
            }

            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates real estate images order
     */
    public function actionPhotoSortSave() {
        foreach ($_POST['photo'] as $photo) {
            $model = RealEstatePhoto::findOne($photo['id']);
            $model->index = $photo['index'];
            $model->save();
        }
    }

    /**
     * Finds the RealEstate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RealEstate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = RealEstate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDelete($id) {
        $model = RealEstate::findOne($id);
        Translation::deleteAll(['model' => $model::className(), 'model_id' => $id]);
        $model->delete();
        UserLine::deleteAll(['type_id' => [UserLine::TYPE_ID_OBJECT_VIEWED, UserLine::TYPE_ID_OBJECT_ADDED_TO_USERLINE], 'model_id' => $id]);
        $this->redirect(['admin']);
    }

}
