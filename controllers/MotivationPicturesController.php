<?php

namespace app\controllers;

use app\models\Translation;
use Yii;
use app\models\MotivationPicture;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * MotivationPictures implements the CRUD actions for Lookup model.
 */
class MotivationPicturesController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Lookup models.
     * @return mixed
     */
    public function actionAdmin() {
        $dataProvider = new ActiveDataProvider([
            'query' => MotivationPicture::findBySql("SELECT * from {{%motivation_picture}} t order by t.type asc, t.order asc"),
            'pagination' => [
                'pageSize' => 999999999,
            ],
        ]);

        return $this->render('admin', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Lookup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new MotivationPicture();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->uploadedFile = UploadedFile::getInstance($model, 'image');
            $model->image = $model->uploadedFile->baseName . '-' . time() . '.' . $model->uploadedFile->extension;

            if ($model->save()) {
                $dir = Yii::getAlias('@webroot/upload/motivation_pictures') . '/' . $model->image;
                $model->uploadedFile->saveAs($dir);
                \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
            }
        }

        return $this->render('create', [
                    'model' => $model
        ]);
    }

    /**
     * Updates an existing Lookup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            Translation::loadTranslations($model);
            $model->load(Yii::$app->request->post());

            if ($model->save()) {
                \Yii::$app->session->setFlash('success', \Yii::t('app', 'Запись успешно сохранена'));
            }
        }

        return $this->render('update', [
                    'model' => $model
        ]);
    }

    /**
     * Deletes an existing Lookup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = MotivationPicture::findOne($id);
        Translation::deleteAll(['model' => $model::className(), 'model_id' => $id]);
        $model->delete();
        $this->redirect(['admin']);
    }

    /**
     * Finds the Lookup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lookup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MotivationPicture::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSetItemOrderDown($id, $type) {
        $currentItem = MotivationPicture::findOne($id);
        $currentOrder = $currentItem->order;
        $switchOrder = MotivationPicture::findBySql("SELECT m.id, m.order from {{%motivation_picture}} m WHERE m.type = :type AND m.order > $currentOrder ORDER BY m.order asc", [
                    ':type' => $type
                ])->one();

        if ($switchOrder) {
            $currentItem->order = $switchOrder->order;
            $currentItem->save();
            $switchItem = MotivationPicture::findOne($switchOrder->id);
            $switchItem->order = $currentOrder;
            $switchItem->save();
        }

        $this->redirect(Yii::$app->request->referrer);
    }

    public function actionSetItemOrderUp($id, $type) {
        $currentItem = MotivationPicture::findOne($id);
        $currentOrder = $currentItem->order;
        $switchOrder = MotivationPicture::findBySql("SELECT m.id, m.order from {{%motivation_picture}} m WHERE m.type = :type AND m.order < $currentOrder ORDER BY m.order desc", [
                    ':type' => $type
                ])->one();

        if ($switchOrder) {
            $currentItem->order = $switchOrder->order;
            $currentItem->save();
            $switchItem = MotivationPicture::findOne($switchOrder->id);
            $switchItem->order = $currentOrder;
            $switchItem->save();
        }

        $this->redirect(Yii::$app->request->referrer);
    }

}
