<?php

namespace app\controllers;

use app\models\Auth;
use app\models\ContactForm;
use app\models\FeedbackForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\SignupForm;
use app\models\SearchForm;
use app\models\User;
use app\models\Region;
use app\models\RealEstate;
use app\models\Translation;
use app\models\UserLine;
use Yii;
use yii\authclient\ClientInterface;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Page;
use app\helpers\ContentHelper;
use yii\helpers\ArrayHelper;
use app\models\PressArticle;
use app\models\Team;
use app\models\Partner;
use app\models\PageAbout;
use app\components\Mailer;
use app\models\MotivationPicture;
use app\models\Reviews;
use app\helpers\SearchHelper;
use yii\web\NotFoundHttpException;

class SiteController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @param ClientInterface $client
     */
    public function onAuthSuccess($client) {
        $attributes = $client->getUserAttributes();

        /* @var $auth Auth */
        $auth = Auth::find()->where([
                    'source' => $client->getId(),
                    'source_id' => $attributes['id'],
                ])->one();

        if (Yii::$app->user->isGuest) {
            if ($auth) { // login
                $user = $auth->user;
                Yii::$app->user->login($user);
            } else { // signup
                if (isset($attributes['email']) && User::find()->where(['email' => $attributes['email']])->exists()) {
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('app', "Пользователь с таким же адресом электронной почты как и в {client} клиент уже существует, но не связан с ним. Войдите с помощью электронной почты что бы связать их.", ['client' => $client->getTitle()]),
                    ]);
                    Yii::$app->session->set('socialAuth', [
                        'source' => $client->getId(),
                        'attributes' => $attributes,
                    ]);
                    Yii::$app->getUser()->setReturnUrl(['/site/login']);
                } else {
                    /** @todo Вывести форму регистрации и связать аккаунты, заполнить в форме существующие поля */
                    //$password = Yii::$app->security->generateRandomString(6);

                    Yii::$app->session->set('socialAuth', [
                        'source' => $client->getId(),
                        'attributes' => $attributes,
                    ]);
                    Yii::$app->getUser()->setReturnUrl(['/site/signup']);
                    /* $user = new User([
                      'first_name' => $attributes['first_name'],
                      'last_name' => $attributes['last_name'],
                      'email' => $attributes['email'],
                      'password' => $password,
                      ]);
                      $user->generateAuthKey();
                      $user->generatePasswordResetToken();
                      $transaction = $user->getDb()->beginTransaction();
                      if ($user->save()) {
                      $auth = new Auth([
                      'user_id' => $user->id,
                      'source' => $client->getId(),
                      'source_id' => (string)$attributes['id'],
                      ]);
                      if ($auth->save()) {
                      $transaction->commit();
                      Yii::$app->user->login($user);
                      } else {
                      // @todo catch errors
                      print_r($auth->getErrors());
                      }
                      } else {
                      // @todo catch errors
                      print_r($user->getErrors());
                      } */
                }
            }
        } else { // user already logged in
            if (!$auth) { // add auth provider
                $auth = new Auth([
                    'user_id' => Yii::$app->user->id,
                    'source' => $client->getId(),
                    'source_id' => $attributes['id'],
                ]);
                $auth->save();
            }
        }
    }

    public function actionIndex() {
        $model = Page::findOne(['slug' => 'site_index', 'language_id' => Yii::$app->languageInfo->id]);
        return $this->render('index', [
                    'model' => $model
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $page = Page::findOne(['slug' => 'site_login', 'language_id' => Yii::$app->languageInfo->id]);
        $model = new LoginForm();

        if (($socialAuth = Yii::$app->session->get('socialAuth')) !== null) {
            $model->email = $socialAuth['attributes']['email'];
        }

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $user = $model->getUser();
            (new Auth([
        'user_id' => $user->id,
        'source' => $socialAuth['source'],
        'source_id' => (string) $socialAuth['attributes']['id'],
            ]))->save();

            return $this->goBack();
        } else {
            return $this->render('login', [
                        'model' => $model,
                        'page' => $page
            ]);
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionAbout() {
        $this->layout = 'about';
        $team = Team::find()->all();
        $pressArticles = PressArticle::find()->all();
        $partners = Partner::find()->all();
        $model = Page::findOne(['slug' => 'site_about', 'language_id' => Yii::$app->languageInfo->id]);
        $pageAbout = PageAbout::find()->one();
        return $this->render('about', [
                    'model' => $model,
                    'team' => $team,
                    'pressArticles' => $pressArticles,
                    'partners' => $partners,
                    'pageAbout' => $pageAbout
        ]);
    }

    public function actionFeedback() {
        $model = new FeedbackForm();
        if ($model->load(Yii::$app->request->post()) && $model->feedback(Yii::$app->params['feedbackEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        }
        return $this->render('feedback', [
                    'model' => $model,
        ]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup() {
        $model = new SignupForm();
        $page = Page::findOne(['slug' => 'site_signup', 'language_id' => Yii::$app->languageInfo->id]);

        if (($socialAuth = Yii::$app->session->get('socialAuth')) !== null) {
            $model->email = $socialAuth['attributes']['email'];
            $model->first_name = $socialAuth['attributes']['first_name'];
            $model->last_name = $socialAuth['attributes']['last_name'];
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                (new Auth([
            'user_id' => $user->id,
            'source' => $socialAuth['source'],
            'source_id' => (string) $socialAuth['attributes']['id'],
                ]))->save();

                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
                    'model' => $model,
                    'page' => $page
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
                    'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

    public function actionReviews() {
        //Отзывы
        $model = Page::findOne(['slug' => 'site_reviews', 'language_id' => Yii::$app->languageInfo->id]);
        $reviews = Reviews::find()->where(['status_id' => Reviews::STATUS_ACTIVE])->orderBy('created_at desc')->all();
        $videoReviews = Reviews::find()->where('status_id = :status_id AND youtube_id <> :empty', [
                    ':status_id' => Reviews::STATUS_ACTIVE,
                    ':empty' => ''
                ])->orderBy('created_at desc')->all();
        return $this->render('reviews', [
                    'reviews' => $reviews,
                    'videoReviews' => $videoReviews,
                    'model' => $model
        ]);
    }

    public function actionTerms() {
        $model = Page::findOne(['slug' => 'site_terms', 'language_id' => Yii::$app->languageInfo->id]);
        return $this->render('terms', [
                    'model' => $model
        ]);
    }

    public function actionPromotions() {
        $model = Page::findOne(['slug' => 'site_promotions', 'language_id' => Yii::$app->languageInfo->id]);
        return $this->render('promotions', [
                    'model' => $model
        ]);
    }

    public function actionPayments() {
        $model = Page::findOne(['slug' => 'site_payments', 'language_id' => Yii::$app->languageInfo->id]);
        return $this->render('payments', [
                    'model' => $model
        ]);
    }

    public function actionSearch() {
        $form = new SearchForm();
        $form->load(Yii::$app->request->get());
        $model = SearchHelper::getSearchData($form);
        $page = Page::findOne(['slug' => 'site_search', 'language_id' => Yii::$app->languageInfo->id]);

        return $this->render('search', [
                    'model' => $model,
                    'page' => $page
        ]);
    }

    public function actionAjaxGetRegions($cityId) {
        $regions = Region::find()
                ->where(['city_id' => $cityId])
                ->asArray()
                ->all();

        echo json_encode($regions);
    }

    /**
     * Displays a single RealEstate model.
     * @param integer $id
     * @return mixed
     */
    public function actionObject($id) {
        $model = RealEstate::find()->where(['status_id' => ContentHelper::ITEM_STATUS_ACTIVE, 'id' => $id])->one();

        if (!$model) {
            throw new NotFoundHttpException(\Yii::t('core', 'Данная страница не существует'));
        }

        $reviews = $model->getReviews()->where(['status_id' => Reviews::STATUS_ACTIVE])->all();
        $similiarRealEstate = RealEstate::find()
                ->andWhere(['between', 'daily_price', $model->daily_price - 50, $model->daily_price + 50])
                ->andWhere(['residents' => $model->residents])
                ->andWhere(['bedrooms' => $model->bedrooms])
                ->andWhere(['<>', 'id', $model->id])
                ->limit(3)
                ->all();
        $this->view->title = Translation::t($model, 'title');
        $project = $model->project;
        $map = $project->map;

        if ($map) {
            $this->view->registerJs('Main.mapInit("' . $map . '", "' . $model->title . '")');
        }

        UserLine::saveViewedObject($id);

        return $this->render('object_view', [
                    'model' => $model,
                    'similiarRealEstate' => $similiarRealEstate,
                    'project' => $project,
                    'map' => $map,
                    'reviews' => $reviews
        ]);
    }

    public function actionEmail() {
        $mailer = new Mailer();
        $mailer->send([
            'sendTo' => 'dag451@yandex.ru',
            'subject' => 'hello',
            'template' => 'layouts/html',
            'params' => ['content' => 'Hello!!']
        ]);
        echo 'Ok!';
    }

}
