<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \yii\base\Model|null */

$this->title = Yii::t('app', 'Права');
$this->params['breadcrumbs'][] = $this->title;
?>
<table class="table table-hover">
    <tr>
        <td></td>
        <?php foreach($roles as $role) {?>
            <td class="text-center">
                <?= $role->description ?>
            </td>
        <? } ?>
    </tr>
    <?php foreach($permissions as $permission) { ?>
    <tr>
        <td class="text-right"><?= $permission->name ?></td>
        <?php foreach($roles as $role) { ?>
        <td class="text-center">
            <?php
                $hasPermission = false;
                $roleItem = Yii::$app->authManager->getRole($role->name);
                $permissionItem = Yii::$app->authManager->getPermission($permission->name);
                if(Yii::$app->authManager->hasChild($roleItem, $permissionItem)) $hasPermission = true;
                echo Html::checkbox(null, $hasPermission, ['class' => 'permission', 'data-role-name' => $role->name, 'data-permission-name' => $permission->name]);
            ?>
        </td>
        <?php } ?>
    </tr>
    <?php } ?>
</table>