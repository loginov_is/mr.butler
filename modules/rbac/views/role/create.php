<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yiicms\widgets\Select2;
use app\models\AuthItem;
use app\models\Translation;

/* @var $this yii\web\View */
/* @var $model app\models\AuthItem */

$this->title = Yii::t('app', 'Создание роли');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Новая роль');
?>
<div class="assignment-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-md-12">
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->errorSummary($model) ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p class="text-bold"><?= Yii::t('app', 'Новая роль') ?></p>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <?= $form->field($model, 'name')->textInput() ?>
                    </div>

                    <div class="form-group">
                        <?= $form->field($model, 'description')->textInput() ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
