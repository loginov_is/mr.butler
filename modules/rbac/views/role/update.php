<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yiicms\widgets\Select2;
use app\models\AuthItem;
use app\models\Translation;

/* @var $this yii\web\View */
/* @var $model app\models\AuthItem */

$this->title = Yii::t('app', 'Роль {role}', ['role' => Translation::t($model, 'description')]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="assignment-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p class="text-bold"><?= Translation::t($model, 'description') ?></p>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin(); ?>

                    <div class="form-group">
                        <?= $form->field($model, 'name')->textInput(['disabled' => 'disabled']) ?>
                    </div>

                    <div class="form-group">
                        <?= $form->field($model, 'description')->textInput() ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p class="text-bold"><?= Yii::t('app', 'Дочерние роли') ?></p>
                </div>
                <div class="panel-body">
                    <?= Html::beginForm(['role/add-child']) ?>
                    <div class="form-group">
                        <ul>
                            <?php foreach($model->getChildren()->where(['type' => 1])->all() as $childRole) { ?>
                                <li><?= $childRole->description . ' (' . $childRole->name . ')' ?> <?= Html::a(Yii::t('app', 'удалить'), ['delete-child', 'parentRole' => $model->name, 'childRole' => $childRole->name]) ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="form-group">
                        <?= Html::hiddenInput('parentRole', $model->name) ?>
                        <?= Html::dropDownList('childRole', null, ArrayHelper::map(AuthItem::find()->where(['type' => 1])->andWhere(['<>', 'name', $model->name])->all(), 'name', 'description'), ['class' => 'form-control']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Добавить'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?= Html::endForm() ?>
                </div>
            </div>
        </div>
    </div>
</div>
