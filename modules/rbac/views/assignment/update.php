<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yiicms\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('app', 'Роли пользователя {user}', ['user' => $user->email]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="assignment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'roles')->widget(Select2::className(), [
        'items' => ArrayHelper::map(Yii::$app->authManager->getRoles(), function($model) {
            return $model->name;
        }, function($model) {
            return "{$model->name} ({$model->description})";
        }),
        'options' => [
            'multiple' => true,
        ],
        'clientOptions' => [
            'placeholder' => 'Roles',
            'allowClear' => true,
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
