<?php

namespace app\modules\rbac\controllers;

use app\modules\rbac\models\AssignmentForm;
use app\modules\rbac\Module;
use app\models\AuthItem;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class RoleController extends Controller
{
    public function actionIndex()
    {
        //Yii::$app->authManager->getRoles();
        $query = AuthItem::find()->where(['type' => 1]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id) {
        $model = AuthItem::findOne(['name' => $id]);
        if($model) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {

            }
            return $this->render('update', ['model' => $model]);
        }
    }

    public function actionCreate() {
        $model = new AuthItem();
        if ($model->load(Yii::$app->request->post())) {
            $model->type = 1;
            if($model->save()) {
                return $this->redirect(['update', 'id' => $model->name]);
            }
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionAddChild() {
        $data = Yii::$app->request->post();

        $parentRole = Yii::$app->authManager->getRole($data['parentRole']);
        $childRole = Yii::$app->authManager->getRole($data['childRole']);

        if(!Yii::$app->authManager->hasChild($parentRole, $childRole) && !Yii::$app->authManager->hasChild($childRole, $parentRole)) {
            Yii::$app->authManager->addChild($parentRole, $childRole);
        }
        $this->redirect(['role/update', 'id' => $data['parentRole']]);
    }

    public function actionDeleteChild($parentRole, $childRole) {
        $parent = Yii::$app->authManager->getRole($parentRole);
        $child = Yii::$app->authManager->getRole($childRole);
        Yii::$app->authManager->removeChild($parent, $child);
        $this->redirect(['role/update', 'id' => $parentRole]);
    }


    public function actionDelete($id) {
        $model = AuthItem::findOne(['name' => $id]);
        if($model) {
            $role = Yii::$app->authManager->getRole($model->name);
            Yii::$app->authManager->removeChildren($role);
            $model->delete();
        }
        return $this->redirect(['index']);
    }
}
