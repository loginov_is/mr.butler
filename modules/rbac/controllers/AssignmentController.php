<?php

namespace app\modules\rbac\controllers;

use app\modules\rbac\models\AssignmentForm;
use app\modules\rbac\Module;
use app\models\AuthItem;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AssignmentController implements the CRUD actions for User model.
 *
 * @property Module $module
 *
 */
class AssignmentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {

        if ($this->module->filterModel !== null) {
            $searchModel = new $this->module->filterModel;
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        }  else {
            $searchModel = null;
            /** @var ActiveRecord $userModel */
            $userModel = $this->module->userModel;
            $dataProvider = new ActiveDataProvider([
                'query' => $userModel::find(),
            ]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $userModel = $this->module->userModel;
        /** @var ActiveRecord $model */
        $model = new $userModel;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = new AssignmentForm();
        $user = $this->findModel($id);
        $model->roles = ArrayHelper::getColumn(Yii::$app->authManager->getRolesByUser($user->id), 'name');

        if ($model->load(Yii::$app->request->post())) {
            $authManager = Yii::$app->authManager;
            $authManager->revokeAll($user->id);
            if (!is_array($model->roles)) {
                $model->roles = [];
            }
            foreach ($model->roles as $role) {
                $authManager->assign($authManager->getRole($role), $user->id);
            }
            $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'user' => $user,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ActiveRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /** @var ActiveRecord $userModel */
        $userModel = $this->module->userModel;
        /** @var ActiveRecord $model */
        if (($model = $userModel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
