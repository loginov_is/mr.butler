<?php

namespace app\modules\rbac\controllers;

use app\modules\rbac\models\AssignmentForm;
use app\modules\rbac\Module;
use app\models\AuthItem;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class PermissionController extends Controller
{
    public function actionAdmin()
    {
        //Yii::$app->authManager->getRoles();
        $roles = AuthItem::findAll(['type' => 1]);
        $permissions = AuthItem::findAll(['type' => 2]);

        return $this->render('admin', [
            'roles' => $roles,
            'permissions' => $permissions,
        ]);
    }

    public function actionUpdate($id) {
        //Yii::$app->authManager->getRole($id)
        $role = AuthItem::findOne(['name' => $id]);
        if($role) {
            return $this->render('update', ['model' => $role]);
        }
    }

    public function actionAjaxUpdate() {
        $data = Yii::$app->request->post();

        $role = Yii::$app->authManager->getRole($data['roleName']);
        $permission = Yii::$app->authManager->getPermission($data['permissionName']);
        $active = $data['active'];

        if($active == 'true') {
            if(!Yii::$app->authManager->hasChild($role, $permission)) {
                Yii::$app->authManager->addChild($role, $permission);
            }
        } else {
            if(Yii::$app->authManager->hasChild($role, $permission)) {
                Yii::$app->authManager->removeChild($role, $permission);
            }
        }
    }
}
