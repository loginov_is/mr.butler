<?php

namespace app\modules\rbac\models;


use yii\base\Model;

class AssignmentForm extends Model
{
    public $roles = [];

    public function rules()
    {
        return [
            [['roles'], 'safe']
        ];
    }
}
