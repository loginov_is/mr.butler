<?php

namespace app\modules\rbac;

use yii\filters\AccessControl;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\rbac\controllers';

    public $allowedRoles = ['@'];

    public $userModel = 'app\models\User';
    public $filterModel;

    public $userNameColumn = 'username';

    public $defaultRoute = 'assignment';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => $this->allowedRoles,
                    ]
                ],
            ],
        ];
    }
}
