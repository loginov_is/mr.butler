<?php

namespace app\components;

use Yii;

class Mailer
{
    /**
     * �������� e-mail
     *
     * @param $options['sendTo'] - e-mail ����� ����������
     * @param $options['template'] - ������ �������������
     * @param $options['subject'] - ���� ������
     * @param $options['params'] - ������������� ������ ���������� ��� ����������� �� view ����
     * @return bool
     */
    public function send($options) {
        return Yii::$app->mailer->compose($options['template'], $options['params'])
            ->setFrom('noreply@mrbutler.com')
            ->setTo($options['sendTo'])
            ->setSubject($options['subject'])
            ->send();
    }

}