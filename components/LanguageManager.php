<?php

namespace app\components;

use app\models\Language;
use Yii;
use yii\base\BootstrapInterface;
use yii\base\Component;
use yii\bootstrap\Html;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class LanguageManager extends Component implements BootstrapInterface
{
    /**
     * @var string Parameter used to set the language
     */
    public $requestParam = 'l';

    /**
     * @var array Supported languages
     */
    public $languages;

    public $defaultLanguage = 'ru';

    public function init()
    {
        parent::init();

        $this->languages = $this->getAllowedLanguages();
    }

    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        /**
         * @todo Show preferred language?
         */
        /*$app->on(Application::EVENT_BEFORE_REQUEST, function () use ($app) {
            $app->getView()->on(View::EVENT_END_BODY, [$this, 'renderToolbar']);
        });*/
    }

    /**
     * @param \yii\base\Event $event
     */
    public function renderToolbar($event)
    {
        $preferredLanguage = Yii::$app->request->getPreferredLanguage(array_keys($this->languages));

        if ($preferredLanguage === Yii::$app->language || Yii::$app->getRequest()->getIsAjax()) {
            return;
        }

        $message = Yii::t('app', 'Страница доступна на другом языке - {language}', [
            'language' => $this->languages[$preferredLanguage]
        ], $preferredLanguage);

        echo Html::tag('div', Html::a($message, Url::current(['l' => $this->languages[$preferredLanguage]['C_CODE']])), [
            'id' => 'preferred-language',
            'data' => [
                'preferred-language' => $preferredLanguage,
            ],
        ]);
    }

    public function isSupported($language)
    {
        return array_key_exists($language, $this->languages);
    }

    private function getAllowedLanguages()
    {
        return Language::getDb()->cache(function ($db) {
            return ArrayHelper::map((new Query())->select(['code', 'name'])->from(Language::tableName())->all($db),
                'code',
                'name'
            );
        });
    }
}
