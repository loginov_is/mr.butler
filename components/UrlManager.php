<?php

namespace app\components;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class UrlManager extends \yii\web\UrlManager
{
    /**
     * @var LanguageManager
     */
    public $languageManager = 'languageManager';

    public function init()
    {
        parent::init();

        $this->languageManager = Yii::$app->get($this->languageManager);
    }

    /**
     * @inheritdoc
     */
    public function parseRequest($request)
    {
        $result = parent::parseRequest($request);

        $language = ArrayHelper::getValue(
            $result,
            "1.{$this->languageManager->requestParam}",
            $this->languageManager->defaultLanguage
        );

        if ($this->languageManager->isSupported($language)) {
            Yii::$app->language = $language;
        } else {
            throw new NotFoundHttpException();
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function createUrl($params)
    {
        $languageParam = $this->languageManager->requestParam;
        if (!is_array($params)) {
            return parent::createUrl($params);
        }
        if (empty($params[$languageParam])) {
            $params[$languageParam] = Yii::$app->language;
        }

        if ($params[$languageParam] === $this->languageManager->defaultLanguage) {
            unset($params[$languageParam]);
        }

        return parent::createUrl($params);
    }
}
