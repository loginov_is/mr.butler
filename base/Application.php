<?php

namespace app\base;


use app\models\Language;
use app\models\Page;
use Yii;
use yii\base\ViewEvent;
use yii\web\View;

/**
 * Class Application
 *
 * @property Language $languageInfo
 */
class Application extends \yii\web\Application
{
    public function init()
    {
        $this->on(Application::EVENT_BEFORE_REQUEST, function () {
            $this->getView()->on(View::EVENT_BEFORE_RENDER, function ($event) {
                /**
                 * @var ViewEvent $event
                 * @var View $view
                 */
                $view = $event->sender;

                $page = Page::getByAction();

                if ($page !== null) {
                    $view->registerMetaTag(['content' => $page->language->code], 'language');
                    $view->registerMetaTag(['content' => $page->meta_description], 'description');
                    $view->registerMetaTag(['content' => $page->meta_keywords], 'keywords');
                    $view->title = $page->title;
                    /**
                     * @todo Meta cache control?
                     */
                }
            });
        });

        parent::init();
    }

    public function getLanguageInfo()
    {
        return Language::findOne(['code' => Yii::$app->language]);
    }
}
